(* [@@@warning "-27"] *)

module T = Icr.Tree
module Seq = Support.Seq
module A = Backend.Assem
open Support
include Registers

type boxed =
  [ `Boxed
  | `Unboxed
  | `Callesaved of Temp.t
  | `HPointer of int * Temp.t option
  | `SLink ]
[@@deriving show { with_path = false }]

type access = InFrame of int * boxed | InReg of Temp.t
[@@deriving show { with_path = false }]

type t = {
  name : Temp.label;
  formals : access list;
  mutable locals : (int * bool) list;
  mutable loc_num : int;
  mutable spilled : (Temp.t * int) list;
  mutable calleesaves : int;
}
[@@deriving show { with_path = false }]

type frag =
  | Proc of { body : T.stm; frame : t }
  | String of Temp.label * string
[@@deriving show { with_path = false }]

let in_frame_formals frame =
  List.filter_map
    (function InFrame (x, y) -> Some (x, y) | _ -> None)
    frame.formals

let wordSize = 8

let header_of_block_size = 2

let local_offset = 1

let max_spilled = 8

let newFrame name (formals : (bool * _) list) =
  let rec calc_access arg_num frame_occ reg_occ = function
    | [] -> ([], frame_occ)
    | (escaped, bs) :: tl ->
        if (not escaped) && reg_occ <= arg_num then
          let formals, frame_occ' =
            calc_access arg_num frame_occ (reg_occ + 1) tl
          in
          (InReg (Temp.newtemp bs) :: formals, frame_occ')
        else
          let formals, frame_occ' =
            calc_access arg_num (frame_occ + 1) reg_occ tl
          in
          (InFrame (frame_occ, (bs :> boxed)) :: formals, frame_occ')
  in

  let formals, loc_num = calc_access (List.length arguments) 0 0 formals in
  { name; formals; locals = []; loc_num; spilled = []; calleesaves = 0 }

let name frame = frame.name

let formals frame = frame.formals

(* type b_u = [ `Boxed | `Unboxed ] *)

let box_conv = function #boxed as x -> x | _ -> assert false

let allocLocal ?spilled frame boxed (esc : bool) : access =
  let pos = frame.loc_num in

  match spilled with
  | None when esc ->
      frame.loc_num <- pos + 1;
      frame.locals <-
        (pos, match boxed with `Boxed -> true | `Unboxed -> false)
        :: frame.locals;
      InFrame (pos, (boxed :> boxed))
  | None -> InReg (Temp.newtemp boxed)
  | Some t ->
      frame.loc_num <- pos + 1;
      frame.spilled <- (t, pos) :: frame.spilled;
      InFrame (pos, box_conv @@ Temp.boxed t)

(* if esc then (
     frame.loc_num <- pos + 1;
     frame.locals <- (spilled, (pos, (boxed :> boxed))) :: frame.locals;
     InFrame (pos, (boxed :> boxed)) )
   else InReg (Temp.newtemp boxed) *)

let var_op_aux op access t =
  match access with
  | InReg _ -> assert false
  | InFrame (p, _) ->
      let offset = (-1 * wordSize * p) - wordSize in
      op t offset

let load_var =
  var_op_aux (fun t offset ->
      A.oper (`C "mov" :: `D t :: `C "," :: Codegen.(deref ~offset fp)))

let save_var =
  var_op_aux (fun t offset ->
      A.oper ([ `C "mov" ] @ Codegen.(deref ~offset fp) @ [ `C ","; `S t ]))

(* let outer_fp fp = T.(Mem fp) *)

module Codegen = Codegen

let string_of_register (_, n) = n

let exp access static_link =
  match access with
  | InReg t -> T.Temp t
  | InFrame (p, boxed) ->
      (* let t = Temp.newtemp () in  *)
      let offset = (-local_offset * wordSize * p) - wordSize in
      T.(
        (* Eseq (  Move ( Temp t, static_link  ), *)
        Mem (Binop (Plus, Const offset, static_link), T.bx_of_boxed boxed)
        (* ) *))

(* let assign access static_link exp =
   match access with
   | InReg t -> T.Move (T.Temp t, exp)
   | InFrame p ->
       let offset = (-1 * wordSize * p) - wordSize in
       T.(Move (Mem (Binop (Plus, Const offset, static_link)), exp)) *)

let assign access static_link exp boxed =
  match access with
  | InReg t -> T.Move (T.Temp t, exp)
  | InFrame (p, _) ->
      let offset = (-local_offset * wordSize * p) - wordSize in
      let t = Temp.newtemp boxed in
      T.(
        Seq
          ( Move (Temp t, exp),
            Move
              ( Mem
                  (Binop (Plus, Const offset, static_link), T.bx_of_boxed boxed),
                Temp t ) ))

let externalCall name args bx ca = T.Call (Name name, args, bx, ca)

let string lab (lit : string) =
  let l = ref ((String.length lit / wordSize) + 1) in
  let padding_num = (!l * wordSize) - String.length lit - 1 in
  let buff = Buffer.create 128 in
  Buffer.add_string buff
  @@ Printf.sprintf "%s db 2, 0, 0, 0, 0, 0, 0, 0, " (Temp.labelname lab);
  l := !l + header_of_block_size;
  for _ = 1 downto 0 do
    Buffer.add_string buff @@ (string_of_int @@ Int.logand !l 0xff) ^ ", ";
    l := !l lsr 8
  done;
  Buffer.add_string buff "0, 192, 0, 0, 0, 0, ";
  String.to_seq lit
  |> Seq.iter (fun c ->
         Buffer.add_string buff @@ (string_of_int @@ Char.code c) ^ ", ");
  (* Buffer.add_string buff "0\n"; *)
  for _ = padding_num downto 1 do
    Buffer.add_string buff "0, "
  done;
  Buffer.add_string buff (string_of_int padding_num ^ "\n");
  Buffer.contents buff

let procEntryExit1 frame stm =
  let incoming =
    List.fold_left
      (fun (inc, args) f ->
        (* let i, tl = *)
        match (f, args) with
        | InReg t, a :: tl -> (T.(Move (Temp t, Temp a)) :: inc, tl)
        | InFrame (_p, _boxed), _a :: tl ->
            (inc, tl)
            (* let offset = (-1 * wordSize * p) - wordSize in
               ( T.(
                   Move
                     ( Mem
                         ( Binop (Plus, Const offset, Temp fp),
                           T.bx_of_boxed boxed ),
                       Temp a )),
                 tl ) *)
        | _ -> assert false
        (* in
           (i :: inc, tl) *))
      ([], arguments) frame.formals
  in

  let a, b =
    List.fold_right
      (fun t (a, b) ->
        T.(
          let t1 = Temp.newtemp (`Callesaved t) in
          (Move (Temp t1, Temp t) :: a, Move (Temp t, Temp t1) :: b)))
      calleesaves ([], [])
  in
  T.seq (List.rev a @ fst incoming @ [ stm ] @ b)

(* let procEntryExit2 frame body =
     body @ A.[ oper [ `C "" ] ~src:(rv :: sp :: calleesaves) ]

   type func = { prolog : string; body : A.instr list; epilog : string } *)
type func = { prolog : string; body : A.instr list; epilog : string }

(* type rl = register list [@@deriving show { with_path = false }] *)

let pos_of_spilled frame t =
  List.assoc t frame.spilled - (frame.loc_num - List.length frame.spilled)

(* let cut_tail n l =
     let rec f x (i, a) = if i > 0 then (i - 1, a) else (0, x :: a)
     and aux = function x :: xs -> f x (aux xs) | [] -> (n, []) in
     snd @@ aux l

   let rec cut_head n l =
     if n = 0 then l else match l with [] -> [] | _ :: xs -> cut_head (n - 1) xs *)

let push_calleesave frame t =
  frame.calleesaves <- 1 + frame.calleesaves;
  match Temp.boxed t with
  | `Callesaved t ->
      A.oper [ `C "push"; `C (register_of_temp t |> string_of_register) ]
  | _ -> assert false

let pop_calleesave _frame t =
  match Temp.boxed t with
  | `Callesaved t ->
      A.oper [ `C "pop"; `C (register_of_temp t |> string_of_register) ]
  | _ -> assert false

let pointer_map frame alloc start_pos f temps =
  let regs =
    List.map
      (fun (reg, t) ->
        print_string ">>>>>>>  ";
        print_string @@ Temp.show t;
        print_string " > ";
        print_string @@ f reg;

        match Temp.boxed t with
        | `Boxed ->
            print_endline " >>>>> Boxed >>>>  ";
            Some (1, None)
        | `HPointer (i, None) ->
            print_endline
              (" >>>>> HPointer const >>>>  " ^ string_of_int i ^ "  \n");
            Some (2, Some i)
        | `HPointer (_, Some d) ->
            print_string " >>>>> HPointer temp >>>>  ";
            print_endline @@ Temp.show d;
            (* Some (r, Some (3, pos_of_localvar frame d)) *)
            let r = TempMap.find d alloc in

            Some
              (if List.mem r calleesave_registers then
               ( 3,
                 Some
                   (List.mapi (fun i r -> (r, i)) calleesave_registers
                   |> List.assoc r) )
              else (4, Some (pos_of_spilled frame d)))
        | `Offset ->
            print_endline " >>>>> Offset >>>>  ";
            Some (5, None)
        | `Callesaved _ ->
            print_endline " >>>>> Callesaved >>>>  ";
            None
        | `Register ->
            print_endline " >>>>> Register >>>>  ";
            None
        | `SLink ->
            print_endline " >>>>> SLink >>>>  ";
            None
        | `Unboxed ->
            print_endline " >>>>> Unboxed >>>>  ";
            None)
      temps
  in
  let bitmap, data, _ =
    List.fold_right
      (fun r (bm, data, pos) ->
        print_endline @@ string_of_int pos;
        assert (pos >= 0);
        match r with
        | Some (k, None) ->
            Nativeint.(logor bm @@ shift_left (of_int k) pos, data, pos - 4)
        | Some (k, Some d) ->
            Nativeint.(logor bm @@ shift_left (of_int k) pos, d :: data, pos - 4)
        | None -> (bm, data, pos - 4))
      regs
      (Nativeint.zero, [], start_pos)
  in

  A.(
    [ oper [ `C (Printf.sprintf "dq 0x%nx" bitmap) ] ]
    @ List.rev_map (fun d -> oper [ `C (Printf.sprintf "dq 0x%x" d) ]) data)

let procEntryExit3 frame body alloc =
  let ptr_map_lab = Temp.newlabel () in

  print_string @@ show frame;

  let body =
    List.fold_right
      (fun (i, temps) body ->
        match i with
        | A.Oper { kind = A.Call; _ } ->
            print_string "++++  \n";
            let tm =
              List.filter_map
                (fun t ->
                  let r = TempMap.find t alloc in

                  if RegSet.mem r callesavesSet then Some (r, t) else None)
                temps
              |> List.sort (fun (r1, _) (r2, _) -> compare_register r1 r2)
              (* |> List.map snd  *)
              |> pointer_map frame alloc
                   ((List.length calleesaves - 1) * 4)
                   string_of_register
            in

            let lab = Temp.newlabel () in

            (* Format.printf "\nf: %s %a  " (name frame |> ASymbol.name) pp_rl regs; *)
            A.(
              [
                i;
                oper [ `C "jmp"; `L lab ];
                oper [ `C "align 8" ];
                oper [ `C "dq"; `C (Temp.labelname ptr_map_lab) ];
              ]
              @ tm
              @ [ label [ `L lab; `C ":" ] ])
            @ body
        | _ -> i :: body)
      body []
  in

  let body' =
    (List.fold_left
       (fun (inc, args) f ->
         match (f, args) with
         | InFrame _, n :: tl -> (A.oper [ `C "push"; `C n ] :: inc, tl)
         | InReg _, _ :: tl -> (inc, tl)
         | _ -> assert false)
       ([], argument_names) frame.formals
    |> fst |> List.rev)
    @ Seq.(
        iternums 0
        |> take List.(length frame.locals + length frame.spilled)
        |> Seq.map (fun _ -> A.oper [ `C "push 0" ])
        |> List.of_seq)
    @ (if (frame.loc_num + frame.calleesaves) mod 2 <> 0 then
       [ A.oper [ `C "push 0" ] ]
      else [])
    @ body
  in

  let loc_bitmap =
    List.filter_map
      (function InFrame (p, `Boxed) -> Some (p + local_offset) | _ -> None)
      frame.formals
    @ List.filter_map
        (function p, true -> Some (p + local_offset) | _ -> None)
        frame.locals
    |> List.sort Int.compare
    |> List.fold_left
         (fun bm offset ->
           assert (offset < 63);
           Nativeint.(logor bm @@ shift_left one (63 - offset)))
         Nativeint.one
    (* |> Nativeint.to_int *)
  in

  let spilled_map =
    List.sort (fun (_, x) (_, y) -> x - y) frame.spilled
    (* |> List.map fst  *)
    |> List.map (fun (x, y) -> (y, x))
    |> pointer_map frame alloc ((max_spilled - 1) * 4) string_of_int
    |> List.map (A.format (fun _ -> assert false))
    |> String.concat "\n"
  in
  {
    prolog =
      Printf.sprintf
        {|
align 8
%s:
db %d
db %d
db %d
db 0
dd 0

dq 0x%nx
%s
%s:
push rbp
mov rbp, rsp
|}
        (Temp.labelname ptr_map_lab)
        List.(
          length (in_frame_formals frame)
          + length frame.locals + max_spilled + local_offset)
        (frame.loc_num + local_offset)
        frame.calleesaves loc_bitmap spilled_map
        (Temp.labelname frame.name);
    body = body';
    epilog = {|
mov rsp, rbp
pop rbp
ret
|};
  }
