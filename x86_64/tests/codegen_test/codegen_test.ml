open OUnit2
module F = X86_64.Frame

module Assem = Backend.Assem
open X86_64.Codegen
open Icr.Tree


let saytemp x  = 
         F.TempMap.find_opt x F.tempMap |> 
         Option.value ~default:(  "reg" ^ (Support.Temp.show x) )
  

let show_assem tree =
   let (f, _ ) =  munchExp tree in  f ()   |> ignore;
  List.map ( Assem.format saytemp       
      ) (get_assem ()) |> String.concat "\n"

let t = Temp.newtemp ()

let case1 = Binop (Plus, Const 12, Temp t)

let t2 = Temp.newtemp () 
let case2 = Binop (Plus, case1, Temp t2)
 
let case3 = Binop (Plus, Const 13, case2)


let case4 = Binop (Plus, Temp t , Temp t2 )

let t3 = Temp.newtemp ()
let case5 = Binop (Plus ,case4 ,  Temp t3 ) 

let case6 = Binop (Plus ,case5 ,  Const 14 ) 


let lab1 = Temp.newlabel () 
let call_case1 = Call ( Name lab1, [ case6 ] )


let assem_test name x ~expect =
  name >:: fun _ ->
  assert_equal ~printer:Fun.id ~cmp:(fun x y -> x = y) expect (show_assem x)

let tests = [
  
(* assem_test "case1" case1 ~expect:" " ; *)
(* assem_test "case2" case2 ~expect:" " ; *)
(* assem_test "case3" case3 ~expect:" " ; *)
(* assem_test "case5" case5 ~expect:" " ; *)
(* assem_test "case6" case6 ~expect:" " ; *)

assem_test "call_case1" call_case1 ~expect:" " ;



]

let () = "Semant tests" >::: tests |> run_test_tt_main
