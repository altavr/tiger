open Support

type register = int * string

let registers =
  [
    (0, "rax");
    (1, "rbx");
    (2, "rcx");
    (3, "rdx");
    (4, "rsi");
    (5, "rdi");
    (6, "rbp");
    (7, "rsp");
    (8, "r8");
    (9, "r9");
    (10, "r10");
    (11, "r11");
    (12, "r12");
    (13, "r13");
    (14, "r14");
    (15, "r15");
  ]

module StrMap = Map.Make (String)

let regidMap =
  List.fold_left
    (fun m ((_, n) as r) -> StrMap.add n r m)
    StrMap.empty registers

let pp_register f (_, s) = Format.pp_print_string f s

let compare_register (r1, _) (r2, _) = Int.compare r1 r2

module TempMap = Support.Temp.Map

module C = struct
  type t = register

  let compare = compare_register
end

module RegMap = Map.Make (C)

let make_tempMap rl =
  List.to_seq rl
  |> Seq.map (fun ((_, n) as r) -> (Temp.labeltemp n `Register, r))
  |> TempMap.of_seq

let make_regMap m =
  TempMap.fold (fun r t m' -> RegMap.add t r m') m RegMap.empty

let tempMap = make_tempMap registers

let regMap = make_regMap tempMap

let getregs = List.map (fun r -> RegMap.find r regMap)

let getreg r = RegMap.find r regMap

let temp_of_regname n = StrMap.find n regidMap |> getreg

let temps_of_regnames = List.map temp_of_regname

let register_of_temp t = TempMap.find t tempMap

let rv = temp_of_regname "rax"

let sp = temp_of_regname "rsp"

let fp = temp_of_regname "rbp"

let calleesave_names = [ "rbx"; "r12"; "r13"; "r14"; "r15" ]

let argument_names = [ "rdi"; "rsi"; "rdx"; "rcx"; "r8"; "r9" ]

let calleesaves = temps_of_regnames calleesave_names

let callersaves = temps_of_regnames [ "r10"; "r11" ]

let arguments = temps_of_regnames argument_names

let calleesave_registers =
  List.map (fun n -> StrMap.find n regidMap) calleesave_names

module RegSet = Set.Make (C)

let callesavesSet = RegSet.of_list calleesave_registers

(* let is_calleesaves (i, _) =  *)
