open Backend.Assem
open Icr.Tree
module R = Registers
module A = Backend.Assem

type wide = Byte | Word | DWord | QWord

let stiring_of_wide = function
  | Byte -> "byte"
  | Word -> "word"
  | DWord -> "dword"
  | QWord -> "qword"

let assem = ref []

let get_assem () =
  let a = List.rev !assem in
  assem := [];
  a

let emit x = assem := x :: !assem

let deref ?idx ?offset ?wide base : insfrag list =
  let wide = Option.map stiring_of_wide wide |> Option.value ~default:"" in
  match (idx, offset) with
  | None, Some offset ->
      [ `C wide; `C "["; `S base; `C (Printf.sprintf " %+d ]" offset) ]
  | None, None -> [ `C wide; `C "["; `S base; `C "]" ]
  | Some idx, None ->
      [ `C wide; `C "["; `S base; `C "+ ("; `S idx; `C "* 8) ]" ]
  | _ -> assert false

exception NotMatch

(* let select_uni  *)

[@@@warning "-8"]

let plus_or_minus = function Plus -> "add" | Minus -> "sub"

(* let mul_or_div = function Div -> "idiv" | Mul -> "imul" *)

let move d s = move [ `C "mov"; `D d; `C ","; `S s ]

let wide_of_integer x =
  let x = abs x in
  if Int.logand 0x7fffffff00000000 x != 0 then QWord
  else if Int.logand 0x7fffffffffff0000 x != 0 then DWord
  else if Int.logand 0x7fffffffffffff00 x != 0 then Word
  else Byte

let binop_patt =
  [
    (fun f exp (t : Temp.t) ->
      match exp with
      | Binop ((Plus as op), e1, Const x)
      | Binop ((Plus as op), Const x, e1)
      | Binop ((Minus as op), e1, Const x) ->
          let pat1, w1 = f e1 in
          ( (fun () ->
              let op = plus_or_minus op in
              let d = pat1 () in
              emit @@ move t d;
              emit @@ oper [ `C op; `D t; `C ","; `N x ];
              t),
            w1 + 1 )
      | _ -> raise NotMatch);
    (fun f exp t ->
      match exp with
      | Binop ((Plus as op), e1, e2) | Binop ((Minus as op), e1, e2) ->
          let pat1, w1 = f e1 in
          let pat2, w2 = f e2 in
          ( (fun () ->
              let op = plus_or_minus op in
              let d = pat1 () in
              let s = pat2 () in
              emit @@ move t d;
              emit @@ oper [ `C op; `D t; `C ","; `S s ];
              t),
            w1 + w2 + 1 )
      | _ -> raise NotMatch);
    (fun f exp t ->
      match exp with
      | Binop (Mul, e1, e2) ->
          let pat1, w1 = f e1 in
          let pat2, w2 = f e2 in
          ( (fun () ->
              let op1 = pat1 () in
              let op2 = pat2 () in
              emit @@ move t op1;
              emit @@ oper [ `C "imul"; `D t; `C ","; `S op2 ];
              t),
            w1 + w2 + 1 )
      | _ -> raise NotMatch);
    (fun f exp t ->
      let e1, x =
        match exp with
        | Binop (Mul, e1, Const x)
          when match wide_of_integer x with QWord -> false | _ -> true ->
            (e1, x)
        | Binop (Mul, Const x, e1)
          when match wide_of_integer x with QWord -> false | _ -> true ->
            (e1, x)
        | _ -> raise NotMatch
      in
      let pat1, w1 = f e1 in
      ( (fun () ->
          let s = pat1 () in
          emit @@ oper [ `C "imul"; `D t; `C ","; `S s; `C ","; `N x ];
          t),
        w1 + 1 ));
    (fun f exp t ->
      match exp with
      | Binop (Div, e1, e2) ->
          let pat1, w1 = f e1 in
          let pat2, w2 = f e2 in
          ( (fun () ->
              let op1 = pat1 () in
              let op2 = pat2 () in
              let rax = R.temp_of_regname "rax" in
              let rdx = R.temp_of_regname "rdx" in
              emit @@ move rax op1;
              emit @@ oper [ `C "cqo" ] ~dst:[ rax; rdx ] ~src:[ rax ];
              emit @@ oper [ `C "idiv"; `S op2 ] ~dst:[ rax ] ~src:[ rax; rdx ];
              emit @@ move t rax;
              t),
            w1 + w2 + 1 )
      | _ -> raise NotMatch);
  ]

let temp_patt =
  [ (fun _ exp _ -> match exp with Temp t -> ((fun _ -> t), 0)) ]

let const_patt =
  [
    (fun _ exp t ->
      match exp with
      | Const x ->
          ( (fun () ->
              (* let t = Temp.newtemp () in *)
              emit @@ oper [ `C "mov"; `D t; `C ","; `N x ];
              t),
            1 ));
  ]

let derived_ptr = function
  | Temp t -> (
      match Temp.boxed t with `HPointer (_, Some t') -> [ t' ] | _ -> [])
  | _ -> []

let move_patt =
  [
    (fun f t ->
      match t with
      | Move (Mem (Binop (Plus, e1, Const offset), _), Const x)
      | Move (Mem (Binop (Plus, Const offset, e1), _), Const x) ->
          let pat1, w1 = f e1 in
          ( (fun () ->
              let d = pat1 () in
              emit
              @@ oper
                   ((`C "mov" :: deref ~offset ~wide:QWord d) @ [ `C ","; `N x ]);
              ()),
            w1 + 1 )
      | Move (Mem (Binop (Plus, e1, Const offset), _), Name lab)
      | Move (Mem (Binop (Plus, Const offset, e1), _), Name lab) ->
          let pat1, w1 = f e1 in
          ( (fun () ->
              let d = pat1 () in
              emit
              @@ oper
                   ((`C "mov" :: deref ~offset ~wide:QWord d)
                   @ [ `C ","; `C (Temp.labelname lab) ]);
              ()),
            w1 + 1 )
      | _ -> raise NotMatch);
    (fun f t ->
      match t with
      | Move (Mem (Binop (Plus, e1, Const offset), _), e2)
      | Move (Mem (Binop (Plus, Const offset, e1), _), e2) ->
          let pat1, w1 = f e1 in
          let pat2, w2 = f e2 in
          ( (fun () ->
              let d = pat1 () in
              let s = pat2 () in

              emit @@ oper ((`C "mov" :: deref ~offset d) @ [ `C ","; `S s ]);
              ()),
            w1 + w2 + 1 )
      | _ -> raise NotMatch);
    (fun f t ->
      match t with
      | Move (Mem (Binop (Plus, e1, Binop (Mul, idx, Const 8)), _), e2)
      | Move (Mem (Binop (Plus, e1, Binop (Mul, Const 8, idx)), _), e2)
      | Move (Mem (Binop (Plus, Binop (Mul, idx, Const 8), e1), _), e2)
      | Move (Mem (Binop (Plus, Binop (Mul, Const 8, idx), e1), _), e2) ->
          let pat1, w1 = f e1 in
          let pat2, w2 = f e2 in
          let pat3, w3 = f idx in
          ( (fun () ->
              let d = pat1 () in
              let s = pat2 () in
              let idx = pat3 () in
              emit @@ oper ((`C "mov" :: deref ~idx d) @ [ `C ","; `S s ]);
              ()),
            w1 + w2 + w3 + 1 )
      | _ -> raise NotMatch);
    (fun f t ->
      match t with
      | Move (e1, Mem (Binop (Plus, e2, Binop (Mul, idx, Const 8)), _))
      | Move (e1, Mem (Binop (Plus, e2, Binop (Mul, Const 8, idx)), _))
      | Move (e1, Mem (Binop (Plus, Binop (Mul, idx, Const 8), e2), _))
      | Move (e1, Mem (Binop (Plus, Binop (Mul, Const 8, idx), e2), _)) ->
          let pat1, w1 = f e1 in
          let pat2, w2 = f e2 in
          let pat3, w3 = f idx in
          ( (fun () ->
              let d = pat1 () in
              let s = pat2 () in
              let idx = pat3 () in
              emit @@ oper (`C "mov" :: `D d :: `C "," :: deref ~idx s);
              ()),
            w1 + w2 + w3 + 1 )
      | _ -> raise NotMatch);
    (fun f t ->
      match t with
      | Move (e1, Mem (Binop (Plus, e2, Const offset), _))
      | Move (e1, Mem (Binop (Plus, Const offset, e2), _)) ->
          let pat1, w1 = f e1 in
          let pat2, w2 = f e2 in
          ( (fun () ->
              let d = pat1 () in
              let s = pat2 () in
              emit @@ oper (`C "mov" :: `D d :: `C "," :: deref ~offset s);
              ()),
            w1 + w2 + 1 )
      | _ -> raise NotMatch);
    (fun f t ->
      match t with
      | Move (Mem (e1, _), Const x) ->
          let pat1, w1 = f e1 in
          ( (fun () ->
              let d = pat1 () in
              let src = derived_ptr e1 in
              emit
              @@ oper
                   ((`C "mov" :: deref ~wide:QWord d) @ [ `C ","; `N x ])
                   ~src;
              ()),
            w1 + 1 )
      | Move (Mem (e1, _), Name lab) ->
          let pat1, w1 = f e1 in
          ( (fun () ->
              let d = pat1 () in
              let src = derived_ptr e1 in
              emit
              @@ oper
                   ((`C "mov" :: deref ~wide:QWord d)
                   @ [ `C ","; `C (Temp.labelname lab) ])
                   ~src;
              ()),
            w1 + 1 )
      | _ -> raise NotMatch);
    (fun f t ->
      match t with
      | Move (Mem (e1, _), e2) ->
          let pat1, w1 = f e1 in
          let pat2, w2 = f e2 in
          ( (fun () ->
              let d = pat1 () in
              let s = pat2 () in
              let src = derived_ptr e1 in
              emit @@ oper ((`C "mov" :: deref d) @ [ `C ","; `S s ]) ~src;
              ()),
            w1 + w2 + 1 )
      | _ -> raise NotMatch);
    (fun f t ->
      match t with
      | Move (Temp t, e2) ->
          let pat2, w2 = f e2 in
          ( (fun () ->
              let s = pat2 () in
              emit @@ move t s;
              ()),
            w2 + 1 )
      | _ -> raise NotMatch);
  ]

let mem_patt =
  [
    (fun f exp t ->
      match exp with
      | Mem (e1, _) ->
          let pat1, w1 = f e1 in
          ( (fun () ->
              let s = pat1 () in
              let src = derived_ptr e1 in
              emit @@ oper (`C "mov" :: `D t :: `C "," :: deref s) ~src;
              t),
            w1 + 1 )
      | _ -> raise NotMatch);
    (fun f exp t ->
      match exp with
      | Mem (Binop (Plus, e1, Const offset), _)
      | Mem (Binop (Plus, Const offset, e1), _) ->
          let pat1, w1 = f e1 in
          ( (fun () ->
              let s = pat1 () in
              emit @@ oper (`C "mov" :: `D t :: `C "," :: deref s ~offset);
              t),
            w1 + 1 )
      | _ -> raise NotMatch);
    (fun f exp t ->
      match exp with
      | Mem (Binop (Plus, e1, Binop (Mul, idx, Const 8)), _)
      | Mem (Binop (Plus, e1, Binop (Mul, Const 8, idx)), _)
      | Mem (Binop (Plus, Binop (Mul, idx, Const 8), e1), _)
      | Mem (Binop (Plus, Binop (Mul, Const 8, idx), e1), _) ->
          let pat1, w1 = f e1 in
          let pat2, w2 = f idx in
          ( (fun () ->
              let s = pat1 () in
              let idx = pat2 () in
              emit @@ oper (`C "mov" :: `D t :: `C "," :: deref s ~idx);
              t),
            w1 + w2 + 1 )
      | _ -> raise NotMatch);
  ]

let relop2instr = function
  | Eq -> "je"
  | Ne -> "jne"
  | Lt -> "jl"
  | Le -> "jle"
  | Gt -> "jg"
  | Ge -> "jge"
  | Ult -> "jb"
  | Ule -> "jbe"
  | Ugt -> "ja"
  | Uge -> "jae"

let cjump_patt =
  [
    (fun em t ->
      match t with
      | CJump (relop, e1, e2, t, f) ->
          let pat1, w1 = em e1 in
          let pat2, w2 = em e2 in
          ( (fun () ->
              let t1 = pat1 () in
              let t2 = pat2 () in
              emit @@ oper [ `C "cmp"; `S t1; `C ", "; `S t2 ];
              emit @@ oper [ `C (relop2instr relop); `L t ] ~jump:[ t; f ]),
            w1 + w2 + 2 )
      | _ -> raise NotMatch);
    (fun em t ->
      match t with
      | CJump (relop, e1, Const x, t, f) ->
          let pat1, w1 = em e1 in
          ( (fun () ->
              let t1 = pat1 () in
              emit @@ oper [ `C "cmp"; `S t1; `C ", "; `N x ];
              emit @@ oper [ `C (relop2instr relop); `L t ] ~jump:[ t; f ]),
            w1 + 2 )
      | _ -> raise NotMatch);
    (fun em t ->
      match t with
      | CJump (relop, Const x, e1, t, f) ->
          let pat1, w1 = em e1 in
          ( (fun () ->
              let t1 = pat1 () in
              emit @@ oper [ `C "cmp"; `S t1; `C ", "; `N x ];
              emit
              @@ oper
                   [ `C (relop2instr @@ invertRel relop); `L t ]
                   ~jump:[ t; f ]),
            w1 + 2 )
      | _ -> raise NotMatch);
  ]

let select (f : _ -> int) (l : _ list) =
  let rec aux x = function
    | [] -> x
    | y :: tl -> if f y < f x then aux y tl else aux x tl
  in
  match l with [] -> assert false | h :: tl -> aux h tl

let rec munchStm s : (unit -> unit) * int =
  match s with
  | Move _ -> sequ s move_patt
  | Exp e ->
      let f, w = munchExp e in
      ((fun _ -> f () |> ignore), w)
  | Jump (Name l, ll) ->
      ((fun () -> emit @@ oper [ `C "jmp"; `L l ] ~jump:ll), 1)
  | CJump _ -> sequ s cjump_patt
  | Label lab -> ((fun () -> emit @@ label [ `L lab; `C ":" ]), 1)
  | _ ->
      print_endline "Error: No matched patterns";
      print_endline @@ Icr.Tree.show_stm s;
      exit 1

and munchArgs args =
  let rec aux = function
    | [], _ | _, [] -> []
    | r :: r_tl, a :: a_tl ->
        let s = Move (Temp r, a) in
        let f, _ = munchStm s in
        f ();
        r :: aux (r_tl, a_tl)
  in
  aux (R.arguments, args)

and sequ s patt =
  List.filter_map
    (fun patt -> try Some (patt munchExp s) with NotMatch -> None)
    patt
  |> select snd

and munchExp e : (unit -> temp) * int =
  match e with
  | Binop _ -> result e binop_patt
  | Temp _ -> result e temp_patt
  | Const _ -> result e const_patt
  | Mem _ -> result e mem_patt
  | Call (Name l, args, _, ca) ->
      ( (fun () ->
          emit
          @@ oper
               ~kind:(if ca then A.Call else A.Other)
               [ `C "call "; `C (Temp.labelname l) ]
               ~src:(munchArgs args)
               ~dst:([ R.rv ] @ R.arguments @ R.callersaves);
          R.rv),
        1 )
  | Name lab ->
      ( (fun () ->
          let t = Temp.newtemp `Unboxed in
          emit @@ oper [ `C "mov"; `D t; `C ","; `C (Temp.labelname lab) ];
          t),
        1 )

and result e patt =
  List.filter_map
    (fun patt ->
      let t =
        try Temp.newtemp (exp_boxed e :> Temp.boxed)
        with Assert_failure _ as e' ->
          print_string @@ show_exp e;
          raise e'
      in

      try Some (patt munchExp e t) with NotMatch -> None)
    patt
  |> select snd

let run tree =
  let f, _ = munchStm tree in
  f ()
