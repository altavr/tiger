  include Stdlib.Seq


  let rec iternums x () = Cons (x, iternums (x + 1))

  let rec take x s () =
    match s () with
    | Nil -> Nil
    | Cons (y, s) -> if x = 0 then Nil else Cons (y, take (x - 1) s)