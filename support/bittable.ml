[@@@warning "-27"]

type int64_ = (Int64.t[@printer fun fmt x -> fprintf fmt "0x%Lx" x])
[@@deriving show { with_path = false }, eq]

type t = int64_ array * int [@@deriving show { with_path = false }, eq]

let base = 64

let init dim =
  let bits = dim * dim in
  let size = if Int.rem bits base = 0 then bits / base else (bits / base) + 1 in
  (Array.init size (fun _ -> Int64.zero), dim)

let nword dim x y =
  if x >= dim || y >= dim then raise @@ Invalid_argument "dim"
  else
    let n = (dim * y) + x in
    (n, n / base, Int64.(shift_left one @@ Int.rem n base))

let set (a, dim) x y =
  let n, b, i = nword dim x y in
  a.(b) <- Int64.(logor a.(b) i)

let mem (a, dim) x y =
  let n, b, i = nword dim x y in
  not Int64.(equal zero @@ logand a.(b) i)

let unset (a, dim) x y =
  let n, b, i = nword dim x y in
  a.(b) <- Int64.(logxor a.(b) i)

let clone (a, n) = (Array.copy a, n)

let%test_module _ =
  (module struct
    [@@@warning "-37"]

    let b1 = init 15

    let _ =
      set b1 10 14;
      set b1 12 12

    let%test "(set b1 10 14);   set b1 12 12 " =
      equal b1 Int64.([| zero; zero; zero; of_int 0x10000001 |], 15)

    let%test " mem b1 10 14" = mem b1 10 14 && mem b1 12 12

    let%test " mem b1 11 14" = mem b1 11 14 = false

    let%test " unset b1 12 12;" =
      unset b1 12 12;
      equal b1 Int64.([| zero; zero; zero; of_int 0x10000000 |], 15)
  end)
