let make_show_aux simpp e =
  let buff = Buffer.create 128 in
  let formatter = Format.formatter_of_buffer buff in
  simpp formatter e;
  Format.pp_print_flush formatter ();
  Buffer.contents buff

let make_show pp e = make_show_aux pp e

let make_show_1 af pp e = make_show_aux (fun f -> pp af f) e

let make_draw_graph to_dot path g name =
  let buff = to_dot g in
  let dir = path in
  let dotfile = name ^ ".dot" in
  let svgfile = name ^ ".svg" in
  let ochan = open_out (Filename.concat dir dotfile) in
  Buffer.output_buffer ochan buff;
  flush ochan;
  close_out ochan;
  let cmd =
    Printf.sprintf "dot -Tsvg -o %s %s"
      (Filename.concat dir svgfile)
      (Filename.concat dir dotfile)
  in

  let _status = Sys.command cmd in
  ()

module List = struct
  include List

  let is_empty = function [] -> true | _ -> false

  let rec add_if_miss eq x = function
    | [] -> [ x ]
    | h :: tl -> if eq h x then tl else h :: add_if_miss eq x tl

  let rec remove eq x = function
    | [] -> []
    | h :: tl -> if eq x h then tl else h :: remove eq x tl

  let rec replace eq that other = function
    | [] -> []
    | h :: tl ->
        if eq that h then other :: replace eq that other tl
        else h :: replace eq that other tl

  let rec splitlast = function
    | [ x ] -> ([], x)
    | h :: tl ->
        let t', last = splitlast tl in
        (h :: t', last)
    | _ -> assert false
end

module L = Lexing

module Interval : sig
  type lexpos = L.position

  type t

  val create : lexpos -> lexpos -> t

  val empty : t

  val linenum : t -> int

  val start_col : t -> int

  val union : t -> t -> t

  val dia : t -> int * int

  val to_string : string -> t -> string

  val equal : t -> t -> bool

  val pp : Format.formatter -> t -> unit

  val show : t -> string
end = struct
  type lexpos = L.position

  type t = Pos of lexpos * lexpos | Undefined

  let create p1 p2 = Pos (p1, p2)

  let empty = Undefined

  let linenum = function Pos (p1, _) -> p1.L.pos_lnum | _ -> -1

  let start_col = function
    | Pos (p1, _) -> p1.L.pos_cnum - p1.L.pos_bol
    | _ -> -1

  let union i1 i2 =
    match (i1, i2) with
    | Pos (p1, _), Pos (_, p2) -> Pos (p1, p2)
    | Pos (p1, p2), Undefined -> Pos (p1, p2)
    | Undefined, Pos (p1, p2) -> Pos (p1, p2)
    | _ -> Undefined

  let dia = function
    | Pos (p1, p2) -> (p1.L.pos_cnum, p2.L.pos_cnum)
    | _ -> (0, 0)

  let to_string text = function
    | Pos (p1, p2) -> String.sub text p1.L.pos_cnum p2.L.pos_cnum
    | Undefined -> "undefined"

  let equal _ _ = true

  let pp _ _ = ()

  let show _ = ""
end

module TokenInfo = struct
  type info = Interval.t [@@deriving show { with_path = false }, eq]

  type 'a t = { i : info; v : 'a } [@@deriving show { with_path = false }, eq]

  exception Exit of int

  let create i v = { i; v }

  let error i s =
    Printf.printf "Error: line %d, position %d\n%s\n" (Interval.linenum i)
      (Interval.start_col i) s;
    raise @@ Exit 1
end

module ASymbol : sig
  type t

  val symbol : string -> t

  val name : t -> string

  type 'a table

  val empty : 'a table

  val enter : t -> 'a -> 'a table -> 'a table

  val look : t -> 'a table -> 'a option

  val remove : t -> 'a table -> 'a table

  val equal : t -> t -> bool

  val compare : t -> t -> int

  val pp : Format.formatter -> t -> unit

  val show : t -> string
end = struct
  type t = string * int

  let pp f (s, _) = Format.pp_print_string f s

  let show x = make_show pp x

  let equal (_, a) (_, b) = a = b

  let compare (_, a) (_, b) = b - a

  let nextsym = ref 0

  module H = Hashtbl

  let (hashtbl : (string, int) H.t) = H.create 1024

  let symbol name =
    try (name, H.find hashtbl name)
    with _ ->
      let i = !nextsym in
      nextsym := i + 1;
      H.add hashtbl name i;
      (name, i)

  let name (s, _) = s

  module M = Map.Make (Int)

  type 'a table = 'a M.t

  let empty = M.empty

  let enter (_, n) = M.add n

  let look (_, n) = M.find_opt n

  let remove (_, n) = M.remove n
end

module type TempType = sig
  type t

  and boxed =
    [ `Boxed
    | `Unboxed
    | `Callesaved of t
    | `Register
    | `HPointer of int * t option
    | `SLink
    | `Offset ]

  val newtemp : ?label:string -> [< boxed ] -> t

  val labeltemp : string -> [< boxed ] -> t

  val move_from : t -> t

  val boxed : t -> boxed

  module S = ASymbol

  module Map : sig
    include Map.S with type key = t

    val pp :
      (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit

    val show : (Format.formatter -> 'a -> unit) -> 'a t -> string
  end

  module Set : sig
    include Set.S

    val pp : Format.formatter -> t -> unit

    val show : t -> string
  end
  with type elt = t

  type label = S.t

  val newlabel : unit -> label

  val labelname : label -> string

  val namedLabel : string -> label

  val equal_label : label -> label -> bool

  val equal : t -> t -> bool

  val pp : Format.formatter -> t -> unit

  val show : t -> string

  val pp_label : Format.formatter -> label -> unit

  val compare : t -> t -> int

  val reset : unit -> unit

  val show_all : t -> string
end

module Temp : TempType = struct
  type t = { id : int; name : string option; boxed : boxed }
  [@@deriving show { with_path = false }]

  and boxed =
    [ `Boxed
    | `Unboxed
    | `Callesaved of t
    | `Register
    | `HPointer of int * t option
    | `SLink
    | `Offset ]

  let show_all = make_show pp

  let temps = ref 100

  let newtemp ?label boxed =
    let t = !temps in
    (* (if  t = 274 then assert false); *)
    temps := t + 1;
    (* assert( t != 116 ); *)
    { id = t; name = label; boxed = (boxed :> boxed) }

  let labeltemp label bx = newtemp ~label bx

  let move_from src = newtemp src.boxed

  let boxed t = t.boxed

  let compare t1 t2 = Int.compare t1.id t2.id

  let equal t1 t2 = t1.id = t2.id

  let pp f { id; name; _ } =
    match name with
    | None -> Format.fprintf f "%d" id
    | Some s -> Format.pp_print_string f s

  let show = make_show pp
  (* module Table = Map.Make (Int) *)

  (* let makestring t = "t" ^ string_of_int t *)

  module Set = struct
    type u = t list [@@deriving show { with_path = false }]

    include Set.Make (struct
      type v = t

      type t = v

      let compare = compare
    end)

    let pp f x = pp_u f @@ elements x

    let show = make_show pp
  end

  module Map = struct
    type 'a u = (t * 'a) list [@@deriving show { with_path = false }]

    include Map.Make (struct
      type v = t

      type t = v

      let compare = compare
    end)

    let pp af f x = pp_u af f @@ bindings x

    let show af = make_show_1 af pp
  end

  module S = ASymbol

  type label = S.t [@@deriving show { with_path = false }, eq]

  let labs = ref 0

  let postinc x =
    let i = !x in
    x := i + 1;
    i

  let newlabel () = S.symbol @@ Printf.sprintf "L%d" @@ postinc labs

  let labelname = S.name

  let namedLabel = S.symbol

  let reset () =
    labs := 0;
    temps := 100
end

module BitTable = Bittable
module MutialDisjointSet = Mutial_disjoint_sets
module DoubleLinkedList = Dllist
module Graph = Graph
module OrdList = Ordlist
module Seq = Seq
