
module type OrderListElm = sig
  type t

  val compare : t -> t -> int

  val pp : Format.formatter -> t -> unit

  val equal : t -> t -> bool
end

module type OrdListType = sig
  type t

  type elt

  val cmp : elt -> elt -> int

  val of_list : elt list -> t

  val elements : t -> elt list

  val empty : t

  val union : t -> t -> t 

  val diff : t -> t -> t

  val equal : t -> t -> bool

  val pp : Format.formatter -> t -> unit

  val show : t -> string

  val remove : elt -> t -> t

  val add : elt -> t -> t

  val fold : (elt -> 'a -> 'a) -> t -> 'a -> 'a
end

module NSOrdList (E : OrderListElm) = struct
  type elt = E.t [@@deriving show { with_path = false }, eq]

  type t = elt list [@@deriving show { with_path = false }, eq]

  let cmp = E.compare

  let of_list x = List.sort_uniq cmp x

  let elements x = x

  let fold = List.fold_right

  let empty = []

  let rec union l1 l2 =
    match l1 with
    | [] -> l2
    | x1 :: tl1 -> (
        match l2 with
        | [] -> l1
        | x2 :: tl2 ->
            let c = cmp x1 x2 in
            if c < 0 then x1 :: union tl1 l2
            else if c > 0 then x2 :: union l1 tl2
            else x1 :: union tl1 tl2 )

  let rec diff l1 l2 =
    match l2 with
    | [] -> l1
    | x2 :: tl2 -> (
        match l1 with
        | [] -> []
        | x1 :: tl1 ->
            let c = cmp x1 x2 in
            if c < 0 then x1 :: diff tl1 l2
            else if c > 0 then  diff l1 tl2
            else diff tl1 tl2 )

  let rec remove x = function
    | [] -> []
    | h :: tl -> if cmp x h = 0 then tl else h :: remove x tl

  let rec add x = function
    | [] -> [x]
    | h :: tl as l ->
        let c = cmp x h in
        if c < 0 then x :: l else if c > 0 then  h :: add x tl else l
end



module OrdList (E : OrderListElm) : OrdListType with type elt = E.t =
  NSOrdList (E)

let%test_module "order list tests" =
  ( module struct
    module OL = NSOrdList (struct
      include Int

      let pp = Format.pp_print_int
    end)

    open OL

    let eq_show x1 x2 = 
        print_endline @@ show x1 ;
        equal x1 x2


    let%test _ =
    eq_show (union [ 1; 4; 6; 7; 8 ] [ 4; 5; 8; 9 ]) [ 1; 4; 5; 6; 7; 8; 9 ]

    let%test _ =
    eq_show (union [ 1; 4; 6; 7; 8 ] [ 1; 4; 6; 7; 8 ]) [ 1; 4; 6; 7; 8 ]

    let%test _ = eq_show (diff [ 1; 4; 6; 7; 8 ] [ 4; 5; 8; 9 ]) [ 1; 6; 7 ]

    let%test "remove1" = eq_show (remove 5 [ 4; 5; 8; 9 ]) [ 4;  8; 9 ]

    let%test "remove2" = eq_show (remove 9 [ 4; 5; 8; 9 ]) [ 4; 5; 8; ]

    let%test "add1" = eq_show (add 6 [ 4; 5; 8; 9 ]) [ 4; 5; 6; 8; 9 ]

    let%test "add2" = eq_show (add 60 [ 4; 5; 8; 9 ]) [ 4; 5;  8; 9; 60 ]

    let%test "add3" = eq_show (add 1 [ 4; 5; 8; 9 ]) [1;  4; 5;  8; 9; ]

  end )