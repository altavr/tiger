[@@@warning "-27"]

module type Enumerate = sig
  type kind

  type t

  val kind_to_enum : kind -> int

  val compare : t -> t -> int

  val pp : Format.formatter -> t -> unit

  val number : int
end

module type DisjointSet = sig
  type t

  type kind

  type elt

  module S : Set.S with type elt = elt

  val init : unit -> t

  val add : t -> elt -> kind -> unit

  val mem : t -> elt -> kind -> bool

  val find : t -> elt -> int -> int

  (* val remove : t -> elt -> unit *)

  val displace : t -> elt -> kind -> unit

  val to_list : t -> kind -> elt list

  val choose : t -> kind -> elt option

  val meml : t -> elt -> kind list -> bool

  val of_set : S.t -> kind -> t

  val iter : (elt -> unit) -> t -> kind -> unit

  val fold : (elt -> 'a -> 'a) -> t -> kind -> 'a -> 'a

  val is_empty : t -> kind -> bool

  val equal : t -> t -> bool

  val clone : (elt -> elt) -> t -> t

  val build : (kind * elt list) list -> t

  val pp : Format.formatter -> t -> unit

  val show : t -> string

  val replace : t -> (elt * elt * (unit -> unit)) list  -> kind list -> unit
end

module type EnumerateSet = sig
  type kind

  type t

  val kind_to_enum : kind -> int

  val pp : Format.formatter -> t -> unit

  val number : int

  module S : Set.S with type elt = t
end

module MakeFromSet (I : EnumerateSet) = struct
  module S = I.S

  type t = S.t array [@@deriving eq]

  type kind = I.kind

  type elt = I.t [@@deriving show { with_path = false }]

  type u = elt list array [@@deriving show { with_path = false }]

  let pp f t = pp_u f @@ Array.map S.elements t

  let show t = show_u @@ Array.map S.elements t

  let init () : t = Array.init I.number (fun _ -> S.empty)

  let add (t : t) (i : elt) (k : kind) : unit =
    t.(I.kind_to_enum k) <- S.add i t.(I.kind_to_enum k)

  let mem_aux t i j = S.mem i t.(j)

  let mem t i k = mem_aux t i @@ I.kind_to_enum k

  let rec find t i j =
    if j = Array.length t then raise Not_found
    else if mem_aux t i j then j
    else find t i @@ (j + 1)

  let remove (t : t) (i : elt) : unit =
    let ri = find t i 0 in
    t.(ri) <- S.remove i t.(ri)

  let displace (t : t) (i : elt) (k : kind) : unit =
    remove t i;
    add t i k

  let to_list (t : t) (k : kind) : elt list = S.elements t.(I.kind_to_enum k)

  let choose (t : t) k = S.choose_opt @@ t.(I.kind_to_enum k)

  let rec meml t i = function
    | [] -> false
    | k :: tl -> if mem t i k then true else meml t i tl

  let insert t s k = t.(I.kind_to_enum k) <- s

  let of_set s k =
    let t = init () in
    insert t s k;
    t

  let iter f t k = S.iter f t.(I.kind_to_enum k)

  let fold f t k = S.fold f t.(I.kind_to_enum k)

  let is_empty t k = S.is_empty t.(I.kind_to_enum k)

  let clone f = Array.map @@ S.map f

  let replace t c kl =
    (* let others n l =
         let rec aux n = function
           | _ when n < 0 -> []
           | [] -> n :: aux (n - 1) []
           | h :: tl as l -> if h = n then aux (n - 1) tl else n :: aux (n - 1) l
         in
         aux n (List.sort_uniq (fun x y -> y - x) l)
       in *)
    let targets = List.map I.kind_to_enum kl in

    let rec exist_in_others i e x =
      if i < 0 then false
      else if i != e then
        if S.mem x t.(i) then true else exist_in_others (i - 1) e x
      else exist_in_others (i - 1) e x
    in

    let rec check x = function
      | [] -> -1
      | h :: tl -> if S.mem x t.(h) then h else check x tl
    in

    List.iter
      (fun (m1, m2, f) ->
        let i = check m1 targets in

        if i > 0 then
          if not @@ exist_in_others (Array.length t - 1) i m2 then (
            f ();
            t.(i) <- S.remove m1 t.(i) |> S.add m2))
      c

  let build ll =
    let t = init () in
    List.iter (fun (k, il) -> insert t (S.of_list il) k) ll;
    if I.number != Array.length t then
      raise @@ Invalid_argument "Different lenghts";
    t
end

module MakeUnsigned (I : Enumerate) = struct
  include MakeFromSet (struct
    include I
    module S = Set.Make (I)
  end)
end

module Make (I : Enumerate) :
  DisjointSet with type elt = I.t and type kind = I.kind = MakeUnsigned ((
  I : Enumerate))

let%test_module _ =
  (module struct
    module Etest = struct
      [@@@warning "-32"]

      type kind = A | B | C [@@deriving enum]

      type t = int [@@deriving ord, show { with_path = false }]

      let number = 3

      let clone x = x
    end

    open Etest

    open MakeUnsigned (Etest)

    let s = build [ (A, [ 0; 1 ]); (B, [ 2; 3 ]); (C, [ 4; 5 ]) ]

    let f _ = ()

    let%expect_test _ =
      replace s [ (4, 8 , f) ] [ B ];
      print_string @@ show s;
      [%expect {| [|[0; 1]; [2; 3]; [4; 5]|] |}]

    let%expect_test _ =
      replace s [ 2, 5, f] [ B ];
      print_string @@ show s;
      [%expect {| [|[0; 1]; [2; 3]; [4; 5]|] |}]

    let%expect_test _ =
      replace s [ 2, 6 , f] [ B ];
      print_string @@ show s;
      [%expect {| [|[0; 1]; [3; 6]; [4; 5]|] |}]

    (* let f1 = replace_func s [ B; C ] *)

    let%expect_test _ =
      replace s [ 2, 0, f] [ B; C ];
      print_string @@ show s;
      [%expect {| [|[0; 1]; [3; 6]; [4; 5]|] |}]

    let%expect_test _ =
      replace s [ 4, 7, f] [ B; C ];
      print_string @@ show s;
      [%expect {| [|[0; 1]; [3; 6]; [5; 7]|] |}]

    let%expect_test _ =
      replace s [5, 3, f] [ B; C ];
      print_string @@ show s;
      [%expect {| [|[0; 1]; [3; 6]; [5; 7]|] |}]

    let%test _ =
      let t = init () in
      add t 1 A;
      mem t 1 A

    let%test _ =
      let t = init () in
      add t 1 A;
      not @@ mem t 1 B

    let%test _ =
      let t = init () in
      add t 1 A;
      displace t 1 B;
      mem t 1 B

    let%test _ =
      let t = init () in
      add t 1 A;
      displace t 1 B;
      not @@ mem t 1 A
  end)
