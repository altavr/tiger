module type OrderListElm = sig
  type t

  val compare : t -> t -> int

  val pp : Format.formatter -> t -> unit

  val equal : t -> t -> bool
end

module type AssocOrdListType = sig
  type 'a t

  type key

  val of_list : (key * 'a) list -> 'a t

  val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool

  val bindings : 'a t -> (key * 'a) list

  val empty : 'a t

  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t

  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit

  val show : (Format.formatter -> 'a -> unit) -> 'a t -> string

  val remove : key -> 'a t -> 'a t

  val add : key -> 'a -> 'a t -> 'a t

  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b

  val iter : (key -> 'a -> unit) -> 'a t -> unit

  val find : key -> 'a t -> 'a
end

module AssocOrdListUnsigned (E : OrderListElm) = struct
  type key = E.t [@@deriving show { with_path = false }, eq]

  type 'a t = (key * 'a) list [@@deriving show { with_path = false }, eq]

  let cmp = E.compare

  let of_list x = List.sort_uniq (fun (k1, _) (k2, _) -> cmp k1 k2) x

  let bindings x = x

  let fold f l a = List.fold_right (fun (k, v) a -> f k v a) l a

  let empty = []

  let rec union f l1 l2 =
    match l1 with
    | [] -> l2
    | ((k1, v1) as x1) :: tl1 -> (
        match l2 with
        | [] -> l1
        | ((k2, v2) as x2) :: tl2 -> (
            let c = cmp k1 k2 in
            if c < 0 then x1 :: union f tl1 l2
            else if c > 0 then x2 :: union f l1 tl2
            else
              match f k1 v1 v2 with
              | Some x -> (k1, x) :: union f tl1 tl2
              | None -> union f tl1 tl2))

  (* let rec diff l1 l2 =
     match l2 with
     | [] -> l1
     | x2 :: tl2 -> (
         match l1 with
         | [] -> []
         | x1 :: tl1 ->
             let c = cmp x1 x2 in
             if c < 0 then x1 :: diff tl1 l2
             else if c > 0 then  diff l1 tl2
             else diff tl1 tl2 ) *)

  let rec remove x = function
    | [] -> []
    | ((k, _) as h) :: tl -> if cmp x k = 0 then tl else h :: remove x tl

  let rec add x v = function
    | [] -> [ (x, v) ]
    | ((k, _) as h) :: tl as l ->
        let c = cmp x k in
        if c < 0 then (x, v) :: l else if c > 0 then h :: add x v tl else l


  let iter f l = List.iter ( fun (k, v) -> f k v ) l

  let rec find k  = function 
      | [] -> raise Not_found 
      | (k', v) :: tl -> if equal_key k k' then v else find k tl 

end

module AssocOrdList (E : OrderListElm) : AssocOrdListType with type key = E.t =
  AssocOrdListUnsigned (E)

let%test_module "order list tests" =
  (module struct
    module OL = AssocOrdListUnsigned (struct
      include Int

      let pp = Format.pp_print_int
    end)

    open OL

    let show = show Format.pp_print_char

    let equal = equal ( = )

    let union = union (fun _ v _ -> Some v)

    let eq_show x1 x2 =
      print_endline @@ show x1;
      equal x1 x2

    let%test _ =
      eq_show
        (union
           [ (1, 'a'); (4, 'b'); (6, 'c'); (7, 'd'); (8, 'e') ]
           [ (4, 'f'); (5, 'g'); (8, 'h'); (9, 'i') ])
        [ (1, 'a'); (4, 'b'); (5, 'g'); (6, 'c'); (7, 'd'); (8, 'e'); (9, 'i') ]

    let%test _ =
      eq_show
        (union
           [ (1, 'a'); (4, 'b'); (6, 'c'); (7, 'd'); (8, 'e') ]
           [ (1, 'f'); (4, 'g'); (6, 'h'); (7, 'i'); (8, 'j') ])
        [ (1, 'a'); (4, 'b'); (6, 'c'); (7, 'd'); (8, 'e') ]

    (* let%test _ = eq_show (diff [ 1; 4; 6; 7; 8 ] [ 4; 5; 8; 9 ]) [ 1; 6; 7 ] *)

    let%test "remove1" =
      eq_show
        (remove 5 [ (4, 'a'); (5, 'b'); (8, 'c'); (9, 'd') ])
        [ (4, 'a'); (8, 'c'); (9, 'd') ]

    let%test "remove2" =
      eq_show
        (remove 9 [ (4, 'a'); (5, 'b'); (8, 'c'); (9, 'd') ])
        [ (4, 'a'); (5, 'b'); (8, 'c') ]

    let%test "add1" =
      eq_show
        (add 6 'c' [ (4, 'a'); (5, 'b'); (8, 'd'); (9, 'e') ])
        [ (4, 'a'); (5, 'b'); (6, 'c'); (8, 'd'); (9, 'e') ]

    let%test "add2" =
      eq_show
        (add 60 'e' [ (4, 'a'); (5, 'b'); (8, 'c'); (9, 'd') ])
        [ (4, 'a'); (5, 'b'); (8, 'c'); (9, 'd'); (60, 'e') ]

    let%test "add3" =
      eq_show
        (add 1 'a' [ (4, 'b'); (5, 'c'); (8, 'd'); (9, 'e') ])
        [ (1, 'a'); (4, 'b'); (5, 'c'); (8, 'd'); (9, 'e') ]
  end)
