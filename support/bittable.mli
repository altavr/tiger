type t 
val init : int -> t
val set : t -> int -> int -> unit
val mem : t -> int -> int -> bool
val unset : t -> int -> int -> unit
val pp: Format.formatter -> t -> unit
val show : t -> string 
val equal : t -> t -> bool
val clone : t -> t