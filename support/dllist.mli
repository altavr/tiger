type 'a t 
val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool
val show : (Format.formatter -> 'a -> unit) -> 'a t -> string
val set : 'a t -> 'a -> unit
val replace_value : 'a t -> ('a -> 'a) -> unit
val value : 'a t -> 'a
val end_node : 'a t -> 'a t
val map_to_list : ('a -> 'b) -> 'a t -> 'b list
val insert_before : 'a t -> 'a -> unit
val insert_after : 'a t -> 'a -> unit
val of_list : 'a list -> 'a t
val next : 'a t ->  'a t 
val prev : 'a t ->  'a t 

val fold : ('a -> 'b -> 'b) -> 'a t -> 'b -> 'b
val wrappedfold : ('a t -> 'b -> 'b) -> 'a t -> 'b -> 'b
val lenght : 'a t -> int