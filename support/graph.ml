module type Mg = sig
  type data

  type payload

  val make_payload : data -> payload

  val pp_payload : Format.formatter -> payload -> unit

  val label : payload -> data

  val clone : payload -> payload
end

module type ExtType = sig
  type t

  val pp : Format.formatter -> t -> unit

  val clone : t -> t
end

module type Node = sig
  include ExtType
end

module Placeholder = struct
  type t = unit

  let pp _ _ = ()

  let clone () = ()
end

module type Edge = sig
  type data

  type tag

  type t = { node : int; tag : tag }

  val compare_tag : tag -> tag -> int

  (* val empty_data : data *)
  val pp_data : Format.formatter -> data -> unit

  val pp_tag : Format.formatter -> tag -> unit

  val show_tag : tag -> string

  val pp : Format.formatter -> t -> unit

  val compare : t -> t -> int

  val equal_tag : tag -> tag -> bool
end

module Gen (N : Node) (E : Edge) (Ext : ExtType) (M : Mg with type data = N.t) =
struct
  type payload = M.payload [@@deriving show]

  type t = {
    mutable arr : payload option array;
    mutable idx : int;
    ext : Ext.t;
  }
  [@@deriving show { with_path = false }]

  type node = int * t

  type data = N.t

  type ext = Ext.t

  type etag = E.tag

  type edata = E.data

  type et = E.t = { node : int; tag : etag }

  type edge = int * int * etag * t

  let get (v, g) = Option.get g.arr.(v)

  let set (v, g) p = g.arr.(v) <- Some p

  let node_id (u, _) = u

  let clone t =
    {
      arr = Array.map (Option.map M.clone) t.arr;
      idx = t.idx;
      ext = Ext.clone t.ext;
    }

  let clone_node (n, _) t = (n, t)

  let clone_edge (u, v, tag, _) t = (u, v, tag, t)

  let graph (_, t) = t

  let graph_of_edge (_, _, _, t) = t

  (* let node_of_id  t i  =  i,t *)

  let idle_edge t (u, _) (v, _) tag = (u, v, tag, t)

  let pp_node f (n : node) = N.pp f @@ M.label (get n)

  let show_node n =
    let buff = Buffer.create 16 in
    let formatter = Format.formatter_of_buffer buff in
    pp_node formatter n;
    Format.pp_print_flush formatter ();
    Buffer.contents buff

  let pp_edge_aux edge_value f ((u, v, tag, _) as e) =
    try
      Format.(
        fprintf f "%a - %a. tag: %a; data: %a" pp_print_int u pp_print_int v
          E.pp_tag tag E.pp_data (edge_value e))
    with Not_found -> Format.fprintf f "error edge"

  let show_edge_aux edge_value e =
    let buff = Buffer.create 128 in
    let formatter = Format.formatter_of_buffer buff in
    pp_edge_aux edge_value formatter e;
    Format.pp_print_flush formatter ();
    Buffer.contents buff

  let equal_node (v, _) (u, _) = v = u

  let compare_node (v, _) (u, _) = Int.compare v u

  let remove_node (v, g) = g.arr.(v) <- None

  let init n ext =
    if n <= 0 then raise (Invalid_argument "n")
    else { arr = Array.init n (fun _ -> None); idx = 0; ext }

  let ext_of_node (_, g) = g.ext

  let new_node g l =
    let len = Array.length g.arr in
    if g.idx = len then
      let arr =
        Array.init (2 * len) (fun i -> if i < len then g.arr.(i) else None)
      in
      g.arr <- arr
    else ();
    let u = M.make_payload l in
    let node = (g.idx, g) in
    g.arr.(g.idx) <- Some u;
    g.idx <- g.idx + 1;
    node

  let nodes ({ idx; _ } as g) =
    Seq.unfold (fun i -> if i < idx then Some ((i, g), i + 1) else None) 0
    |> List.of_seq

  let node_iter ({ idx; _ } as g) =
    let rec aux i () =
      if i < idx then Seq.Cons ((i, g), aux (i + 1)) else Seq.Nil
    in
    aux 0

  let nodelist (_, g) = List.map (fun i -> (i, g))

  let nodes_of_edge (u, v, _, g) = ((u, g), (v, g))

  let adj_aux adj_map ((_, g) as v) (tag : etag) =
    nodelist v
    @@ adj_map
         (function
           | { node; tag = tag' }, _
             when Option.is_some g.arr.(node) && E.equal_tag tag tag' ->
               Some node
           | _ -> None)
         v

  let draw_node f ((i, _) as n) =
    let lab, attrs = f (M.label @@ get n) in
    match attrs with
    | None -> Printf.sprintf "%d [label=\"%s\"];\n" i lab
    | Some attrs -> Printf.sprintf "%d [label=\"%s\" %s];\n" i lab attrs

  let to_dot_aux name global add_edges show_node (g : t) =
    let buff = Buffer.create 1024 in
    Buffer.add_string buff (Printf.sprintf "%s {\n%s\n" name global);
    List.iter (fun n -> Buffer.add_string buff @@ draw_node show_node n)
    @@ nodes g;
    add_edges buff;
    Buffer.add_string buff "}";
    buff
end

module type Set = sig
  type t

  type elt

  val elements : t -> elt list

  val empty : t

  val pp : Format.formatter -> t -> unit

  val remove : elt -> t -> t

  val add : elt -> t -> t

  val union : t -> t -> t

  val of_list : elt list -> t

  val fold : (elt -> 'a -> 'a) -> t -> 'a -> 'a
end

module type MapType = sig
  type 'a t

  type key

  val bindings : 'a t -> (key * 'a) list

  val empty : 'a t

  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit

  val remove : key -> 'a t -> 'a t

  val add : key -> 'a -> 'a t -> 'a t

  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t

  val of_list : (key * 'a) list -> 'a t

  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b

  val iter : (key -> 'a -> unit) -> 'a t -> unit

  val find : key -> 'a t -> 'a
end

module type Comparable = sig
  type t

  val compare : t -> t -> int
end

module MakeBTMap (E : Edge) = struct
  include Map.Make (E)

  let pp_key = E.pp

  type 'a u = (key * 'a) list [@@deriving show { with_path = false }]

  let pp af f x = pp_u af f @@ bindings x

  let of_list l = List.fold_left (fun m (k, v) -> add k v m) empty l
end

module MakeIGraphUnsigned
    (N : Node)
    (E : Edge)
    (M : MapType with type key = E.t)
    (Ext : ExtType) =
struct
  (* module S = SF (E) *)

  type map = E.data M.t [@@deriving show { with_path = false }]

  type u = { l : N.t; adj : map } [@@deriving show { with_path = false }]

  module G =
    Gen (N) (E) (Ext)
      (struct
        type data = N.t

        type payload = u [@@deriving show { with_path = false }]

        let make_payload l = { l; adj = M.empty }

        let label p = p.l

        let clone p = { l = N.clone p.l; adj = p.adj }
      end)

  include G

  type esnap = map option array

  let edges_snapshot g =
    Array.init (Array.length g.arr) (fun i ->
        Option.map (fun x -> x.adj) g.arr.(i))

  let process_snapshot f g s =
    Array.iteri
      (fun i adj ->
        Option.iter
          (fun adj ->
            Option.iter
              (fun p ->
                let adj = f p.adj adj in
                g.arr.(i) <- Some { p with adj })
              g.arr.(i))
          adj)
      s

  let restore_edges = process_snapshot (fun _ x -> x)

  let merge_edges =
    process_snapshot (fun adj adj' -> M.union (fun _ _ v -> Some v) adj adj')

  let adj_map f u = List.filter_map f (M.bindings (get u).adj)

  let adj = adj_aux adj_map

  let value v = (get v).l

  let edge_ch ch ((vi, _) as v : node) ((ui, _) as u : node) =
    let vr = get v in
    let ur = get u in
    set v { vr with adj = ch ui vr.adj };
    set u { ur with adj = ch vi ur.adj }

  let mk_edge u v (tag : etag) data =
    edge_ch (fun node -> M.add { node; tag } data) u v

  let rm_edge u v (tag : etag) =
    edge_ch (fun x -> M.remove { node = x; tag }) u v

  (* let mk_edges ((v, g) : node) (ul : (node * etag * edata) list) =
     let vr = Option.get g.arr.(v) in
     let es =
       List.map (fun ((node, _), tag, data) -> ({ node; tag }, data)) ul
       |> M.of_list
     in
     g.arr.(v) <- Some { vr with adj = M.union (fun _ v _ -> Some v) es vr.adj };

     List.iter
       (fun (((node, _) as u), tag, data) ->
         let ur = get u in
         set u { ur with adj = M.add { node; tag } data ur.adj })
       ul *)

  let edge ((ui, g) as u) (vi, _) tag =
    try
      let _ = M.find { node = vi; tag } (get u).adj in
      Some (ui, vi, tag, g)
    with Not_found -> None

  let adj_edges ((ui, g) as u) tag : (node * edge) list =
    adj_map
      (function
        | { node; tag = tag' }, _
          when Option.is_some g.arr.(node) && E.equal_tag tag tag' ->
            Some ((node, g), (ui, node, tag, g))
        | _ -> None)
      u

  let edge_value (u, v, tag, g) = M.find { node = v; tag } (get (u, g)).adj

  let adj_edge_data ((_, g) as u) tag =
    adj_map
      (function
        | { node; tag = tag' }, data
          when Option.is_some g.arr.(node) && E.equal_tag tag tag' ->
            Some data
        | _ -> None)
      u

  let adj_node_with_tag_iter f ((_, g) as u) tag =
    M.iter
      (fun adj _ ->
        if E.equal_tag adj.tag tag then
          match g.arr.(adj.node) with None -> () | Some x -> f x.l
        else ())
      (get u).adj

  let pp_edge = pp_edge_aux edge_value

  let show_edge e = show_edge_aux edge_value e

  let compare_edge (u1, v1, tag1, _) (u2, v2, tag2, _) =
    let u1, v1 = if u1 < v1 then (u1, v1) else (v1, u1) in
    let u2, v2 = if u2 < v2 then (u2, v2) else (v2, u2) in
    let c = u1 - u2 in
    if c != 0 then c
    else
      let c = v1 - v2 in
      if c != 0 then c else E.compare_tag tag1 tag2

  module EdgeSet = Set.Make (struct
    type t = edge

    let compare = compare_edge
  end)

  let all_edges (g : t) =
    Array.fold_left
      (fun (s, i) n ->
        match n with
        | None -> (s, i + 1)
        | Some n ->
            ( M.fold
                (fun adj _ s ->
                  if Option.is_some g.arr.(adj.node) then
                    EdgeSet.add (i, adj.node, adj.tag, g) s
                  else s)
                n.adj s,
              i + 1 ))
      (EdgeSet.empty, 0) g.arr
    |> fst

  let draw_edge f e u v =
    Option.map (fun attrs -> Printf.sprintf "%d -- %d [%s];\n" u v attrs) (f e)

  let to_dot ~global ~show_node ~show_edge (g : t) =
    to_dot_aux "graph" global
      (fun buff ->
        EdgeSet.iter (fun (u, v, tag, _) ->
            Option.map (Buffer.add_string buff) @@ draw_edge show_edge tag u v
            |> ignore)
        @@ all_edges g)
      show_node g
end

module type GraphType = sig
  type node

  type t

  type edge

  type data

  type etag

  type ext

  type edata

  val pp : Format.formatter -> t -> unit

  val show : t -> string

  val pp_node : Format.formatter -> node -> unit

  val show_node : node -> string

  val pp_edge : Format.formatter -> edge -> unit

  val show_edge : edge -> string

  val equal_node : node -> node -> bool

  val compare_node : node -> node -> int

  val init : int -> ext -> t

  val clone : t -> t

  val clone_node : node -> t -> node

  val clone_edge : edge -> t -> edge

  val graph : node -> t

  val graph_of_edge : edge -> t

  val idle_edge : t -> node -> node -> etag -> edge

  val ext_of_node : node -> ext

  val new_node : t -> data -> node

  val node_id : node -> int

  val nodes : t -> node list

  val node_iter : t -> node Seq.t

  val remove_node : node -> unit

  val value : node -> data

  val mk_edge : node -> node -> etag -> edata -> unit

  val rm_edge : node -> node -> etag -> unit

  val nodes_of_edge : edge -> node * node

  val to_dot :
    global:string ->
    show_node:(data -> string * string option) ->
    show_edge:(etag -> string option) ->
    t ->
    Buffer.t
end

module type IGtaphType = sig
  include GraphType

  type esnap

  val edges_snapshot : t -> esnap

  val restore_edges : t -> esnap -> unit

  val merge_edges : t -> esnap -> unit

  val adj : node -> etag -> node list

  val compare_edge : edge -> edge -> int

  module EdgeSet : Set.S with type elt = edge

  val edge : node -> node -> etag -> edge option

  val edge_value : edge -> edata

  val adj_edges : node -> etag -> (node * edge) list

  val adj_edge_data : node -> etag -> edata list

  val adj_node_with_tag_iter : (data -> unit) -> node -> etag -> unit
end

module MakeIGraph
    (N : Node)
    (E : Edge)
    (M : MapType with type key = E.t)
    (Ext : ExtType) :
  IGtaphType
    with type data = N.t
     and type etag = E.tag
     and type edata = E.data
     and type ext = Ext.t =
  MakeIGraphUnsigned (N) (E) (M) (Ext)

module MakeDGraphUnsigned
    (N : Node)
    (E : Edge)
    (M : MapType with type key = E.t)
    (Ext : ExtType) =
struct
  type map = E.data M.t [@@deriving show { with_path = false }]

  type u = { l : N.t; pred : map; succ : map }
  [@@deriving show { with_path = false }]

  module G =
    Gen (N) (E) (Ext)
      (struct
        type data = N.t

        type payload = u [@@deriving show { with_path = false }]

        let make_payload l = { l; pred = M.empty; succ = M.empty }

        let label p = p.l

        let clone p = { l = N.clone p.l; pred = p.pred; succ = p.succ }
      end)

  include G

  let succ_map f u = List.filter_map f (M.bindings (get u).succ)

  let succ = adj_aux succ_map

  let succ_pred f u = List.filter_map f (M.bindings (get u).pred)

  let pred = adj_aux succ_pred

  let adj v tag = pred v tag @ succ v tag

  let value v = (get v).l

  let edge_ch ch ((v, g) : node) ((u, _) : node) =
    let vr = Option.get g.arr.(v) in
    g.arr.(v) <- Some { vr with succ = ch u vr.succ };
    let ur = Option.get g.arr.(u) in
    g.arr.(u) <- Some { ur with pred = ch v ur.pred }

  let mk_edge u v (tag : etag) data =
    edge_ch (fun node -> M.add { node; tag } data) u v

  let rm_edge u v (tag : etag) =
    edge_ch (fun x -> M.remove { node = x; tag }) u v

  let edge_value (u, v, tag, g) = M.find { node = v; tag } (get (u, g)).succ

  let pp_edge = pp_edge_aux edge_value

  let show_edge e = show_edge_aux edge_value e

  let topology_sort_aux next (i, g) =
    let flags = Array.init g.idx (fun _ -> false) in
    let stack = ref [] in
    let rec dfs i =
      let r = Option.get g.arr.(i) in
      if not flags.(i) then (
        flags.(i) <- true;
        aux (next r);
        stack := (i, g) :: !stack)
      else ()
    and aux = function
      | [] -> ()
      | n :: tl ->
          dfs n;
          aux tl
    in
    dfs i;
    !stack

  let topology_sort v =
    topology_sort_aux
      (fun r -> M.bindings r.succ |> List.map (fun ({ node; _ }, _) -> node))
      v

  let backward_topology_sort v =
    topology_sort_aux
      (fun r -> M.bindings r.pred |> List.map (fun ({ node; _ }, _) -> node))
      v

  let draw_edge f e u v =
    Option.map (fun attrs -> Printf.sprintf "%d -> %d [%s];\n" u v attrs) (f e)

  let to_dot ~global ~show_node ~show_edge (g : t) =
    to_dot_aux "digraph" global
      (fun buff ->
        List.iter (fun ((ui, _) as u : node) ->
            List.iter (fun ({ node = vi; tag }, _) ->
                Option.map (Buffer.add_string buff)
                @@ draw_edge show_edge tag ui vi
                |> ignore)
            @@ M.bindings (get u).succ)
        @@ nodes g)
      show_node g
end

module type DGtaphType = sig
  include GraphType

  val succ : node -> etag -> node list

  val pred : node -> etag -> node list

  val topology_sort : node -> node list

  val backward_topology_sort : node -> node list
end

module MakeDGraph
    (N : Node)
    (E : Edge)
    (M : MapType with type key = E.t)
    (Ext : ExtType) :
  DGtaphType
    with type data = N.t
     and type etag = E.tag
     and type edata = E.data
     and type ext = Ext.t =
  MakeDGraphUnsigned (N) (E) (M) (Ext)

let%test_module _ =
  (module struct
    module E = struct
      [@@@warning "-37-26"]

      type data = int [@@deriving show { with_path = false }]

      type tag = A | B [@@deriving show { with_path = false }, eq, ord]

      type t = { node : int; tag : tag }
      [@@deriving show { with_path = false }, eq, ord]
    end

    module N = struct
      type t = int [@@deriving show { with_path = false }]

      let clone x = x
    end

    module S = Assocordlist.AssocOrdList (E)
    module IG = MakeIGraphUnsigned (N) (E) (S) (Placeholder)
    open IG

    [@@@warning "-26"]

    (* type edgelst = edge list  [@@deriving show { with_path = false }] *)
    type nodelst = node list [@@deriving show { with_path = false }]

    let%test _ =
      let g = IG.init 1 () in
      compare_edge (1, 2, A, g) (2, 1, A, g) = 0

    let%test _ =
      let g = IG.init 1 () in
      compare_edge (1, 2, A, g) (2, 1, B, g) < 0

    let%test "compare_edge ( 3, 2, g  ) (2,1,g) " =
      let g = IG.init 1 () in

      (* Printf.printf " -- -- -- %d \n" @@ E.compare_data E.A E.A ;   *)
      compare_edge (3, 2, A, g) (2, 1, A, g) > 0

    let%expect_test _ =
      let g = IG.init 1 () in
      print_string @@ show g;
      [%expect {| { arr = [|None|]; idx = 0; ext =  } |}]

    let%expect_test _ =
      let g = IG.init 1 () in
      let node = new_node g 3 in
      print_string @@ show_node node;
      [%expect {| 3 |}];
      print_string @@ show g;
      [%expect
        {| 
      { arr = [|(Some { l = 3; adj = [] })|]; idx = 1; ext =  }
      |}];

      let _ = new_node g 4 in
      print_string @@ show g;
      [%expect
        {|
           { arr = [|(Some { l = 3; adj = [] }); (Some { l = 4; adj = [] })|]; idx = 2;
             ext =  }
            |}]

    let%expect_test _ =
      let g = IG.init 1 () in
      let n1 = new_node g 3 in
      let n2 = new_node g 4 in
      let n3 = new_node g 5 in

      mk_edge n1 n2 E.A 88;
      print_string @@ show g;
      [%expect
        {|
            { arr =
              [|(Some { l = 3; adj = [({ node = 1; tag = A }, 88)] });
                (Some { l = 4; adj = [({ node = 0; tag = A }, 88)] });
                (Some { l = 5; adj = [] }); None|];
              idx = 3; ext =  }
        
           |}];
      mk_edge n2 n3 E.B 89;
      print_string @@ show g;

      [%expect
        {|
          { arr =
            [|(Some { l = 3; adj = [({ node = 1; tag = A }, 88)] });
              (Some { l = 4;
                      adj = [({ node = 0; tag = A }, 88); ({ node = 2; tag = B }, 89)]
                      });
              (Some { l = 5; adj = [({ node = 1; tag = B }, 89)] }); None|];
            idx = 3; ext =  }
        
           |}]

    let%expect_test _ =
      let g = IG.init 1 () in
      let n1 = new_node g 3 in
      let n2 = new_node g 4 in
      let n3 = new_node g 5 in

      mk_edge n1 n2 E.A 88;
      mk_edge n2 n3 E.B 89;
      let snapshot = edges_snapshot g in

      rm_edge n1 n2 E.B;
      print_string @@ show g;
      [%expect
        {|
          { arr =
            [|(Some { l = 3; adj = [({ node = 1; tag = A }, 88)] });
              (Some { l = 4;
                      adj = [({ node = 0; tag = A }, 88); ({ node = 2; tag = B }, 89)]
                      });
              (Some { l = 5; adj = [({ node = 1; tag = B }, 89)] }); None|];
            idx = 3; ext =  }
    
      |}];

      rm_edge n1 n2 E.A;
      print_string @@ show g;
      [%expect
        {|
          { arr =
            [|(Some { l = 3; adj = [] });
              (Some { l = 4; adj = [({ node = 2; tag = B }, 89)] });
              (Some { l = 5; adj = [({ node = 1; tag = B }, 89)] }); None|];
            idx = 3; ext =  }
          |}];

      restore_edges g snapshot;
      print_string @@ show g;
      [%expect
        {|
        { arr =
          [|(Some { l = 3; adj = [({ node = 1; tag = A }, 88)] });
            (Some { l = 4;
                    adj = [({ node = 0; tag = A }, 88); ({ node = 2; tag = B }, 89)]
                    });
            (Some { l = 5; adj = [({ node = 1; tag = B }, 89)] }); None|];
          idx = 3; ext =  } |}]

    let%expect_test _ =
      let g = IG.init 1 () in
      let n1 = new_node g 3 in
      let n2 = new_node g 4 in
      let n3 = new_node g 5 in

      mk_edge n1 n2 E.A 88;
      mk_edge n2 n3 E.B 89;

      print_string @@ show_nodelst @@ adj n2 E.A;

      remove_node n1;
      print_string @@ show_nodelst @@ adj n2 E.A;
      [%expect {|
                   [3][]
                |}];
      print_string @@ show g;
      [%expect
        {| 
        { arr =
          [|None;
            (Some { l = 4;
                    adj = [({ node = 0; tag = A }, 88); ({ node = 2; tag = B }, 89)]
                    });
            (Some { l = 5; adj = [({ node = 1; tag = B }, 89)] }); None|];
          idx = 3; ext =  } |}]
  end)

let%test_module _ =
  (module struct
    module E = struct
      [@@@warning "-37-26"]

      type data = int [@@deriving show { with_path = false }]

      type tag = A | B [@@deriving show { with_path = false }, eq, ord]

      type t = { node : int; tag : tag }
      [@@deriving show { with_path = false }, eq, ord]
    end

    module N = struct
      type t = int [@@deriving show { with_path = false }]

      let clone x = x
    end

    module S = Assocordlist.AssocOrdList (E)
    module DG = MakeDGraphUnsigned (N) (E) (S) (Placeholder)
    open DG

    [@@@warning "-26"]

    (* type edgelst = edge list  [@@deriving show { with_path = false }] *)
    type nodelst = node list [@@deriving show { with_path = false }]

    let%expect_test _ =
      let g = DG.init 1 () in
      let n1 = new_node g 3 in
      let n2 = new_node g 4 in
      let n3 = new_node g 5 in

      mk_edge n1 n2 E.A 88;
      mk_edge n2 n3 E.B 89;
      print_string @@ show g;
      [%expect
        {|
        { arr =
          [|(Some { l = 3; pred = []; succ = [({ node = 1; tag = A }, 88)] });
            (Some { l = 4; pred = [({ node = 0; tag = A }, 88)];
                    succ = [({ node = 2; tag = B }, 89)] });
            (Some { l = 5; pred = [({ node = 1; tag = B }, 89)]; succ = [] }); None|];
          idx = 3; ext =  }
  
    |}];

      rm_edge n1 n2 E.B;
      print_string @@ show g;
      [%expect
        {|
          { arr =
            [|(Some { l = 3; pred = []; succ = [({ node = 1; tag = A }, 88)] });
              (Some { l = 4; pred = [({ node = 0; tag = A }, 88)];
                      succ = [({ node = 2; tag = B }, 89)] });
              (Some { l = 5; pred = [({ node = 1; tag = B }, 89)]; succ = [] }); None|];
            idx = 3; ext =  }
    
      |}];

      rm_edge n1 n2 E.A;
      print_string @@ show g;
      [%expect
        {|
          { arr =
            [|(Some { l = 3; pred = []; succ = [] });
              (Some { l = 4; pred = []; succ = [({ node = 2; tag = B }, 89)] });
              (Some { l = 5; pred = [({ node = 1; tag = B }, 89)]; succ = [] }); None|];
            idx = 3; ext =  }
          |}]

    (* module Seq = Seq *)

    let%expect_test _ =
      let graph = DG.init 1 () in
      let ns =
        Seq.iternums 0 |> Seq.take 8
        |> Seq.map (fun x -> new_node graph x)
        |> Array.of_seq
      in
      let mke u v = mk_edge u v A 0 in
      mke ns.(0) ns.(1);
      mke ns.(1) ns.(2);
      mke ns.(2) ns.(3);
      mke ns.(3) ns.(4);
      mke ns.(1) ns.(5);
      mke ns.(5) ns.(2);
      mke ns.(5) ns.(6);
      mke ns.(6) ns.(3);
      mke ns.(6) ns.(7);
      mke ns.(7) ns.(4);

      print_string @@ show_nodelst @@ DG.topology_sort ns.(0);
      [%expect {| [0; 1; 5; 6; 7; 2; 3; 4] |}]
  end)
