type 'a t = {
  mutable data : 'a;
  mutable prev : 'a t option;
  mutable next : 'a t option;
}

let pp af (f : Format.formatter) d =
  let open Format in
  let print data = fprintf f "%a" af data in
  let rec cur = function
    | { data; prev; next } ->
        prev_ prev;
        print data;
        next_ next
  and prev_ = function
    | None -> fprintf f "@[["
    | Some { data; prev; _ } ->
        prev_ prev;
        print data;
        fprintf f ",@ "
  and next_ = function
    | None -> fprintf f "]@]"
    | Some { data; next; _ } ->
        fprintf f ",@ ";
        print data;
        next_ next
  in
  cur d

let equal eq d1 d2 =
  let rec cur d1 d2 =
    match (d1, d2) with
    | ( { data = data1; prev = prev1; next = next1 },
        { data = data2; prev = prev2; next = next2 } ) ->
        prev_ prev1 prev2 && eq data1 data2 && next_ next1 next2
  and prev_ d1 d2 =
    match (d1, d2) with
    | None, None -> true
    | ( Some { data = data1; prev = prev1; _ },
        Some { data = data2; prev = prev2; _ } ) ->
        eq data1 data2 && prev_ prev1 prev2
    | _ -> false
  and next_ d1 d2 =
    match (d1, d2) with
    | None, None -> true
    | ( Some { data = data1; next = next1; _ },
        Some { data = data2; next = next2; _ } ) ->
        eq data1 data2 && next_ next1 next2
    | _ -> false
  in
  cur d1 d2

let show f x =
  let buff = Buffer.create 128 in
  let formatter = Format.formatter_of_buffer buff in
  pp f formatter x;
  Format.pp_print_flush formatter ();
  Buffer.contents buff

let set d x = d.data <- x

let value d = d.data

let replace_value d f = d.data <- f d.data

let next = function
  | { next = None; _ } -> raise Not_found
  | { next = Some n; _ } -> n

let prev = function
  | { prev = None; _ } -> raise Not_found
  | { prev = Some n; _ } -> n

let rec end_node = function
  | { next = None; _ } as n -> n
  | { next = Some n; _ } -> end_node n

let map_to_list f d =
  let rec aux a = function
    | { prev = None; data; _ } -> f data :: a
    | { prev = Some n; data; _ } -> aux (f data :: a) n
  in
  aux [] @@ end_node d

let rec start_node = function
  | { prev = None; _ } as n -> n
  | { prev = Some n; _ } -> start_node n

let fold f d a =
  let rec aux a = function
    | { next = None; data; _ } -> f data a
    | { next = Some n; data; _ } -> aux (f data a) n
  in
  aux a @@ start_node d

let lenght d = fold (fun _ i -> i+1) d 0

  let wrappedfold f d a =
    let rec aux a = function
      | { next = None;  _ } as n -> f n a
      | { next = Some next; _ } as n -> aux (f n a) next
    in
    aux a @@ start_node d

let insert_before d x =
  match d with
  | { prev = Some p; _ } as c ->
      let nn = { prev = Some p; data = x; next = Some c } in
      p.next <- Some nn;
      c.prev <- Some nn
  | { prev = None; _ } as c ->
      let nn = { prev = None; data = x; next = Some c } in
      c.prev <- Some nn

let insert_after_aux d x =
  match d with
  | { next = Some n; _ } as c ->
      let nn = { prev = Some c; data = x; next = Some n } in
      n.prev <- Some nn;
      c.next <- Some nn;
      nn
  | { next = None; _ } as c ->
      let nn = { prev = Some c; data = x; next = None } in
      c.next <- Some nn;
      nn

let insert_after d x = insert_after_aux d x |> ignore

let of_list l =
  let rec aux d = function
    | [] -> ()
    | h :: tl -> aux (insert_after_aux d h) tl
  in
  match l with
  | [] -> raise (Invalid_argument "List is empty")
  | h :: tl ->
      let d = { data = h; prev = None; next = None } in
      aux d tl;
      d

let%test_module _ =
  (module struct
    type ilist = int list [@@deriving eq]

    let equal = equal ( = )

    let show = show Format.pp_print_int

    let rec d1 = { data = 1; prev = None; next = Some d2 }

    and d2 = { data = 2; prev = Some d1; next = Some d3 }

    and d3 = { data = 3; prev = Some d2; next = None }

    let%test "map_to_list Fun.id  d1" =
      equal_ilist (map_to_list Fun.id d1) [ 1; 2; 3 ]

    let%test "map_to_list Fun.id  d2" =
      equal_ilist (map_to_list Fun.id d2) [ 1; 2; 3 ]

    let%test "map_to_list Fun.id  d3" =
      equal_ilist (map_to_list Fun.id d3) [ 1; 2; 3 ]

    let%test " insert_before 0 d1" =
      let d1 = { data = 1; prev = None; next = None } in

      insert_before d1 0;
      print_endline @@ show d1;
      equal d1
        {
          data = 1;
          prev = Some { data = 0; prev = None; next = Some d1 };
          next = None;
        }

    let%test "insert_before 0 d2" =
      let rec d1 = { data = 1; prev = None; next = Some d2 }
      and d2 = { data = 2; prev = Some d1; next = Some d3 }
      and d3 = { data = 3; prev = Some d2; next = None } in

      insert_before d2 0;
      print_endline @@ show d2;

      let rec d1 = { data = 1; prev = None; next = Some d0 }
      and d0 = { data = 0; prev = Some d1; next = Some d2' }
      and d2' = { data = 2; prev = Some d0; next = Some d3 }
      and d3 = { data = 3; prev = Some d2'; next = None } in

      equal d2 d2'

    let%test " insert_after 0 d1" =
      let d1 = { data = 1; prev = None; next = None } in

      insert_after d1 0;
      print_endline @@ show d1;
      equal d1
        {
          data = 1;
          prev = None;
          next = Some { data = 0; prev = Some d1; next = None };
        }

    let%test "insert_after 0 d2" =
      let rec d1 = { data = 1; prev = None; next = Some d2 }
      and d2 = { data = 2; prev = Some d1; next = Some d3 }
      and d3 = { data = 3; prev = Some d2; next = None } in

      insert_after d2 0;
      print_endline @@ show d2;

      let rec d1 = { data = 1; prev = None; next = Some d2' }
      and d2' = { data = 2; prev = Some d1; next = Some d0 }
      and d0 = { data = 0; prev = Some d2'; next = Some d3 }
      and d3 = { data = 3; prev = Some d0; next = None } in

      equal d2 d2'

    let%test "of_list  [1;2;3]  )  d1" =
      let rec d1 = { data = 1; prev = None; next = Some d2 }
      and d2 = { data = 2; prev = Some d1; next = Some d3 }
      and d3 = { data = 3; prev = Some d2; next = None } in

      equal (of_list [ 1; 2; 3 ]) d1

    let%test "fold (fun i a -> value i + a) d1 0" =
      let rec d1 = { data = 1; prev = None; next = Some d2 }
      and d2 = { data = 2; prev = Some d1; next = Some d3 }
      and d3 = { data = 3; prev = Some d2; next = None } in

      fold (fun i a ->  i + a) d1 0 = 6

      let%test "wrappedfold (fun i a -> value i + a) d1 0" =
      let rec d1 = { data = 1; prev = None; next = Some d2 }
      and d2 = { data = 2; prev = Some d1; next = Some d3 }
      and d3 = { data = 3; prev = Some d2; next = None } in

      wrappedfold (fun i a -> value i + a) d1 0 = 6

  end)
