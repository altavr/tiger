(* open Icr.Types *)

open Icr.Tree
module F = X86_64.Frame
module T = Icr.Translate.MakeUnsighned (F)
module Temp = Support.Temp

[@@@warning "-26"]

(* 1. Локальные переменные на стеке
   2. Локальные переменные в calleesaves registers
*)
let case1 =
  let om = T.outermost () in
  let a1 = T.allocLocal om `Boxed true in

  let a2 = T.allocLocal om `Boxed true in

  let r1 = T.record [ (`Unboxed, T.Ex (Const 1)) ] in

  let r2 = T.record [ (`Boxed, T.Ex (Const 0)) ] in

  let as_r1 = T.assignVar a1 om r1 `Boxed in

  let as_r2 = T.assignVar a2 om r2 `Boxed in

  let v_r1 = T.simpleVar a1 om in
  let v_r2 = T.simpleVar a2 om in

  let e = T.ptrAssign ~ptr:(T.ptrFieldVar ~var:v_r2 ~idx:0) v_r1 `Boxed in

  let s = T.sequence [ as_r1; as_r2 ] ~last:e in
  print_string @@ show_stm (T.unNx s);

  T.procEntryExit om s;

  ("gc1", T.getResult (), "", "")

(* 1. Локальные переменные на стеке
   2. Производный указатель в calleesaves registers с константным смещением
*)
let case2 =
  let om = T.outermost () in

  let a2 = T.allocLocal om `Boxed true in

  let r0 = T.record [ (`Unboxed, T.Ex (Const 1)) ] in
  let r1 = T.record [ (`Unboxed, T.Ex (Const 11)) ] in

  let r2 = T.record [ (`Boxed, r0); (`Boxed, r1) ] in

  let as_r2 = T.assignVar a2 om r2 `Boxed in

  let s = T.sequence [ as_r2 ] ~last:(T.Nx (Exp (Const 0))) in
  print_string @@ show_stm (T.unNx s);

  T.procEntryExit om s;

  ("gc2", T.getResult (), "", "")

(* 1. Локальные переменные на стеке
   2. Производный указатель в calleesaves registers со смещением, сохраненным в
       другом calleesaves register (незаспиленная переменная)
*)

let case3 =
  let om = T.outermost () in

  let a2 = T.allocLocal om `Boxed true in

  let r0 = T.record [ (`Unboxed, T.Ex (Const 222)) ] in
  let r1 = T.record [ (`Unboxed, T.Ex (Const 11)) ] in

  let r2 = T.array ~len:(T.Ex (Const 2)) `Boxed r0 in

  let as_r2 = T.assignVar a2 om r2 `Boxed in

  let dp = T.ptrSubscribeVar ~var:(T.simpleVar a2 om) ~idx:(T.Ex (Const 1)) in

  let t = Temp.newtemp @@ exp_boxed @@ T.unEx dp in
  let m = T.Nx (Move (Temp t, T.unEx dp)) in

  let as_3 = T.ptrAssign ~ptr:(T.Ex (Temp t)) r1 `Boxed in

  let s = T.sequence [ as_r2; m; as_3 ] ~last:(T.Nx (Exp (Const 0))) in
  print_string @@ show_stm (T.unNx s);

  T.procEntryExit om s;

  ("gc3", T.getResult (), "", "")

let foo_frame om =
  let lab = Temp.namedLabel "foo" in
  let foo = T.newLevel om lab [] in
  let a1 = T.allocLocal foo `Boxed false in
  let a2 = T.allocLocal foo `Boxed false in
  let r1 = T.record [ (`Unboxed, T.Ex (Const 33)) ] in
  let r2 = T.record [ (`Boxed, T.Ex (Const 0)) ] in
  let as_r1 = T.assignVar a1 foo r1 `Boxed in
  let as_r2 = T.assignVar a2 foo r2 `Boxed in
  let v_r1 = T.simpleVar a1 foo in
  let v_r2 = T.simpleVar a2 foo in
  let e = T.ptrAssign ~ptr:(T.ptrFieldVar ~var:v_r2 ~idx:0) v_r1 `Boxed in
  let s = T.sequence [ as_r1; as_r2 ] ~last:e in
  (foo, lab, s)

(* 1. Локальные переменные на стеке
   2. Сalleesaves registers на стеке
*)
let case4 =
  let om = T.outermost () in

  let foo, foo_lab, foo_s = foo_frame om in
  T.procEntryExit foo foo_s;

  let a0 = T.allocLocal om `Boxed false in
  let a1 = T.allocLocal om `Boxed false in

  let a2 = T.allocLocal om `Boxed true in

  let r0 = T.record [ (`Unboxed, T.Ex (Const 22)) ] in

  let r1 = T.record [ (`Unboxed, T.Ex (Const 44)) ] in

  let r2 = T.record [ (`Boxed, T.Ex (Const 0)); (`Boxed, T.Ex (Const 0)) ] in

  let as_r0 = T.assignVar a0 om r0 `Boxed in
  let as_r1 = T.assignVar a1 om r1 `Boxed in

  let as_r2 = T.assignVar a2 om r2 `Boxed in

  let v_r0 = T.simpleVar a0 om in
  let v_r1 = T.simpleVar a1 om in
  let v_r2 = T.simpleVar a2 om in

  let e0 = T.ptrAssign ~ptr:(T.ptrFieldVar ~var:v_r2 ~idx:0) v_r0 `Boxed in
  let e = T.ptrAssign ~ptr:(T.ptrFieldVar ~var:v_r2 ~idx:1) v_r1 `Boxed in

  let call_foo = T.funCall foo_lab foo false ~env:om [] `Unboxed in

  let om_s = T.sequence [ as_r0; as_r1; call_foo; as_r2; e0 ] ~last:e in

  (* print_string @@ show_stm (T.unNx s); *)
  T.procEntryExit om om_s;

  ("gc4", T.getResult (), "", "")

(* 1. Локальные переменные на стеке
   2. Сalleesaves registers на стеке с константным смещением
*)
let case5 =
  let om = T.outermost () in

  let foo, foo_lab, foo_s = foo_frame om in
  T.procEntryExit foo foo_s;

  let a2 = T.allocLocal om `Boxed true in

  let r0 = T.record [ (`Unboxed, T.Ex (Const 1)) ] in
  let r1 = T.record [ (`Unboxed, T.Ex (Const 11)) ] in

  let r2 = T.record [ (`Boxed, r0); (`Boxed, r1) ] in

  let as_r2 = T.assignVar a2 om r2 `Boxed in

  let call_foo = T.funCall foo_lab foo false ~env:om [] `Unboxed in
  let om_s = T.sequence [ as_r2 ] ~last:call_foo in

  (* print_string @@ show_stm (T.unNx s); *)
  T.procEntryExit om om_s;

  ("gc5", T.getResult (), "", "")

(* 1. Локальные переменные на стеке
   2. Сalleesaves registers на стеке со смещением в другом Сalleesaves register
     (незаспиленная переменная)
*)

let case6 =
  let om = T.outermost () in

  let foo, foo_lab, foo_s = foo_frame om in
  T.procEntryExit foo foo_s;

  let a2 = T.allocLocal om `Boxed true in

  let r0 = T.record [ (`Unboxed, T.Ex (Const 222)) ] in
  let r1 = T.record [ (`Unboxed, T.Ex (Const 11)) ] in

  let r2 = T.array ~len:(T.Ex (Const 2)) `Boxed r0 in

  let as_r2 = T.assignVar a2 om r2 `Boxed in

  let dp = T.ptrSubscribeVar ~var:(T.simpleVar a2 om) ~idx:(T.Ex (Const 1)) in

  let t = Temp.newtemp @@ exp_boxed @@ T.unEx dp in
  let m = T.Nx (Move (Temp t, T.unEx dp)) in

  let as_3 = T.ptrAssign ~ptr:(T.Ex (Temp t)) r1 `Boxed in

  let call_foo = T.funCall foo_lab foo false ~env:om [] `Unboxed in
  let om_s = T.sequence [ as_r2; m; call_foo ] ~last:as_3 in

  T.procEntryExit om om_s;

  ("gc6", T.getResult (), "", "")

let () = Testrun.from_frags [ case1; case2; case3; case4; case5; case6 ]
(* let () = Testrun.from_frags [ case2 ] *)
