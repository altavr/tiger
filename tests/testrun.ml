let runcmd x =
  if Sys.command x = 0 then ()
  else (
    prerr_string @@ "Error: " ^ x;
    exit 1)

let root = Filename.concat (Sys.getcwd ()) "../../../.."

let test name basename asm_file expect expect_err =
  print_endline @@ "testing " ^ basename ^ "...\n";

  runcmd
  @@ Printf.sprintf "yasm -g dwarf2 -f elf64 -o %s.o -l %s.lst %s" basename
       basename asm_file;
  runcmd
  @@ Printf.sprintf
       "gcc -g -no-pie -o %s %s.o runtime.o  -L \
        /home/alex/documents/ocaml/ed_compiler/tiger/runtime/runtime/target/debug \
        -lrt -lpthread -ldl"
       basename basename;

  let buff = Buffer.create 1024 in
  let buff_err = Buffer.create 1024 in
  Unix.open_process_full (Filename.concat "./" basename) [||]
  |> fun (c, _, e) ->
  (try Buffer.add_channel buff c 1024 with End_of_file -> ());
  (try Buffer.add_channel buff_err e 1024 with End_of_file -> ());

  let res = Buffer.contents buff in
  let res_err = Buffer.contents buff_err in
  if res = expect && expect_err = res_err then ()
  else (
    prerr_string @@ "Error: output is not as expected\nFile: " ^ name;
    prerr_newline ();
    Printf.printf "Expect: \"%s\",\nbut got: \"%s\"\n" expect res;
    Printf.printf "Expect error: \"%s\",\nbut got error: \"%s\"\n" expect_err
      res_err;
    exit 1)

let init () =
  let tempdir = Filename.concat root "temp" in
  if not @@ Sys.file_exists tempdir then Sys.mkdir tempdir 0o777;
  Sys.chdir tempdir;
  runcmd
  @@ Printf.sprintf "cc -g -Wall -Wextra -Wpedantic -c -o runtime.o %s"
  @@ Filename.concat root "runtime/runtime.c"

let from_src cases =
  init ();

  List.iter
    (fun (fn, expect, expect_err) ->
      let fn =
        Filename.(concat root @@ concat "tests/compiler_test/testdata" fn)
      in
      let basename = Filename.(chop_extension @@ basename fn) in
      let asm_file = basename ^ ".asm" in
      Compiler.main fn asm_file;
      test fn basename asm_file expect expect_err)
    cases

let from_frags cases =
  init ();
  List.iter
    (fun (name, fl, expect, expect_err) ->
      let asm_file = name ^ ".asm" in
      let buff = Buffer.create (2 * 1024) in

      Compiler.C.compile buff fl;
      let outchan = open_out asm_file in
      Buffer.output_buffer outchan buff;
      close_out outchan;

      test name name asm_file expect expect_err)
    cases
