[@@@warning "-33"]

open Color_test


module F = Liveness.Frame 
open Support
open Backend.Assem
open Liveness  




let _ = 
  let regs = F.RegMap.union (fun _ _ _ ->  failwith "register intersection")
        F.regMap @@ regs_make [ "a"; "b"; "c"; "d"; "e" ] in
  let te s =  F.RegMap.find s regs in
  (* let show_temp = make_show_temp regs in *)
  let lab1 = Temp.newlabel () in
  let instr =
    [
      test_move (te "c") (te "r3");
      test_move (te "a") (te "r1");
      test_move (te "b") (te "r2");
      test_oper [ te "d" ] [];
      test_move (te "e") (te "a");
      test_label lab1;
      test_oper [ te "d" ] [ te "d"; te "b" ];
      test_oper [ te "e" ] [ te "e" ];
      test_oper ~jump:[ lab1 ] [] [ te "e" ];
      test_move (te "r1") (te "d");
      test_move (te "r3") (te "c");
    ]
  in
  RA.alloc () instr 



let () = ()
