
(* open Backend *)
(* open Support *)


open Misc 
 

let draw_graph ~show_temp g n = draw_graph ~show_temp g ( "case2_" ^ n )

let regs = regs_make [ "r1"; "r2"; "r3"; "a"; "b"; "c1"; "c2"; "d"; "e" ]
 
let show_temp t = RM.find t regs

let colormap (t : Support.Temp.t) =
  match show_temp t with
  | "r1" -> Some 0
  | "r2" -> Some 1
  | "r3" -> Some 2
  | _ -> None

let make () =
  let g = IG.init ~k:3 20 in
  let nodemap = nodemap_make g regs in

  let no s = SM.find s nodemap in

  let _ =
    let open IG in
    mk_edge (no "r1") (no "r2");
    mk_edge (no "r1") (no "r3");
    mk_edge (no "r1") (no "c1");
    mk_edge (no "r1") (no "c2");

    mk_edge (no "r2") (no "r3");
    mk_edge (no "r2") (no "a");
    mk_edge (no "r2") (no "c2");

    mk_edge (no "a") (no "b");
    mk_edge (no "a") (no "d");

    mk_edge (no "b") (no "e");
    mk_edge (no "b") (no "d");

    mk_edge (no "d") (no "e");

    add_move (no "r1") (no "d"); 
    add_move (no "r1") (no "a");

    add_move (no "r2") (no "b");

    add_move (no "r3") (no "c1");
    add_move (no "r3") (no "c2");
    
    add_move (no "a") (no "e")
  in
  (g, no)

let g, no = make ()

let _ = draw_graph ~show_temp g "init0"



let%expect_test _ =
  let g, _ = make () in
  let spilled = Color.color 3 colormap g in
  draw_graph ~show_temp g "color";
  (* check_invariants nms mms; *)
  print_string @@ show_nodelist spilled;

  [%expect {| [] |}];

  print_string @@ IG.show g;
  [%expect
    {|
         { arr =
           [|(Some { l = { temp = r1; degree = 2; color = 0; head = (Some true) };
                     adj =
                     [({ node = 1; tag = (Intf false) }, None);
                       ({ node = 2; tag = (Intf false) }, None);
                       ({ node = 3; tag = Move }, (Some (r1, a)));
                       ({ node = 3; tag = Coalesced }, None);
                       ({ node = 5; tag = (Intf false) }, None);
                       ({ node = 6; tag = (Intf false) }, None);
                       ({ node = 7; tag = Move }, (Some (r1, d)));
                       ({ node = 8; tag = Move }, (Some (e, r1)));
                       ({ node = 8; tag = Coalesced }, None)]
                     });
             (Some { l = { temp = r2; degree = 2; color = 1; head = (Some true) };
                     adj =
                     [({ node = 0; tag = (Intf false) }, None);
                       ({ node = 2; tag = (Intf false) }, None);
                       ({ node = 3; tag = (Intf false) }, None);
                       ({ node = 4; tag = Move }, (Some (r2, b)));
                       ({ node = 4; tag = Coalesced }, None);
                       ({ node = 6; tag = (Intf false) }, None)]
                     });
             (Some { l = { temp = r3; degree = 2; color = 2; head = (Some true) };
                     adj =
                     [({ node = 0; tag = (Intf false) }, None);
                       ({ node = 1; tag = (Intf false) }, None);
                       ({ node = 5; tag = Move }, (Some (r3, c1)));
                       ({ node = 5; tag = Coalesced }, None);
                       ({ node = 6; tag = Move }, (Some (r3, c2)));
                       ({ node = 6; tag = Coalesced }, None)]
                     });
             (Some { l = { temp = a; degree = 0; color = 0; head = (Some false) };
                     adj =
                     [({ node = 0; tag = Move }, (Some (r1, a)));
                       ({ node = 0; tag = Coalesced }, None);
                       ({ node = 1; tag = (Intf false) }, None);
                       ({ node = 4; tag = (Intf false) }, None);
                       ({ node = 7; tag = (Intf false) }, None);
                       ({ node = 8; tag = Move }, (Some (a, e)));
                       ({ node = 8; tag = Coalesced }, None)]
                     });
             (Some { l = { temp = b; degree = 0; color = 1; head = (Some false) };
                     adj =
                     [({ node = 1; tag = Move }, (Some (r2, b)));
                       ({ node = 1; tag = Coalesced }, None);
                       ({ node = 3; tag = (Intf false) }, None);
                       ({ node = 7; tag = (Intf false) }, None);
                       ({ node = 8; tag = (Intf false) }, None)]
                     });
             (Some { l = { temp = c1; degree = 0; color = 2; head = (Some false) };
                     adj =
                     [({ node = 0; tag = (Intf false) }, None);
                       ({ node = 2; tag = Move }, (Some (r3, c1)));
                       ({ node = 2; tag = Coalesced }, None)]
                     });
             (Some { l = { temp = c2; degree = 0; color = 2; head = (Some false) };
                     adj =
                     [({ node = 0; tag = (Intf false) }, None);
                       ({ node = 1; tag = (Intf false) }, None);
                       ({ node = 2; tag = Move }, (Some (r3, c2)));
                       ({ node = 2; tag = Coalesced }, None)]
                     });
             (Some { l = { temp = d; degree = 0; color = 2; head = None };
                     adj =
                     [({ node = 0; tag = Move }, (Some (r1, d)));
                       ({ node = 3; tag = (Intf false) }, None);
                       ({ node = 4; tag = (Intf false) }, None);
                       ({ node = 8; tag = (Intf false) }, None)]
                     });
             (Some { l = { temp = e; degree = 0; color = 0; head = (Some false) };
                     adj =
                     [({ node = 0; tag = Move }, (Some (e, r1)));
                       ({ node = 0; tag = Coalesced }, None);
                       ({ node = 3; tag = Move }, (Some (a, e)));
                       ({ node = 3; tag = Coalesced }, None);
                       ({ node = 4; tag = (Intf false) }, None);
                       ({ node = 7; tag = (Intf false) }, None)]
                     });
             None; None; None; None; None; None; None; None; None; None; None|];
           idx = 9; ext =  } |}]
