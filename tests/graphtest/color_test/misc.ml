open Backend
open Support  

module IG = Igraph     
module RM = Support.Temp.Map
module SM = Map.Make (String)


 


module Color = Color
module MMS = Color.MMS
module NMS = Color.NMS
module N = Color.N
module M = Color.M

type mms = NMS.t * MMS.t [@@deriving eq, show { with_path = false }]

type nodelist = IG.node list [@@deriving eq, show { with_path = false }]

(* [@@@warning "-32"] *)
(* open IG *) 

let draw_digraph =
  Support.make_draw_graph
    (Flow.DGraph.to_dot ~global:""
       ~show_node:(fun data -> (Flow.Node.show data, None))
       ~show_edge:(fun _ -> Some ""))
    "/home/alex/documents/ocaml/ed_compiler/tiger/tests/graphtest"

let draw_graph ~show_temp =
  Support.make_draw_graph (IG.to_dot ~show_temp)
    "/home/alex/documents/ocaml/ed_compiler/tiger/tests/graphtest"
 
  
  (* match status with
  | Unix.WEXITED s -> Printf.printf "process status: %d\n" s
  | Unix.WSIGNALED s ->
      Printf.printf "The process was killed by a signal: %d\n" s
  | Unix.WSTOPPED s ->
      Printf.printf "The process was stopped by a signal: %d\n" s *)

let regs_make rl =
  List.fold_left
    (fun m r -> let t = Temp.labeltemp r in

                RM.add t r m)
    RM.empty rl

let nodemap_make g regs =
  RM.fold
    (fun t r ->
      let node = IG.new_node g t in
      SM.add r node)
    regs SM.empty

let all_moves igraph =
  List.fold_left
    (fun m n -> List.fold_left (fun m move -> MMS.S.add move m) m (IG.moves n))
    MMS.S.empty (IG.nodes igraph)
  |> MMS.S.elements

let degree_invariant node nms =
  List.fold_left 
    (fun i k ->
      List.fold_left (fun i n -> if  NMS.mem nms n k then i + 1 else i) i
      @@ IG.adj node)
    0
    N.[ Precolored; SimplifyWL; FreezeWL; SpillWL ]
  = IG.degree node

let simplify_invariant node mms =
  List.fold_left
    (fun i k ->
      List.fold_left (fun i n -> if MMS.mem mms n k then i + 1 else i) i
      @@ IG.moves node)
    0
    M.[ Active; Worklist ]
  = 0
  && (not @@ IG.is_significant node)

let freeze_invariant node mms =
  List.fold_left
    (fun i k ->
      List.fold_left (fun i n -> if MMS.mem mms n k then i + 1 else i) i
      @@ IG.moves node)
    0
    M.[ Active; Worklist ]
  > 0
  && (not @@ IG.is_significant node)

let spill_invariant = IG.is_significant

let check_invariants nms mms =
  List.iter
    (fun k -> NMS.iter (fun node -> assert (degree_invariant node nms)) nms k)
    N.[ SimplifyWL; FreezeWL; SpillWL ];
  NMS.iter (fun n -> assert (simplify_invariant n mms)) nms N.SimplifyWL;
  NMS.iter (fun n -> assert (freeze_invariant n mms)) nms N.FreezeWL;
  NMS.iter (fun n -> assert (spill_invariant n)) nms N.SpillWL

