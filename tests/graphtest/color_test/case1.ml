
(* open Backend *)
(* open Support *)


open Misc

let regs = regs_make [ "r1"; "r2"; "r3"; "a"; "b"; "c"; "d"; "e" ]

let show_temp t = RM.find t regs

let colormap (t : Support.Temp.t) =
  match show_temp t with
  | "r1" -> Some 0
  | "r2" -> Some 1
  | "r3" -> Some 2
  | _ -> None
 
let make () = 
  let g = IG.init ~k:3 20 in
  let nodemap = nodemap_make g regs in

  let no s = SM.find s nodemap in

  (* let initial t =    *)
  let _ =
    let open IG in
    mk_edge (no "r1") (no "r2");
    mk_edge (no "r1") (no "r3");

    mk_edge (no "r2") (no "r3");
    mk_edge (no "r2") (no "a");
    mk_edge (no "r2") (no "c");

    mk_edge (no "a") (no "b");
    mk_edge (no "a") (no "d");
    mk_edge (no "a") (no "c");

    mk_edge (no "b") (no "c");
    mk_edge (no "b") (no "d");
    mk_edge (no "b") (no "e");

    mk_edge (no "c") (no "d");
    mk_edge (no "c") (no "e");

    mk_edge (no "d") (no "e");

    add_move (no "r1") (no "d");
    add_move (no "r1") (no "a");
    add_move (no "r3") (no "c");
    add_move (no "r2") (no "b");
    add_move (no "a") (no "e")
  in
  (g, no)

let g, no = make ()

(* draw_graph ~show_temp g "graph"; *)
(* combine (no "r3") (no "c") *)
(* remove_node  (no "c")  *)
let _ = draw_graph ~show_temp g "init0"

(* let expect_sets =
   let moves = all_moves g in
   assert (List.length moves = 5);

   ( NMS.build
       N.
         [
           (Precolored, [ no "r1"; no "r2"; no "r3" ]);
           (SpillWL, [ no "a"; no "b"; no "c"; no "d"; no "e" ]);
         ],
     MMS.build M.[ (Worklist, moves) ] ) *)

(* let expect_after_simplify *)

let snapshot = IG.adj_snapshot g

let sets = Color.build colormap g

let nms, mms = sets

let%expect_test _ =
  check_invariants nms mms;
  print_string @@ show_mms sets;
  [%expect
    {|
        ([|[{ temp = r1; degree = 2; color = 0; head = None };
             { temp = r2; degree = 4; color = 1; head = None };
             { temp = r3; degree = 2; color = 2; head = None }];
           []; []; [];
           [{ temp = a; degree = 4; color = -1; head = None };
             { temp = b; degree = 4; color = -1; head = None };
             { temp = c; degree = 5; color = -1; head = None };
             { temp = d; degree = 4; color = -1; head = None };
             { temp = e; degree = 3; color = -1; head = None }];
           []; []; []; []|],
         [|[]; []; [];
           [0 - 3. tag: Move; data: (Some (r1, a));
             0 - 6. tag: Move; data: (Some (r1, d));
             1 - 4. tag: Move; data: (Some (r2, b));
             2 - 5. tag: Move; data: (Some (r3, c));
             3 - 7. tag: Move; data: (Some (a, e))];
           []|]) |}]

let%expect_test _ =
  let n = Color.select_spill nms |> Option.get in
  print_string @@ IG.show_node n;
  [%expect {| { temp = c; degree = 5; color = -1; head = None } |}];

  Color.spill nms mms n;
  check_invariants nms mms;
  draw_graph ~show_temp g "after_spill";
  print_string @@ show_mms sets;
  [%expect
    {|
      ([|[{ temp = r1; degree = 2; color = 0; head = None };
           { temp = r2; degree = 3; color = 1; head = None };
           { temp = r3; degree = 2; color = 2; head = None }];
         []; []; [{ temp = e; degree = 2; color = -1; head = None }];
         [{ temp = a; degree = 3; color = -1; head = None };
           { temp = b; degree = 3; color = -1; head = None };
           { temp = d; degree = 3; color = -1; head = None }];
         []; []; []; [{ temp = c; degree = 0; color = -1; head = None }]|],
       [|[]; []; [5 - 2. tag: Move; data: (Some (r3, c))];
         [0 - 3. tag: Move; data: (Some (r1, a));
           0 - 6. tag: Move; data: (Some (r1, d));
           1 - 4. tag: Move; data: (Some (r2, b));
           3 - 7. tag: Move; data: (Some (a, e))];
         []|]) |}];

  print_string @@ IG.show g;
  [%expect
    {|
      { arr =
        [|(Some { l = { temp = r1; degree = 2; color = 0; head = None };
                  adj =
                  [({ node = 1; tag = (Intf false) }, None);
                    ({ node = 2; tag = (Intf false) }, None);
                    ({ node = 3; tag = Move }, (Some (r1, a)));
                    ({ node = 6; tag = Move }, (Some (r1, d)))]
                  });
          (Some { l = { temp = r2; degree = 3; color = 1; head = None };
                  adj =
                  [({ node = 0; tag = (Intf false) }, None);
                    ({ node = 2; tag = (Intf false) }, None);
                    ({ node = 3; tag = (Intf false) }, None);
                    ({ node = 4; tag = Move }, (Some (r2, b)))]
                  });
          (Some { l = { temp = r3; degree = 2; color = 2; head = None };
                  adj =
                  [({ node = 0; tag = (Intf false) }, None);
                    ({ node = 1; tag = (Intf false) }, None);
                    ({ node = 5; tag = Move }, (Some (r3, c)))]
                  });
          (Some { l = { temp = a; degree = 3; color = -1; head = None };
                  adj =
                  [({ node = 0; tag = Move }, (Some (r1, a)));
                    ({ node = 1; tag = (Intf false) }, None);
                    ({ node = 4; tag = (Intf false) }, None);
                    ({ node = 6; tag = (Intf false) }, None);
                    ({ node = 7; tag = Move }, (Some (a, e)))]
                  });
          (Some { l = { temp = b; degree = 3; color = -1; head = None };
                  adj =
                  [({ node = 1; tag = Move }, (Some (r2, b)));
                    ({ node = 3; tag = (Intf false) }, None);
                    ({ node = 6; tag = (Intf false) }, None);
                    ({ node = 7; tag = (Intf false) }, None)]
                  });
          (Some { l = { temp = c; degree = 0; color = -1; head = None };
                  adj = [({ node = 2; tag = Move }, (Some (r3, c)))] });
          (Some { l = { temp = d; degree = 3; color = -1; head = None };
                  adj =
                  [({ node = 0; tag = Move }, (Some (r1, d)));
                    ({ node = 3; tag = (Intf false) }, None);
                    ({ node = 4; tag = (Intf false) }, None);
                    ({ node = 7; tag = (Intf false) }, None)]
                  });
          (Some { l = { temp = e; degree = 2; color = -1; head = None };
                  adj =
                  [({ node = 3; tag = Move }, (Some (a, e)));
                    ({ node = 4; tag = (Intf false) }, None);
                    ({ node = 6; tag = (Intf false) }, None)]
                  });
          None; None; None; None; None; None; None; None; None; None; None; None|];
        idx = 8; ext =  } |}]

let%expect_test _ =
  let move = IG.move (no "a") (no "e") |> Option.get in

  Color.coalesce nms mms move;
  check_invariants nms mms;
  draw_graph ~show_temp g "coalesce";
  print_string @@ show_mms sets;
  [%expect
    {|
      ([|[{ temp = r1; degree = 2; color = 0; head = None };
           { temp = r2; degree = 3; color = 1; head = None };
           { temp = r3; degree = 2; color = 2; head = None }];
         []; [];
         [{ temp = b; degree = 2; color = -1; head = None };
           { temp = d; degree = 2; color = -1; head = None }];
         [{ temp = e; degree = 3; color = -1; head = (Some true) }]; [];
         [{ temp = a; degree = 0; color = -1; head = (Some false) }]; [];
         [{ temp = c; degree = 0; color = -1; head = None }]|],
       [|[3 - 7. tag: Move; data: (Some (a, e))]; [];
         [5 - 2. tag: Move; data: (Some (r3, c))];
         [0 - 6. tag: Move; data: (Some (r1, d));
           7 - 0. tag: Move; data: (Some (e, r1));
           1 - 4. tag: Move; data: (Some (r2, b))];
         []|]) |}];
  print_string @@ IG.show g;
  [%expect
    {|
  { arr =
    [|(Some { l = { temp = r1; degree = 2; color = 0; head = None };
              adj =
              [({ node = 1; tag = (Intf false) }, None);
                ({ node = 2; tag = (Intf false) }, None);
                ({ node = 6; tag = Move }, (Some (r1, d)));
                ({ node = 7; tag = Move }, (Some (e, r1)))]
              });
      (Some { l = { temp = r2; degree = 3; color = 1; head = None };
              adj =
              [({ node = 0; tag = (Intf false) }, None);
                ({ node = 2; tag = (Intf false) }, None);
                ({ node = 4; tag = Move }, (Some (r2, b)));
                ({ node = 7; tag = (Intf false) }, None)]
              });
      (Some { l = { temp = r3; degree = 2; color = 2; head = None };
              adj =
              [({ node = 0; tag = (Intf false) }, None);
                ({ node = 1; tag = (Intf false) }, None);
                ({ node = 5; tag = Move }, (Some (r3, c)))]
              });
      (Some { l = { temp = a; degree = 0; color = -1; head = (Some false) };
              adj =
              [({ node = 7; tag = Move }, (Some (a, e)));
                ({ node = 7; tag = Coalesced }, None)]
              });
      (Some { l = { temp = b; degree = 2; color = -1; head = None };
              adj =
              [({ node = 1; tag = Move }, (Some (r2, b)));
                ({ node = 6; tag = (Intf false) }, None);
                ({ node = 7; tag = (Intf false) }, None)]
              });
      (Some { l = { temp = c; degree = 0; color = -1; head = None };
              adj = [({ node = 2; tag = Move }, (Some (r3, c)))] });
      (Some { l = { temp = d; degree = 2; color = -1; head = None };
              adj =
              [({ node = 0; tag = Move }, (Some (r1, d)));
                ({ node = 4; tag = (Intf false) }, None);
                ({ node = 7; tag = (Intf false) }, None)]
              });
      (Some { l = { temp = e; degree = 3; color = -1; head = (Some true) };
              adj =
              [({ node = 0; tag = Move }, (Some (e, r1)));
                ({ node = 1; tag = (Intf false) }, None);
                ({ node = 3; tag = Move }, (Some (a, e)));
                ({ node = 3; tag = Coalesced }, None);
                ({ node = 4; tag = (Intf false) }, None);
                ({ node = 6; tag = (Intf false) }, None)]
              });
      None; None; None; None; None; None; None; None; None; None; None; None|];
    idx = 8; ext =  } |}]

 
let%expect_test _ =
  let move = IG.move (no "r2") (no "b") |> Option.get in

  Color.coalesce nms mms move;
  check_invariants nms mms;
  draw_graph ~show_temp g "coalesce1";
  print_string @@ show_mms sets;
  [%expect
    {|
      ([|[{ temp = r1; degree = 2; color = 0; head = None };
           { temp = r2; degree = 4; color = 1; head = (Some true) };
           { temp = r3; degree = 2; color = 2; head = None }];
         []; [];
         [{ temp = d; degree = 2; color = -1; head = None };
           { temp = e; degree = 2; color = -1; head = (Some true) }];
         []; [];
         [{ temp = a; degree = 0; color = -1; head = (Some false) };
           { temp = b; degree = 0; color = -1; head = (Some false) }];
         []; [{ temp = c; degree = 0; color = -1; head = None }]|],
       [|[1 - 4. tag: Move; data: (Some (r2, b));
           3 - 7. tag: Move; data: (Some (a, e))];
         []; [5 - 2. tag: Move; data: (Some (r3, c))];
         [0 - 6. tag: Move; data: (Some (r1, d));
           7 - 0. tag: Move; data: (Some (e, r1))];
         []|]) |}];
  print_string @@ IG.show g;
  [%expect
    {|
  { arr =
    [|(Some { l = { temp = r1; degree = 2; color = 0; head = None };
              adj =
              [({ node = 1; tag = (Intf false) }, None);
                ({ node = 2; tag = (Intf false) }, None);
                ({ node = 6; tag = Move }, (Some (r1, d)));
                ({ node = 7; tag = Move }, (Some (e, r1)))]
              });
      (Some { l = { temp = r2; degree = 4; color = 1; head = (Some true) };
              adj =
              [({ node = 0; tag = (Intf false) }, None);
                ({ node = 2; tag = (Intf false) }, None);
                ({ node = 4; tag = Move }, (Some (r2, b)));
                ({ node = 4; tag = Coalesced }, None);
                ({ node = 6; tag = (Intf false) }, None);
                ({ node = 7; tag = (Intf false) }, None)]
              });
      (Some { l = { temp = r3; degree = 2; color = 2; head = None };
              adj =
              [({ node = 0; tag = (Intf false) }, None);
                ({ node = 1; tag = (Intf false) }, None);
                ({ node = 5; tag = Move }, (Some (r3, c)))]
              });
      (Some { l = { temp = a; degree = 0; color = -1; head = (Some false) };
              adj =
              [({ node = 7; tag = Move }, (Some (a, e)));
                ({ node = 7; tag = Coalesced }, None)]
              });
      (Some { l = { temp = b; degree = 0; color = -1; head = (Some false) };
              adj =
              [({ node = 1; tag = Move }, (Some (r2, b)));
                ({ node = 1; tag = Coalesced }, None)]
              });
      (Some { l = { temp = c; degree = 0; color = -1; head = None };
              adj = [({ node = 2; tag = Move }, (Some (r3, c)))] });
      (Some { l = { temp = d; degree = 2; color = -1; head = None };
              adj =
              [({ node = 0; tag = Move }, (Some (r1, d)));
                ({ node = 1; tag = (Intf false) }, None);
                ({ node = 7; tag = (Intf false) }, None)]
              });
      (Some { l = { temp = e; degree = 2; color = -1; head = (Some true) };
              adj =
              [({ node = 0; tag = Move }, (Some (e, r1)));
                ({ node = 1; tag = (Intf false) }, None);
                ({ node = 3; tag = Move }, (Some (a, e)));
                ({ node = 3; tag = Coalesced }, None);
                ({ node = 6; tag = (Intf false) }, None)]
              });
      None; None; None; None; None; None; None; None; None; None; None; None|];
    idx = 8; ext =  } |}]
  
let%expect_test _ =
  let move = IG.move (no "r1") (no "e") |> Option.get in

  Color.coalesce nms mms move;
  check_invariants nms mms;
  draw_graph ~show_temp g "coalesce2";
  print_string @@ show_mms sets;
  [%expect
    {|
      ([|[{ temp = r1; degree = 3; color = 0; head = (Some true) };
           { temp = r2; degree = 3; color = 1; head = (Some true) };
           { temp = r3; degree = 2; color = 2; head = None }];
         []; []; [{ temp = d; degree = 2; color = -1; head = None }]; []; [];
         [{ temp = a; degree = 0; color = -1; head = (Some false) };
           { temp = b; degree = 0; color = -1; head = (Some false) };
           { temp = e; degree = 0; color = -1; head = (Some false) }];
         []; [{ temp = c; degree = 0; color = -1; head = None }]|],
       [|[0 - 7. tag: Move; data: (Some (e, r1));
           1 - 4. tag: Move; data: (Some (r2, b)); error edge];
         []; [5 - 2. tag: Move; data: (Some (r3, c))];
         [0 - 6. tag: Move; data: (Some (r1, d))]; []|]) |}];
  print_string @@ IG.show g;
  [%expect
    {|
  { arr =
    [|(Some { l = { temp = r1; degree = 3; color = 0; head = (Some true) };
              adj =
              [({ node = 1; tag = (Intf false) }, None);
                ({ node = 2; tag = (Intf false) }, None);
                ({ node = 3; tag = Move }, (Some (r1, a)));
                ({ node = 3; tag = Coalesced }, None);
                ({ node = 6; tag = (Intf false) }, None);
                ({ node = 6; tag = Move }, (Some (r1, d)));
                ({ node = 7; tag = Move }, (Some (e, r1)));
                ({ node = 7; tag = Coalesced }, None)]
              });
      (Some { l = { temp = r2; degree = 3; color = 1; head = (Some true) };
              adj =
              [({ node = 0; tag = (Intf false) }, None);
                ({ node = 2; tag = (Intf false) }, None);
                ({ node = 4; tag = Move }, (Some (r2, b)));
                ({ node = 4; tag = Coalesced }, None);
                ({ node = 6; tag = (Intf false) }, None)]
              });
      (Some { l = { temp = r3; degree = 2; color = 2; head = None };
              adj =
              [({ node = 0; tag = (Intf false) }, None);
                ({ node = 1; tag = (Intf false) }, None);
                ({ node = 5; tag = Move }, (Some (r3, c)))]
              });
      (Some { l = { temp = a; degree = 0; color = -1; head = (Some false) };
              adj =
              [({ node = 0; tag = Move }, (Some (r1, a)));
                ({ node = 0; tag = Coalesced }, None);
                ({ node = 7; tag = Coalesced }, None)]
              });
      (Some { l = { temp = b; degree = 0; color = -1; head = (Some false) };
              adj =
              [({ node = 1; tag = Move }, (Some (r2, b)));
                ({ node = 1; tag = Coalesced }, None)]
              });
      (Some { l = { temp = c; degree = 0; color = -1; head = None };
              adj = [({ node = 2; tag = Move }, (Some (r3, c)))] });
      (Some { l = { temp = d; degree = 2; color = -1; head = None };
              adj =
              [({ node = 0; tag = (Intf false) }, None);
                ({ node = 0; tag = Move }, (Some (r1, d)));
                ({ node = 1; tag = (Intf false) }, None)]
              });
      (Some { l = { temp = e; degree = 0; color = -1; head = (Some false) };
              adj =
              [({ node = 0; tag = Move }, (Some (e, r1)));
                ({ node = 0; tag = Coalesced }, None);
                ({ node = 3; tag = Coalesced }, None)]
              });
      None; None; None; None; None; None; None; None; None; None; None; None|];
    idx = 8; ext =  } |}]
    
  
let%expect_test _ =
  let move = IG.move (no "r1") (no "d") |> Option.get in
  Color.coalesce nms mms move;
  check_invariants nms mms;
  draw_graph ~show_temp g "coalesce3";
  print_string @@ show_mms sets;
  [%expect
    {|
      ([|[{ temp = r1; degree = 3; color = 0; head = (Some true) };
           { temp = r2; degree = 3; color = 1; head = (Some true) };
           { temp = r3; degree = 2; color = 2; head = None }];
         []; [{ temp = d; degree = 2; color = -1; head = None }]; []; []; [];
         [{ temp = a; degree = 0; color = -1; head = (Some false) };
           { temp = b; degree = 0; color = -1; head = (Some false) };
           { temp = e; degree = 0; color = -1; head = (Some false) }];
         []; [{ temp = c; degree = 0; color = -1; head = None }]|],
       [|[0 - 7. tag: Move; data: (Some (e, r1));
           1 - 4. tag: Move; data: (Some (r2, b)); error edge];
         [0 - 6. tag: Move; data: (Some (r1, d))];
         [5 - 2. tag: Move; data: (Some (r3, c))]; []; []|]) |}];
  print_string @@ IG.show g;
  [%expect
    {|
  { arr =
    [|(Some { l = { temp = r1; degree = 3; color = 0; head = (Some true) };
              adj =
              [({ node = 1; tag = (Intf false) }, None);
                ({ node = 2; tag = (Intf false) }, None);
                ({ node = 3; tag = Move }, (Some (r1, a)));
                ({ node = 3; tag = Coalesced }, None);
                ({ node = 6; tag = (Intf false) }, None);
                ({ node = 6; tag = Move }, (Some (r1, d)));
                ({ node = 7; tag = Move }, (Some (e, r1)));
                ({ node = 7; tag = Coalesced }, None)]
              });
      (Some { l = { temp = r2; degree = 3; color = 1; head = (Some true) };
              adj =
              [({ node = 0; tag = (Intf false) }, None);
                ({ node = 2; tag = (Intf false) }, None);
                ({ node = 4; tag = Move }, (Some (r2, b)));
                ({ node = 4; tag = Coalesced }, None);
                ({ node = 6; tag = (Intf false) }, None)]
              });
      (Some { l = { temp = r3; degree = 2; color = 2; head = None };
              adj =
              [({ node = 0; tag = (Intf false) }, None);
                ({ node = 1; tag = (Intf false) }, None);
                ({ node = 5; tag = Move }, (Some (r3, c)))]
              });
      (Some { l = { temp = a; degree = 0; color = -1; head = (Some false) };
              adj =
              [({ node = 0; tag = Move }, (Some (r1, a)));
                ({ node = 0; tag = Coalesced }, None);
                ({ node = 7; tag = Coalesced }, None)]
              });
      (Some { l = { temp = b; degree = 0; color = -1; head = (Some false) };
              adj =
              [({ node = 1; tag = Move }, (Some (r2, b)));
                ({ node = 1; tag = Coalesced }, None)]
              });
      (Some { l = { temp = c; degree = 0; color = -1; head = None };
              adj = [({ node = 2; tag = Move }, (Some (r3, c)))] });
      (Some { l = { temp = d; degree = 2; color = -1; head = None };
              adj =
              [({ node = 0; tag = (Intf false) }, None);
                ({ node = 0; tag = Move }, (Some (r1, d)));
                ({ node = 1; tag = (Intf false) }, None)]
              });
      (Some { l = { temp = e; degree = 0; color = -1; head = (Some false) };
              adj =
              [({ node = 0; tag = Move }, (Some (e, r1)));
                ({ node = 0; tag = Coalesced }, None);
                ({ node = 3; tag = Coalesced }, None)]
              });
      None; None; None; None; None; None; None; None; None; None; None; None|];
    idx = 8; ext =  } |}]

let%expect_test _ =
  let _ = Color.simplify (no "d") nms mms in
  check_invariants nms mms;
  draw_graph ~show_temp g "simplify0";
  print_string @@ show_mms sets;
  [%expect
    {|
      ([|[{ temp = r1; degree = 2; color = 0; head = (Some true) };
           { temp = r2; degree = 2; color = 1; head = (Some true) };
           { temp = r3; degree = 2; color = 2; head = None }];
         []; []; []; []; [];
         [{ temp = a; degree = 0; color = -1; head = (Some false) };
           { temp = b; degree = 0; color = -1; head = (Some false) };
           { temp = e; degree = 0; color = -1; head = (Some false) }];
         [];
         [{ temp = c; degree = 0; color = -1; head = None };
           { temp = d; degree = 0; color = -1; head = None }]
         |],
       [|[0 - 7. tag: Move; data: (Some (e, r1));
           1 - 4. tag: Move; data: (Some (r2, b)); error edge];
         [0 - 6. tag: Move; data: (Some (r1, d))];
         [5 - 2. tag: Move; data: (Some (r3, c))]; []; []|]) |}];
  print_string @@ IG.show g;
  [%expect
    {|
  { arr =
    [|(Some { l = { temp = r1; degree = 2; color = 0; head = (Some true) };
              adj =
              [({ node = 1; tag = (Intf false) }, None);
                ({ node = 2; tag = (Intf false) }, None);
                ({ node = 3; tag = Move }, (Some (r1, a)));
                ({ node = 3; tag = Coalesced }, None);
                ({ node = 6; tag = Move }, (Some (r1, d)));
                ({ node = 7; tag = Move }, (Some (e, r1)));
                ({ node = 7; tag = Coalesced }, None)]
              });
      (Some { l = { temp = r2; degree = 2; color = 1; head = (Some true) };
              adj =
              [({ node = 0; tag = (Intf false) }, None);
                ({ node = 2; tag = (Intf false) }, None);
                ({ node = 4; tag = Move }, (Some (r2, b)));
                ({ node = 4; tag = Coalesced }, None)]
              });
      (Some { l = { temp = r3; degree = 2; color = 2; head = None };
              adj =
              [({ node = 0; tag = (Intf false) }, None);
                ({ node = 1; tag = (Intf false) }, None);
                ({ node = 5; tag = Move }, (Some (r3, c)))]
              });
      (Some { l = { temp = a; degree = 0; color = -1; head = (Some false) };
              adj =
              [({ node = 0; tag = Move }, (Some (r1, a)));
                ({ node = 0; tag = Coalesced }, None);
                ({ node = 7; tag = Coalesced }, None)]
              });
      (Some { l = { temp = b; degree = 0; color = -1; head = (Some false) };
              adj =
              [({ node = 1; tag = Move }, (Some (r2, b)));
                ({ node = 1; tag = Coalesced }, None)]
              });
      (Some { l = { temp = c; degree = 0; color = -1; head = None };
              adj = [({ node = 2; tag = Move }, (Some (r3, c)))] });
      (Some { l = { temp = d; degree = 0; color = -1; head = None };
              adj = [({ node = 0; tag = Move }, (Some (r1, d)))] });
      (Some { l = { temp = e; degree = 0; color = -1; head = (Some false) };
              adj =
              [({ node = 0; tag = Move }, (Some (e, r1)));
                ({ node = 0; tag = Coalesced }, None);
                ({ node = 3; tag = Coalesced }, None)]
              });
      None; None; None; None; None; None; None; None; None; None; None; None|];
    idx = 8; ext =  } |}]
(* 
let%expect_test _ =
  let _ = Color.simplify (no "d") nms mms in
  check_invariants nms mms;
  draw_graph ~show_temp g "simplify0";
  print_string @@ show_mms sets;
  [%expect
    {|
      ([|[{ temp = r1; degree = 2; color = 0; head = (Some true) };
           { temp = r2; degree = 2; color = 1; head = (Some true) };
           { temp = r3; degree = 2; color = 2; head = None }];
         []; []; []; []; [];
         [{ temp = a; degree = 0; color = -1; head = (Some false) };
           { temp = b; degree = 0; color = -1; head = (Some false) };
           { temp = e; degree = 0; color = -1; head = (Some false) }];
         [];
         [{ temp = c; degree = 0; color = -1; head = None };
           { temp = d; degree = 0; color = -1; head = None }]
         |],
       [|[0 - 3. tag: Move; data: (Some (r1, a));
           1 - 4. tag: Move; data: (Some (r2, b));
           3 - 7. tag: Move; data: (Some (a, e))];
         [0 - 6. tag: Move; data: (Some (r1, d))];
         [5 - 2. tag: Move; data: (Some (r3, c))]; []; []|]) |}];
  print_string @@ IG.show g;
  [%expect
    {|
  { arr =
    [|(Some { l = { temp = r1; degree = 2; color = 0; head = (Some true) };
              adj =
              [({ node = 1; tag = Intf }, None);
                ({ node = 2; tag = Intf }, None);
                ({ node = 3; tag = Move }, (Some (r1, a)));
                ({ node = 3; tag = Coalesced }, None);
                ({ node = 6; tag = Move }, (Some (r1, d)));
                ({ node = 7; tag = Coalesced }, None)]
              });
      (Some { l = { temp = r2; degree = 2; color = 1; head = (Some true) };
              adj =
              [({ node = 0; tag = Intf }, None);
                ({ node = 2; tag = Intf }, None);
                ({ node = 4; tag = Move }, (Some (r2, b)));
                ({ node = 4; tag = Coalesced }, None)]
              });
      (Some { l = { temp = r3; degree = 2; color = 2; head = None };
              adj =
              [({ node = 0; tag = Intf }, None);
                ({ node = 1; tag = Intf }, None);
                ({ node = 5; tag = Move }, (Some (r3, c)))]
              });
      (Some { l = { temp = a; degree = 0; color = -1; head = (Some false) };
              adj =
              [({ node = 0; tag = Move }, (Some (r1, a)));
                ({ node = 0; tag = Coalesced }, None);
                ({ node = 7; tag = Move }, (Some (a, e)));
                ({ node = 7; tag = Coalesced }, None)]
              });
      (Some { l = { temp = b; degree = 0; color = -1; head = (Some false) };
              adj =
              [({ node = 1; tag = Move }, (Some (r2, b)));
                ({ node = 1; tag = Coalesced }, None)]
              });
      (Some { l = { temp = c; degree = 0; color = -1; head = None };
              adj = [({ node = 2; tag = Move }, (Some (r3, c)))] });
      (Some { l = { temp = d; degree = 0; color = -1; head = None };
              adj = [({ node = 0; tag = Move }, (Some (r1, d)))] });
      (Some { l = { temp = e; degree = 0; color = -1; head = (Some false) };
              adj =
              [({ node = 0; tag = Coalesced }, None);
                ({ node = 3; tag = Move }, (Some (a, e)));
                ({ node = 3; tag = Coalesced }, None)]
              });
      None; None; None; None; None; None; None; None; None; None; None; None|];
    idx = 8; ext =  } |}] *)

let%expect_test _ =
  IG.restore_adj g snapshot;
  Color.assighColors 3 [ no "d"; no "c" ] nms mms;
  (* check_invariants nms mms; *)
  draw_graph ~show_temp g "assighColors";
  print_string @@ show_mms sets;
  [%expect
    {|
        ([|[{ temp = r1; degree = 2; color = 0; head = (Some true) };
             { temp = r2; degree = 2; color = 1; head = (Some true) };
             { temp = r3; degree = 2; color = 2; head = None }];
           []; []; []; []; [{ temp = c; degree = 0; color = -1; head = None }];
           [{ temp = a; degree = 0; color = 0; head = (Some false) };
             { temp = b; degree = 0; color = 1; head = (Some false) };
             { temp = e; degree = 0; color = 0; head = (Some false) }];
           [{ temp = d; degree = 0; color = 2; head = None }]; []|],
         [|[0 - 7. tag: Move; data: (Some (e, r1));
             1 - 4. tag: Move; data: (Some (r2, b));
             3 - 7. tag: Move; data: (Some (a, e))];
           [0 - 6. tag: Move; data: (Some (r1, d))];
           [5 - 2. tag: Move; data: (Some (r3, c))]; []; []|]) |}];

  print_string @@ IG.show g;
  [%expect
    {|
           { arr =
             [|(Some { l = { temp = r1; degree = 2; color = 0; head = (Some true) };
                       adj =
                       [({ node = 1; tag = (Intf false) }, None);
                         ({ node = 2; tag = (Intf false) }, None);
                         ({ node = 3; tag = Move }, (Some (r1, a)));
                         ({ node = 3; tag = Coalesced }, None);
                         ({ node = 6; tag = Move }, (Some (r1, d)));
                         ({ node = 7; tag = Move }, (Some (e, r1)));
                         ({ node = 7; tag = Coalesced }, None)]
                       });
               (Some { l = { temp = r2; degree = 2; color = 1; head = (Some true) };
                       adj =
                       [({ node = 0; tag = (Intf false) }, None);
                         ({ node = 2; tag = (Intf false) }, None);
                         ({ node = 3; tag = (Intf false) }, None);
                         ({ node = 4; tag = Move }, (Some (r2, b)));
                         ({ node = 4; tag = Coalesced }, None);
                         ({ node = 5; tag = (Intf false) }, None)]
                       });
               (Some { l = { temp = r3; degree = 2; color = 2; head = None };
                       adj =
                       [({ node = 0; tag = (Intf false) }, None);
                         ({ node = 1; tag = (Intf false) }, None);
                         ({ node = 5; tag = Move }, (Some (r3, c)))]
                       });
               (Some { l = { temp = a; degree = 0; color = 0; head = (Some false) };
                       adj =
                       [({ node = 0; tag = Move }, (Some (r1, a)));
                         ({ node = 0; tag = Coalesced }, None);
                         ({ node = 1; tag = (Intf false) }, None);
                         ({ node = 4; tag = (Intf false) }, None);
                         ({ node = 5; tag = (Intf false) }, None);
                         ({ node = 6; tag = (Intf false) }, None);
                         ({ node = 7; tag = Move }, (Some (a, e)));
                         ({ node = 7; tag = Coalesced }, None)]
                       });
               (Some { l = { temp = b; degree = 0; color = 1; head = (Some false) };
                       adj =
                       [({ node = 1; tag = Move }, (Some (r2, b)));
                         ({ node = 1; tag = Coalesced }, None);
                         ({ node = 3; tag = (Intf false) }, None);
                         ({ node = 5; tag = (Intf false) }, None);
                         ({ node = 6; tag = (Intf false) }, None);
                         ({ node = 7; tag = (Intf false) }, None)]
                       });
               (Some { l = { temp = c; degree = 0; color = -1; head = None };
                       adj =
                       [({ node = 1; tag = (Intf false) }, None);
                         ({ node = 2; tag = Move }, (Some (r3, c)));
                         ({ node = 3; tag = (Intf false) }, None);
                         ({ node = 4; tag = (Intf false) }, None);
                         ({ node = 6; tag = (Intf false) }, None);
                         ({ node = 7; tag = (Intf false) }, None)]
                       });
               (Some { l = { temp = d; degree = 0; color = 2; head = None };
                       adj =
                       [({ node = 0; tag = Move }, (Some (r1, d)));
                         ({ node = 3; tag = (Intf false) }, None);
                         ({ node = 4; tag = (Intf false) }, None);
                         ({ node = 5; tag = (Intf false) }, None);
                         ({ node = 7; tag = (Intf false) }, None)]
                       });
               (Some { l = { temp = e; degree = 0; color = 0; head = (Some false) };
                       adj =
                       [({ node = 0; tag = Move }, (Some (e, r1)));
                         ({ node = 0; tag = Coalesced }, None);
                         ({ node = 3; tag = Move }, (Some (a, e)));
                         ({ node = 3; tag = Coalesced }, None);
                         ({ node = 4; tag = (Intf false) }, None);
                         ({ node = 5; tag = (Intf false) }, None);
                         ({ node = 6; tag = (Intf false) }, None)]
                       });
               None; None; None; None; None; None; None; None; None; None; None; None|];
             idx = 8; ext =  } |}]

let%expect_test _ =
  let g, _ = make () in
  let spilled = Color.color 3 colormap g in

  (* check_invariants nms mms; *)
  print_string @@ show_nodelist spilled;

  [%expect {|
    [{ temp = c; degree = 0; color = -1; head = None }] |}];

  print_string @@ IG.show g;
  [%expect
    {|
         { arr = 
           [|(Some { l = { temp = r1; degree = 2; color = 0; head = (Some true) };
                     adj =
                     [({ node = 1; tag = (Intf false) }, None);
                       ({ node = 2; tag = (Intf false) }, None);
                       ({ node = 3; tag = Move }, (Some (r1, a)));
                       ({ node = 3; tag = Coalesced }, None);
                       ({ node = 6; tag = Move }, (Some (r1, d)));
                       ({ node = 7; tag = Move }, (Some (r1, e)));
                       ({ node = 7; tag = Coalesced }, None)]
                     });
             (Some { l = { temp = r2; degree = 2; color = 1; head = (Some true) };
                     adj =
                     [({ node = 0; tag = (Intf false) }, None);
                       ({ node = 2; tag = (Intf false) }, None);
                       ({ node = 3; tag = (Intf false) }, None);
                       ({ node = 4; tag = Move }, (Some (r2, b)));
                       ({ node = 4; tag = Coalesced }, None);
                       ({ node = 5; tag = (Intf false) }, None)]
                     });
             (Some { l = { temp = r3; degree = 2; color = 2; head = None };
                     adj =
                     [({ node = 0; tag = (Intf false) }, None);
                       ({ node = 1; tag = (Intf false) }, None);
                       ({ node = 5; tag = Move }, (Some (r3, c)))]
                     });
             (Some { l = { temp = a; degree = 0; color = 0; head = (Some false) };
                     adj =
                     [({ node = 0; tag = Move }, (Some (r1, a)));
                       ({ node = 0; tag = Coalesced }, None);
                       ({ node = 1; tag = (Intf false) }, None);
                       ({ node = 4; tag = (Intf false) }, None);
                       ({ node = 5; tag = (Intf false) }, None);
                       ({ node = 6; tag = (Intf false) }, None);
                       ({ node = 7; tag = Move }, (Some (a, e)));
                       ({ node = 7; tag = Coalesced }, None)]
                     });
             (Some { l = { temp = b; degree = 0; color = 1; head = (Some false) };
                     adj =
                     [({ node = 1; tag = Move }, (Some (r2, b)));
                       ({ node = 1; tag = Coalesced }, None);
                       ({ node = 3; tag = (Intf false) }, None);
                       ({ node = 5; tag = (Intf false) }, None);
                       ({ node = 6; tag = (Intf false) }, None);
                       ({ node = 7; tag = (Intf false) }, None)]
                     });
             (Some { l = { temp = c; degree = 0; color = -1; head = None };
                     adj =
                     [({ node = 1; tag = (Intf false) }, None);
                       ({ node = 2; tag = Move }, (Some (r3, c)));
                       ({ node = 3; tag = (Intf false) }, None);
                       ({ node = 4; tag = (Intf false) }, None);
                       ({ node = 6; tag = (Intf false) }, None);
                       ({ node = 7; tag = (Intf false) }, None)]
                     });
             (Some { l = { temp = d; degree = 0; color = 2; head = None };
                     adj =
                     [({ node = 0; tag = Move }, (Some (r1, d)));
                       ({ node = 3; tag = (Intf false) }, None);
                       ({ node = 4; tag = (Intf false) }, None);
                       ({ node = 5; tag = (Intf false) }, None);
                       ({ node = 7; tag = (Intf false) }, None)]
                     });
             (Some { l = { temp = e; degree = 0; color = 0; head = (Some false) };
                     adj =
                     [({ node = 0; tag = Move }, (Some (r1, e)));
                       ({ node = 0; tag = Coalesced }, None);
                       ({ node = 3; tag = Move }, (Some (a, e)));
                       ({ node = 3; tag = Coalesced }, None);
                       ({ node = 4; tag = (Intf false) }, None);
                       ({ node = 5; tag = (Intf false) }, None);
                       ({ node = 6; tag = (Intf false) }, None)]
                     });
             None; None; None; None; None; None; None; None; None; None; None; None|];
           idx = 8; ext =  } |}] 
