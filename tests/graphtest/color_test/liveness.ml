[@@@warning "-27-26-33"]

open Misc
open Support
open Backend
open Backend.Assem
open Backend.Liveness
module DLL = Support.DoubleLinkedList
 


let regs_make rl =
  List.fold_left
    (fun m r -> let t = Temp.labeltemp r in 

                SM.add r t m)
    SM.empty rl
 
let make_temp m = SM.fold (fun r t m' -> RM.add t r m') m RM.empty
 
let make_show_temp m =
  let m' = make_temp m in
  fun t -> RM.find t m' 
 
let%expect_test _ =
  let regs = regs_make [ "r1"; "r2"; "r3"; "a"; "b"; "c"; "d"; "e" ] in
  let te s = SM.find s regs in
  let show_temp = make_show_temp regs in
  let lab1 = Temp.newlabel () in
  let lab2 = Temp.newlabel () in
  let instr =
    DLL.of_list
      [
        test_move (te "c") (te "r3");
        test_move (te "a") (te "r1");
        test_move (te "b") (te "r2");
        test_oper [ te "d" ] [];
        test_move (te "e") (te "a");
        test_label lab1;
        test_oper [ te "d" ] [ te "d"; te "b" ];
        test_oper [ te "e" ] [ te "e" ];
        test_oper ~jump:[ lab1; lab2 ] [] [ te "e" ];
        test_label lab2;
        test_move (te "r1") (te "d");
        test_move (te "r3") (te "c");
      ]
  in 
  let out = [ te "r1"; te "r3" ] in
  let ((flowgraph, _) as flow) = Flow.instr2graph ~size:16 out instr in
  print_string @@ Flow.DGraph.show flowgraph;
  [%expect
    {|
    { arr =
      [|(Some { l = { def = [c]; use = [r3]; ismov = true; in_ = []; out = [] };
                pred = []; succ = [({ node = 1; tag = () }, ())] });
        (Some { l = { def = [a]; use = [r1]; ismov = true; in_ = []; out = [] };
                pred = [({ node = 0; tag = () }, ())];
                succ = [({ node = 2; tag = () }, ())] });
        (Some { l = { def = [b]; use = [r2]; ismov = true; in_ = []; out = [] };
                pred = [({ node = 1; tag = () }, ())];
                succ = [({ node = 3; tag = () }, ())] });
        (Some { l = { def = [d]; use = []; ismov = false; in_ = []; out = [] };
                pred = [({ node = 2; tag = () }, ())];
                succ = [({ node = 4; tag = () }, ())] });
        (Some { l = { def = [e]; use = [a]; ismov = true; in_ = []; out = [] };
                pred = [({ node = 3; tag = () }, ())];
                succ = [({ node = 5; tag = () }, ())] });
        (Some { l = { def = []; use = []; ismov = false; in_ = []; out = [] };
                pred =
                [({ node = 4; tag = () }, ()); ({ node = 8; tag = () }, ())];
                succ = [({ node = 6; tag = () }, ())] });
        (Some { l =
                { def = [d]; use = [b; d]; ismov = false; in_ = []; out = [] };
                pred = [({ node = 5; tag = () }, ())];
                succ = [({ node = 7; tag = () }, ())] });
        (Some { l = { def = [e]; use = [e]; ismov = false; in_ = []; out = [] }; 
                pred = [({ node = 6; tag = () }, ())];
                succ = [({ node = 8; tag = () }, ())] });
        (Some { l = { def = []; use = [e]; ismov = false; in_ = []; out = [] };
                pred = [({ node = 7; tag = () }, ())];
                succ =
                [({ node = 5; tag = () }, ()); ({ node = 9; tag = () }, ())] });
        (Some { l = { def = []; use = []; ismov = false; in_ = []; out = [] };
                pred = [({ node = 8; tag = () }, ())];
                succ = [({ node = 10; tag = () }, ())] });
        (Some { l = { def = [r1]; use = [d]; ismov = true; in_ = []; out = [] };
                pred = [({ node = 9; tag = () }, ())];
                succ = [({ node = 11; tag = () }, ())] });
        (Some { l = { def = [r3]; use = [c]; ismov = true; in_ = []; out = [] };
                pred = [({ node = 10; tag = () }, ())];
                succ = [({ node = 12; tag = () }, ())] });
        (Some { l =
                { def = []; use = [r1; r3]; ismov = false; in_ = []; out = [] };
                pred = [({ node = 11; tag = () }, ())]; succ = [] })
        |];
      idx = 13; ext =  } |}];

  let igraph = interferenceGraph ~size:16 3 [] flow in

  draw_digraph flowgraph "flow";
  draw_graph igraph ~show_temp "liveness";
  print_string @@ Flow.DGraph.show flowgraph;
  [%expect
    {|
    { arr =
      [|(Some { l =
                { def = [c]; use = [r3]; ismov = true; in_ = [r1; r2; r3];
                  out = [r1; r2; c] };
                pred = []; succ = [({ node = 1; tag = () }, ())] });
        (Some { l =
                { def = [a]; use = [r1]; ismov = true; in_ = [r1; r2; c];
                  out = [r2; a; c] };
                pred = [({ node = 0; tag = () }, ())];
                succ = [({ node = 2; tag = () }, ())] });
        (Some { l =
                { def = [b]; use = [r2]; ismov = true; in_ = [r2; a; c];
                  out = [a; b; c] };
                pred = [({ node = 1; tag = () }, ())];
                succ = [({ node = 3; tag = () }, ())] });
        (Some { l =
                { def = [d]; use = []; ismov = false; in_ = [a; b; c];
                  out = [a; b; c; d] };
                pred = [({ node = 2; tag = () }, ())];
                succ = [({ node = 4; tag = () }, ())] });
        (Some { l =
                { def = [e]; use = [a]; ismov = true; in_ = [a; b; c; d];
                  out = [b; c; d; e] };
                pred = [({ node = 3; tag = () }, ())];
                succ = [({ node = 5; tag = () }, ())] });
        (Some { l =
                { def = []; use = []; ismov = false; in_ = [b; c; d; e];
                  out = [b; c; d; e] };
                pred =
                [({ node = 4; tag = () }, ()); ({ node = 8; tag = () }, ())];
                succ = [({ node = 6; tag = () }, ())] });
        (Some { l =
                { def = [d]; use = [b; d]; ismov = false; in_ = [b; c; d; e];
                  out = [b; c; d; e] };
                pred = [({ node = 5; tag = () }, ())];
                succ = [({ node = 7; tag = () }, ())] });
        (Some { l =
                { def = [e]; use = [e]; ismov = false; in_ = [b; c; d; e];
                  out = [b; c; d; e] };
                pred = [({ node = 6; tag = () }, ())];
                succ = [({ node = 8; tag = () }, ())] });
        (Some { l =
                { def = []; use = [e]; ismov = false; in_ = [b; c; d; e];
                  out = [b; c; d; e] };
                pred = [({ node = 7; tag = () }, ())];
                succ =
                [({ node = 5; tag = () }, ()); ({ node = 9; tag = () }, ())] });
        (Some { l =
                { def = []; use = []; ismov = false; in_ = [c; d]; out = [c; d] };
                pred = [({ node = 8; tag = () }, ())];
                succ = [({ node = 10; tag = () }, ())] });
        (Some { l =
                { def = [r1]; use = [d]; ismov = true; in_ = [c; d];
                  out = [r1; c] };
                pred = [({ node = 9; tag = () }, ())];
                succ = [({ node = 11; tag = () }, ())] });
        (Some { l =
                { def = [r3]; use = [c]; ismov = true; in_ = [r1; c];
                  out = [r1; r3] };
                pred = [({ node = 10; tag = () }, ())];
                succ = [({ node = 12; tag = () }, ())] });
        (Some { l =
                { def = []; use = [r1; r3]; ismov = false; in_ = [r1; r3];
                  out = [] };
                pred = [({ node = 11; tag = () }, ())]; succ = [] })
        |];
      idx = 13; ext =  } |}];

  print_string @@ show_igraph igraph;
  [%expect
    {|
    { arr =
      [|(Some { l = { temp = r1; degree = 2; color = -1; head = None };
                adj =
                [({ node = 2; tag = (Intf false) }, None);
                  ({ node = 3; tag = Move }, (Some (a, r1)));
                  ({ node = 5; tag = (Intf false) }, None);
                  ({ node = 6; tag = Move }, (Some (r1, d)))]
                });
        (Some { l = { temp = r2; degree = 2; color = -1; head = None };
                adj =
                [({ node = 3; tag = (Intf false) }, None);
                  ({ node = 4; tag = Move }, (Some (b, r2)));
                  ({ node = 5; tag = (Intf false) }, None)]
                });
        (Some { l = { temp = r3; degree = 1; color = -1; head = None };
                adj =
                [({ node = 0; tag = (Intf false) }, None);
                  ({ node = 5; tag = Move }, (Some (c, r3)))]
                });
        (Some { l = { temp = a; degree = 4; color = -1; head = None };
                adj =
                [({ node = 0; tag = Move }, (Some (a, r1)));
                  ({ node = 1; tag = (Intf false) }, None);
                  ({ node = 4; tag = (Intf false) }, None);
                  ({ node = 5; tag = (Intf false) }, None);
                  ({ node = 6; tag = (Intf false) }, None);
                  ({ node = 7; tag = Move }, (Some (e, a)))]
                });
        (Some { l = { temp = b; degree = 4; color = -1; head = None };
                adj =
                [({ node = 1; tag = Move }, (Some (b, r2)));
                  ({ node = 3; tag = (Intf false) }, None);
                  ({ node = 5; tag = (Intf false) }, None);
                  ({ node = 6; tag = (Intf false) }, None);
                  ({ node = 7; tag = (Intf false) }, None)]
                });
        (Some { l = { temp = c; degree = 6; color = -1; head = None };
                adj =
                [({ node = 0; tag = (Intf false) }, None);
                  ({ node = 1; tag = (Intf false) }, None);
                  ({ node = 2; tag = Move }, (Some (c, r3)));
                  ({ node = 3; tag = (Intf false) }, None);
                  ({ node = 4; tag = (Intf false) }, None);
                  ({ node = 6; tag = (Intf false) }, None);
                  ({ node = 7; tag = (Intf false) }, None)]
                });
        (Some { l = { temp = d; degree = 4; color = -1; head = None };
                adj =
                [({ node = 0; tag = Move }, (Some (r1, d)));
                  ({ node = 3; tag = (Intf false) }, None);
                  ({ node = 4; tag = (Intf false) }, None);
                  ({ node = 5; tag = (Intf false) }, None);
                  ({ node = 7; tag = (Intf false) }, None)]
                });
        (Some { l = { temp = e; degree = 3; color = -1; head = None };
                adj =
                [({ node = 3; tag = Move }, (Some (e, a)));
                  ({ node = 4; tag = (Intf false) }, None);
                  ({ node = 5; tag = (Intf false) }, None);
                  ({ node = 6; tag = (Intf false) }, None)]
                })
        |];
      idx = 8; ext =  } |}];

  let colormap (t : Support.Temp.t) =
    match show_temp t with
    | "r1" -> Some 0
    | "r2" -> Some 1
    | "r3" -> Some 2
    | _ -> None
  in

  let spilled = Color.color 3 colormap igraph in
  draw_graph igraph ~show_temp "color_liveness";

  print_string @@ show_nodelist spilled;
  [%expect {| [{ temp = c; degree = 0; color = -1; head = None }] |}];
  print_string @@ show_igraph igraph;
  [%expect
    {|
    { arr =
      [|(Some { l = { temp = r1; degree = 2; color = 0; head = (Some true) };
                adj =
                [({ node = 1; tag = (Intf false) }, None);
                  ({ node = 2; tag = (Intf false) }, None);
                  ({ node = 3; tag = Move }, (Some (a, r1)));
                  ({ node = 3; tag = Coalesced }, None);
                  ({ node = 5; tag = (Intf false) }, None);
                  ({ node = 6; tag = Move }, (Some (r1, d)));
                  ({ node = 7; tag = Move }, (Some (r1, e)));
                  ({ node = 7; tag = Coalesced }, None)]
                });
        (Some { l = { temp = r2; degree = 1; color = 1; head = (Some true) };
                adj =
                [({ node = 0; tag = (Intf false) }, None);
                  ({ node = 3; tag = (Intf false) }, None);
                  ({ node = 4; tag = Move }, (Some (b, r2)));
                  ({ node = 4; tag = Coalesced }, None);
                  ({ node = 5; tag = (Intf false) }, None)]
                });
        (Some { l = { temp = r3; degree = 1; color = 2; head = None };
                adj =
                [({ node = 0; tag = (Intf false) }, None);
                  ({ node = 5; tag = Move }, (Some (c, r3)))]
                });
        (Some { l = { temp = a; degree = 0; color = 0; head = (Some false) };
                adj =
                [({ node = 0; tag = Move }, (Some (a, r1)));
                  ({ node = 0; tag = Coalesced }, None);
                  ({ node = 1; tag = (Intf false) }, None);
                  ({ node = 4; tag = (Intf false) }, None);
                  ({ node = 5; tag = (Intf false) }, None);
                  ({ node = 6; tag = (Intf false) }, None);
                  ({ node = 7; tag = Move }, (Some (e, a)));
                  ({ node = 7; tag = Coalesced }, None)]
                });
        (Some { l = { temp = b; degree = 0; color = 1; head = (Some false) };
                adj =
                [({ node = 1; tag = Move }, (Some (b, r2)));
                  ({ node = 1; tag = Coalesced }, None);
                  ({ node = 3; tag = (Intf false) }, None);
                  ({ node = 5; tag = (Intf false) }, None);
                  ({ node = 6; tag = (Intf false) }, None);
                  ({ node = 7; tag = (Intf false) }, None)]
                });
        (Some { l = { temp = c; degree = 0; color = -1; head = None };
                adj =
                [({ node = 0; tag = (Intf false) }, None);
                  ({ node = 1; tag = (Intf false) }, None);
                  ({ node = 2; tag = Move }, (Some (c, r3)));
                  ({ node = 3; tag = (Intf false) }, None);
                  ({ node = 4; tag = (Intf false) }, None);
                  ({ node = 6; tag = (Intf false) }, None);
                  ({ node = 7; tag = (Intf false) }, None)]
                });
        (Some { l = { temp = d; degree = 0; color = 2; head = None };
                adj =
                [({ node = 0; tag = Move }, (Some (r1, d)));
                  ({ node = 3; tag = (Intf false) }, None);
                  ({ node = 4; tag = (Intf false) }, None);
                  ({ node = 5; tag = (Intf false) }, None);
                  ({ node = 7; tag = (Intf false) }, None)]
                });
        (Some { l = { temp = e; degree = 0; color = 0; head = (Some false) };
                adj =
                [({ node = 0; tag = Move }, (Some (r1, e)));
                  ({ node = 0; tag = Coalesced }, None);
                  ({ node = 3; tag = Move }, (Some (e, a)));
                  ({ node = 3; tag = Coalesced }, None);
                  ({ node = 4; tag = (Intf false) }, None);
                  ({ node = 5; tag = (Intf false) }, None);
                  ({ node = 6; tag = (Intf false) }, None)]
                })
        |];
      idx = 8; ext =  } |}]

let%expect_test _ =
  let regs = regs_make [ "r1"; "r2"; "r3"; "a"; "b"; "c1"; "c2"; "d"; "e" ] in
  let te s = SM.find s regs in
  let show_temp = make_show_temp regs in
  let lab1 = Temp.newlabel () in
  let lab2 = Temp.newlabel () in
  let instr =
    DLL.of_list
      [
        test_move (te "c1") (te "r3");
        test_oper [] [ te "c1" ];
        test_move (te "a") (te "r1");
        test_move (te "b") (te "r2");
        test_oper [ te "d" ] [];
        test_move (te "e") (te "a");
        test_label lab1;
        test_oper [ te "d" ] [ te "d"; te "b" ];
        test_oper [ te "e" ] [ te "e" ];
        test_oper ~jump:[ lab1 ;lab2] [] [ te "e" ];
        test_label lab2;
        test_move (te "r1") (te "d");
        test_oper [ te "c2" ] [];
        test_move (te "r3") (te "c2");
      ]
  in
  let out = [ te "r1"; te "r3" ] in
  let ((_, _) as flow) = Flow.instr2graph ~size:16 out instr in
  let igraph = interferenceGraph ~size:16 3 [] flow in

  draw_graph igraph ~show_temp "liveness_2";
  print_string @@ show_igraph igraph;
  [%expect
    {|
    { arr =
      [|(Some { l = { temp = r1; degree = 3; color = -1; head = None };
                adj =
                [({ node = 2; tag = (Intf false) }, None);
                  ({ node = 3; tag = Move }, (Some (a, r1)));
                  ({ node = 5; tag = (Intf false) }, None);
                  ({ node = 6; tag = (Intf false) }, None);
                  ({ node = 7; tag = Move }, (Some (r1, d)))]
                });
        (Some { l = { temp = r2; degree = 2; color = -1; head = None };
                adj =
                [({ node = 3; tag = (Intf false) }, None);
                  ({ node = 4; tag = Move }, (Some (b, r2)));
                  ({ node = 5; tag = (Intf false) }, None)]
                });
        (Some { l = { temp = r3; degree = 1; color = -1; head = None };
                adj =
                [({ node = 0; tag = (Intf false) }, None);
                  ({ node = 5; tag = Move }, (Some (c1, r3)));
                  ({ node = 6; tag = Move }, (Some (r3, c2)))]
                });
        (Some { l = { temp = a; degree = 3; color = -1; head = None };
                adj =
                [({ node = 0; tag = Move }, (Some (a, r1)));
                  ({ node = 1; tag = (Intf false) }, None);
                  ({ node = 4; tag = (Intf false) }, None);
                  ({ node = 7; tag = (Intf false) }, None);
                  ({ node = 8; tag = Move }, (Some (e, a)))]
                });
        (Some { l = { temp = b; degree = 3; color = -1; head = None };
                adj =
                [({ node = 1; tag = Move }, (Some (b, r2)));
                  ({ node = 3; tag = (Intf false) }, None);
                  ({ node = 7; tag = (Intf false) }, None);
                  ({ node = 8; tag = (Intf false) }, None)]
                });
        (Some { l = { temp = c1; degree = 2; color = -1; head = None };
                adj =
                [({ node = 0; tag = (Intf false) }, None);
                  ({ node = 1; tag = (Intf false) }, None);
                  ({ node = 2; tag = Move }, (Some (c1, r3)))]
                });
        (Some { l = { temp = c2; degree = 1; color = -1; head = None };
                adj =
                [({ node = 0; tag = (Intf false) }, None);
                  ({ node = 2; tag = Move }, (Some (r3, c2)))]
                });
        (Some { l = { temp = d; degree = 3; color = -1; head = None };
                adj =
                [({ node = 0; tag = Move }, (Some (r1, d)));
                  ({ node = 3; tag = (Intf false) }, None);
                  ({ node = 4; tag = (Intf false) }, None);
                  ({ node = 8; tag = (Intf false) }, None)]
                });
        (Some { l = { temp = e; degree = 2; color = -1; head = None };
                adj =
                [({ node = 3; tag = Move }, (Some (e, a)));
                  ({ node = 4; tag = (Intf false) }, None);
                  ({ node = 7; tag = (Intf false) }, None)]
                })
        |];
      idx = 9; ext =  } |}];

  let colormap (t : Support.Temp.t) =
    match show_temp t with
    | "r1" -> Some 0
    | "r2" -> Some 1
    | "r3" -> Some 2
    | _ -> None
  in

  let spilled = Color.color 3 colormap igraph in
  draw_graph igraph ~show_temp "color_liveness_2";

  print_string @@ show_nodelist spilled;
  [%expect {| [] |}];
  print_string @@ show_igraph igraph;
  [%expect
    {|
    { arr =
      [|(Some { l = { temp = r1; degree = 2; color = 0; head = (Some true) };
                adj =
                [({ node = 1; tag = (Intf false) }, None);
                  ({ node = 2; tag = (Intf false) }, None);
                  ({ node = 3; tag = Move }, (Some (a, r1)));
                  ({ node = 3; tag = Coalesced }, None);
                  ({ node = 5; tag = (Intf false) }, None);
                  ({ node = 6; tag = (Intf false) }, None);
                  ({ node = 7; tag = Move }, (Some (r1, d)));
                  ({ node = 8; tag = Move }, (Some (e, r1)));
                  ({ node = 8; tag = Coalesced }, None)]
                });
        (Some { l = { temp = r2; degree = 2; color = 1; head = (Some true) };
                adj =
                [({ node = 0; tag = (Intf false) }, None);
                  ({ node = 2; tag = (Intf false) }, None);
                  ({ node = 3; tag = (Intf false) }, None);
                  ({ node = 4; tag = Move }, (Some (b, r2)));
                  ({ node = 4; tag = Coalesced }, None);
                  ({ node = 5; tag = (Intf false) }, None)]
                });
        (Some { l = { temp = r3; degree = 2; color = 2; head = (Some true) };
                adj =
                [({ node = 0; tag = (Intf false) }, None);
                  ({ node = 1; tag = (Intf false) }, None);
                  ({ node = 5; tag = Move }, (Some (c1, r3)));
                  ({ node = 5; tag = Coalesced }, None);
                  ({ node = 6; tag = Move }, (Some (r3, c2)));
                  ({ node = 6; tag = Coalesced }, None)]
                });
        (Some { l = { temp = a; degree = 0; color = 0; head = (Some false) };
                adj =
                [({ node = 0; tag = Move }, (Some (a, r1)));
                  ({ node = 0; tag = Coalesced }, None);
                  ({ node = 1; tag = (Intf false) }, None);
                  ({ node = 4; tag = (Intf false) }, None);
                  ({ node = 7; tag = (Intf false) }, None);
                  ({ node = 8; tag = Move }, (Some (e, a)));
                  ({ node = 8; tag = Coalesced }, None)]
                });
        (Some { l = { temp = b; degree = 0; color = 1; head = (Some false) };
                adj =
                [({ node = 1; tag = Move }, (Some (b, r2)));
                  ({ node = 1; tag = Coalesced }, None);
                  ({ node = 3; tag = (Intf false) }, None);
                  ({ node = 7; tag = (Intf false) }, None);
                  ({ node = 8; tag = (Intf false) }, None)]
                });
        (Some { l = { temp = c1; degree = 0; color = 2; head = (Some false) };
                adj =
                [({ node = 0; tag = (Intf false) }, None);
                  ({ node = 1; tag = (Intf false) }, None);
                  ({ node = 2; tag = Move }, (Some (c1, r3)));
                  ({ node = 2; tag = Coalesced }, None)]
                });
        (Some { l = { temp = c2; degree = 0; color = 2; head = (Some false) };
                adj =
                [({ node = 0; tag = (Intf false) }, None);
                  ({ node = 2; tag = Move }, (Some (r3, c2)));
                  ({ node = 2; tag = Coalesced }, None)]
                });
        (Some { l = { temp = d; degree = 0; color = 2; head = None };
                adj =
                [({ node = 0; tag = Move }, (Some (r1, d)));
                  ({ node = 3; tag = (Intf false) }, None);
                  ({ node = 4; tag = (Intf false) }, None);
                  ({ node = 8; tag = (Intf false) }, None)]
                });
        (Some { l = { temp = e; degree = 0; color = 0; head = (Some false) };
                adj =
                [({ node = 0; tag = Move }, (Some (e, r1)));
                  ({ node = 0; tag = Coalesced }, None);
                  ({ node = 3; tag = Move }, (Some (e, a)));
                  ({ node = 3; tag = Coalesced }, None);
                  ({ node = 4; tag = (Intf false) }, None);
                  ({ node = 7; tag = (Intf false) }, None)]
                })
        |];
      idx = 9; ext =  } |}]

module Frame = struct
  type access = unit 
 
  type t = unit 

  type register = string [@@deriving show { with_path = false }]

  module TempMap = RM
  module RegMap = SM

  let allocLocal _ _ = ()

  let load_var _  t = test_oper [ t ] []

  let save_var _  t = test_oper [] [ t ]

  let registers = [ "r1"; "r2"; "r3" ; "fp"; "sp"]

  let regMap = regs_make registers

  let tempMap = make_temp regMap

  let rv = RegMap.find "r1" regMap

  let sp = RegMap.find "sp" regMap

  let fp = RegMap.find "fp" regMap

  let calleesaves =
    List.fold_left (fun a r -> RegMap.find r regMap :: a) [] [ "r3" ]
end

module RA = Reg_alloc.Make (Frame)

type instrlst = instr list [@@deriving show { with_path = false }]

let%expect_test _ =
  let regs =
    SM.union (fun _ _ _ -> failwith "register intersection") Frame.regMap
    @@ regs_make [ "a"; "b"; "c"; "d"; "e" ]
  in
  let te s = SM.find s regs in
  (* let show_temp = make_show_temp regs in *)
  let lab1 = Temp.newlabel () in
  let lab2 = Temp.newlabel () in
  let instr =
    [
      test_move (te "c") (te "r3");
      test_move (te "a") (te "r1");
      test_move (te "b") (te "r2");
      test_oper [ te "d" ] [];
      test_move (te "e") (te "a");
      test_label lab1;
      test_oper [ te "d" ] [ te "d"; te "b" ];
      test_oper [ te "e" ] [ te "e" ];
      test_oper ~jump:[ lab1 ; lab2] [] [ te "e" ];
      test_label lab2;
      test_move (te "r1") (te "d");
      test_move (te "r3") (te "c");
    ]
  in
  let instr, alloc = RA.alloc () instr in

  print_string @@ show_instrlst instr;
  [%expect
    {|
     spilled!!
     [Move {assem = []; dst = 128; src = r3};
       (Oper { assem = []; dst = []; src = [128]; jump = None });
       Move {assem = []; dst = a; src = r1}; Move {assem = []; dst = b; src = r2};
       (Oper { assem = []; dst = [d]; src = []; jump = None });
       Move {assem = []; dst = e; src = a}; Label {assem = []; lab = L4};
       (Oper { assem = []; dst = [d]; src = [d; b]; jump = None });
       (Oper { assem = []; dst = [e]; src = [e]; jump = None });
       (Oper { assem = []; dst = []; src = [e]; jump = (Some [L4; L5]) });
       Label {assem = []; lab = L5}; Move {assem = []; dst = r1; src = d};
       (Oper { assem = []; dst = [127]; src = []; jump = None });
       Move {assem = []; dst = r3; src = 127}] |}];

  print_string @@ RM.show Format.pp_print_string alloc;
  [%expect
    {|
     [(r1, r1); (r2, r2); (r3, r3); (fp, fp); (sp, sp); (a, r1); (b, r2); (
       d, r3); (e, r1); (127, r3); (128, r3)] |}] 
