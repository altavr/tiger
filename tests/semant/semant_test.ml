open OUnit2
open Testsemantlib.Semant

let pass_case_test fn =
  fn >:: fun ctx ->
  let test_file = OUnitTestData.in_testdata_dir ctx.OUnitTest.conf [ fn ] in

  let ast =
    try Absttree.Parsing.run test_file
    with Absttree.Parsing.SyntaxError msg ->
      assert_string msg;
      exit 1
  in

  (* transProg ast *)
  try transProg ast |> ignore
  with TypeError (k, msg, pos) ->
    let f = open_in test_file in
    let be, en = I.dia pos in
    seek_in f be;
    let quote = really_input_string f (en - be) in
    (* A.pp_exp Format.std_formatter ast; *)
    let msg =
      match msg with
      | None ->
          Printf.sprintf "\n%s\nLine: %d, Column: %d\n%s\n" (show_errkind k)
            (I.linenum pos) (I.start_col pos) quote
      | Some msg ->
          Printf.sprintf "\n%s: %s\nLine: %d, Column: %d\n%s\n" (show_errkind k)
            msg (I.linenum pos) (I.start_col pos) quote
    in
    assert_string msg;
    exit 1

let nonpass_case_test fn =
  fn >:: fun ctx ->
  let test_file = OUnitTestData.in_testdata_dir ctx.OUnitTest.conf [ fn ] in

  assert_bool fn
    (try
       let ast = Absttree.Parsing.run test_file in
       transProg ast |> ignore;
       false
     with TypeError _ | Absttree.Parsing.SyntaxError _ -> true)

let valid_cases =
  List.map pass_case_test
    [
      "test1.tig";
      "test2.tig";
      "test3.tig";
      "test4.tig";
      "test5.tig";
      "test6.tig";
      "test7.tig";
      "test8.tig";
      "test12.tig";
      "test27.tig";
      "test30.tig";
      "test37.tig";
      "test41.tig";
      "test42.tig";
      "test44.tig";
      "test46.tig";
      "test47.tig";
      "test48.tig";
    ]

let nonvalid_cases =
  List.map nonpass_case_test
    [
      "test9.tig";
      "test10.tig";
      "test11.tig";
      "test13.tig";
      "test14.tig";
      "test15.tig";
      "test16.tig";
      "test17.tig";
      "test18.tig";
      "test19.tig";
      "test20.tig";
      "test21.tig";
      "test22.tig";
      "test23.tig";
      "test24.tig";
      "test25.tig";
      "test26.tig";
      "test28.tig";
      "test29.tig";
      "test31.tig";
      "test32.tig";
      "test33.tig";
      "test34.tig";
      "test35.tig";
      "test36.tig";
      "test38.tig";
      "test39.tig";
      "test40.tig";
      "test43.tig";
      "test45.tig";
      "test49.tig";
    ]

let () = "Semant tests" >::: valid_cases @ nonvalid_cases |> run_test_tt_main
