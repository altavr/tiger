module S = Absttree.Symbol
module P = Absttree.Parsing
module F = X86_64.Frame
include Icr.Semant.Make (Translate)

type fraglst = F.frag list [@@deriving show { with_path = false }]

let pass_case_test fn =
  let fn =
    Filename.concat
      (Filename.concat (Sys.getcwd () |> Filename.dirname) "testdata")
      fn
  in
  let ast =
    try Absttree.Parsing.run fn
    with Absttree.Parsing.SyntaxError msg ->
      print_string msg;
      exit 1
  in
  (* transProg ast *)
  try transProg ast
  with TypeError (k, msg, pos) ->
    let f = open_in fn in
    let be, en = I.dia pos in
    seek_in f be;
    let quote = really_input_string f (en - be) in
    let msg =
      match msg with
      | None ->
          Printf.sprintf "\n%s\nLine: %d, Column: %d\n%s\n" (show_errkind k)
            (I.linenum pos) (I.start_col pos) quote
      | Some msg ->
          Printf.sprintf "\n%s: %s\nLine: %d, Column: %d\n%s\n" (show_errkind k)
            msg (I.linenum pos) (I.start_col pos) quote
    in
    print_string msg;
    exit 1

let%expect_test _ =
  print_string @@ show_fraglst @@ pass_case_test "test1.tig";
  [%expect
    {| 
    [Proc {
       body =
       (Move ((Temp rax),
          (Eseq (
             (Seq ((Move ((Temp 109), (Temp rbp))),
                (Seq (
                   (Move ((Temp 110),
                      (Eseq (
                         (Move ((Temp 108),
                            (Call ((Name initArray), [(Const 10); (Const 0)])))),
                         (Temp 108)))
                      )),
                   (Move ((Mem (Binop (Plus, (Const -16), (Temp 109)))),
                      (Temp 110)))
                   ))
                )),
             (Eseq ((Move ((Temp 111), (Temp rbp))),
                (Mem (Binop (Plus, (Const -16), (Temp 111))))))
             ))
          ));
       frame = { name = tigermain; formals = [(InFrame 0)]; loc_num = 2 }}
      ] |}]

(* module I = Support.Interval

let%expect_test _ =
  let decs =
    A.
      [
        TypeDec
          [
            {
              tyname = S.symbol "arr";
              ty = ArrayTy (S.symbol "int", I.empty);
              typos = I.empty;
            };
          ];
        VarDec
          {
            name = S.symbol "arr";
            escape = ref true;
            typ = Some (S.symbol "arr", I.empty);
            init =
              ArrayExp
                {
                  typ = S.symbol "arr";
                  size = IntExp 10;
                  init = IntExp 3;
                  pos = I.empty;
                };
            pos = I.empty;
          };
      ]
  in
  let exp =
    A.(
      AssignExp
        {
          var =
            SubscriptVar (SimpleVar (S.symbol "a", I.empty), IntExp 0, I.empty);
          exp = IntExp 1;
          pos = I.empty;
        })
  in

  print_string @@ show_fraglst
  @@ transProg A.(LetExp { decs; body = exp; pos = I.empty }) *)
