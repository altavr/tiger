open Support

let () = Temp.reset ()

module F = struct
  include X86_64.Frame
end

include Icr.Translate.MakeUnsighned (F)


let canon x = Backend.Canon.(linearize x |> basicBlocks |> traceSchelduler)

let print x = print_string @@ T.show_stm @@ unNx x
 
let omlev = outermost 

let lev1 = newLevel omlev (Temp.namedLabel "lev1") [ true ]

let a_var = Temp.labeltemp "a"

let a_acc = F.InReg a_var

let b_acc = F.InFrame 1

let%expect_test _ =  
  match lev1 with
  | Func (_, f, _) ->
      print_string @@ F.show f;
      [%expect
        {| { name = lev1; formals = [(InFrame 0); (InFrame 1)]; loc_num = 2 } |}]
  | _ ->
      assert false |> ignore;
      [%expect.unreachable]

let%expect_test _ =
  print_string @@ T.show_exp @@ static_link omlev omlev (fun () -> T.Temp F.fp);
  [%expect {| (Temp rbp) |}]

let%expect_test _ =
  print_string @@ T.show_exp @@ static_link omlev lev1 (fun () -> T.Temp F.fp);
  [%expect {| (Mem (Binop (Plus, (Const -8), (Temp rbp)))) |}]

let%expect_test _ =
  let e = simpleVar (omlev, a_acc) omlev in
  print_string @@ T.show_stm @@ unNx e  ;
  [%expect {| (Exp (Eseq ((Move ((Temp 101), (Temp rbp))), (Temp a)))) |}]
 
let%expect_test _ =
  let e =   simpleVar (omlev, b_acc) lev1 in
  print_string   @@ T.show_stm @@ unNx e   ;
  [%expect
    {|   
    (Exp
       (Eseq ((Move ((Temp 102), (Temp rbp))),
          (Mem
             (Binop (Plus, (Const -16),
                (Mem (Binop (Plus, (Const -8), (Temp 102)))))))
          ))) |}]
 
let%expect_test _ =
  let e = assignVar (omlev, b_acc) omlev 
  @@  Ex ( Const 1 )  in
  print_string @@ T.show_stm @@ unNx e;
  [%expect {|
    (Seq ((Move ((Temp 103), (Temp rbp))),
       (Seq ((Move ((Temp 104), (Const 1))),
          (Move ((Mem (Binop (Plus, (Const -16), (Temp 103)))), (Temp 104)))))
       )) |}]
  
  let%expect_test _ =
  let arr =  array ~len:(integer 10) (integer 0) in
  print_string @@ T.show_stm @@ unNx  arr ;
  [%expect {|
    (Exp
       (Eseq (
          (Move ((Temp 105), (Call ((Name initArray), [(Const 10); (Const 0)])))),
          (Temp 105)))) |}] ; 

    let v = assignVar (omlev, b_acc) omlev   arr  in 
    print_string @@ T.show_stm @@ unNx  v ;
    [%expect {|
      (Seq ((Move ((Temp 106), (Temp rbp))),
         (Seq (
            (Move ((Temp 107),
               (Eseq (
                  (Move ((Temp 105),
                     (Call ((Name initArray), [(Const 10); (Const 0)])))),
                  (Temp 105)))
               )),
            (Move ((Mem (Binop (Plus, (Const -16), (Temp 106)))), (Temp 107)))))
         )) |}] ; 

    print_string @@ T.show_stmlist @@ canon @@ unNx v;
    [%expect {|
      [(Label L1); (Move ((Temp 106), (Temp rbp)));
        (Move ((Temp 105), (Call ((Name initArray), [(Const 10); (Const 0)]))));
        (Move ((Temp 107), (Temp 105)));
        (Move ((Mem (Binop (Plus, (Const -16), (Temp 106)))), (Temp 107)));
        (Jump ((Name L0), [L0])); (Label L0)] |}]
 
(* let%expect_test _ =
  let arr =  array ~len:(integer 10) (integer 0) in
  print_string @@ T.show_stm @@ unNx  arr ; *)
