let cases =
  [
    ("vars00.tig", "pl subscript", "");
    ("vars01.tig", "global", "");
    ("vars02.tig", "redefine", "");
    ("vars03.tig", "empty field", "");
    ("vars04.tig", "", "access to NIL");
    ("vars05.tig", "global", "");
    ("vars06.tig", "global", "");
    ("vars07.tig", "global", "");
    (*   print( if 1 then "true" else "false" ) *)
    ("cond00.tig", "true", "");
    (*  print( if 0 then "true" else "false" )*)
    ("cond01.tig", "false", "");
    (*  if 1 then print("true") else print("false") *)
    ("cond02.tig", "true", "");
    (*  if 0 then print("true") else print("false")*)
    ("cond03.tig", "false", "");
    (* if 3=3 then print("true") else print("false") *)
    ("cond04.tig", "true", "");
    (*  4>3 *)
    ("cond05.tig", "true", "");
    (*  2<3 *)
    ("cond06.tig", "true", "");
    (*  3<=3 *)
    ("cond07.tig", "true", "");
    (*  3>=3 *)
    ("cond08.tig", "true", "");
    (*  3<>3 *)
    ("cond09.tig", "false", "");
    (* "abc"="abc" *)
    ("cond10.tig", "true", "");
    (* "abc"<>"abd" *)
    ("cond11.tig", "true", "");
    ("func00.tig", "arg0_4", "");
    ("func01.tig", "24", "");
    ("func02.tig", "bsss", "");
    ("func03.tig", "result", "");
    ("cond04.tig", "true", "");
    ("cycl00.tig", "for for for for for for ", "");
    ("cycl01.tig", "while while while while while ", "");
    ("cycl02.tig", "to for for for break", "");
    ("cycl03.tig", "for for break for for break ", "");
    ("cycl04.tig", "while ", "");
    (* 3+5 *)
    ("ariph00.tig", "8", "");
    (* 3-5 *)
    ("ariph01.tig", "-2", "");
    (* 3*5 *)
    ("ariph02.tig", "15", "");
    (* 15/5 *)
    ("ariph03.tig", "-3", "");
    ("boolean00.tig", "true", "");
    ("boolean01.tig", "false", "");
    ("boolean02.tig", "true", "");
    ("boolean03.tig", "false", "");
    (* (("merge.tig", "false", "");  *)
    ("extn00.tig", "789456", "");
    (* stringEqual( "abc", "abc" )  *)
    ("extn01.tig", "true", "");
    (* stringEqual( "abc", "abd" )  *)
    ("extn02.tig", "false", "");
    (* ord( "a") *)
    ("extn03.tig", "97", "");
    (* chr( 97 ) *)
    ("extn04.tig", "a", "");
    (* size(  "tiger" ) *)
    ("extn05.tig", "5", "");
    (* substring(  "not tiger", 4, 3 ) *)
    ("extn06.tig", "tig", "");
    (*  concat(  "wild " "tiger" ) *)
    ("extn07.tig", "wild tiger", "");
    (* ("gc00.tig", "a", ""); *)
  ]

let () = Testrun.from_src cases

(* let runcmd x =
     if Sys.command x = 0 then ()
     else (
       prerr_string @@ "Error: " ^ x;
       exit 1)

   let root = Filename.concat (Sys.getcwd ()) "../../../.."

   let test fn expect expect_err =
     let basename = Filename.(chop_extension @@ basename fn) in
     print_endline @@ "testing " ^ basename ^ "...\n";

     let asm_file = basename ^ ".asm" in
     Compiler.main fn asm_file;
     runcmd
     @@ Printf.sprintf "yasm -g dwarf2 -f elf64 -o %s.o -l %s.lst %s" basename
          basename asm_file;
     runcmd
     @@ Printf.sprintf
          "gcc -g -no-pie -o %s %s.o runtime.o  -L \
           /home/alex/documents/ocaml/ed_compiler/tiger/runtime/runtime/target/debug \
           -lrt -lpthread -ldl"
          basename basename ;

     let buff = Buffer.create 1024 in
     let buff_err = Buffer.create 1024 in
     Unix.open_process_full (Filename.concat "./" basename) [||]
     |> fun (c, _, e) ->
     (try Buffer.add_channel buff c 1024 with End_of_file -> ());
     (try Buffer.add_channel buff_err e 1024 with End_of_file -> ());

     let res = Buffer.contents buff in
     let res_err = Buffer.contents buff_err in
     if res = expect && expect_err = res_err then ()
     else (
       prerr_string @@ "Error: output is not as expected\nFile: " ^ fn;
       prerr_newline ();
       Printf.printf "Expect: \"%s\",\nbut got: \"%s\"\n" expect res;
       Printf.printf "Expect error: \"%s\",\nbut got error: \"%s\"\n" expect_err
         res_err;
       exit 1)

   let () =
     let tempdir = Filename.concat root "temp" in
     if not @@ Sys.file_exists tempdir then Sys.mkdir tempdir 0o777;
     Sys.chdir tempdir;
     runcmd
     @@ Printf.sprintf "cc -g -Wall -Wextra -Wpedantic -c -o runtime.o %s"
     @@ Filename.concat root "runtime/runtime.c";

     List.iter
       (fun (fn, expect, expect_err) ->
         test
           Filename.(concat root @@ concat "tests/compiler_test/testdata" fn)
           expect expect_err)
       cases *)
