(* type access *)
module Temp = Support.Temp

module Make (T : Translate.S) = struct
  open Types

  type enventry =
    | VarEntry of { access : T.access; ty : ty }
    | FunEntry of {
        level : T.level;
        externfun : bool;
        label : Temp.label;
        formals : ty list;
        result : ty;
      }

  let add_list env l = List.fold_left (fun t (x, ty) -> S.enter x ty t) env l

  let add_list_string env l =
    List.iter (fun (x, _) -> S.symbol x |> ignore) l;
    List.fold_left (fun t (x, ty) -> S.enter (S.symbol x) ty t) env l

  let base_tenv : ty S.table =
    let init = [ ("int", Int); ("string", String) ] in
    add_list_string S.empty init

  let global_funcs =
    [
      "init_array";
      "alloc_record";
      "string";
      "stringEqual";
      "print";
      "flush";
      "ord";
      "chr";
      "size";
      "substring";
      "concat";
      "not";
      "getchar_";
      "string_of_int";
      "error_handler";
    ]

  let base_venv outermost : enventry S.table =
    let make_fun (name, formals, result) =
      ( name,
        FunEntry
          {
            level = outermost;
            externfun = true;
            label = Temp.namedLabel name;
            formals;
            result;
          } )
    in

    let global =
      List.map make_fun
        [
          ("stringEqual", [ String; String ], Int);
          ("print", [ String ], Unit);
          ("flush", [], Unit);
          ("ord", [ String ], Int);
          ("chr", [ Int ], String);
          ("size", [ String ], Int);
          ("substring", [ String; Int; Int ], String);
          ("concat", [ String; String ], String);
          ("not", [ Int ], Int);
          ("getchar_", [], String);
          ("string_of_int", [ Int ], String);
          ("error_handler", [ String ], Unit);
        ]
    in

    add_list_string S.empty global
end
