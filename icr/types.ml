module S = Absttree.Symbol

type unique = unit ref [@@deriving show { with_path = false }]

(*Need physical equality of  unique *)
let equal_unique u1 u2 = u1 == u2

type ty =
  | Record of (S.t * ty) list * unique
  | Nil
  | Int
  | String
  | Array of ty * unique
  | Name of S.t * ty option ref
  | Unit
(* [@@deriving show { with_path = false }] *)




(* let equal = equal_ty
let equal_ty t1 t2 = 
    match t1,t2 with 
    | Nil, Name ( _, {contents= Some Record _} ) |
     Name ( _, {contents= Some Record _} ) , Nil  -> true 
    | _ -> 
      
      equal t1 t2 *)

let rec equal_ty t1 t2 = 
    match t1,t2 with 
    | Nil, Nil | Int , Int | String, String | Unit , Unit -> true 
    | Record (_, u1), Record (_, u2) -> equal_unique u1 u2 
    | Array (_, u1), Array (_, u2) -> equal_unique u1 u2 
    | Name ( _,  {contents = Some x1}  ), Name ( _,  {contents = Some x2} ) ->  equal_ty x1 x2 
    | Name ( _,  {contents = None}  ), Name ( _,  {contents = None} ) -> true 
    | _ -> false