module Temp = Support.Temp

type boxed = [ `Boxed | `Unboxed ] [@@deriving show { with_path = false }]

type boxedwl = [ boxed | `SLink ]

module type Frame = sig
  type access

  type t

  type frag =
    | Proc of { body : Tree.stm; frame : t }
    | String of Temp.label * string

  (* type boxed = [ `Boxed | `Unboxed ] *)

  val pp : Format.formatter -> t -> unit

  val pp_access : Format.formatter -> access -> unit

  val show_access : access -> string

  val pp_frag : Format.formatter -> frag -> unit

  val show_frag : frag -> string

  val fp : Temp.t

  val rv : Temp.t

  val wordSize : int

  val header_of_block_size : int

  val newFrame : Temp.label -> (bool * [< boxedwl ]) list -> t

  val name : t -> Temp.label

  val formals : t -> access list

  val allocLocal :
    ?spilled:Temp.t -> t -> [< `Boxed | `Unboxed ] -> bool -> access

  val exp : access -> Tree.exp -> Tree.exp

  val assign : access -> Tree.exp -> Tree.exp -> boxed -> Tree.stm

  (* val outer_fp : Tree.exp -> Tree.exp *)

  val externalCall : Temp.label -> Tree.exp list -> Tree.bx -> bool -> Tree.exp
end

module type S = sig
  type exp

  type level

  type access

  val show_exp : exp -> string

  val show_level : level -> string

  val show_access : access -> string

  val outermost : unit -> level

  val newLevel : level -> Temp.label -> (bool * boxed) list -> level

  val formals : level -> access list

  val allocLocal : level -> boxed -> bool -> access

  val integer : int -> exp

  val simpleVar : access -> level -> exp

  val assignVar : access -> level -> exp -> boxed -> exp

  val ptrSubscribeVar : var:exp -> idx:exp -> exp

  val ptrFieldVar : var:exp -> idx:int -> exp

  val deref : exp -> boxed -> exp

  val ptrAssign : ptr:exp -> exp -> boxed -> exp

  val ariph : [< `Plus | `Minus | `Mul | `Div ] -> exp -> exp -> exp

  val conditinal : [< `Eq | `Neq | `Lt | `Le | `Gt | `Ge ] -> exp -> exp -> exp

  val compare_string : [< `Eq | `Neq ] -> exp -> exp -> exp

  val ifThenElse : ?else':exp -> exp -> exp -> boxed -> exp

  val sequence : exp list -> last:exp -> exp

  val string : string -> exp

  val record : (boxed * exp) list -> exp

  val array : len:exp -> boxed -> exp -> exp

  val whileLoop : done':Temp.label -> cond:exp -> body:exp -> exp

  val forLoop :
    done':Temp.label ->
    lo_access:access ->
    level:level ->
    lo:exp ->
    hi:exp ->
    body:exp ->
    exp

  val break : Temp.label -> exp

  val return : exp -> exp

  val placeholder : exp

  val funCall :
    Temp.label -> level -> bool -> env:level -> exp list -> boxed -> exp

  val procEntryExit : level -> exp -> unit

  module Frame : Frame

  val getResult : unit -> Frame.frag list
end

module MakeUnsighned (F : Frame) = struct
  module T = Tree
  module Frame = F

  type unique = unit ref [@@deriving show { with_path = false }]

  type level = Func of level * F.t * unique | Outermost of F.t
  [@@deriving show { with_path = false }]

  let equal_level l1 l2 =
    match (l1, l2) with
    | Func (_, _, u1), Func (_, _, u2) -> u1 == u2
    | Outermost _, Outermost _ -> true
    | _ -> false

  type access = level * F.access [@@deriving show { with_path = false }]

  let outermost () : level =
    let lab = Temp.namedLabel "tigermain" in
    Outermost (F.newFrame lab [ (true, `SLink) ])

  let newLevel (parent : level) (name : Temp.label) formals : level =
    let frame =
      F.newFrame name ((true, `SLink) :: (formals :> (_ * boxedwl) list))
    in
    (* let accsess_list = Frame.formals frame in  *)
    Func (parent, frame, ref ())

  let formals = function
    | Outermost _ -> assert false
    | Func (_, frame, _) as level ->
        List.map (fun f -> (level, f)) @@ F.formals frame |> List.tl

  let allocLocal level boxed (escape : bool) : access =
    match level with
    | Outermost frame | Func (_, frame, _) ->
        let a = F.allocLocal frame boxed escape in
        print_endline
          ("allocLocal >> " ^ F.show_access a ^ " " ^ show_boxed boxed);

        (level, a)

  let frag_list : F.frag list ref = ref []

  let getResult () =
    let fl = !frag_list in
    frag_list := [];
    fl

  type exp =
    | Ex of T.exp
    | Nx of T.stm
    | Cx of (Temp.label -> Temp.label -> T.stm)
  [@@deriving show { with_path = false }]

  let unCx = function
    | Ex e -> fun t f -> T.(CJump (Ne, e, Const 0, t, f))
    | Cx cs -> cs
    | Nx _ -> assert false

  let unNx = function
    | Ex e -> T.Exp e
    | Cx cs ->
        let t = Temp.newlabel () and f = Temp.newlabel () in
        cs t f
    | Nx s -> s

  let unEx = function
    | Ex e -> e
    | Cx cs ->
        let t = Temp.newlabel ()
        and f = Temp.newlabel ()
        and r = T.Temp (Temp.newtemp `Unboxed) in
        T.eseq
          [ Move (r, Const 1); cs t f; Label f; Move (r, Const 0); Label t ]
          r
    | Nx (T.Exp x) -> x
    | Nx s -> T.(Eseq (s, Const 0))

  let rec static_link dec_level level k =
    match level with
    | Func _ when equal_level level dec_level -> k
    | Outermost _ -> k
    | Func (up, frame, _) ->
        static_link dec_level up (F.exp (F.formals frame |> List.hd) k)

  let simpleVar (dec_lev, access) (level : level) : exp =
    Ex T.(F.exp access @@ static_link dec_lev level (Temp F.fp))

  let assignVar (dec_lev, access) level exp boxed =
    Nx
      T.(
        F.assign access (static_link dec_lev level (Temp F.fp)) (unEx exp) boxed)

  let integer x = Ex T.(Const x)

  let conv_binop = function
    | `Plus -> T.Plus
    | `Minus -> T.Minus
    | `Mul -> T.Mul
    | `Div -> T.Div

  let ariph op left right = Ex T.(Binop (conv_binop op, unEx left, unEx right))

  let ptrSubscribeVar ~var ~idx =
    (* print_endline ">>>>>>>>>> ptrSubscribeVar"; *)
    let t = Temp.newtemp `Offset in

    (* Ex T.(Binop (Plus, unEx var, Binop (Mul, unEx idx, Const F.wordSize))) *)
    Ex
      T.(
        Eseq
          ( Move
              ( Temp t,
                Binop
                  ( Mul,
                    Binop (Plus, Const F.header_of_block_size, unEx idx),
                    Const F.wordSize ) ),
            Binop (Plus, unEx var, Temp t) ))

  let ptrAssign ~ptr exp boxed =
    (* print_endline ">>>>>>>>>> ptrAssign"; *)
    Nx T.(Move (Mem (unEx ptr, T.bx_of_boxed boxed), unEx exp))

  let conv_relop = function
    | `Eq -> T.Eq
    | `Neq -> T.Ne
    | `Lt -> T.Lt
    | `Le -> T.Le
    | `Gt -> T.Gt
    | `Ge -> T.Ge

  let conditinal op left right =
    (* print_endline ">>>>>>>>>> conditinal"; *)
    (* let t = Temp.newlabel () and f = Temp.newlabel () in
       let r = T.Temp (Temp.newtemps ()) in *)
    Cx T.(fun t f -> CJump (conv_relop op, unEx left, unEx right, t, f))

  let compare_string op left right =
    match op with
    | `Eq ->
        Ex
          T.(
            Call
              ( Name (Temp.namedLabel "stringEqual"),
                [ unEx @@ left; unEx @@ right ],
                Unboxed,
                false ))
    | `Neq ->
        Ex
          T.(
            Call
              ( Name (Temp.namedLabel "not"),
                [
                  Call
                    ( Name (Temp.namedLabel "stringEqual"),
                      [ unEx @@ left; unEx @@ right ],
                      Unboxed,
                      false );
                ],
                Unboxed,
                false ))

  let ifThenElse ?else' test then' bx =
    (* let t = Temp.newlabel () and f = Temp.newlabel () in *)
    let te = Temp.newtemp bx in
    let z = Temp.newlabel ()
    and t = Temp.newlabel ()
    and f = Temp.newlabel () in
    match else' with
    | Some else' ->
        (* print_endline ">>>>>>>>>> ifThenElse else"; *)
        Ex
          T.(
            Eseq
              ( seq
                  [
                    unCx test t f;
                    Label f;
                    Move (Temp te, unEx else');
                    Jump (Name z, [ z ]);
                    Label t;
                    Move (Temp te, unEx then');
                    Label z;
                  ],
                Temp te ))
    | None ->
        (* print_endline ">>>>>>>>>> ifThenElse nonhe"; *)
        Nx T.(seq [ unCx test t f; Label t; unNx then'; Label f ])

  let sequence expl ~last = Ex (T.eseq (List.map unNx expl) (unEx last))

  let string s =
    let lab = Temp.newlabel () in
    frag_list := F.String (lab, s) :: !frag_list;

    Ex T.(Name lab)

  let record expl =
    let r = T.Temp (Temp.newtemp `Boxed) in
    let _, fields_init, bitmap =
      List.fold_right
        (fun (b, e) (i, fl, bitmap) ->
          ( i - 1,
            T.(
              Move
                ( Mem
                    ( Binop
                        ( Plus,
                          r,
                          Const (F.wordSize * (i + F.header_of_block_size)) ),
                      T.bx_of_boxed b ),
                  unEx e ))
            :: fl,
            match b with
            | `Unboxed -> bitmap
            | `Boxed -> Int.logor bitmap @@ (1 lsl i) ))
        expl
        (List.length expl - 1, [], 0)
    in

    Ex
      T.(
        eseq
          (Move
             ( r,
               F.externalCall
                 (Temp.namedLabel "alloc_record")
                 [ Temp F.fp; Const (List.length expl); Const bitmap ]
                 Boxed true )
          :: fields_init)
          r)

  let array ~len bx exp =
    let r = T.Temp (Temp.newtemp `Boxed) in
    Ex
      T.(
        eseq
          [
            Move
              ( r,
                F.externalCall
                  (Temp.namedLabel "init_array")
                  [
                    Temp F.fp;
                    unEx len;
                    unEx exp;
                    Const (match bx with `Boxed -> 1 | `Unboxed -> 0);
                  ]
                  Boxed true );
          ]
          r)

  let whileLoop ~done' ~cond ~body =
    (* print_endline ">>>>>>>>>> whileLoop"; *)
    let test = Temp.newlabel () in
    let body_lab = Temp.newlabel () in
    Nx
      T.(
        seq
          [
            Jump (Name test, [ test ]);
            Label body_lab;
            unNx body;
            Label test;
            unCx cond body_lab done';
            Label done';
          ])

  let forLoop ~done' ~lo_access ~level ~lo ~hi ~body =
    (* print_endline ">>>>>>>>>> forLoop"; *)
    let test = Temp.newlabel () in
    let body_lab = Temp.newlabel () in
    (* let i = T.Temp (Temp.newtemp ()) in *)
    let lim = T.Temp (Temp.newtemp `Unboxed) in

    let i = unEx @@ simpleVar lo_access level in

    Nx
      T.(
        seq
          [
            (* Move (lim,    unEx @@  assignVar lo_access level lo ) ; *)
            (* Move (i, unEx lo); *)
            unNx @@ assignVar lo_access level lo `Unboxed;
            Move (lim, unEx hi);
            Jump (Name test, [ test ]);
            Label body_lab;
            unNx body;
            (* Move (i, unEx @@ simpleVar lo_access level); *)
            (* Move (i, Binop (Plus, i, Const 1)); *)
            unNx
            @@ assignVar lo_access level
                 (Ex (Binop (Plus, i, Const 1)))
                 `Unboxed;
            Label test;
            CJump (Le, unEx @@ simpleVar lo_access level, lim, body_lab, done');
            Label done';
          ])

  let break lab = Nx T.(Jump (Name lab, [ lab ]))

  let return exp = Nx T.(Move (Temp F.rv, unEx exp))

  let placeholder = Nx T.(Exp (Const 0))

  let additional_extern_args =
    List.to_seq
      [
        ("substring", T.Temp F.fp);
        ("concat", T.Temp F.fp);
        ("string_of_int", T.Temp F.fp);
      ]
    |> Seq.map (fun (x, y) -> (x, Ex y))
    |> Hashtbl.of_seq

  let funCall lab dec_lev extern ~env formals boxed =
    let args =
      if extern then
        List.map unEx
          ((Option.to_list
           @@ Hashtbl.find_opt additional_extern_args (Temp.labelname lab))
          @ formals)
      else
        let sl =
          match dec_lev with
          | Func (lev, _, _) -> static_link lev env (Temp F.fp)
          | _ -> assert false
        in

        sl :: List.map unEx formals
    in

    Ex T.(Call (Name lab, args, T.bx_of_boxed boxed, true))

  let ptrFieldVar ~var ~idx =
    (* print_endline ">>>>>>>>>> ptrFieldVar"; *)
    ifThenElse var
      ~else':
        (Ex
           T.(
             Call
               ( Name (Temp.namedLabel "error_handler"),
                 [ unEx @@ string "access to NIL" ],
                 Unboxed,
                 false )))
      (Ex
         T.(
           Binop
             ( Plus,
               unEx var,
               Const ((idx + F.header_of_block_size) * F.wordSize) )))
      (`HPointer ((idx + F.header_of_block_size) * F.wordSize, None))

  let deref ptr (boxed : boxed) = Ex T.(Mem (unEx ptr, T.bx_of_boxed boxed))

  let procEntryExit level e =
    (* (try T.boxed_check @@ unNx e *)
    let body =
      try unNx e |> T.sfolding
      with Failure _ as err ->
        print_string @@ T.show_stm @@ unNx e;
        raise err
    in
    match level with
    | Outermost frame | Func (_, frame, _) ->
        frag_list := F.(Proc { body; frame }) :: !frag_list
end

module Make (F : Frame) : S with module Frame = F = MakeUnsighned ((F : Frame))
