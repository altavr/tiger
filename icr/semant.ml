module Temp = Support.Temp
module S = Absttree.Symbol

module Make (T : Translate.S) = struct
  module A = Absttree.Ast
  module E = Env.Make (T)
  module I = Absttree.Interval
  open Types

  type venv = E.enventry S.table

  type tenv = ty S.table

  type expty = { exp : T.exp; ty : ty }

  type expenv = { exp : T.exp option; venv : venv; tenv : tenv }

  type errkind =
    | SymbolNotFound
    | FieldNotExist
    | NotRecord
    | TypeMismatch
    | ArrayIndexNotInt
    | NotArray
    | NotFunction
    | ExceedArguments
    | NotEnoughArguments
    | RecordFieldNotCorrect
    | TypeCycleDetect
    | VarDecExpMismatch
    | DoubleDefRecursiveType
    | NilAssign
  [@@deriving show { with_path = false }]

  exception TypeError of errkind * string option * I.t

  (* [@@@warning "-27-8"] *)

  let error ?msg pos k =
    let e = TypeError (k, msg, pos) in
    raise e

  let listfind (f : 'a -> 'b option) l =
    let rec aux n = function
      | [] -> None
      | h :: tl -> (
          match f h with Some x -> Some (n, x) | None -> aux (n + 1) tl)
    in
    aux 0 l

  (*todo: проверить можно ли создать переменую типа nil и если нельзя,
     то приравнять nil и record *)
  let var_type_for ty ty' =
    match (ty, ty') with
    | Name (_, { contents = Some (Record _) }), Nil -> true
    | _ -> equal_ty ty ty'

  let check_doubles ~symfun tydec_lst =
    let rec aux = function
      | x1 :: x2 :: tl ->
          if S.equal (symfun x1) (symfun x2) then Some x2 else aux (x2 :: tl)
      | _ -> None
    in
    aux
    @@ List.stable_sort (fun x y -> S.compare (symfun x) (symfun y)) tydec_lst

  let boxstat_of_type = function Int | Unit -> `Unboxed | _ -> `Boxed

  let ops_conv = function
    | A.EqOp -> `Eq
    | NeqOp -> `Neq
    | LtOp -> `Lt
    | LeOp -> `Le
    | GtOp -> `Gt
    | GeOp -> `Ge
    | A.PlusOp -> `Plus
    | MinusOp -> `Minus
    | TimesOp -> `Mul
    | DivideOp -> `Div

  let rec transExp ~(break : Temp.label option) (level : T.level) (venv : venv)
      (tenv : tenv) (exp : A.exp) : expty =
    let rec aux exp : expty =
      match exp with
      | A.VarExp v -> transVarExp v
      | A.NilExp -> { exp = T.integer 0; ty = Nil }
      | A.IntExp v -> { exp = T.integer v; ty = Int }
      | A.StringExp (v, _) -> { exp = T.string v; ty = String }
      | A.CallExp { func; args; pos } -> callExp pos func args
      | A.OpExp { left; oper; right; pos } -> opExp pos left oper right
      | A.RecordExp { fields; typ; pos } -> recordExp pos fields typ
      | A.SeqExp exps -> seqExp exps
      | A.AssignExp { var; exp; pos } -> assignExp pos var exp
      | A.IfExp { test; then'; else'; pos } -> ifExp pos test then' else'
      | A.WhileExp { test; body; pos } -> whileExp pos test body
      | A.ForExp { var; escape; lo; hi; body; pos } ->
          forExp pos var escape lo hi body
      | A.BreakExp _ -> breakExp break
      | A.LetExp { decs; body; pos } -> letExp pos decs body
      | A.ArrayExp { typ; size; init; pos } -> arrayExp pos typ size init
    and transVarExp v =
      let { exp; ty } = transVar level venv tenv v in
      match v with
      | A.FieldVar _ | A.SubscriptVar _ ->
          { exp = T.deref exp (boxstat_of_type ty); ty }
      | A.SimpleVar _ -> { exp; ty }
    and breakExp = function
      | None -> { exp = T.placeholder; ty = Unit }
      | Some lab -> { exp = T.break lab; ty = Unit }
    and callExp pos func args =
      let rec check_func_args pos = function
        | ty :: tl1, exp :: tl2 ->
            let { exp; ty = ty' } = transExp ~break level venv tenv exp in

            if var_type_for ty ty' then exp :: check_func_args pos (tl1, tl2)
            else error pos TypeMismatch
        | [], _ :: _ -> error pos ExceedArguments
        | _ :: _, [] -> error pos NotEnoughArguments
        | [], [] -> []
      in

      match S.look func venv with
      | None -> error pos SymbolNotFound ~msg:"callExp"
      | Some
          (E.FunEntry { label; level = own_level; formals; result; externfun })
        ->
          let args = check_func_args pos (formals, args) in
          {
            exp =
              T.funCall label own_level externfun ~env:level args
                (boxstat_of_type result);
            ty = result;
          }
      | Some _ -> error pos NotFunction
    and opExp pos left op right =
      (* print_endline
         (">>>>>>>>>> opExp " ^ A.show_exp left ^ "\n right: " ^ A.show_exp right); *)
      let int_exp lexp rexp = function
        | (`Plus | `Minus | `Mul | `Div) as op -> T.ariph op lexp rexp
        | (`Eq | `Neq | `Lt | `Le | `Gt | `Ge) as op ->
            T.conditinal op lexp rexp
      in
      let non_int_exp pos lexp rexp = function
        | (`Eq | `Neq) as op -> T.conditinal op lexp rexp
        | _ -> error pos TypeMismatch
      in

      let compare_string pos lexp rexp = function
        | (`Eq | `Neq) as op -> T.compare_string op lexp rexp
        | _ -> error pos TypeMismatch
      in

      let { exp = lexp; ty = lty } = transExp ~break level venv tenv left in
      let { exp = rexp; ty = rty } = transExp ~break level venv tenv right in
      let op = ops_conv op in
      match (lty, rty) with
      | Int, Int -> { exp = int_exp lexp rexp op; ty = Int }
      | String, String -> { exp = compare_string pos lexp rexp op; ty = Int }
      | Name (_, { contents = Some (Record _) }), Nil
      | Nil, Name (_, { contents = Some (Record _) }) ->
          { exp = non_int_exp pos lexp rexp op; ty = Int }
      | _ -> error pos TypeMismatch
    and recordExp pos fields typ =
      match S.look typ tenv with
      | None -> error pos SymbolNotFound ~msg:"recordExp"
      | Some (Name (_, { contents = Some (Record (rfl, _)) }) as rcd) ->
          let m =
            List.fold_left
              (fun m ((s, _, _) as f) -> S.enter s f m)
              S.empty fields
          in

          let _, exp_lst =
            List.fold_right
              (fun (s, ty) (m, exp_lst) ->
                match S.look s m with
                | None -> error pos RecordFieldNotCorrect
                | Some (_, exp, pos) ->
                    let { exp; ty = ty' } =
                      transExp ~break level venv tenv exp
                    in
                    let bs = boxstat_of_type ty in
                    if var_type_for ty ty' then
                      (S.remove s m, (bs, exp) :: exp_lst)
                    else
                      error pos TypeMismatch
                        ~msg:(Printf.sprintf "Record field: %s" (S.name s)))
              rfl (m, [])
          in
          (* let boxed =   List.map ( fun (s) ) fields
             in *)
          { exp = T.record exp_lst; ty = rcd }
      | Some _ -> error pos NotRecord
    and seqExp exps =
      match List.rev_map (transExp ~break level venv tenv) exps with
      | [] -> { exp = T.integer 0; ty = Unit }
      | { ty; exp = last } :: tl ->
          let expl = List.rev_map (fun ({ exp; _ } : expty) -> exp) tl in
          { exp = T.sequence expl ~last; ty }
    and assignExp pos var exp =
      let { exp = ptrVar; ty = var_ty } = transVar level venv tenv var in

      let { exp; ty } = transExp ~break level venv tenv exp in
      if not @@ var_type_for var_ty ty then error pos TypeMismatch
      else
        let exp =
          match var with
          | A.FieldVar _ | A.SubscriptVar _ ->
              T.ptrAssign ~ptr:ptrVar exp (boxstat_of_type ty)
          | A.SimpleVar (s, _) -> (
              match S.look s venv with
              | None | Some (E.FunEntry _) -> assert false
              | Some (E.VarEntry { access; _ }) ->
                  T.assignVar access level exp (boxstat_of_type ty))
        in

        { exp; ty = Unit }
    and ifExp pos test then' else' =
      match
        ( transExp ~break level venv tenv test,
          transExp ~break level venv tenv then' )
      with
      | { exp = test_exp; ty = Int }, { exp = then_exp; ty = then_ty } -> (
          let bx = boxstat_of_type then_ty in
          match
            (Option.map (transExp ~break level venv tenv) else', then_ty)
          with
          | None, Unit -> { exp = T.ifThenElse test_exp then_exp bx; ty = Unit }
          | None, _ -> error pos TypeMismatch ~msg:"if-then returns non unit"
          | Some { ty = else_ty; exp = else' }, _ ->
              if var_type_for then_ty else_ty then
                { exp = T.ifThenElse ~else' test_exp then_exp bx; ty = then_ty }
              else
                error pos TypeMismatch
                  ~msg:"then and else branches must be same type")
      | _ -> error pos TypeMismatch ~msg:"Test expression must be type int"
    and whileExp pos test body =
      let label = Temp.newlabel () in
      match transExp ~break level venv tenv test with
      | { exp = cond; ty = Int; _ } -> (
          match transExp ~break:(Some label) level venv tenv body with
          | { exp = body; ty = Unit } ->
              { exp = T.whileLoop ~done':label ~cond ~body; ty = Unit }
          | _ -> error pos TypeMismatch ~msg:"Body of while not unit")
      | _ -> error pos TypeMismatch
    and forExp pos var escape lo hi body =
      let access = T.allocLocal level `Unboxed !escape in
      let venv = S.enter var E.(VarEntry { access; ty = Int }) venv in
      let label = Temp.newlabel () in
      match
        ( transExp ~break level venv tenv lo,
          transExp ~break level venv tenv hi,
          transExp ~break:(Some label) level venv tenv body )
      with
      | ( { ty = Int; exp = lo },
          { ty = Int; exp = hi },
          { ty = Unit; exp = body } ) ->
          let exp =
            T.forLoop ~done':label ~lo_access:access ~level ~lo ~hi ~body
          in
          { exp; ty = Unit }
      | _ -> error pos TypeMismatch
    and letExp _pos decs body =
      let venv, tenv, expl =
        List.fold_right
          (fun dec (venv, tenv, expl) ->
            match transDec ~break level venv tenv dec with
            | { venv; tenv; exp = None } -> (venv, tenv, expl)
            | { venv; tenv; exp = Some exp } -> (venv, tenv, exp :: expl))
          decs (venv, tenv, [])
      in

      let { exp; ty } = transExp ~break level venv tenv body in

      (* T.procEntryExit level @@ T.return (T.sequence expl ~last:exp); *)
      { exp = T.sequence (List.rev expl) ~last:exp; ty }
    and arrayExp pos typ size init =
      match S.look typ tenv with
      | None -> error pos SymbolNotFound ~msg:"arrayExp"
      | Some (Name (_, { contents = Some (Array (ty, _)) }) as arr) -> (
          match
            ( transExp ~break level venv tenv size,
              transExp ~break level venv tenv init )
          with
          | { ty = Int; exp = len }, { ty = ty'; exp } ->
              if equal_ty ty ty' then
                { exp = T.array ~len (boxstat_of_type ty) exp; ty = arr }
              else
                error pos TypeMismatch
                  ~msg:"Array type not match type expression"
          | _ -> error pos TypeMismatch ~msg:"Index not integer")
      | _ -> error pos TypeMismatch ~msg:"Not array type"
    in
    aux exp

  and transVar (level : T.level) (venv : venv) (tenv : tenv) (var : A.var) :
      expty =
    let rec aux var =
      match var with
      | A.SimpleVar (symbol, pos) -> simpleVar pos symbol
      | A.FieldVar (var, symbol, pos) -> fieldVar pos symbol var
      | A.SubscriptVar (var, exp, pos) -> subscriptVar pos exp var
    and simpleVar pos symbol =
      match S.look symbol venv with
      | None -> error pos SymbolNotFound ~msg:"simpleVar"
      | Some (E.FunEntry _) -> error pos TypeMismatch ~msg:"Must be var type"
      | Some (E.VarEntry { ty; access }) ->
          let exp = T.simpleVar access level in
          { exp; ty }
    and fieldVar pos symbol var =
      match aux var with
      | { exp = var; ty = Name (_, { contents = Some (Record (fields, _)) }) }
        -> (
          match
            listfind
              (fun (s, ty) -> if S.equal symbol s then Some ty else None)
              fields
          with
          | None -> error pos FieldNotExist
          | Some (idx, ty) -> { exp = T.ptrFieldVar ~var ~idx; ty })
      | _ -> error pos NotRecord
    and is_integer pos exp =
      match transExp ~break:None level venv tenv exp with
      | { ty = Int; _ } -> true
      | _ -> error pos ArrayIndexNotInt
    and subscriptVar pos exp var =
      match aux var with
      | { exp = var; ty = Name (_, { contents = Some (Array (ty, _)) }) } ->
          if not @@ is_integer pos exp then error pos ArrayIndexNotInt
          else
            let { exp = idx; ty = _ } =
              transExp ~break:None level venv tenv exp
            in
            { exp = T.ptrSubscribeVar ~var ~idx; ty }
      | _ -> error pos NotArray
    in
    aux var

  and transDec ~(break : Temp.label option) (level : T.level) (venv : venv)
      (tenv : tenv) (dec : A.dec) : expenv =
    let rec get_fields tenv = function
      | [] -> []
      | A.{ name; typ; pos; _ } :: tl -> (
          match S.look typ tenv with
          | None -> error pos SymbolNotFound ~msg:"get_fields"
          | Some ty -> (name, ty) :: get_fields tenv tl)
    in

    let get_ty pos tenv symbol =
      match S.look symbol tenv with
      | None -> error pos SymbolNotFound ~msg:"get_ty"
      | Some ty -> ty
    in

    let rec aux dec =
      match dec with
      | A.VarDec { name; typ; init; pos; escape } ->
          (* print_endline ( ">>>>> " ^ A.show_exp init); *)
          let venv, exp = trVarDec pos venv tenv name typ init escape in
          { exp; venv; tenv }
      | A.TypeDec type_decs ->
          { exp = None; venv; tenv = trTypeDecs tenv type_decs }
      | A.FunctionDec func_decs ->
          { exp = None; venv = trFunDecs venv tenv func_decs; tenv }
    and trVarDec pos venv tenv name typ init escape : venv * T.exp option =
      (* let access = T.allocLocal  *)
      let { exp; ty } = transExp ~break level venv tenv init in

      let access = T.allocLocal level (boxstat_of_type ty) !escape in
      let exp = T.assignVar access level exp (boxstat_of_type ty) in

      match Option.map (fun (t, _) -> (S.look t tenv, pos)) typ with
      | Some (None, _) -> error pos SymbolNotFound ~msg:"Type is not defined"
      | Some (Some ty', pos) ->
          if var_type_for ty' ty then
            (S.enter name (E.VarEntry { access; ty = ty' }) venv, Some exp)
          else error pos TypeMismatch
      | None ->
          ( (match ty with
            | Nil -> error pos NilAssign
            | _ -> S.enter name (E.VarEntry { access; ty }) venv),
            Some exp )
    and trFunDecs (venv : venv) tenv func_decs =
      let func_params =
        List.map
          (fun (A.{ funname; params; result; _ } as fun_dec) ->
            let label = Temp.namedLabel (S.name funname) in
            let level =
              T.newLevel level label
                (List.map
                   (fun A.{ escape; typ; pos; _ } ->
                     ( !escape,
                       match S.look typ tenv with
                       | Some t -> boxstat_of_type t
                       | None ->
                           error pos SymbolNotFound ~msg:"Type is not defined"
                     ))
                   params)
            in
            let params = get_fields tenv params in
            let formals = List.map snd params in

            let result =
              Option.map (fun (s, pos) -> get_ty pos tenv s) result
              |> Option.value ~default:Unit
            in

            ( fun_dec,
              E.FunEntry { formals; result; level; label; externfun = false },
              params ))
          func_decs
      in
      let _ =
        Option.map
          (fun (A.{ funpos; _ }, _, _) -> error funpos DoubleDefRecursiveType)
          (check_doubles
             ~symfun:(fun (A.{ funname; _ }, _, _) -> funname)
             func_params)
      in

      let venv =
        List.fold_right
          (fun (A.{ funname; _ }, func, _) venv -> S.enter funname func venv)
          func_params venv
      in

      List.iter
        (function
          | A.{ body; funpos; _ }, E.FunEntry { level; result; _ }, params ->
              let formals = T.formals level in
              let venv =
                List.map2
                  (fun (name, ty) access -> (name, E.VarEntry { access; ty }))
                  params formals
                |> E.add_list venv
              in
              let { ty; exp = body } = transExp ~break level venv tenv body in
              if equal_ty ty result then
                let exp =
                  match result with Unit -> body | _ -> T.return body
                in
                T.procEntryExit level exp
              else
                error funpos TypeMismatch
                  ~msg:"Function return type not match type of function body"
          | _ -> assert false)
        func_params;
      venv
    and trTypeDecs tenv type_decs =
      let check_cycle A.{ typos; _ } (name, orig_ty) =
        let rec aux ty =
          match ty with
          | Name (_, { contents = Some (Name (name', _) as ty') }) ->
              if S.equal name name' then error typos TypeCycleDetect
              else aux ty'
          | Name (_, { contents = Some (Array _) })
          | Name (_, { contents = Some (Record _) }) ->
              ty
          | Name (_, { contents = Some ty }) -> ty
          | Name (_, { contents = None }) -> assert false
          | ty -> ty
        in
        ( name,
          match aux orig_ty with
          | Name (_, ({ contents = Some (Array (ty, uniq)) } as r)) as arr ->
              r := Some (Array (aux ty, uniq));
              arr
          | Name (_, ({ contents = Some (Record (fields, uniq)) } as r)) as rcd
            ->
              let fields = List.map (fun (s, ty) -> (s, aux ty)) fields in
              r := Some (Record (fields, uniq));
              rcd
          | ty -> ty )
      in

      let trTypeDec tenv A.{ ty; _ } (_, t) =
        match t with
        | Name (_, ({ contents = None } as ty')) -> (
            match ty with
            | A.NameTy (s, pos) -> ty' := Some (get_ty pos tenv s)
            | A.RecordTy fields ->
                ty' := Some (Record (get_fields tenv fields, ref ()))
            | A.ArrayTy (s, pos) ->
                ty' := Some (Array (get_ty pos tenv s, ref ())))
        | _ -> assert false
      in

      let new_types =
        List.map
          (fun A.{ tyname; _ } -> (tyname, Name (tyname, ref None)))
          type_decs
      in

      let tenv' = E.add_list tenv new_types in
      let _ =
        Option.map
          (fun A.{ typos; _ } -> error typos DoubleDefRecursiveType)
          (check_doubles ~symfun:(fun A.{ tyname; _ } -> tyname) type_decs)
      in
      List.iter2 (trTypeDec tenv') type_decs new_types;
      List.map2 check_cycle type_decs new_types |> E.add_list tenv
    in

    aux dec

  (* let transTy (tenv : tenv) (ty : A.ty) : ty = Int *)

  let transProg ast =
    (* print_endline (">> ast >> " ^ A.show_exp ast); *)
    Findescape.findEscape ast;
    let outermost = T.outermost () in
    let exp =
      match
        transExp ~break:None outermost (E.base_venv outermost) E.base_tenv ast
      with
      | { ty = Unit; exp } -> exp
      | { exp; _ } -> T.return exp
    in
    T.procEntryExit outermost exp;
    T.getResult ()
end
