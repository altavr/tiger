module Temp = Support.Temp

type bx = Boxed | Unboxed | SPointer | HPointer
(* | Register *)
(* | SLink  *)
[@@deriving show { with_path = false }, eq]

type exp =
  | Const of int
  | Name of Temp.label
  | Temp of Temp.t
  | Binop of binop * exp * exp
  | Mem of exp * bx
  | Call of exp * exp list * bx * bool
  | Eseq of stm * exp
[@@deriving show { with_path = false }, eq]

and stm =
  | Move of exp * exp
  | Exp of exp
  | Jump of exp * Temp.label list
  | CJump of relop * exp * exp * Temp.label * Temp.label
  | Seq of stm * stm
  | Label of Temp.label

and binop =
  | Plus
  | Minus
  | Mul
  | Div
  | And
  | Or
  | Lshift
  | Rshift
  | Arshift
  | Xor

and relop = Eq | Ne | Lt | Gt | Le | Ge | Ult | Ule | Ugt | Uge

let notRel = function
  | Eq -> Ne
  | Ne -> Eq
  | Lt -> Ge
  | Gt -> Le
  | Le -> Gt
  | Ge -> Lt
  | Ult -> Uge
  | Ule -> Ugt
  | Ugt -> Ule
  | Uge -> Ult

let invertRel = function
  | Lt -> Ge
  | Gt -> Le
  | Le -> Gt
  | Ge -> Lt
  | Ult -> Uge
  | Ule -> Ugt
  | Ugt -> Ule
  | Uge -> Ult
  | x -> x

let rec bx_of_boxed : 'a. ([< Temp.boxed ] as 'a) -> bx = function
  | `Register | `SLink -> SPointer
  | `Boxed -> Boxed
  | `Unboxed -> Unboxed
  | `Callesaved t' -> bx_of_boxed @@ Temp.boxed t'
  | `HPointer _ -> HPointer
  | `Offset -> assert false

let rec folding = function
  | Binop (Plus, Const e1, Const e2) -> Const (e1 + e2)
  | Binop (Minus, Const e1, Const e2) -> Const (e1 - e2)
  | Binop (Mul, Const e1, Const e2) -> Const (e1 * e2)
  | Binop (Div, Const e1, Const e2) -> Const (e1 / e2)
  | Binop (op, e1, e2) -> Binop (op, folding e1, folding e2)
  | Mem (e, b) -> Mem (folding e, b)
  | Call (e, el, b, ca) -> Call (e, List.map folding el, b, ca)
  | Eseq (s, e) -> Eseq (sfolding s, folding e)
  | x -> x

and sfolding = function
  | Move (e1, e2) -> Move (folding e1, folding e2)
  | CJump (relop, e1, e2, l1, l2) ->
      CJump (relop, folding e1, folding e2, l1, l2)
  | Seq (s1, s2) -> Seq (sfolding s1, sfolding s2)
  | Exp e -> Exp (folding e)
  | x -> x

let rec exp_boxed = function
  | Const _ | Name _ -> `Unboxed
  | Temp t -> Temp.boxed t
  | Binop ((Plus as op), e1, e2) | Binop ((Minus as op), e1, e2) -> (
      let e1' = folding e1 in
      let e2' = folding e2 in

      match (e1', exp_boxed e1', e2', exp_boxed e2') with
      | _, `Unboxed, _, `Unboxed -> `Unboxed
      | _, `Boxed, Const x, _ | Const x, _, _, `Boxed -> (
          match op with
          | Plus -> `HPointer (x, None)
          | Minus -> `HPointer (-x, None)
          | _ -> assert false)
      | _, `Boxed, Temp t, _ | Temp t, _, _, `Boxed ->
          if Temp.boxed t = `Offset then
            match op with
            | Plus -> `HPointer (0, Some t)
            | Minus -> `HPointer (0, Some t)
            | _ -> assert false
          else assert false
      | _, `Register, _, `Unboxed | _, `Unboxed, _, `Register -> `Unboxed
      | _ -> assert false)
  | Binop _ -> `Unboxed
  | Mem (_, Boxed) -> `Boxed
  | Mem (_, Unboxed) | Mem (_, SPointer) ->
      `Unboxed
      (* (
         match exp_boxed exp with
         |   SPointer | HPointer -> x
         | _ ->
             print_endline @@ " >>>>>>>>" ^ show_exp exp';
             print_string "\n_______________________________\n";
             failwith "Value dereference") *)
  | Call (_, _, Boxed, _) -> `Boxed
  | Call (_, _, Unboxed, _) -> `Unboxed
  | Eseq (s, e) ->
      boxed_check s;
      exp_boxed e
  | _ -> assert false

and boxed_check s =
  let check e = exp_boxed e |> ignore in
  match s with
  | Move (e1, e2) ->
      check e1;
      check e2
  | Exp e -> check e
  | Jump (e, _) -> check e
  | CJump (_, e1, e2, _, _) ->
      check e1;
      check e2
  | Seq (s1, s2) ->
      boxed_check s1;
      boxed_check s2
  | Label _ -> ()

(* let boxed_of_bx = function
   | Ubx -> `Unboxed
   | Bx -> `Boxed
   | Ptr Bx -> `Ptr
   | _ -> assert false *)

let rec seq = function
  (* let rec aux = function *)
  | [] -> Exp (Const 0)
  | [ s ] -> s
  | s :: (_ :: _ as tl) -> Seq (s, seq tl)
(* in
   match sl with [] -> last | _ :: _ -> f (aux sl) last *)

let eseq main last = Eseq (seq main, last)

type stmlist = stm list [@@deriving show { with_path = false }]

type explist = exp list [@@deriving show { with_path = false }]

let%test_module _ =
  (module struct
    [@@@warning "-37"]

    let%expect_test _ =
      let s =
        seq [ Exp (Const 0); Exp (Const 1); Exp (Const 2); Exp (Const 3) ]
      in
      print_string @@ show_stm s;
      [%expect
        {|
      (Seq ((Exp (Const 0)),
         (Seq ((Exp (Const 1)), (Seq ((Exp (Const 2)), (Exp (Const 3)))))))) |}]
  end)
