module S = Absttree.Symbol
open Absttree.Ast

type depth = int

type env = (depth * bool ref) S.table

(* [@@@warning "-27-39-8"] *)

let rec traverseExp (env : env) (d : depth) (s : exp) : unit =
  match s with
  | VarExp var -> traverseVar env d var
  | NilExp | IntExp _ | StringExp _ -> ()
  | CallExp { args; _ } -> List.iter (traverseExp env d) args
  | OpExp { left; right; _ } ->
      traverseExp env d left;
      traverseExp env d right
  | RecordExp { fields; _ } ->
      List.iter (fun (_, exp, _) -> traverseExp env d exp) fields
  | SeqExp el -> List.iter (traverseExp env d) el
  | AssignExp { var; exp; _ } ->
      traverseVar env d var;
      traverseExp env d exp
  | IfExp { test; then'; else'; _ } ->
      traverseExp env d test;
      traverseExp env d then';
      Option.iter (fun exp -> traverseExp env d exp) else'
  | WhileExp { test; body; _ } ->
      traverseExp env d test;
      traverseExp env d body
  | ForExp { var; escape; lo; hi; body; _ } ->
      escape := false;
      let env = S.enter var (d, escape) env in
      traverseExp env d lo;
      traverseExp env d hi;
      traverseExp env d body
  | BreakExp _ -> ()
  | LetExp { decs; body; _ } ->
      let env = traverseDecs env d decs in
      traverseExp env d body
  | ArrayExp { size; init; _ } ->
      traverseExp env d size;
      traverseExp env d init

and traverseVar (env : env) (d : depth) (s : var) : unit =
  match s with
  | SimpleVar (var, _) -> (
      match S.look var env with
      | None -> assert false
      | Some (d', esc) ->
          (* Printf.printf "sym - %s depth env - %d; depth define - %d\n"
            (S.name var) d d'; *)
          if d > d' then esc := true)
  | FieldVar (var, _, _) -> traverseVar env d var
  | SubscriptVar (var, exp, _) ->
      traverseVar env d var;
      traverseExp env d exp

and traverseDecs (env : env) (d : depth) (s : dec list) : env =
  let trDec env dec =
    let trParams env d { name; escape; _ } : env =
      escape := false;
      S.enter name (d + 1, escape) env
    in

    let trFuncdec { params; body; _ } =
      let env = List.fold_right (fun p env -> trParams env d p) params env in
      traverseExp env (d + 1) body
    in

    match dec with
    | FunctionDec fdc ->
        List.iter trFuncdec fdc;
        env
    | VarDec { name; escape; init; _ } ->
        escape := false;
        traverseExp env d init;
        S.enter name (d, escape) env
    | TypeDec _ -> env
  in

  List.fold_right (fun dec env -> trDec env dec) s env

let findEscape (prog : exp) : unit = traverseExp S.empty 0 prog
