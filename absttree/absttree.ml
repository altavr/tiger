module Symbol = Support.ASymbol
module TokenInfo = Support.TokenInfo
module Interval = Support.Interval
module Ast = Abstsyn
module Parsing = Driver.Parse

module Parser = Parser
module Lexer = Lexer