open OUnit2
open Absttree.Ast

module P = Absttree.Parsing

 
let test_string_tokens  =
  List.map @@ fun (fn, expected) ->
  fn >:: fun ctx ->
  let test_file = OUnitTestData.in_testdata_dir ctx.OUnitTest.conf [ fn ] in
  assert_equal  ~cmp:equal_exp ~printer:show_exp ( P.run test_file) expected

let tests =
  test_string_tokens 
    [
      ( "test.txt", NilExp );  
    ( "test2.txt", NilExp);
      ]

let () = "Parser tests" >::: tests |> run_test_tt_main
