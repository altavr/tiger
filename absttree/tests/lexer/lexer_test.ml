open OUnit2
module P = Absttree.Parser
module L = Absttree.Lexer

exception StopExeption

type token = [%import: Absttree.Parser.token] [@@deriving show { with_path = false }, eq   ]

type token_lst = token list [@@deriving show, eq]

type string_lst = string list [@@deriving show, eq]

let ei = Absttree.Interval.empty

let tokens gen fn =
  let f = open_in fn in
  let lexbuf = Lexing.from_channel f in
  let rec aux ts = try aux (gen lexbuf :: ts) with StopExeption -> List.rev ts in
  aux []

let string_token lexbuf =
  match L.main lexbuf with
  | P.STRING i -> i.v
  | P.EOF _ -> raise StopExeption
  | _ -> failwith "Unexpected token"

let string_tokens = tokens string_token

let misc_token lexbuf = 
    match L.main lexbuf with 
    | P.EOF _ -> raise StopExeption
    | i -> i 


let test_tokens ~tt ~cmp ~printer =
  List.map @@ fun (fn, expected) ->
  fn >:: fun ctx ->
  let test_file = OUnitTestData.in_testdata_dir ctx.OUnitTest.conf [ fn ] in
  assert_equal ~cmp ~printer (tt test_file) expected

let tests_string =
  test_tokens ~tt:string_tokens ~cmp:equal_string_lst ~printer:show_string_lst
    [ ("test.txt", [ "aergaegeagre t ht" ]) ]


  let tests_misc =
    test_tokens ~tt:( tokens  misc_token ) ~cmp:equal_token_lst ~printer:show_token_lst
      [ ("test2.txt", [(ID { i = ei ; v = "a" }); (LBRACK ei);
           (ID { i = ei; v = "b" }); (RBRACK ei )]) ]

let () = "Lexer tests" >::: tests_string @ tests_misc |> run_test_tt_main
