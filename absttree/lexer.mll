{

    open Parser



let reservedWords = [
  ((fun x ->TYPE x ) , "type")
;  ((fun x ->VAR  x ), "var")
;  ((fun x ->FUNCTION x ), "function")
;  ((fun x ->BREAK x), "break")
;  ((fun x ->OF x), "of")
;  ((fun x ->END x), "end")
;  ((fun x ->IN  x ), "in")
;  ((fun x ->NIL x), "nil")
;  ((fun x ->LET x),  "let")
;  ((fun x ->DO x), "do")
;  ((fun x ->TO x), "to")
;  ((fun x ->FOR x), "for")
;  ((fun x ->WHILE x ), "while")
;  ((fun x ->ELSE x ), "else")
;  ((fun x ->THEN x), "then")
;  ((fun x ->IF x), "if")
;  ((fun x ->ARRAY x), "array")
;  ((fun x ->ASSIGN x ), ":=")
;  ((fun x ->OR x), "|")
;  ((fun x ->AND x), "&")
;  ((fun x ->GE x), ">=")
;  ((fun x ->GT x), ">")
;  ((fun x ->LE x), "<=")
;  ((fun x ->LT x), "<")
;  ((fun x ->NEQ x), "<>")
;  ((fun x ->EQ x), "=")
;  ((fun x ->DIVIDE x), "/")
;  ((fun x ->TIMES x), "*")
;  ((fun x ->MINUS x), "-")
;  ((fun x ->PLUS x), "+")
;  ((fun x ->DOT x), ".")
;  ((fun x ->RBRACE x), "}")
;  ((fun x ->LBRACE x), "{")
;  ((fun x ->RBRACK x), "]")
;  ((fun x ->LBRACK x), "[")
;  ((fun x ->RPAREN x), ")")
;  ((fun x ->LPAREN x), "(")
;  ((fun x ->SEMICOLON x), ";")
;  ((fun x ->COLON x), ":")
;  ((fun x ->COMMA x), ",")

  ] 

open Support

type token_constr_fun = TokenInfo.info -> token

let (symbolTable : (string, token_constr_fun) Hashtbl.t)  = Hashtbl.create 1024

let _ = List.iter (fun (f, v) -> Hashtbl.add symbolTable v f ) reservedWords



let createInfo lexbuf =  
    let open Lexing in 
    let sp = lexeme_start_p lexbuf in 
    let ep = lexeme_end_p lexbuf in 
    Interval.create sp ep

let text  = Lexing.lexeme

let createID i v : token = 
    try  Hashtbl.find symbolTable v i with 
    _ -> ID ( TokenInfo.create i v  )

let stringbuf = Buffer.create 1024

}


let white = [ ' ' '\t' ]+

rule main =
    parse  
    | white {main lexbuf }
    | '\n' { Lexing.new_line lexbuf; main lexbuf }
    | "*/" { TokenInfo.error  ( createInfo lexbuf)  "Illegal character"  }
    | ['A'-'Z' 'a'-'z' '_' ] 
        [ 'A'-'Z' 'a'-'z' '_'  '0'-'9' ]* { createID  ( createInfo lexbuf ) @@ text lexbuf }
    | ":=" | ">=" | ">" |"<=" |"<"| "<>" | "=" |"/" |"*" | "-" | "+" | "." 
    | "}" | "{" | "]" | "[" | ")" |  "(" | ";" | ":" | "," | "&" | "|"
        { createID (createInfo lexbuf ) @@ text lexbuf  }
    | ['0'-'9']+ {INT TokenInfo.{i= createInfo lexbuf; v=  int_of_string  @@ text lexbuf  } }
    | '"' { Buffer.clear stringbuf; string (createInfo lexbuf) lexbuf }
    | eof { EOF ( createInfo lexbuf ) }
    | "/*" {  comment 1 lexbuf }
    | _ { TokenInfo.error  ( createInfo lexbuf)  "Illegal character"  }

and string info =
    parse 
    | '"' { STRING {i = Interval.union info @@ createInfo lexbuf;
             v= Buffer.contents stringbuf}   }
    | '\n' {  Lexing.new_line lexbuf; string info lexbuf }
    | '\\' {  Buffer.add_char stringbuf @@ escaped info lexbuf; string info lexbuf } 
    | [ ^ '\\' '\"' '\n' ]* { Buffer.add_string stringbuf @@ text lexbuf; string info lexbuf }
    | eof { TokenInfo.(error info "String not terminated" ) }

and escaped info =
    parse 
    | 'n' {  '\n'}
    | 't' {  '\t' }
    | '\\' { '\\'}
    | eof { TokenInfo.(error info "String not terminated" ) }
    | _ { Lexing.lexeme_char lexbuf 0}

and comment level = 
    parse 
    | "*/" { if level = 1 then  main lexbuf else comment (level -1 ) lexbuf  }
    | "/*"  {  comment (level + 1 ) lexbuf  }
    | [^ '*' '/' ]+ |  '/' | '*'   { comment level lexbuf }
    | eof { TokenInfo.(error Interval.empty "Comment not terminated" )  }