%{
    open Abstsyn
    module T = Support.TokenInfo
    module S = Support.ASymbol
    module I = Support.Interval

     let symbol T.{i; v} = S.symbol v, i

%}

%token <string Support.TokenInfo.t> ID
%token <int Support.TokenInfo.t> INT
%token <string Support.TokenInfo.t > STRING

%token <Support.TokenInfo.info> TYPE
 VAR 
 FUNCTION
 BREAK
 OF
 END
 IN
 NIL
 LET
 DO
 TO
 FOR
 WHILE
 ELSE
 THEN
 IF
 ARRAY
 ASSIGN
 OR
 AND
 GE
 GT
 LE
 LT
 NEQ
 EQ
 DIVIDE
 TIMES
 MINUS
 PLUS
 DOT
 RBRACE
 LBRACE
 RBRACK
 LBRACK
 RPAREN
 LPAREN
 SEMICOLON
 COLON
 COMMA
 EOF

%nonassoc LOWEST

%nonassoc ASSIGN DO OF FUNCTION TYPE
// %right  THEN
// %right FUNC
%right  THEN ELSE
%right OR
%right AND
%nonassoc GE GT LE LT NEQ EQ
%left PLUS MINUS
%left TIMES DIVIDE
%left UMINUS


%start < exp >  main

%%

main:  
    | e = exp;  EOF {  e  } 

decs:
    | { [] }
    | d = dec ; dl = decs {  d :: dl  }

dec: 
    | tl = tydecs { TypeDec tl } %prec LOWEST
    | d = vardec { d } 
    | fl = fundecs { FunctionDec  fl  } %prec LOWEST


fundecs: 
    | f = fundec { [f] }
    | fl =  fundecs ; f = fundec  {  fl @ [f]  }


fundec : 
    | kw = FUNCTION; id = ID; LPAREN; params = tyfields; RPAREN; EQ; body = exp 
        { {funname = S.symbol id.T.v; 
            params; 
            result = None;
            body =  body;
            funpos = I.union kw ( position_exp  body ) }
         }

    |  kw = FUNCTION; id = ID; LPAREN; params = tyfields; RPAREN; COLON; ty = ID; EQ; body = exp 
        { let  ( res_ty, pos) = symbol ty 
        in {funname = S.symbol id.T.v; 
            params; 
            result = Some (  res_ty, pos);
            body =  body;
            funpos = I.union kw ( position_exp  body )
            }
         }

tydecs : 
    | t = tydec { [t] }
    | tl = tydecs; t = tydec  { tl @ [t] }

tydec: 
    | kw = TYPE; id = ID; EQ; ty =  ty 
      {     
            { tyname = S.symbol id.T.v; ty; typos=I.union kw  ( position_ty ty)  }  }

ty: 
    | id = ID 
        { let (v, pos )= symbol id  in
              NameTy ( v , pos ) } 

    | LBRACE; tfl =  tyfields; RBRACE { RecordTy tfl } 
    | kw = ARRAY; OF; id = ID { ArrayTy (S.symbol id.T.v, I.union kw id.T.i  ) }

tyfields: 
    | { [] } 
    | tf = tyfield { [tf]}
    | tf = tyfield; COMMA; tfl = tyfields { tf :: tfl }

tyfield: id = ID; COLON; ty = ID 
        {  let  (name,pos) = symbol id 
        in { name; escape = ref true ; typ = S.symbol ty.T.v; pos = I.union pos ty.T.i } }

vardec: 
    | var = VAR; id = ID; ASSIGN; e =  exp 
       { VarDec 
            {name= S.symbol id.T.v;
            escape = ref true; 
            typ = None; 
            init =  e;
             pos = I.union var  ( position_exp  e ) } }

    |   var = VAR; id = ID; COLON; ty = ID; ASSIGN; e = exp 
        { let (ty, ty_pos) = symbol ty 
          in  VarDec 
            {name= S.symbol id.T.v;
            escape = ref true;
            typ = Some ( ty, ty_pos ) ;
            init=  e;
             pos = I.union var  ( position_exp e ) }  }

lvalue: 
    | id =  ID; make_var =  lvalue_tail 
        { let (v,pos) = symbol id 
         in  make_var ( SimpleVar ( v, pos ), pos )   }

lvalue_tail: 
    | { fun (x, _) -> x }

    |  DOT; id = ID ; make_var = lvalue_tail 
        { let (v, pos) = symbol id in  
        fun (var, pos_id) -> make_var ( FieldVar (var,  v,  I.union pos_id  pos), pos_id )  }

    |  LBRACK; e = exp; rb = RBRACK; make_var =  lvalue_tail 
        
        { fun (var, pos_id) -> make_var ( SubscriptVar ( var,  e, I.union pos_id  rb ), pos_id  ) } 

exp: 
    | i = INT {  IntExp i.T.v }

    | lv =  lvalue {  VarExp lv}

    | e1 = exp ;  PLUS; e2 = exp 
    { OpExp {left= e1; oper=PlusOp; right= e2; pos=I.union  ( position_exp e1 ) ( position_exp e2 ) }  }

    | e1 = exp ;  MINUS; e2 = exp 
    { OpExp {left= e1; oper=MinusOp; right= e2; pos=I.union  ( position_exp e1 ) ( position_exp e2 ) }  }

    | e1 = exp ;  TIMES; e2 = exp 
    { OpExp {left= e1; oper=TimesOp; right= e2; pos=I.union  ( position_exp e1 ) ( position_exp e2 ) }  }

    | e1 = exp ;  DIVIDE; e2 = exp 
    { OpExp {left= e1; oper=DivideOp; right= e2; pos=I.union  ( position_exp e1 ) ( position_exp e2 ) } }

    | op = MINUS; e =  exp  
     { OpExp {left= IntExp 0; oper = MinusOp; right =  e; pos = I.union op (position_exp e) }  } %prec UMINUS

    | s = STRING {  StringExp (s.T.v, s.T.i )  }

    | e1 = exp;  EQ; e2 = exp 
    { OpExp {left= e1; oper=EqOp; right= e2; pos=I.union  ( position_exp e1 ) ( position_exp e2 ) }  }

    | e1 = exp;  NEQ; e2 = exp 
    { OpExp {left= e1; oper=NeqOp; right= e2; pos=I.union  ( position_exp e1 ) ( position_exp e2 ) }  }

    | e1 = exp;  LT; e2 = exp 
    { OpExp {left= e1; oper=LtOp; right= e2; pos=I.union  ( position_exp e1 ) ( position_exp e2 ) }  }

    | e1 = exp;  GT; e2 = exp 
    { OpExp {left= e1; oper=GtOp; right= e2; pos=I.union  ( position_exp e1 ) ( position_exp e2 ) }  }

    | e1 = exp;  LE; e2 = exp 
    { OpExp {left= e1; oper=LeOp; right= e2; pos=I.union  ( position_exp e1 ) ( position_exp e2 ) }  }

    | e1 = exp; GE; e2 = exp 
    { OpExp {left= e1; oper=GeOp; right= e2; pos=I.union  ( position_exp e1 ) ( position_exp e2 ) }  }
    
    | e1 = exp ; AND; e2 = exp 
    {  IfExp { test= e1; then'= e2; else'=Some ( IntExp 0); 
                pos=I.union  ( position_exp e1 ) ( position_exp e2 ) }   }
    
    | e1 = exp;  OR; e2 = exp 
    {  IfExp { test= e1; then'= IntExp 1; else'=Some ( e2); 
        pos=I.union  ( position_exp e1 ) ( position_exp e2 ) }  }

    | LPAREN; e = expseq; RPAREN  { SeqExp (e)}
    // | LPAREN; e = exp;  RPAREN { e}
    |   NIL { NilExp }

    | id = ID; LBRACK; size = exp; RBRACK; OF; e = exp  
    {    let (typ, pos) = symbol id 
        in  ArrayExp {  typ; size =  size; init =  e;pos = I.union pos ( position_exp e ) } }
    
    | var = lvalue; ASSIGN; exp = exp 
    { AssignExp {var= var; exp= exp; pos= I.union (position_var var) ( position_exp exp)}  }

    | kw = IF; test= exp; THEN; e1 = exp; ELSE e2 = exp 
        { IfExp { test=  test; then'=  e1; else'=Some ( e2); pos = I.union kw (position_exp e2) }}

    | kw = IF; test= exp; THEN; e1 = exp 
        { IfExp { test=  test; then'=  e1; else'=None; pos = I.union kw (position_exp e1) } }

    | kw = WHILE; test = exp; DO; body = exp 
        { WhileExp {test=  test; body =  body; pos=I.union kw (position_exp body)}   }

    | kw = FOR; id = ID; ASSIGN; lo=exp; TO; hi=exp; DO; body=exp 
        { let (var, _) = symbol id 
           in  ForExp {var;
             escape=ref true;
              lo =  lo;
              hi =  hi;
              body =  body; 
              pos= I.union kw (position_exp body)}
            }

    | kw = BREAK { BreakExp kw }

    | kw = LET; decs = decs; IN; body = exp ; END 
        {  LetExp { decs = List.rev decs; body=  body; pos = I.union kw (position_exp body) }  } 

    | id = ID; LPAREN; args = funargs; rp = RPAREN 
        { let  (func, pos) = symbol id 
           in CallExp  {func ; args;  pos = I.union pos rp }}

    | id = ID; LBRACE; fields = recfields; rb = RBRACE 
        { let (typ, pos )= symbol id 
        in  RecordExp { fields; typ  ; pos = I.union pos rb } }

expseq:
    | {[]}
    | e = exp {[e]}
    | e = exp; SEMICOLON; el = expseq {e::el}

funargs:
    | { [] }
    | e = exp {[ e]}
    | e = exp; COMMA; el = funargs {  e :: el}


recfields:
    | {[]}
    | rf = recfield {[rf]}
    | rf = recfield; COMMA; rfl = recfields { rf :: rfl }

recfield: 
    | id = ID; EQ; e =  exp 
    { let (id, pos) = symbol id 
    in  id,  e, pos }




