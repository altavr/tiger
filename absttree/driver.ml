

module Parser = Parser
module Lexer = Lexer

module Parse = struct
  module I = Parser.MenhirInterpreter
  module E = MenhirLib.ErrorReports
  module L = MenhirLib.LexerUtil
  open Lexing

  (* [state checkpoint] extracts the number of the current state out of a checkpoint. *)

  exception SyntaxError of string 

  let state env : int =
    match I.top env with
    | Some (I.Element (s, _, _, _)) -> I.number s
    | None -> 0

  (* [show text (pos1, pos2)] displays a range of the input text [text]
     delimited by the positions [pos1] and [pos2]. *)

  let show text positions =
    E.extract text positions |> E.sanitize |> E.compress |> E.shorten 20

  (* max width 43 *)

  let get text env i =
    match I.get i env with
    | Some (I.Element (_, _, pos1, pos2)) -> show text (pos1, pos2)
    | None ->
        (* The index is out of range. This should not happen if [$i]
           keywords are correctly inside the syntax error message
           database. The integer [i] should always be a valid offset
           into the known suffix of the stack. *)
        "???"

  let fail text buffer env =
    (* Indicate where in the input file the error occurred. *)
    let location = L.range (E.last buffer) in
    (* Show the tokens just before and just after the error. *)
    let indication =
      Printf.sprintf "Syntax error %s.\n" (E.show (show text) buffer)
    in
    (* Fetch an error message from the database. *)
    let message = ParserMessages.message (state env) in
    (* Expand away the $i keywords that might appear in the message. *)
    let message = E.expand (get text env) message in
    (* Show these three components. *)
    let msg = Printf.sprintf "%s%s%s%!" location indication message in 
    raise ( SyntaxError msg ) 

  (* let parse text lexfun lexbuf (c : _ I.checkpoint) =
    let rec aux c =
      match c with
      | I.InputNeeded _ ->
          I.offer c (lexfun lexbuf, lexeme_start_p lexbuf, lexeme_end_p lexbuf)
          |> aux
      | I.Shifting _ | I.AboutToReduce _ -> I.resume c |> aux
      | I.HandlingError e -> raise @@ Failure "Fail parse"
      | I.Rejected -> raise @@ Failure "Fail parse"
      | I.Accepted v -> v
    in
    aux c
    [@@warning "-27"] *)

  let parse1 text (supplier : unit -> _ * position * position)
      (buffer : _ E.buffer) (c : _ I.checkpoint) =
    let rec aux c =
      match c with
      | I.InputNeeded _ -> I.offer c @@ supplier () |> aux
      | I.Shifting _ | I.AboutToReduce _ -> I.resume c |> aux
      | I.HandlingError e -> fail text buffer e
      | I.Rejected -> raise @@ Failure "Fail parse"
      | I.Accepted v -> v
    in
    aux c
 
  let run filename =
    (* let f = open_in filename in *)
    (* let lexbuf = from_channel f in *)
    let text, lexbuf = L.read filename in
    (* Wrap the lexer and lexbuf together into a supplier, that is, a
       function of type [unit -> token * position * position]. *)
    let supplier = I.lexer_lexbuf_to_supplier Lexer.main lexbuf in
    (* Equip the supplier with a two-place buffer that records the positions
       of the last two tokens. This is useful when a syntax error occurs, as
         these are the token just before and just after the error. *)
    let buffer, supplier = E.wrap_supplier supplier in
    (* parse text Lexer.main lexbuf (Parser.Incremental.main lexbuf.lex_curr_p) *)
    parse1 text supplier buffer (Parser.Incremental.main lexbuf.lex_curr_p)
end
