module I = Support.Interval

type pos = I.t [@@deriving show { with_path = false }, eq]

type symbol = Support.ASymbol.t [@@deriving show { with_path = false }, eq]

type var =
  | SimpleVar of symbol * pos
  | FieldVar of var * symbol * pos
  | SubscriptVar of var * exp * pos
[@@deriving show { with_path = false }, eq]

and exp =
  | VarExp of var
  | NilExp
  | IntExp of int
  | StringExp of string * pos
  | CallExp of { func : symbol; args : exp list; pos : pos }
  | OpExp of { left : exp; oper : oper; right : exp; pos : pos }
  | RecordExp of { fields : (symbol * exp * pos) list; typ : symbol; pos : pos }
  | SeqExp of exp list
  | AssignExp of { var : var; exp : exp; pos : pos }
  | IfExp of { test : exp; then' : exp; else' : exp option; pos : pos }
  | WhileExp of { test : exp; body : exp; pos : pos }
  | ForExp of {
      var : symbol;
      escape : bool ref;
      lo : exp;
      hi : exp;
      body : exp;
      pos : pos;
    }
  | BreakExp of pos
  | LetExp of { decs : dec list; body : exp; pos : pos }
  | ArrayExp of { typ : symbol; size : exp; init : exp; pos : pos }

and dec =
  | FunctionDec of fundec list
  | VarDec of {
      name : symbol;
      escape : bool ref;
      typ : (symbol * pos) option;
      init : exp;
      pos : pos;
    }
  | TypeDec of tydec list

and tydec = { tyname : symbol; ty : ty; typos : pos }

and ty =
  | NameTy of symbol * pos
  | RecordTy of field list
  | ArrayTy of symbol * pos

and oper =
  | PlusOp
  | MinusOp
  | TimesOp
  | DivideOp
  | EqOp
  | NeqOp
  | LtOp
  | LeOp
  | GtOp
  | GeOp

and field = { name : symbol; escape : bool ref; typ : symbol; pos : pos }

and fundec = {
  funname : symbol;
  params : field list;
  result : (symbol * pos) option;
  body : exp;
  funpos : pos;
}

let position_fundec { funpos; _ } = funpos

let position_tydec { typos; _ } = typos

let position_ty = function
  | NameTy (_, pos) -> pos
  | RecordTy _ -> I.empty
  | ArrayTy (_, pos) -> pos

let position_exp = function
  | VarExp _ | NilExp | IntExp _ -> I.empty
  | StringExp (_, pos) -> pos
  | CallExp { pos; _ } -> pos
  | OpExp { pos; _ } -> pos
  | RecordExp { pos; _ } -> pos
  | SeqExp _ -> I.empty
  | AssignExp { pos; _ } -> pos
  | IfExp { pos; _ } -> pos
  | WhileExp { pos; _ } -> pos
  | ForExp { pos; _ } -> pos
  | BreakExp pos -> pos
  | LetExp { pos; _ } -> pos
  | ArrayExp { pos; _ } -> pos

let position_var = function
  | SimpleVar (_, pos) -> pos
  | FieldVar (_, _, pos) -> pos
  | SubscriptVar (_, _, pos) -> pos

let position_dec = function
  | FunctionDec _ -> I.empty
  | VarDec { pos; _ } -> pos
  | TypeDec _ -> I.empty
