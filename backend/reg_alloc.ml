module Temp = Support.Temp
module A = Assem

module type FrameType = sig
  type access

  type t

  type register

  val name : t -> Support.ASymbol.t

  val pp_register : Format.formatter -> register -> unit

  val allocLocal :
    ?spilled:Temp.t -> t -> [< `Boxed | `Unboxed ] -> bool -> access

  val load_var : access -> Temp.t -> A.instr

  val save_var : access -> Temp.t -> A.instr

  val registers : register list

  val rv : Temp.t

  val sp : Temp.t

  val fp : Temp.t

  val calleesaves : Temp.t list

  module TempMap = Support.Temp.Map

  module RegMap : Map.S with type key = register

  val tempMap : register TempMap.t

  val regMap : Temp.t RegMap.t

  val compare_register : register -> register -> int

  val push_calleesave : t -> Temp.t -> A.instr

  val pop_calleesave : t -> Temp.t -> A.instr

  val string_of_register : register -> string
end

module Make (F : FrameType) = struct
  module DLL = Support.DoubleLinkedList
  module IG = Igraph
  module TM = F.TempMap

  module RM = struct
    include F.RegMap

    type 'a u = (F.register * 'a) list [@@deriving show { with_path = false }]

    let pp af f x = pp_u af f @@ bindings x

    let show af = Support.make_show_1 af pp
  end

  type allocation = F.register F.TempMap.t

  type instr = A.instr * Temp.t list

  let draw_digraph =
    Support.make_draw_graph
      (Flow.DGraph.to_dot ~global:""
         ~show_node:(fun data -> (Flow.Node.show data, None))
         ~show_edge:(fun _ -> Some ""))
      "/home/alex/documents/ocaml/ed_compiler/tiger/tests/graphtest"

  let draw_graph =
    Support.make_draw_graph
      (IG.to_dot ~show_temp:Temp.show)
      "/home/alex/documents/ocaml/ed_compiler/tiger/tests/graphtest"

  type b_u =
    [ `Boxed
    | `Unboxed
    | `Callesaved of Support.Temp.t
    | `HPointer of int * Temp.t option ]

  let rewrite (frame : F.t) (imap : A.instr DLL.t list TM.t)
      (spilled : Temp.t list) =
    let inc_change access t instr_node imap =
      let newtemp = Temp.move_from t in
      let instr = DLL.value instr_node in
      let dir, instr' =
        match A.find_temp t instr with
        | `Dst ->
            DLL.insert_after instr_node @@ F.save_var access newtemp;
            (`Dst, DLL.next instr_node)
        | `Src ->
            DLL.insert_before instr_node @@ F.load_var access newtemp;
            (`Src, DLL.prev instr_node)
      in
      DLL.set instr_node @@ A.replace_temp ~that:t ~other:newtemp instr dir;
      TM.add newtemp [ instr_node; instr' ] imap
    in

    let spill_cs frame t instr_node =
      let instr = DLL.value instr_node in
      match A.find_temp t instr with
      | `Dst ->
          DLL.replace_value instr_node (fun _ -> F.push_calleesave frame t)
      | `Src -> DLL.replace_value instr_node (fun _ -> F.pop_calleesave frame t)
    in
    List.fold_left
      (fun imap t ->
        print_endline (">>>> SPILL " ^ Temp.show t);
        (* let boxed =
             match Temp.boxed t with
             | `Register | `SLink -> assert false
             | `Offset -> `Unboxed
             | #b_u as x -> x
           in *)
        match Temp.boxed t with
        | `Callesaved _ ->
            List.iter (spill_cs frame t) (TM.find t imap);
            TM.remove t imap
        | _ ->
            let access = F.allocLocal ~spilled:t frame `Unboxed true in
            List.fold_left
              (fun imap instr_node -> inc_change access t instr_node imap)
              imap (TM.find t imap)
            |> TM.remove t)
      imap spilled
    |> ignore

  let register_of_temp t = F.TempMap.find t F.tempMap

  let color frame instr =
    let regcolor, _ =
      List.fold_left
        (fun (m, i) r -> (RM.add r i m, i + 1))
        (RM.empty, 0) F.registers
    in
    let tempcolor =
      TM.fold (fun t r m -> TM.add t (RM.find r regcolor) m) F.tempMap TM.empty
    in

    let calc_interfere () =
      let ((flowgraph, _) as flow) =
        Flow.instr2graph ~size:256 F.[ rv; sp; fp ] instr
      in
      let igraph, tmap =
        Liveness.interferenceGraph ~size:256 (List.length F.registers) [] flow
      in

      (flowgraph, igraph, tmap)
    in
    let find_cs_related tmap =
      TM.fold
        (fun t _ l ->
          match Temp.boxed t with `Callesaved t' -> (t, t') :: l | _ -> l)
        tmap []
      |> List.sort (fun (_, x) (_, y) ->
             F.compare_register (register_of_temp y) (register_of_temp x))
      (* |> List.map (fun (t, t') ->
             print_endline @@ F.string_of_register @@ register_of_temp t';
             (t, t')) *)
      |> List.map fst
    in

    let rec aux flowgraph igraph tmap cs_related k =
      Printf.printf "Pass %d\n" k;

      if k > 5 then failwith "Registers allocation failed";

      (* draw_digraph _g ("reg_alloc_flow_" ^ string_of_int k  ); *)
      (* draw_graph  igraph ( "reg_alloc_igraph_"  ^ string_of_int k  )  ; *)

      (* IG.nodes igraph
         |> List.iter ( fun n ->
             Printf.printf ">> %s: " ( IG.temp n |> Temp.show );
             List.iter ( fun m ->
               Printf.printf "%s, " ( IG.temp m |> Temp.show)
                ;

             ) ( IG.adj n ) ;
             print_newline ();
           )  ; *)
      let spilled =
        Color.color (List.length F.registers)
          (fun t -> TM.find_opt t tempcolor)
          igraph tmap cs_related
      in

      (* print_endline ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.";
         DLL.fold (fun i _ ->
             print_endline @@ A.format (fun t -> Temp.show  t ) i   ) instr   () ; *)

      (* draw_digraph flowgraph
             ("reg_alloc_flow_" ^ string_of_int k ^ "_" ^ Support.ASymbol.name
            @@ F.name frame);
         draw_graph  igraph ( "reg_alloc_igraph_color_"  ^ string_of_int k
            ^ "_" ^ Support.ASymbol.name @@ F.name frame )  ; *)
      if Support.List.is_empty spilled then
        ( (* IG.nodes igraph
             |> List.iter ( fun n ->
                 Printf.printf ">> %s: " ( IG.temp n |> Temp.show );
                 List.iter ( fun m ->
                   Printf.printf "%s, " ( IG.temp m |> Temp.show)
                    ;

                 ) ( IG.adj n ) ;
                 print_newline ();
               )  ; *)
          instr,
          igraph,
          flowgraph )
      else (
        print_endline "spilled!!";

        rewrite frame (A.tempmap_of_dll instr) (List.map IG.temp spilled);
        let flowgraph, igraph, tmap = calc_interfere () in
        aux flowgraph igraph tmap cs_related (k + 1))
    in
    let flowgraph, igraph, tmap = calc_interfere () in

    (* (
       IG.nodes igraph
       |> List.iter ( fun n ->
           Printf.printf ">> %s: " ( IG.temp n |> Temp.show );
           List.iter ( fun m ->
             Printf.printf "%s, " ( IG.temp m |> Temp.show );

           ) ( IG.adj n ) ;
           print_newline ();
         )

       ) ; *)
    let cs_related = ref @@ find_cs_related tmap in
    aux flowgraph igraph tmap cs_related 0
  (* [@@warning "-27-26-39"] *)

  let alloc frame instr : instr list * allocation =
    let instr, igraph, flowgraph = color frame (DLL.of_list instr) in

    let instr =
      List.fold_left
        (fun (l, niter) i ->
          match niter () with
          | Seq.Nil -> assert false
          | Cons (n, niter) ->
              ((i, Temp.Set.elements (Flow.DGraph.value n).Flow.out) :: l, niter))
        ([], Flow.DGraph.node_iter flowgraph)
      @@ DLL.map_to_list Fun.id instr
      |> fst |> List.rev
    in

    let regarr = Array.of_list F.registers in
    let allocation =
      List.fold_left
        (fun m n -> TM.add (IG.temp n) regarr.(IG.color n) m)
        TM.empty (IG.nodes igraph)
    in
    (instr, allocation)
  (* [@@warning "-27-26"] *)
end

let%test_module _ =
  (module struct
    module Frame = struct
      type access = unit

      type t = unit

      type register = string

      let pp_register = Format.pp_print_string

      let name _ = assert false

      module TempMap = Support.Temp.Map
      module RegMap = Map.Make (String)

      let allocLocal ?spilled _ _ _ =
        spilled |> ignore;
        ()

      let load_var _ t = A.test_oper [ t ] []

      let save_var _ t = A.test_oper [] [ t ]

      let registers = [ "r1"; "r2"; "r3" ]

      let make_tempMap rl =
        List.to_seq rl
        |> Seq.map (fun r -> (Temp.labeltemp r `Unboxed, r))
        |> TempMap.of_seq

      let make_regMap m =
        TempMap.fold (fun r t m' -> RegMap.add t r m') m RegMap.empty

      let tempMap = make_tempMap registers

      let regMap = make_regMap tempMap

      let rv = RegMap.find "r1" regMap

      let sp = Temp.labeltemp "sp" `Register

      let fp = Temp.labeltemp "fp" `Register

      let calleesaves = [ RegMap.find "r3" regMap ]

      let compare_register _ _ = failwith "not implemented"

      let push_calleesave _ _ = failwith "not implemented"

      let pop_calleesave _ _ = failwith "not implemented"

      let string_of_register _ = failwith "not implemented"
    end

    include Make (Frame)

    let%expect_test _ =
      let regs =
        Frame.make_tempMap [ "r1"; "r2"; "r3"; "a"; "b"; "c"; "d"; "e" ]
        |> Frame.make_regMap
      in
      let te s = RM.find s regs in
      let lab1 = Temp.newlabel () in
      let instr =
        DLL.of_list
          A.
            [
              test_move (te "c") (te "r3");
              test_move (te "a") (te "r1");
              test_move (te "b") (te "r2");
              test_oper [ te "d" ] [];
              test_move (te "e") (te "a");
              test_label lab1;
              test_oper [ te "d" ] [ te "d"; te "b" ];
              test_oper [ te "e" ] [ te "e" ];
              test_oper ~jump:[ lab1 ] [] [ te "e" ];
              test_move (te "r1") (te "d");
              test_move (te "r3") (te "c");
            ]
      in
      let instr_map = instr |> A.tempmap_of_dll in
      let _ = rewrite () instr_map [ te "c" ] in
      print_string @@ DLL.show A.pp_instr instr;
      [%expect
        {|
        [Move {assem = []; dst = 144; src = r3},
        (Oper { assem = []; dst = []; src = [144]; jump = None }),
        Move {assem = []; dst = a; src = r1}, Move {assem = []; dst = b; src = r2},
        (Oper { assem = []; dst = [d]; src = []; jump = None }),
        Move {assem = []; dst = e; src = a}, Label {assem = []; lab = L2},
        (Oper { assem = []; dst = [d]; src = [d; b]; jump = None }),
        (Oper { assem = []; dst = [e]; src = [e]; jump = None }),
        (Oper { assem = []; dst = []; src = [e]; jump = (Some [L2]) }),
        Move {assem = []; dst = r1; src = d},
        (Oper { assem = []; dst = [143]; src = []; jump = None }),
        Move {assem = []; dst = r3; src = 143}] |}]
  end)
