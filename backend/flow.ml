open Support
module A = Assem
module T = Icr.Tree
module S = ASymbol
module TS = Temp.Set
module TM = Temp.Map
module DLL = DoubleLinkedList
open Graph

module Edge = struct
  type data = unit [@@deriving show { with_path = false }]

  type tag = unit [@@deriving show { with_path = false }, eq, ord]

  type t = { node : int; tag : tag }
  [@@deriving show { with_path = false }, ord]
end

type inf = {
  def : TS.t;
  use : TS.t;
  ismov : bool;
  mutable in_ : TS.t;
  mutable out : TS.t;
}
[@@deriving show { with_path = false }]

module Node = struct
  type t = inf [@@deriving show { with_path = false }]

  let clone { def; use; ismov; in_; out } = { def; use; ismov; in_; out }
end

module DGraph = MakeDGraph (Node) (Edge) (MakeBTMap (Edge)) (Placeholder)

type flowgraph = DGraph.t

let build_graph out graph (a : A.instr DLL.t) =
  let labmap =
    DLL.fold
      (fun i m ->
        match i with
        | A.Label { lab; _ } ->
            let node =
              DGraph.new_node graph
                {
                  def = TS.empty;
                  use = TS.empty;
                  ismov = false;
                  in_ = TS.empty;
                  out = TS.empty;
                }
            in
            S.enter lab node m
        | Move { dst; src; _ } ->
            let _ =
              DGraph.new_node graph
                {
                  def = TS.(singleton dst);
                  use = TS.(singleton src);
                  ismov = true;
                  in_ = TS.empty;
                  out = TS.empty;
                }
            in
            m
        | Oper { dst; src; _ } ->
            let _ =
              DGraph.new_node graph
                {
                  def = TS.of_list dst;
                  use = TS.of_list src;
                  ismov = false;
                  in_ = TS.empty;
                  out = TS.empty;
                }
            in
            m)
      a S.empty
  in
  let rec add_edges nodes instr =
    match (nodes, DLL.value instr) with
    | [ n ], _ -> n
    | v :: nodes, A.Oper { jump = Some (_ :: _ as labs); _ } ->
        (* DGraph.mk_edge v u () (); *)
        List.iter
          (fun l ->
            match S.look l labmap with
            | Some u -> DGraph.mk_edge v u () ()
            | None -> assert false)
          labs;
        add_edges nodes (DLL.next instr)
    | v :: (u :: _ as nodes), _ ->
        DGraph.mk_edge v u () ();
        add_edges nodes (DLL.next instr)
    | _ -> assert false
  in

  let prelast = add_edges (DGraph.nodes graph) a in
  let last =
    DGraph.new_node graph
      {
        use = TS.of_list out;
        def = TS.empty;
        ismov = false;
        in_ = TS.empty;
        out = TS.empty;
      }
  in
  DGraph.mk_edge prelast last () ();

  last

let instr2graph ~size:_ out (a : A.instr DLL.t) =
  let graph = DGraph.init (DLL.lenght a + 1) () in
  let endnode = build_graph out graph a in
  (graph, endnode)

let%test_module _ =
  (module struct
    [@@@warning "-37"]

    open Assem

    type nodelist = DGraph.node list [@@deriving show { with_path = false }]

    let a = Temp.labeltemp "a" `Unboxed

    let b = Temp.labeltemp "b" `Unboxed

    let c = Temp.labeltemp "c" `Unboxed

    let l1 = Temp.newlabel ()

    let instr =
      DLL.of_list
        [
          test_oper [ a ] [];
          test_label l1;
          test_oper [ b ] [ a ];
          test_oper [ c ] [ c; b ];
          test_oper [ a ] [ b ];
          test_oper ~jump:[ l1 ] [] [ a ];
        ]

    let%expect_test _ =
      let g = DGraph.init 256 () in
      let _ = build_graph [ c ] g instr in
      let nodes = DGraph.nodes g |> List.rev in
      print_string @@ show_nodelist nodes;
      [%expect
        {|
          [{ def = []; use = [c]; ismov = false; in_ = []; out = [] };
            { def = []; use = [a]; ismov = false; in_ = []; out = [] };
            { def = [a]; use = [b]; ismov = false; in_ = []; out = [] };
            { def = [c]; use = [b; c]; ismov = false; in_ = []; out = [] };
            { def = [b]; use = [a]; ismov = false; in_ = []; out = [] };
            { def = []; use = []; ismov = false; in_ = []; out = [] };
            { def = [a]; use = []; ismov = false; in_ = []; out = [] }] |}]
  end)
