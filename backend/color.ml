module Temp = Support.Temp
module Seq = Support.Seq
module A = Assem
module List = Support.List

[@@@warning "-27-20"]

(* module Make (F : FrameType) = struct *)
module IG = Igraph

module NS = Set.Make (struct
  type t = IG.node

  let compare = IG.compare_node
end)

module MS = Set.Make (struct
  type t = IG.move

  let compare = IG.compare_move
end)

module M = struct
  type t = IG.move [@@deriving show { with_path = false }]

  type kind =
    (* moves that have been coalesced. *)
    | Coalesced
    (* moves whose source and target interfere. *)
    | Constrained
    (* moves that will no longer be considered for coalescing. *)
    | Frozen
    (* Move instructions that might be coalesceable  *)
    | Worklist
    (* moves not yet ready for coalescing. *)
    | Active
  [@@deriving enum]

  let compare = IG.compare_move

  let number = 5
end

module MMS = Support.MutialDisjointSet.Make (M)

module N = struct
  type t = IG.node [@@deriving show { with_path = false }]

  type kind =
    (* machine registers, preassigned a color. *)
    | Precolored
    (* temporary registers, not precolored and not yet processed. *)
    | Initial
    (* list of low-degree non-move-related nodes. *)
    | SimplifyWL
    (* low-degree move-related nodes. *)
    | FreezeWL
    (* high-degree nodes. *)
    | SpillWL
    (* nodes marked for spilling during this round; initially empty. *)
    | Spilled
    (* registers that have been coalesced; when u <-v is coalesced, v
       is added to this set and u put back on some work-list (or vice versa). *)
    | Coalesced
    (* nodes successfully colored. *)
    | Colored
    | SelectStack
  [@@deriving enum]

  let compare = IG.compare_node

  let number = 9
end

module NMS = Support.MutialDisjointSet.Make (N)

let degree_invariant node nms =
  List.fold_left
    (fun i k ->
      List.fold_left (fun i n -> if NMS.mem nms n k then i + 1 else i) i
      @@ IG.adj node)
    0
    N.[ Precolored; SimplifyWL; FreezeWL; SpillWL ]
  = IG.degree node

let simplify_invariant node mms =
  List.fold_left
    (fun i k ->
      List.fold_left (fun i n -> if MMS.mem mms n k then i + 1 else i) i
      @@ IG.moves node)
    0
    M.[ Active; Worklist ]
  = 0
  && (not @@ IG.is_significant node)

let freeze_invariant node mms =
  List.fold_left
    (fun i k ->
      List.fold_left (fun i n -> if MMS.mem mms n k then i + 1 else i) i
      @@ IG.moves node)
    0
    M.[ Active; Worklist ]
  > 0
  && (not @@ IG.is_significant node)

let spill_invariant = IG.is_significant

let moves_is_alive mms =
  List.for_all
    (fun k ->
      MMS.fold
        (fun m r ->
          let u, v = IG.to_nodes m in
          (Option.is_some @@ IG.move u v) && r)
        mms k true)
    M.[ Coalesced ]

(* let coalesced_invariant nms mms =
    MMS.iter ( fun m  ->
          let u , v = IG.to_nodes m in
            if IG.equal_node (IG.get_alias u) v then
                assert ( NMS.mem nms u N.Coalesced  )
          else if  IG.equal_node (IG.get_alias v) u then
            assert ( NMS.mem nms v N.Coalesced  )
          else assert false

      )  mms M.Coalesced *)

let assert_show_node f n =
  try f n
  with Assert_failure _ as e ->
    print_endline
      "VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV";
    print_endline @@ IG.show_node n;
    raise e

let check_invariants nms mms =
  try
    List.iter
      (fun k ->
        NMS.iter
          (assert_show_node (fun node -> assert (degree_invariant node nms)))
          nms k)
      N.[ SimplifyWL; FreezeWL; SpillWL ];
    NMS.iter
      (assert_show_node (fun n -> assert (simplify_invariant n mms)))
      nms N.SimplifyWL;
    NMS.iter
      (assert_show_node (fun n -> assert (freeze_invariant n mms)))
      nms N.FreezeWL;
    NMS.iter
      (assert_show_node (fun n -> assert (spill_invariant n)))
      nms N.SpillWL;
    assert (moves_is_alive mms) (* coalesced_invariant nms mms; *)
  with Assert_failure _ as e ->
    print_endline
      "VVVVVVVVVVVVVVVVVVVVVVVVV check invariants VVVVVVVVVVVVVVVVVVVVVVVVVVV";
    print_endline @@ NMS.show nms;
    print_endline @@ MMS.show mms;
    raise e
  [@@warning "-21-32"]

(* let check_invariants _  _ = () *)

let movesProp ~f moves mms props =
  List.filter (fun m -> f (MMS.meml mms m props)) moves

let nodesProp ~f nodes nms props =
  List.filter (fun n -> f (NMS.meml nms n props)) nodes

let adjacent n nms =
  nodesProp ~f:not (IG.adj n) nms [ N.Coalesced; N.SelectStack ]

let adjacent_spillwl adjlist nms = nodesProp ~f:Fun.id adjlist nms [ N.SpillWL ]

let nodeMoves n mms =
  movesProp ~f:Fun.id (IG.moves n) mms [ M.Active; M.Worklist ]

let moveRelated n mms = not @@ List.is_empty @@ nodeMoves n mms

let enableMoves nodes mms =
  List.iter
    (fun n ->
      List.iter
        (fun m -> MMS.displace mms m M.Worklist)
        (movesProp ~f:Fun.id (IG.moves n) mms [ M.Active ]))
    nodes

let decrementDegreeAction n nms mms =
  (* print_endline @@  IG.show_node n ; *)
  if IG.is_significant n then ()
  else (
    enableMoves (n :: adjacent n nms) mms;
    let kind = if moveRelated n mms then N.FreezeWL else N.SimplifyWL in
    NMS.displace nms n kind)

let simplify n nms mms =
  check_invariants nms mms;

  let igraph' = IG.clone @@ IG.graph n in
  let nms' = NMS.clone (fun n -> IG.clone_node n igraph') nms in
  let mms' = MMS.clone (fun m -> IG.clone_move m igraph') mms in

  NMS.displace nms n N.SelectStack;
  let adjlist = IG.adj n in
  IG.remove_adj n;
  List.iter
    (fun n -> decrementDegreeAction n nms mms)
    (adjacent_spillwl adjlist nms);

  (try check_invariants nms mms
   with Assert_failure _ as e ->
     print_endline " <><><><><><><><> BEFORE simplify <><><><><><><><><>";
     print_endline (">> n >" ^ IG.show_node n);
     print_endline @@ NMS.show nms';
     print_endline @@ MMS.show mms';
     raise e);
  n

let addWorkList n nms mms =
  if
    (not @@ NMS.mem nms n N.Precolored)
    && (not @@ moveRelated n mms)
    && (not @@ IG.is_significant n)
  then NMS.displace nms n N.SimplifyWL

let george a (b : IG.node) nms =
  let rec aux = function
    | [] -> true
    | t :: tl when IG.is_adj t a || (not @@ IG.is_significant t) -> aux tl
    | _ -> false
  in
  aux (adjacent b nms)

let conservative u v = function
  | [] -> true
  | n :: _ as nodes ->
      List.fold_left
        (fun a n ->
          let d =
            if IG.is_adj u n && IG.is_adj v n then IG.degree n - 1
            else IG.degree n
          in
          if d >= IG.colors n then a + 1 else a)
        0 nodes
      < IG.colors n

type intlist = int list [@@deriving show]

type nodelist = IG.node list [@@deriving show]

let combine u v nms mms =
  (* print_endline " <><><><><><><><> combine <><><><><><><><><>";
     print_endline (">> u >" ^ IG.show_node u);
     print_endline (">> v >" ^ IG.show_node v); *)
  (* if IG.temp u |> Temp.show = "162" then  *)
  ( (* print_endline (">>>>> u >>> " ^ IG.show_node u );
       print_endline (">>>>> v >>> " ^ IG.show_node v ); *)
    (* print_endline (">> adj v >>>>>> \n" ^ show_nodelist @@ IG.adj v     ); *) );

  (* Printf.printf "++ combine ++ %s %s\n" (IG.show_node u) (IG.show_node v); *)
  NMS.displace nms v N.Coalesced;
  let adj_v = adjacent_spillwl (IG.adj v) nms in
  MMS.replace mms (IG.combine ~check:true u v) M.[ Active; Worklist ];

  (* if IG.temp u |> Temp.show = "162" then (
       print_endline (">> adj u >>>>>>\n " ^ show_nodelist @@ IG.adj u     );
     ); *)
  (* if IG.temp u |> Temp.show = "162" &&  I then  *)
  List.iter (fun t -> decrementDegreeAction t nms mms) adj_v;
  if IG.is_significant u && NMS.mem nms u N.FreezeWL then
    NMS.displace nms u N.SpillWL

let coalesce nms mms m =
  let x, y = IG.to_nodes m in

  (* print_endline " <><><><><><><><> BEFORE get_alias <><><><><><><><><>";
     print_endline (">> x >" ^ IG.show_node x);
     print_endline (">> y >" ^ IG.show_node y); *)
  let x, y = (IG.get_alias x, IG.get_alias y) in

  (* print_endline " <><><><><><><><> BEFORE after <><><><><><><><><>";
     print_endline  ( ">> x >" ^ IG.show_node x ) ;
     print_endline  ( ">> y >" ^ IG.show_node y ) ; *)
  let u, v = if NMS.mem nms x N.Precolored then (x, y) else (y, x) in

  (* ( if  Temp.equal ( IG.temp u ) fp then
       ( print_endline @@  ( IG.show_node v ) ^ "  "  ^ IG.show_move m  ;

        if george u v nms then print_endline "geo" ;
        if IG.is_adj u v then print_endline "adj"

        )

     ) ; *)
  if IG.equal_node u v then (
    MMS.displace mms m M.Coalesced;
    addWorkList u nms mms;

    check_invariants nms mms)
  else if NMS.mem nms v N.Precolored || IG.is_adj u v then (
    (* print_endline @@ MMS.show mms;
       print_endline " <><><><><><><><> Precolored <><><><><><><><><>"; *)
    MMS.displace mms m M.Constrained;
    (* print_endline @@ MMS.show mms; *)
    addWorkList u nms mms;
    addWorkList v nms mms;
    check_invariants nms mms)
  else if
    let s =
      NMS.S.(union (of_list (adjacent u nms)) (of_list (adjacent v nms)))
    in
    (NMS.mem nms u N.Precolored && george u v nms)
    || (not @@ NMS.mem nms u N.Precolored)
       && conservative u v (NMS.S.elements s)
  then (
    let igraph' = IG.clone @@ IG.graph u in
    let nms' = NMS.clone (fun n -> IG.clone_node n igraph') nms in
    let mms' = MMS.clone (fun m -> IG.clone_move m igraph') mms in

    MMS.displace mms m M.Coalesced;
    combine u v nms mms;
    addWorkList u nms mms;

    try check_invariants nms mms
    with Assert_failure _ as e ->
      print_endline " <><><><><><><><> BEFORE <><><><><><><><><>";
      print_endline (">> u >" ^ IG.show_node u);
      print_endline (">> v >" ^ IG.show_node v);
      print_endline @@ NMS.show nms';
      print_endline @@ MMS.show mms';
      raise e)
  else MMS.displace mms m M.Active;
  check_invariants nms mms

let freezeMoves n nms mms =
  List.iter (fun m ->
      let n = IG.get_alias n in
      MMS.displace mms m M.Frozen;
      match nodeMoves n mms with
      | []
        when (not @@ IG.is_significant n) && (not @@ NMS.mem nms n N.SelectStack)
        ->
          NMS.displace nms n N.SimplifyWL
      | _ -> ())
  @@ nodeMoves n mms

let freeze nms mms n =
  check_invariants nms mms;
  NMS.displace nms n N.SimplifyWL;
  freezeMoves n nms mms;

  check_invariants nms mms

module IntSet = Set.Make (Int)

let assighColors k stack nms mms =
  (* print_endline "assighColors"; *)
  let rec select_color n used_colors =
    if n = k then None
    else if IntSet.mem n used_colors then select_color (n + 1) used_colors
    else Some n
  in

  List.iter
    (fun n ->
      let used_colors =
        List.filter_map (fun m ->
            let m = IG.get_alias m in
            (* assert (
                 (IG.temp n |>  Temp.show  ) <> "221"
                 ||
               IG.color m <> 8
               ); *)
            if IG.color m >= 0 then Some (IG.color m) else None)
        @@ IG.adj n
        |> IntSet.of_list
      in

      (* if IG.temp n |> Temp.show = "162" then (
         print_endline (">>>>> assighColors u >>> " ^ IG.show_node n );
         print_endline (">> adj >>>>>> \n" ^ show_nodelist @@ IG.adj n     );); *)

      (* if IG.temp n |> Temp.show = "162" then
         print_endline (">>>>>>>> " ^ show_intlist @@ IntSet.elements used_colors); *)
      match select_color 0 used_colors with
      | None ->
          print_endline (">> not colored >> " ^ IG.show_node n);
          NMS.displace nms n N.Spilled
      | Some c ->
          IG.set_color n c;
          NMS.displace nms n N.Colored)
    stack;
  List.iter (fun n ->
      (*
           if IG.temp n |> Temp.show = "148" then (
             print_endline (">>>>> Coalesced >>> " ^ IG.show_node n );
             print_endline (">> combine u v >>>>>> \n" ^ show_nodelist @@ IG.adj n     );
           ); *)
      let u = IG.get_alias n in
      let x = IG.set_color n (IG.color u) in
      x)
  @@ NMS.to_list nms N.Coalesced

let build (initial : Temp.t -> IG.color option) (igraph : IG.t) : NMS.t * MMS.t
    =
  let nms = NMS.init () in

  let moves =
    List.fold_left
      (fun m n ->
        List.fold_left (fun m move -> MMS.S.add move m) m (IG.moves n))
      MMS.S.empty (IG.nodes igraph)
  in
  let mms = MMS.of_set moves M.Worklist in
  List.iter
    (fun n ->
      (* if IG.temp n |> Temp.show = "148" then (
           print_endline (">>>>> build >>> " ^ IG.show_node n );
           print_endline (">> build >>>>>> " ^ show_nodelist @@ IG.adj n     );
         ); *)
      let kind =
        match initial @@ IG.temp n with
        | Some c ->
            IG.set_color n c;
            N.Precolored
        | None ->
            if IG.is_significant n then N.SpillWL
            else if moveRelated n mms then N.FreezeWL
            else N.SimplifyWL
      in
      NMS.add nms n kind)
    (IG.nodes igraph);

  check_invariants nms mms;
  (nms, mms)

let select_spill tmap calleesaves nms =
  (* print_endline
       ("select_spill. list len - " ^ string_of_int @@ List.length !calleesaves);

     let n = *)
  if NMS.is_empty nms N.SpillWL then None
  else
    match !calleesaves with
    | c :: tl ->
        calleesaves := tl;
        Some (Temp.Map.find c tmap)
    | [] ->
        NMS.fold
          (fun n a ->
            match a with
            | Some n' -> if IG.degree n > IG.degree n' then Some n else Some n'
            | None -> Some n)
          nms N.SpillWL None
(* in

   (match Option.map (fun n -> IG.temp n |> Temp.boxed) n with
   | Some (`Callesaved _) -> print_endline "select callesaves"
   | _ -> ());
   n *)

let spill nms mms n =
  check_invariants nms mms;
  NMS.displace nms n N.SelectStack;
  let adj = adjacent n nms in
  IG.remove_adj n;
  freezeMoves n nms mms;
  List.iter
    (fun n -> decrementDegreeAction n nms mms)
    (nodesProp ~f:Fun.id adj nms [ N.SpillWL ])

(** [color initial igraph tmap cs_relate] раскрашивает переменные в графе [igraph] 
    с учетом предварительно покрашенных переменных (регистров) [initial], 
      где [tmap] - карта графа, [cs_relate] - список переменных, связанных с
       callesaves, начало списка соответствует старшим адресам стека.
       Возвращает список переменных, которые невозможно раскрасить *)
let color k (initial : Temp.t -> IG.color option) (igraph : IG.t) tmap
    calleesaves =
  (* print_endline "\n\n********* color **********\n"; *)
  let graph_edges = IG.adj_snapshot igraph in
  let nms, mms = build initial igraph in

  (* List.iter ( fun n ->
     if IG.temp n |> Temp.show = "162" then (
       print_endline (">>>>> before u >>> " ^ IG.show_node n );
       print_endline (">> adj >>>>>> \n" ^ show_nodelist @@ IG.adj n     ););

     )@@ IG.nodes igraph ; *)
  let rec aux ss =
    let ss =
      (* Упрощение: помещаем переменные со степенью < k в стек *)
      NMS.fold
        (fun n stack ->
          (* Printf.printf "+++++++ sympify +++++ %s\n" @@ IG.show_node n; *)
          simplify n nms mms :: stack)
        nms N.SimplifyWL ss
    in
    (* Слияние: если существуют переменные:
        1 степень < k;
        2 входят в одну инстукцию mov;
        3 не смежные;
        4 соответствуют критерию conservative или george
          то сливаем их в одну. Идем на этап упрощения *)
    match MMS.choose mms M.Worklist with
    | Some m ->
        (* Printf.printf "+++++++ coalesce +++++ %s\n" @@ IG.show_move m; *)
        (try coalesce nms mms m
         with Assert_failure _ as e ->
           print_endline
             "VVVVVVVVVVVVVVV MMS.choose mms M.Worklist VVVVVVVVVVVVVVVVVVVVV";
           print_endline @@ IG.show_move m;
           let u, v = IG.to_nodes m in
           print_endline (">> u >" ^ IG.show_node u);
           print_endline (">> v >" ^ IG.show_node v);
           raise e);
        aux ss
    | None -> (
        (* Разморозка: помечаем переменные, подходящие под все критерии слияния
           кроме 4, готовыми к упрощению. Идем на этап упрощения *)
        match NMS.choose nms N.FreezeWL with
        | Some n ->
            (* Printf.printf "+++++++ freeze +++++ %s\n" @@ IG.show_node n; *)
            freeze nms mms n;
            aux ss
        (*Spill: Выбираем переменную cо степенью <= k, согласно алгоритму select_spill
          и удаляем ее смежные ребра, переменную помещаем в стек. Идем на этап
          упрощения*)
        | None -> (
            match select_spill tmap calleesaves nms with
            | Some n ->
                (* Printf.printf "+++++++ spill +++++ %s\n" @@ IG.show_node n; *)
                spill nms mms n;
                aux (n :: ss)
            | None ->
                if
                  NMS.is_empty nms N.SimplifyWL
                  && NMS.is_empty nms N.FreezeWL
                  && NMS.is_empty nms N.SpillWL
                  && MMS.is_empty mms M.Worklist
                then ss
                else aux ss))
  in

  let selectStack =
    try aux []
    with Assert_failure _ as e ->
      print_endline "VVVVVVVVVVVVVVV color VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV";
      Printexc.print_backtrace stdout;
      flush stdout;
      print_endline @@ NMS.show nms;
      print_endline @@ MMS.show mms;

      raise e
  in

  (* print_endline "selectStack"; *)
  IG.restore_adj igraph graph_edges;

  List.iter
    (fun u -> List.iter (fun v -> IG.combine u v |> ignore) @@ IG.coalesced u)
    selectStack;

  (* IG.nodes igraph
     |> List.iter ( fun n ->
         Printf.printf ">> %s: " ( IG.temp n |> Temp.show );
         List.iter ( fun m ->
           Printf.printf "%s, " ( IG.temp m |> Temp.show)
            ;

         ) ( IG.adj n ) ;
         print_newline ();
       )  ; *)

  (* Раскраска:  извлекая переменные из стека раскрашиваем их *)
  assighColors k selectStack nms mms;

  NMS.to_list nms N.Spilled
