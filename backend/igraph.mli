
type color = int

type t 
type node 
type move 
(* type edata  = (Support.Temp.t * Support.Temp.t) option  *)
type esnap
val adj_snapshot : t -> esnap 
val restore_adj : t -> esnap -> unit
val clone : t -> t
val clone_node : node -> t -> node 
val clone_move : move -> t -> move
val graph : node -> t
val pp_node : Format.formatter -> node -> unit
val pp_move : Format.formatter -> move -> unit
val show_node : node -> string
val show_move : move -> string
val pp : Format.formatter -> t -> unit
val show : t -> string
val equal_node : node -> node -> bool
val compare_node : node -> node -> color
val mk_edge : ?invisible:bool -> node -> node -> unit
val rm_edge : node -> node -> unit
val adj : node -> node list
val nodes : t -> node list
val move : node -> node -> move option
val temp : node -> Support.Temp.t
val coalesced : node -> node list 
val degree : node -> color
val to_nodes : move -> node * node
val color : node -> color
val set_color : node -> color -> unit
val get_alias : node -> node 
val is_significant : node -> bool
val is_adj : node -> node -> bool
val compare_move : move -> move -> color
val init : k:color -> int -> t
val new_node : t  -> Support.Temp.t -> node
val add_move : node -> node -> unit
val moves : node ->  move list
(* val move_related : node -> bool *)
val combine : ?check:bool -> node -> node -> (move * move * (unit -> unit)) list
val remove_adj: node -> unit
val colors: node -> int
val to_dot: show_temp:(Support.Temp.t -> string)  ->  t -> Buffer.t
