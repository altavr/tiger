module Temp = Support.Temp

type reg = string

type temp = Temp.t [@@deriving eq, show { with_path = false }]

type label = Temp.label [@@deriving eq, show { with_path = false }]

type insfrag =
  [ `C of string | `N of int | `D of temp | `S of temp | `L of label ]
[@@deriving eq, show { with_path = false }]

type kind = Call | Other [@@deriving eq, show { with_path = false }]

type oper = {
  assem : insfrag list;
  dst : temp list;
  src : temp list;
  jump : label list option;
  kind : kind;
}
[@@deriving show { with_path = false }]

type instr =
  | Oper of oper
  | Label of { assem : insfrag list; lab : label }
  | Move of { assem : insfrag list; dst : temp; src : temp }
[@@deriving show { with_path = false }]

let test_oper ?(kind = Other) ?jump dst src =
  Oper { assem = []; dst; src; jump; kind }

let test_label l = Label { assem = []; lab = l }

let test_move dst src = Move { assem = []; dst; src }

let find_temp t instr =
  let test_aux l = List.find_opt (Temp.equal t) l |> Option.is_some in
  let test dst src =
    if test_aux dst then `Dst
    else if test_aux src then `Src
    else raise Not_found
  in
  match instr with
  | Label _ -> raise @@ Invalid_argument "This is not instruction"
  | Oper { dst; src; _ } -> test dst src
  | Move { dst; src; _ } -> test [ dst ] [ src ]

let replace_temp ~that ~other instr dir : instr =
  let rep_assem assem =
    List.map
      (function
        | `D t as e -> if Temp.equal that t then `D other else e
        | `S t as e -> if Temp.equal that t then `S other else e
        | e -> e)
      assem
  in
  let rep_in_list l = Support.List.replace Temp.equal that other l in

  match (dir, instr) with
  | _, Label _ -> raise @@ Invalid_argument "This is not instruction"
  | `Dst, Oper { assem; dst; src; jump; kind } ->
      Oper { assem = rep_assem assem; dst = rep_in_list dst; src; jump; kind }
  | `Dst, Move { assem; dst : temp; src : temp } ->
      Move
        { assem = rep_assem assem; dst = List.hd @@ rep_in_list [ dst ]; src }
  | `Src, Oper { assem; dst; src; jump; kind } ->
      Oper { assem = rep_assem assem; dst; src = rep_in_list src; jump; kind }
  | `Src, Move { assem; dst : temp; src : temp } ->
      Move
        { assem = rep_assem assem; dst; src = List.hd @@ rep_in_list [ src ] }

let oper_aux ?(kind = Other) ?jump ?src ?dst assem =
  let rec add_to_list f x = function
    | h :: tl as l -> if f x h then l else h :: add_to_list f x tl
    | [] -> [ x ]
  in

  let rec aux jump src dst lst =
    match lst with
    | `C _ :: tl | `N _ :: tl -> aux jump src dst tl
    | `D t :: tl -> aux jump src (add_to_list equal_temp t dst) tl
    | `S t :: tl -> aux jump (add_to_list equal_temp t src) dst tl
    | `L l :: tl -> aux (add_to_list equal_label l jump) src dst tl
    | [] ->
        let jump = match jump with [] -> None | _ :: _ -> Some jump in

        { assem; dst; src; jump; kind }
  in
  let flat x = Option.to_list x |> List.flatten in

  aux (flat jump) (flat src) (flat dst) assem

let oper ?kind ?jump ?src ?dst
    (assem : _ list)
      (* : (  [< `C of string | `N of int | `D of temp | `S of temp | `L of label ] instr  ) *)
    =
  Oper (oper_aux ?kind ?jump ?src ?dst assem)

let label (al : [< `C of string | `L of label ] list) =
  match oper_aux ~src:[] ~dst:[] (al :> insfrag list) with
  | { assem; dst = []; src = []; jump = Some [ lab ]; _ } ->
      Label { assem; lab }
  | _ -> assert false

let move (al : [< `C of string | `D of temp | `S of temp ] list) =
  match oper_aux ~src:[] ~dst:[] (al :> insfrag list) with
  | { assem; dst = [ dst ]; src = [ src ]; jump = None; _ } ->
      Move { assem; dst; src }
  | _ -> assert false

let format_aux (saytemp : temp -> string) assem =
  let rec aux = function
    | [] -> []
    | `C s :: tl -> (s ^ " ") :: aux tl
    (* | `D t :: (`S _ :: _ as tl)
       | `S t :: (`S _ :: _ as tl)
       | `S t :: (`D _ :: _ as tl) ->
           (saytemp t ^ ", ") :: aux tl *)
    | `L l :: tl -> Temp.labelname l :: aux tl
    | `S t :: tl | `D t :: tl -> saytemp t :: aux tl
    | `N x :: tl -> string_of_int x :: aux tl
  in

  aux assem |> List.map String.to_seq |> List.to_seq |> Seq.flat_map Fun.id
  |> String.of_seq

let format saytemp s =
  match s with
  | Oper { assem; _ } | Label { assem; _ } | Move { assem; _ } ->
      format_aux saytemp assem

let format_wo_samemov saytemp = function
  | Move { src; dst; _ } when saytemp src = saytemp dst -> None
  | Oper { assem; _ } | Label { assem; _ } | Move { assem; _ } ->
      Some (format_aux saytemp assem)

module TM = Temp.Map
module DLL = Support.DoubleLinkedList

let tempmap_of_dll d =
  let add_to_tempmap tl1 tl2 instr map =
    let aux tl map =
      List.fold_left
        (fun m t ->
          TM.update t
            (function Some l -> Some (instr :: l) | None -> Some [ instr ])
            m)
        map tl
    in
    aux tl1 map |> aux tl2
  in

  DLL.wrappedfold
    (fun i m ->
      match DLL.value i with
      | Label _ -> m
      | Move { dst; src; _ } -> add_to_tempmap [ dst ] [ src ] i m
      | Oper { dst; src; _ } -> add_to_tempmap dst src i m)
    d TM.empty

let%test_module _ =
  (module struct
    let _ = Temp.reset ()

    let t1 = Temp.newtemp `Unboxed

    let t2 = Temp.newtemp `Unboxed

    let t3 = Temp.newtemp `Unboxed

    let t4 = Temp.newtemp `Unboxed

    let%expect_test _ =
      let op = oper ~src:[ t1; t2 ] ~dst:[ t3; t4 ] [] in

      print_string @@ show_instr op;
      [%expect
        {| (Oper { assem = []; dst = [102; 103]; src = [100; 101]; jump = None }) |}]

    let%expect_test _ =
      let op = oper [ `C "a"; `D t1; `C ","; `N 11 ] ~src:[ t1 ] ~dst:[ t1 ] in

      print_string @@ show_instr op;
      [%expect
        {|
          (Oper
             { assem = [`C ("a"); `D (100); `C (","); `N (11)]; dst = [100];
               src = [100]; jump = None }) |}]

    let%expect_test _ =
      let op = oper [ `C "op"; `D t1; `C ","; `S t2 ] ~src:[ t1 ] ~dst:[ t1 ] in

      print_string @@ show_instr op;
      [%expect
        {|
                   (Oper
                      { assem = [`C ("op"); `D (100); `C (","); `S (101)]; dst = [100];
                        src = [100; 101]; jump = None }) |}]
  end)
