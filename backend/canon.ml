module T = Icr.Tree
module Temp = Support.Temp
module S = Absttree.Symbol

(* type reot = T.stm * T.exp list [@@deriving show { with_path = false }] *)

let ( ^^ ) a b =
  match (a, b) with
  | T.Exp (Const _), b -> b
  | a, T.Exp (Const _) -> a
  | a, b -> T.Seq (a, b)

let commute s e =
  match (s, e) with
  | T.Exp (Const _), _ | _, T.Name _ | _, T.Const _ -> true
  | _ -> false

let nop = T.Exp (Const 0)

let rec reorder el : T.stm * T.exp list =
  match el with
  | (T.Call _ as e) :: tl ->
      let t = Temp.newtemp (T.exp_boxed e) in
      reorder (Eseq (Move (Temp t, e), Temp t) :: tl)
  | h :: tl ->
      let s, e = do_exp h in
      let s', el = reorder tl in

      if commute s' e then (s ^^ s', e :: el)
      else
        let t = Temp.newtemp (T.exp_boxed e) in
        (s ^^ T.(Move (T.Temp t, e)) ^^ s', T.Temp t :: el)
  | [] -> (nop, [])

and reorder_exp el build =
  let s, el = reorder el in
  (s, build el)

and reorder_stm el build =
  let s, el = reorder el in
  s ^^ build el

and do_exp e =
  match e with
  | T.Eseq (s, e) ->
      let s = do_stm s in
      let s', e = do_exp e in
      (s ^^ s', e)
  | Call (f, args, bx, ca) ->
      reorder_exp (f :: args) (function f :: args -> T.Call (f, args, bx, ca))
  | Mem (a, bx) -> reorder_exp [ a ] (function [ a ] -> T.Mem (a, bx))
  | Binop (op, e1, e2) ->
      reorder_exp [ e1; e2 ] (function [ e1; e2 ] -> T.Binop (op, e1, e2))
  | e -> reorder_exp [] (function [] -> e)
  [@@warning "-8"]

and do_stm s =
  match s with
  | Seq (s1, s2) -> do_stm s1 ^^ do_stm s2
  | Jump (e, labs) -> reorder_stm [ e ] (function [ e ] -> Jump (e, labs))
  | CJump (c, a, b, t, f) ->
      reorder_stm [ a; b ] (function [ a; b ] -> CJump (c, a, b, t, f))
  | Move (Temp t, Call (e, el, b, ca)) ->
      reorder_stm (e :: el) (function e :: el ->
          Move (Temp t, Call (e, el, b, ca)))
  | Move (Temp t, e) -> reorder_stm [ e ] (function [ e ] -> Move (Temp t, e))
  | Move (Mem (e, bx), b) ->
      reorder_stm [ e; b ] (function [ e; b ] -> Move (Mem (e, bx), b))
  | Move (Eseq (s, e), b) ->
      reorder_stm [ e; b ] (function [ e; b ] -> Seq (s, Move (e, b)))
  | Exp (Call (e, el, bx, ca)) ->
      reorder_stm (e :: el) (function e :: el -> Exp (Call (e, el, bx, ca)))
  | Exp e -> reorder_stm [ e ] (function [ e ] -> Exp e)
  | s -> reorder_stm [] (function [] -> s)
  [@@warning "-8"]

(** [linearize stm0]  return list of cleaned trees from an arbitrary [stm0]
	   satisfying the following properties :
	      1.  No SEQ's or ESEQ's
	      2.  The parent of every CALL is an EXP(..) or a MOVE(TEMP t,..)
        *)

let rec linearize_aux (l : _ list) = function
  | T.Seq (a, b) -> linearize_aux (linearize_aux l b) a
  | b -> b :: l

let linearize stm0 = do_stm stm0 |> linearize_aux []

(** [basicBlocks stmlst] return a list of basic blocks from a list of cleaned trees [stmlst], 
    satisfying the following properties:
	      1. and 2. as above;
	      3.  Every block begins with a LABEL;
              4.  A LABEL appears only at the beginning of a block;
              5.  Any JUMP or CJUMP is the last stm in a block;
              6.  Every block ends with a JUMP or CJUMP;
           Also produce the "label" to which control will be passed
           upon exit.
        *)
let basicBlocks stmlst =
  let donelab = Temp.newlabel () in

  let rec start = function
    | (T.Label _ as l) :: tl -> block [ l ] tl
    | lst -> block [ Label (Temp.newlabel ()) ] lst
  and block bl = function
    | (T.Jump _ as s) :: tl | (CJump _ as s) :: tl ->
        List.rev (s :: bl) :: start tl
    | (Label lab as x) :: tl ->
        let jmp = T.Jump (Name lab, [ lab ]) in
        List.rev (jmp :: bl) :: block [ x ] tl
    | s :: tl -> block (s :: bl) tl
    | [] -> [ List.rev (T.Jump (Name donelab, [ donelab ]) :: bl) ]
  in

  (start stmlst, T.Label donelab)

type blocks1 = T.stm list list [@@deriving show { with_path = false }]

let rec trace tab b (rest : T.stm list list) =
  match b with
  | T.Label lab :: _ -> (
      let tab = S.enter lab [] tab in
      match Support.List.splitlast b with
      | most, T.Jump (Name lab, _) -> (
          match S.look lab tab with
          | Some (_ :: _ as b') -> most @ trace tab b' rest
          | _ -> b @ getnext tab rest)
      | most, CJump (op, x, y, t, f) -> (
          match (S.look t tab, S.look f tab) with
          | _, Some (_ :: _ as b') -> b @ trace tab b' rest
          | Some (_ :: _ as b'), _ ->
              most @ [ T.CJump (T.notRel op, x, y, f, t) ] @ trace tab b' rest
          | _ ->
              let f' = Temp.newlabel () in
              most
              @ [
                  T.CJump (op, x, y, t, f'); T.Label f'; T.Jump (Name f, [ f ]);
                ]
              @ getnext tab rest)
      | _ -> assert false)
  | _ -> assert false

and getnext t = function
  | [] -> []
  | (T.Label lab :: _ as b) :: tl -> (
      match S.look lab t with
      | Some (_ :: _) -> trace t b tl
      | _ -> getnext t tl)
  | _ -> assert false

(** [traceSchelduler (blocks, donelab)] from a [blocks], satisfying properties 1-6,
    along with an [donelab] label, return list of stms such that:
      1. - 6. as above;
      7. Every CJUMP(_,t,f) is immediately followed by LABEL f.
      The blocks are reordered to satisfy property 7; also in this reordering as
      many JUMP(T.NAME(lab)) statements as possible are eliminated by falling
      through into T.LABEL(lab)   *)
let traceSchelduler (blocks, donelab) =
  let m =
    List.fold_left (fun m (T.Label l :: _ as b) -> S.enter l b m) S.empty blocks
  in
  getnext m blocks @ [ donelab ]
  [@@warning "-8"]

let%test_module _ =
  (module struct
    [@@@warning "-32"]

    let _ = Temp.reset ()

    let t = Temp.labeltemp "t" `Unboxed

    and t1 = Temp.labeltemp "t1" `Unboxed

    and t2 = Temp.labeltemp "t2" `Unboxed

    and lab = Temp.namedLabel "lab"

    and lab1 = Temp.namedLabel "lab1"

    and lab2 = Temp.namedLabel "lab2"

    and lab3 = Temp.namedLabel "lab3"

    and lab4 = Temp.namedLabel "lab4"

    let left_case =
      T.(
        Seq
          ( Seq
              ( Seq (Exp (Mem (Const 0, Unboxed)), Exp (Mem (Const 1, Unboxed))),
                Exp (Mem (Const 2, Unboxed)) ),
            Exp (Mem (Const 3, Unboxed)) ))

    let%expect_test _ =
      let s =
        linearize
          T.(
            Seq
              ( Exp (Mem (Const 10, Unboxed)),
                Seq
                  ( left_case,
                    Seq
                      ( Exp (Mem (Const 11, Unboxed)),
                        Exp (Mem (Const 12, Unboxed)) ) ) ))
      in
      print_string @@ T.show_stmlist s;
      [%expect
        {|
           [(Exp (Mem (Const 10))); (Exp (Mem (Const 0))); (Exp (Mem (Const 1)));
             (Exp (Mem (Const 2))); (Exp (Mem (Const 3))); (Exp (Mem (Const 11)));
             (Exp (Mem (Const 12)))] |}]

    let mov_eseq =
      T.(Move (Eseq (Exp (Mem (Const 0, Unboxed)), Const 1), Const 2))

    let%expect_test _ =
      let s = linearize mov_eseq in

      print_string @@ T.show_stmlist s;
      [%expect {| [(Exp (Mem (Const 0))); (Move ((Const 1), (Const 2)))] |}]

    let%expect_test _ =
      let s =
        linearize
          T.(
            Seq
              ( Exp (Mem (Const 10, Unboxed)),
                Seq
                  ( mov_eseq,
                    Seq
                      ( Exp (Mem (Const 11, Unboxed)),
                        Exp (Mem (Const 12, Unboxed)) ) ) ))
      in

      print_string @@ T.show_stmlist s;
      [%expect
        {|
          [(Exp (Mem (Const 10))); (Exp (Mem (Const 0)));
            (Move ((Const 1), (Const 2))); (Exp (Mem (Const 11)));
            (Exp (Mem (Const 12)))] |}]

    let eseq_case = T.Eseq (Exp (Mem (Const 1, Unboxed)), Temp t)

    let%expect_test _ =
      let s = linearize @@ T.Exp eseq_case in

      print_string @@ T.show_stmlist s;
      [%expect {| [(Exp (Mem (Const 1))); (Exp (Temp t))] |}]

    let%expect_test _ =
      let s =
        linearize (T.Seq (T.Exp eseq_case, Exp (Mem (Const 1, Unboxed))))
      in

      print_string @@ T.show_stmlist s;
      [%expect
        {|
        [(Exp (Mem (Const 1))); (Exp (Temp t)); (Exp (Mem (Const 1)))] |}]

    (* let case0 =
       T.(
         Exp
           (Eseq
              ( Move
                  ( Mem (Binop (Plus, Const 1, Temp t)),
                    Eseq
                      ( Move (Temp t1, Call (Name lab, [ Const 10; Const 0 ])),
                        Temp t1 ) ),
                Mem (Binop (Plus, Const 2, Temp t)) ))) *)

    let case1 =
      T.(Move (Mem (Temp t, Unboxed), Eseq (Move (Temp t1, Const 1), Temp t1)))

    let%expect_test _ =
      let s = linearize case1 in
      print_string @@ T.show_stmlist s;
      [%expect
        {|
        [(Move ((Temp 103), (Temp t))); (Move ((Temp t1), (Const 1)));
          (Move ((Mem (Temp 103)), (Temp t1)))] |}]

    (** ***************************************************************************)

    (** ***************************************************************************)

    (** ***************************************************************************)

    (** ***************************************************************************)

    type blocks = T.stm list list * T.stm
    [@@deriving show { with_path = false }]

    let%expect_test _ =
      let s =
        T.
          [
            Move (Mem (Const 0, Unboxed), Const 1);
            Move (Mem (Const 2, Unboxed), Const 3);
            Label lab;
            Move (Mem (Const 4, Unboxed), Const 5);
            Jump (Name lab, [ lab ]);
            Move (Mem (Const 6, Unboxed), Const 7);
          ]
      in

      print_string @@ show_blocks @@ basicBlocks s;
      [%expect
        {|
          ([[(Label L1); (Move ((Mem (Const 0)), (Const 1)));
              (Move ((Mem (Const 2)), (Const 3))); (Jump ((Name lab), [lab]))];
             [(Label lab); (Move ((Mem (Const 4)), (Const 5)));
               (Jump ((Name lab), [lab]))];
             [(Label L2); (Move ((Mem (Const 6)), (Const 7))); (Jump ((Name L0), [L0]))
               ]
             ],
           (Label L0)) |}]

    let%expect_test _ =
      let s =
        T.
          [
            Move (Mem (Const 0, Unboxed), Const 1);
            Move (Mem (Const 2, Unboxed), Const 3);
            Label lab;
            Move (Mem (Const 4, Unboxed), Const 5);
            Move (Mem (Const 6, Unboxed), Const 7);
          ]
      in

      print_string @@ show_blocks @@ basicBlocks s;
      [%expect
        {|
          ([[(Label L4); (Move ((Mem (Const 0)), (Const 1)));
              (Move ((Mem (Const 2)), (Const 3))); (Jump ((Name lab), [lab]))];
             [(Label lab); (Move ((Mem (Const 4)), (Const 5)));
               (Move ((Mem (Const 6)), (Const 7))); (Jump ((Name L3), [L3]))]
             ],
           (Label L3)) |}]

    let%expect_test _ =
      let s =
        T.
          [
            Move (Mem (Const 0, Unboxed), Const 1);
            Move (Mem (Const 2, Unboxed), Const 3);
            Label lab;
            Move (Mem (Const 4, Unboxed), Const 5);
            Jump (Name lab, [ lab ]);
            Move (Mem (Const 6, Unboxed), Const 7);
            Jump (Name lab, [ lab ]);
          ]
      in

      print_string @@ show_blocks @@ basicBlocks s;
      [%expect
        {|
               ([[(Label L6); (Move ((Mem (Const 0)), (Const 1)));
                   (Move ((Mem (Const 2)), (Const 3))); (Jump ((Name lab), [lab]))];
                  [(Label lab); (Move ((Mem (Const 4)), (Const 5)));
                    (Jump ((Name lab), [lab]))];
                  [(Label L7); (Move ((Mem (Const 6)), (Const 7)));
                    (Jump ((Name lab), [lab]))];
                  [(Label L8); (Jump ((Name L5), [L5]))]],
                (Label L5)) |}]

    let tr = Temp.namedLabel "tr"

    let fls = Temp.namedLabel "fls"

    let%expect_test _ =
      let s =
        T.
          [
            Move (Mem (Const 0, Unboxed), Const 1);
            Move (Mem (Const 2, Unboxed), Const 3);
            Label tr;
            Move (Mem (Const 4, Unboxed), Const 5);
            CJump (Eq, Name tr, Name fls, tr, fls);
            Move (Mem (Const 6, Unboxed), Const 7);
            Label fls;
            Move (Mem (Const 8, Unboxed), Const 9);
            Move (Mem (Const 10, Unboxed), Const 11);
          ]
      in

      print_string @@ show_blocks @@ basicBlocks s;
      [%expect
        {|
          ([[(Label L10); (Move ((Mem (Const 0)), (Const 1)));
              (Move ((Mem (Const 2)), (Const 3))); (Jump ((Name tr), [tr]))];
             [(Label tr); (Move ((Mem (Const 4)), (Const 5)));
               (CJump (Eq, (Name tr), (Name fls), tr, fls))];
             [(Label L11); (Move ((Mem (Const 6)), (Const 7)));
               (Jump ((Name fls), [fls]))];
             [(Label fls); (Move ((Mem (Const 8)), (Const 9)));
               (Move ((Mem (Const 10)), (Const 11))); (Jump ((Name L9), [L9]))]
             ],
           (Label L9)) |}]

    (** ***************************************************************************)

    (** ***************************************************************************)

    (** ***************************************************************************)

    (** ***************************************************************************)

    let%expect_test _ =
      let s =
        T.
          ( [
              [
                Label lab1;
                Exp (Const 0);
                Exp (Const 1);
                Jump (Name lab, [ lab ]);
              ];
              [ Label lab; Exp (Const 2); Jump (Name lab, [ lab ]) ];
              [ Label lab2; Exp (Const 3); Jump (Name lab3, [ lab3 ]) ];
            ],
            Label lab3 )
      in

      print_string @@ T.show_stmlist @@ traceSchelduler s;
      [%expect
        {|
            [(Label lab1); (Exp (Const 0)); (Exp (Const 1)); (Label lab);
              (Exp (Const 2)); (Jump ((Name lab), [lab])); (Label lab2); (Exp (Const 3));
              (Jump ((Name lab3), [lab3])); (Label lab3)] |}]

    let%expect_test _ =
      let s =
        T.
          ( [
              [
                Label lab1;
                Exp (Const 0);
                Exp (Const 1);
                Jump (Name lab3, [ lab3 ]);
              ];
              [ Label lab; Exp (Const 2); Jump (Name lab, [ lab ]) ];
              [ Label lab2; Exp (Const 3); Jump (Name lab, [ lab ]) ];
              [ Label lab3; Exp (Const 4); Jump (Name lab4, [ lab4 ]) ];
            ],
            Label lab4 )
      in

      print_string @@ T.show_stmlist @@ traceSchelduler s;
      [%expect
        {|
          [(Label lab1); (Exp (Const 0)); (Exp (Const 1)); (Label lab3);
            (Exp (Const 4)); (Jump ((Name lab4), [lab4])); (Label lab);
            (Exp (Const 2)); (Jump ((Name lab), [lab])); (Label lab2); (Exp (Const 3));
            (Jump ((Name lab), [lab])); (Label lab4)] |}]

    let tr = Temp.namedLabel "tr"

    let fls = Temp.namedLabel "fls"

    let%expect_test _ =
      let s =
        T.
          ( [
              [
                Label lab;
                Exp (Const 20);
                Exp (Const 21);
                Exp (Const 22);
                Exp (Const 23);
                Exp (Const 24);
                Exp (Const 25);
                CJump (Ne, Const 14, Const 88, tr, fls);
              ];
              [
                Label fls;
                Exp (Const 2);
                Exp (Const 12);
                Jump (Name lab1, [ lab1 ]);
              ];
              [
                Label tr;
                Exp (Const 3);
                Exp (Const 13);
                Jump (Name lab1, [ lab1 ]);
              ];
              [ Label lab1; Jump (Name lab2, [ lab2 ]) ];
              [ Label lab2; Jump (Name lab3, [ lab3 ]) ];
              [
                Label lab3;
                Exp (Const 30);
                Exp (Const 31);
                Exp (Const 32);
                Exp (Const 33);
                Exp (Const 34);
                Jump (Name lab4, [ lab4 ]);
              ];
            ],
            Label lab4 )
      in

      print_string @@ T.show_stmlist @@ traceSchelduler s;
      [%expect
        {|
        [(Label lab); (Exp (Const 20)); (Exp (Const 21)); (Exp (Const 22));
          (Exp (Const 23)); (Exp (Const 24)); (Exp (Const 25));
          (CJump (Ne, (Const 14), (Const 88), tr, fls)); (Label fls);
          (Exp (Const 2)); (Exp (Const 12)); (Label lab1); (Label lab2);
          (Label lab3); (Exp (Const 30)); (Exp (Const 31)); (Exp (Const 32));
          (Exp (Const 33)); (Exp (Const 34)); (Jump ((Name lab4), [lab4]));
          (Label tr); (Exp (Const 3)); (Exp (Const 13));
          (Jump ((Name lab1), [lab1])); (Label lab4)] |}]

    let%expect_test _ =
      let s =
        T.
          [
            Exp (Const 0);
            Exp (Const 1);
            Label tr;
            Exp (Const 2);
            CJump (Eq, Name tr, Name fls, tr, fls);
            Exp (Const 3);
            Label fls;
            Exp (Const 4);
            Exp (Const 5);
          ]
      in

      print_string @@ T.show_stmlist @@ traceSchelduler @@ basicBlocks s;
      [%expect
        {|
             [(Label L13); (Exp (Const 0)); (Exp (Const 1)); (Label tr); (Exp (Const 2));
               (CJump (Eq, (Name tr), (Name fls), tr, fls)); (Label fls); (Exp (Const 4));
               (Exp (Const 5)); (Jump ((Name L12), [L12])); (Label L14); (Exp (Const 3));
               (Jump ((Name fls), [fls])); (Label L12)] |}]
  end)
