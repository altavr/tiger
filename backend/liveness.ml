open Flow
open Support
module IG = Igraph

type edge = Temp.t * Temp.t [@@deriving ord]

module EdgeSet = Set.Make (struct
  type t = edge

  let compare = compare_edge
end)

type igraph = IG.t [@@deriving show { with_path = false }]

module TS = Support.Temp.Set
module TM = Support.Temp.Map

let calculate_liveness_aux (nl : DGraph.node list) =
  let calc_node (out : TS.t) n =
    (* print_endline @@ "<><><><out><><" ^ TS.show  out ; *)
    let v = DGraph.value n in
    let in_ = TS.union v.use (TS.diff out v.def) in
    let eqflag = TS.equal v.in_ in_ && TS.equal v.out out in
    v.in_ <- in_;
    v.out <- out;
    eqflag
  in

  let rec pass eqflag (nl : DGraph.node list) =
    match nl with
    | [] -> eqflag
    | n :: tl ->
        let in_all_succ =
          List.fold_right
            (fun (n : DGraph.node) in_ -> TS.union (DGraph.value n).in_ in_)
            (DGraph.succ n ()) TS.empty
        in

        let eqflag = calc_node in_all_succ n && eqflag in
        (* print_endline @@ "<><><><><><" ^ DGraph.show_node n ; *)
        pass eqflag tl
  in
  let rec aux () =
    if pass true nl then () else (* print_endline "<><><><><><"; *)
                              aux ()
  in
  aux ()

let calculate_liveness lastnode =
  let nodes = DGraph.backward_topology_sort lastnode in
  calculate_liveness_aux nodes

let lives_for_defs ?src_move defs lives intf =
  let lives_for_def d intf =
    TS.fold
      (fun l intf ->
        match src_move with
        | Some src when Temp.equal l src -> intf
        | _ when Temp.equal d l -> intf
        | _ -> EdgeSet.add (d, l) intf)
      lives intf
  in
  TS.fold (fun d intf -> lives_for_def d intf) defs intf

let rec find_interferences vars intf moves = function
  | [] -> (vars, intf, moves)
  | n :: tl ->
      let v = DGraph.value n in
      let nodes = TS.union vars v.def |> TS.union v.out in
      let intf, moves =
        if v.ismov then
          let dst = TS.choose v.def and src = TS.choose v.use in
          ( lives_for_defs ~src_move:src v.def v.out intf,
            EdgeSet.add (dst, src) moves )
        else (lives_for_defs v.def v.out intf, moves)
      in
      find_interferences nodes intf moves tl

let tempmap_of_vars igraph vars =
  TS.fold (fun t m -> TM.add t (IG.new_node igraph t) m) vars TM.empty

let add_edges f tmap s =
  EdgeSet.iter (fun (u, v) -> f (TM.find u tmap) (TM.find v tmap)) s

let interferenceGraph ~size:_ k isolated ((fgraph, lastnode) : flowgraph * _) =
  calculate_liveness lastnode;
  let nodes = DGraph.nodes fgraph in

  let vars, intf, moves =
    find_interferences TS.empty EdgeSet.empty EdgeSet.empty nodes
  in

  let igraph = IG.init ~k (TS.cardinal vars + List.length isolated) in

  let tmap = tempmap_of_vars igraph vars in
  add_edges IG.mk_edge tmap intf;
  add_edges IG.add_move tmap moves;

  let isolated = List.map (fun t -> TM.find t tmap) @@ isolated in
  List.iter (fun u ->
      List.iter
        (fun v ->
          if
            not
            @@ (IG.equal_node u v || IG.is_adj u v
               || (Option.is_some @@ IG.move u v))
          then IG.mk_edge ~invisible:true u v)
        isolated)
  @@ IG.nodes igraph;

  (igraph, tmap)

let%test_module _ =
  (module struct
    [@@@warning "-37"]

    open Assem

    type nodelist = DGraph.node list [@@deriving show { with_path = false }]

    let%expect_test _ =
      let a = Temp.labeltemp "a" `Unboxed
      and b = Temp.labeltemp "b" `Unboxed
      and c = Temp.labeltemp "c" `Unboxed
      and l1 = Temp.newlabel () in

      let instr =
        DLL.of_list
          [
            test_oper [ a ] [];
            test_label l1;
            test_oper [ b ] [ a ];
            test_oper [ c ] [ c; b ];
            test_oper [ a ] [ b ];
            test_oper ~jump:[ l1 ] [] [ a ];
          ]
      in

      let graph = DGraph.init 256 () in
      let lastnode = Flow.build_graph [ c ] graph instr in

      calculate_liveness lastnode;
      let nodes = DGraph.nodes graph |> List.rev in
      print_string @@ show_nodelist nodes;
      [%expect
        {|
            [{ def = []; use = [c]; ismov = false; in_ = [c]; out = [] };
              { def = []; use = [a]; ismov = false; in_ = [a; c]; out = [c] };
              { def = [a]; use = [b]; ismov = false; in_ = [b; c]; out = [a; c] };
              { def = [c]; use = [b; c]; ismov = false; in_ = [b; c]; out = [b; c] };
              { def = [b]; use = [a]; ismov = false; in_ = [a; c]; out = [b; c] };
              { def = []; use = []; ismov = false; in_ = [a; c]; out = [a; c] };
              { def = [a]; use = []; ismov = false; in_ = [c]; out = [a; c] }] |}]
  end)
