[@@@warning "-27"]

open Support
open Graph

module Edge = struct
  type data = (Temp.t * Temp.t) option [@@deriving show { with_path = false }]

  type tag = Intf | Move | Coalesced [@@deriving show { with_path = false }]

  let enum = function Intf -> 0 | Move -> 1 | Coalesced -> 2

  let compare_tag t1 t2 = Int.compare (enum t1) (enum t2)

  let equal_tag t1 t2 = enum t1 = enum t2

  type t = { node : int; tag : tag }
  [@@deriving show { with_path = false }, ord]
end

module M = MakeBTMap (Edge)

type color = int [@@deriving show { with_path = false }]

module Node = struct
  type t = {
    temp : Temp.t;
    mutable degree : int;
    mutable color : color;
    mutable head : bool option;
  }
  [@@deriving show { with_path = false }]

  let clone { temp; degree; color; head } = { temp; degree; color; head }
end

module BitTable = Support.BitTable

module Ext = struct
  type t = { adjnodes : BitTable.t; k : color }

  let pp _ _ = ()

  let clone t = { adjnodes = BitTable.clone t.adjnodes; k = t.k }
end

(* type ext = Ext.t = { adjnodes : BitTable.t; k : color } *)

module B = MakeIGraph (Node) (Edge) (M) (Ext)

type u = Node.t = {
  temp : Temp.t;
  mutable degree : int;
  mutable color : color;
  mutable head : bool option;
}

type t = B.t

type node = B.node [@@deriving show { with_path = false }]

type move = B.edge

type etag = Edge.tag = Intf | Move | Coalesced

type esnap = B.esnap

let pp = B.pp

let show = B.show

let pp_move = B.pp_edge

let show_move = B.show_edge

let equal_node = B.equal_node

let compare_node = B.compare_node

let to_nodes = B.nodes_of_edge

let nodes = B.nodes

let compare_move = B.compare_edge

let adj_snapshot = B.edges_snapshot

let restore_adj = B.merge_edges

let clone = B.clone

let clone_node = B.clone_node

let clone_move = B.clone_edge

let graph = B.graph

let is_adj (u : node) (v : node) =
  BitTable.mem (B.ext_of_node u).adjnodes (B.node_id u) (B.node_id v)

let mk_edge ?invisible u v =
  if not @@ is_adj u v then (
    (* let k = match invisible with Some true -> Intf  | _ -> Intf false in *)
    B.mk_edge u v Intf None;
    let uv = B.value u in
    let vv = B.value v in
    uv.degree <- uv.degree + 1;
    vv.degree <- vv.degree + 1;
    let an = (B.ext_of_node u).adjnodes in
    BitTable.set an (B.node_id u) (B.node_id v);
    BitTable.set an (B.node_id v) (B.node_id u))

let rm_edge u v =
  let uv = B.value u in
  let vv = B.value v in
  uv.degree <- uv.degree - 1;
  vv.degree <- vv.degree - 1;
  B.rm_edge u v Intf;
  let an = (B.ext_of_node u).adjnodes in
  BitTable.unset an (B.node_id u) (B.node_id v);
  BitTable.unset an (B.node_id v) (B.node_id u)

let adj u = B.adj u Intf

let temp u = (B.value u).temp

let degree u = (B.value u).degree

let color u = (B.value u).color

let colors u = (B.ext_of_node u).k

let set_color u c = (B.value u).color <- c

let get_alias u : node =
  match (B.value u).head with
  | None -> u
  | Some i ->
      if i then u
      else
        List.hd
        @@ List.filter_map (fun v ->
               match (B.value v).head with Some true -> Some v | _ -> None)
        @@ B.adj u Coalesced

let is_significant u =
  assert (adj u |> List.length = degree u);
  degree u >= (B.ext_of_node u).k

let init ~(k : int) (n : int) : t = B.init n { adjnodes = BitTable.init n; k }

let new_node g temp : node =
  let l = { temp; degree = 0; color = -1; head = None } in
  B.new_node g l

let add_move u v : unit =
  B.mk_edge u v Edge.Move (Some ((B.value u).temp, (B.value v).temp))

let moves u =
  match (B.value u).head with
  | None | Some true ->
      List.fold_left
        (fun s n ->
          List.fold_left
            (fun s (_, m) -> B.EdgeSet.add m s)
            s (B.adj_edges u Move))
        B.EdgeSet.empty (u :: B.adj u Coalesced)
      |> B.EdgeSet.elements
  | _ -> []
(* List.map snd @@ B.adj_edges u Edge.Move *)

(* let move_related (u : node) : bool =
   match moves u with [] -> false | _ -> true *)

let coalesced u = B.adj u Coalesced

let move u v = B.edge u v Move

let combine ?check u v =
  (match check with
  | Some true ->
      assert (
        if
          (Option.is_some @@ move u v)
          && (match B.value u with
             | { head = Some true; _ } | { head = None; _ } -> true
             | _ -> false)
          &&
          match B.value v with
          | { head = Some true; _ } | { head = None; _ } -> true
          | _ -> false
        then true
        else (
          print_endline "VVVVV combine VVVVVV";
          print_endline @@ show_node u;
          print_endline @@ show_node v;
          false))
  | _ -> ());

  let uv = B.value u in
  let vv = B.value v in

  uv.head <- Some true;
  vv.head <- Some false;

  List.iter (fun n -> B.mk_edge u n Coalesced None) @@ B.adj v Coalesced;

  B.mk_edge u v Coalesced None;

  List.iter (fun n ->
      rm_edge v n;
      mk_edge u n)
  @@ adj v;

  List.filter_map (fun (n, m) ->
      if not @@ equal_node n u then
        let m' = B.idle_edge (B.graph u) u n Move in
        Some
          ( m,
            m',
            fun () ->
              (* print_endline ("remove >> " ^ show_move m); *)
              (* print_endline ("new >> " ^ show_move m'); *)
              B.rm_edge v n Move;
              B.mk_edge u n Move (Some (temp u, temp n)) )
      else None)
  @@ B.adj_edges v Move

(* module S = Set.Make (struct
     type t = move

     let compare = compare_move
   end)

   let update_move_set u v f =
     List.iter (fun (n, m) ->
         if not @@ equal_node n u then (
           B.rm_edge v n Move;
           B.mk_edge u n Move (Some (temp u, temp n));
           let m' = Option.get @@ move u n in
           f m m'))
     @@ B.adj_edges v Move *)

let remove_adj u = List.iter (fun v -> rm_edge u v) @@ adj u

let to_dot ~show_temp g =
  B.to_dot ~global:"node [colorscheme=set312]"
    ~show_node:(fun n ->
      let attrs =
        match n with
        | ({ head = None; color; _ } | { head = Some true; color; _ })
          when color >= 0 ->
            Some
              (Printf.sprintf "style=\"filled\" fillcolor=%d"
                 ((color mod 12) + 1))
        | { head = None; color; _ } | { head = Some true; color; _ } -> None
        | { head = Some false; color; _ } when color >= 0 ->
            Some
              (Printf.sprintf "style=\"filled\" color=%d" ((color mod 12) + 1))
        | { head = Some false; _ } -> Some (Printf.sprintf "style=\"dashed\"")
      in
      ( Printf.sprintf "%s (%d)\ndeg: %d" (show_temp n.temp) n.color n.degree,
        attrs ))
    ~show_edge:(function
      | Move -> Some "style=\"dotted\""
      | Coalesced -> Some "style=\"dashed\""
      | Intf -> Some "")
    g

let%test_module _ =
  (module struct
    [@@@warning "-37"]

    module Seq = Support.Seq

    let g = init ~k:3 20

    let ns =
      Seq.iternums 0 |> Seq.take 20
      |> Seq.map (fun _ -> new_node g @@ Temp.newtemp `Unboxed)
      |> Array.of_seq

    let _ =
      mk_edge ns.(0) ns.(5);
      mk_edge ns.(1) ns.(19);
      mk_edge ns.(9) ns.(4)

    let%test "is_adj" =
      is_adj ns.(9) ns.(4) && is_adj ns.(1) ns.(19) && is_adj ns.(0) ns.(5)
  end)
