module Temp = Support.Temp
module Tree = Icr.Tree
module Assem = Backend.Assem

module type FrameType = sig
  include Icr.Translate.Frame

  include Backend.Reg_alloc.FrameType with type access := access and type t := t

  module Codegen : sig
    val run : Tree.stm -> unit

    val get_assem : unit -> Assem.instr list
  end

  val string_of_register : register -> string

  val string : Temp.label -> string -> string

  val procEntryExit1 : t -> Tree.stm -> Tree.stm
  (** [procEntryExit1 frame tree] return [tree] with moved incoming parameters
  and added Tree.Move statements for each register of [frame] to a beginning and
  Tree.Move statements for calleesaves registers to an end.
     *)

  type func = { prolog : string; body : Assem.instr list; epilog : string }

  val procEntryExit3 :
    t -> (Assem.instr * Temp.t list) list -> register TempMap.t -> func
end

module Make (F : FrameType) = struct
  module Translate = Icr.Translate.Make (F)
  module Semant = Icr.Semant.Make (Translate)
  module RegAlloc = Backend.Reg_alloc.Make (F)
  module Canon = Backend.Canon

  type fraglist = F.frag list [@@deriving show { with_path = false }]

  type stmlist = Tree.stm list [@@deriving show { with_path = false }]

  type blocklst = Tree.stm list list * Tree.stm
  [@@deriving show { with_path = false }]

  let compile_function buff body frame =
    print_endline ("compile_function " ^ Temp.labelname @@ F.name frame);
    Stdlib.flush_all ();
    F.procEntryExit1 frame body
    (* |> (fun t ->
         print_endline @@ Tree.show_stm t;
         t) *)
    |> Canon.linearize
    (* |> (fun tl ->
         print_endline @@ show_stmlist tl;
         tl) *)
    |> Canon.basicBlocks
    (* |> ( fun t -> print_endline @@ show_blocklst t; t ) *)
    |> Canon.traceSchelduler
    (* |> fun b ->
       print_endline @@ show_stmlist b;
       b *)
    |> List.iter F.Codegen.run;
    let assem = F.Codegen.get_assem () in
    (* List.iter (fun a -> print_endline @@ Assem.format Temp.show a) assem; *)
    let assem, alloc =
      try RegAlloc.alloc frame assem
      with Failure _ as e ->
        List.iter (fun a -> print_endline @@ Assem.format Temp.show a) assem;
        raise e
    in
    print_endline "after alloc reg";
    let F.{ prolog; body; epilog } = F.procEntryExit3 frame assem alloc in
    Buffer.add_string buff prolog;
    List.iter
      (function
        (* | Assem.Move { dst; src; _ } when Temp.equal dst src -> () *)
        | a ->
            (* print_endline @@ Assem.show_instr a ;  *)
            Assem.format_wo_samemov
              (fun t -> F.TempMap.find t alloc |> F.string_of_register)
              a
            |> Option.iter (fun r -> Buffer.add_string buff (r ^ "\n"));

            print_endline @@ Assem.format Temp.show a)
      body;
    Buffer.add_string buff epilog

  let compile buff fl =
    let add_str = Buffer.add_string buff in

    List.iter (fun f -> add_str ("extern " ^ f ^ "\n")) Semant.E.global_funcs;
    add_str "\nsection .data\n";
    fl |> fun bl ->
    List.fold_right
      (fun frag funcs ->
        match frag with
        | F.String (lab, str) ->
            add_str @@ F.string lab str;
            funcs
        | F.Proc { body; frame } -> (body, frame) :: funcs)
      bl []
    |> fun x ->
    add_str "\nsection .text\n\nglobal tigermain\n";
    List.iter (fun (body, frame) -> compile_function buff body frame) x
end
