module C = Driver.Make (X86_64.Frame)
module Semant = C.Semant
module I = Absttree.Interval

let main infile asm_file =
  print_endline asm_file;
  let buff = Buffer.create (2 * 1024) in

  (try C.compile buff (Absttree.Parsing.run infile |> Semant.transProg)
   with Semant.TypeError (k, msg, pos) ->
     let f = open_in infile in
     let be, en = I.dia pos in
     seek_in f be;
     let quote = really_input_string f (en - be) in

     (match msg with
     | None ->
         Printf.printf "\n%s\nLine: %d, Column: %d\n%s\n"
           (Semant.show_errkind k) (I.linenum pos) (I.start_col pos) quote
     | Some msg ->
         Printf.printf "\n%s: %s\nLine: %d, Column: %d\n%s\n"
           (Semant.show_errkind k) msg (I.linenum pos) (I.start_col pos) quote);

     exit 1 |> ignore);

  let outchan = open_out asm_file in
  Buffer.output_buffer outchan buff;

  close_out outchan
