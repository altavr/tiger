use core::mem::size_of;
use core::ops::Deref;
use core::ptr;

use crate::layout::*;
use crate::ARENA;
use crate::{_error_handler, gc, stderr};

#[derive(Debug, Clone, Copy, PartialEq)]
struct MWord([u8; size_of::<usize>()]);

macro_rules! alloc {
    ($fp:ident, len:ident, ret_addr_ptr:ident, temp_arr:ident ) => {};
}

#[repr(C)]
#[derive(Debug, Clone)]
pub struct TString<const N: usize> {
    header: Block,
    s: [MWord; N],
}

impl<const N: usize, const S: usize> From<&[u8; S]> for TString<N> {
    fn from(a: &[u8; S]) -> Self {
        TString::<N>::from_array(a)
    }
}

impl<const N: usize> Deref for TString<N> {
    type Target = [u8];
    fn deref(&self) -> &[u8] {
        self.to_slice()
    }
}

unsafe impl<const N: usize> Sync for TString<N> {}

pub static EMPTY_STRING: TString<1> = TString::<1>::empty();
const CHARS_NUM: usize = 256;
pub static CHARS: [TString<1>; CHARS_NUM] = TString::<1>::init_chars();

impl<const N: usize> TString<N> {
    pub const fn from_array<const S: usize>(a: &[u8; S]) -> TString<N> {
        assert!(size_of::<usize>() * N > S && size_of::<usize>() * N - S <= size_of::<usize>());
        let mut ts = TString::<N>::empty();
        let mut i = 0;
        while i < S {
            ts.s[i / size_of::<MWord>()].0[i % size_of::<MWord>()] = a[i];
            i += 1;
        }

        let padding_len = (size_of::<usize>() * N - S - 1) as u8;
        ts.s[N - 1].0[size_of::<usize>() - 1] = padding_len;

        ts
    }

    const fn empty() -> TString<N> {
        assert!(N < u16::MAX as usize);
        let bb = ((Block::HEADER_SIZE + N) >> 8) as u8;
        let lb = (Block::HEADER_SIZE + N) as u8;
        TString {
            header: Block::new_with_descriptor(Descriptor::from_bytes([
                lb, bb, 0, 0b11000000, 0, 0, 0, 0,
            ])),
            s: [MWord([0, 0, 0, 0, 0, 0, 0, 7]); N],
        }
    }

    const fn char(c: u8) -> TString<1> {
        let lb = (Block::HEADER_SIZE + N) as u8;
        TString {
            header: Block::new_with_descriptor(Descriptor::from_bytes([
                lb, 0, 0, 0b11000000, 0, 0, 0, 0,
            ])),
            s: [MWord([c, 0, 0, 0, 0, 0, 0, 6])],
        }
    }

    const fn init_chars() -> [TString<1>; CHARS_NUM] {
        let mut i = 0;
        const TS: TString<1> = TString::<1>::empty();

        let mut arr = [TS; CHARS_NUM];
        while i < CHARS_NUM {
            arr[i] = TString::<1>::char(i as u8);
            i += 1;
        }
        arr
    }

    pub unsafe fn equal(&self, rhs: &Self) -> isize {
        if self.header.size() != rhs.header.size() {
            return 0;
        } else {
            for i in 0..(self.header.size() - Block::HEADER_SIZE as u16) {
                if *(&self.s as *const MWord).offset(i as isize)
                    != *(&rhs.s as *const MWord).offset(i as isize)
                {
                    return 0;
                }
            }
        };
        1
    }

    pub unsafe fn size(&self) -> usize {
        let padding = (*(&self.s as *const MWord)
            .offset(self.header.size() as isize - Block::HEADER_SIZE as isize - 1))
        .0[size_of::<usize>() - 1];

        (self.header.size() as usize - Block::HEADER_SIZE) * size_of::<usize>()
            - padding as usize
            - 1
    }

    unsafe fn copy_to(&self, ptr: *mut u8) {
        ptr.copy_from(self.s[0].0.as_ptr(), self.size())
    }

    unsafe fn copy_from(&mut self, ptr: *const u8) {
        self.s[0].0.as_mut_ptr().copy_from(ptr, self.size())
    }

    #[cfg(test)]
    pub unsafe fn alloc(len: usize) -> *mut TString<1> {
        let n = len / size_of::<usize>() + 1;
        assert!(n < u16::MAX as usize);
        let size = n * size_of::<usize>() + size_of::<Block>();
        let ptr = libc::malloc(size) as *mut TString<1>;
        Self::alloc_aux(ptr, len, n)
    }

    #[cfg(not(test))]
    pub unsafe fn alloc(
        fp: *const usize,
        len: usize,
        ret_addr_ptr: *const usize,
        temp_arr: *mut usize,
    ) -> *mut TString<1> {
        let n = len / size_of::<usize>() + 1;
        assert!(n < u16::MAX as usize);

        let size = n + size_of::<Block>() / size_of::<usize>();

        let ptr = ARENA
            .assume_init_mut()
            .alloc(size as u16, fp, ret_addr_ptr, temp_arr) as *mut TString<1>;

        Self::alloc_aux(ptr, len, n)
    }
    #[inline(always)]
    unsafe fn alloc_aux(ptr: *mut TString<1>, len: usize, n: usize) -> *mut TString<1> {
        (*ptr).header = Block::new_string((n + Block::HEADER_SIZE) as u16, true);

        let padding_len = (size_of::<usize>() * n - len - 1) as u8;
        (*(*ptr).s.as_mut_ptr().offset(n as isize - 1)).0 = [0, 0, 0, 0, 0, 0, 0, padding_len];
        ptr
    }

    pub fn to_slice(&self) -> &[u8] {
        unsafe { &*ptr::slice_from_raw_parts(self.ptr(0), self.size()) }
    }

    unsafe fn ptr_mut(&mut self, i: usize) -> *mut u8 {
        &mut (*self
            .s
            .as_mut_ptr()
            .offset((i / size_of::<usize>()) as isize))
        .0[i % size_of::<usize>()]
    }

    unsafe fn ptr(&self, i: usize) -> *const u8 {
        &(*self.s.as_ptr().offset((i / size_of::<usize>()) as isize)).0[i % size_of::<usize>()]
    }

    pub unsafe fn substring<A>(&self, first: usize, len: usize, alloc: A) -> *const TString<1>
    where
        A: Fn(usize) -> *mut TString<1>,
    {
        static ERR_MSG: TString<3> = TString::<3>::from_array(b"substring out of range\n");

        if first + len > self.size() {
            _error_handler(&ERR_MSG)
        };
        if len == 1 {
            return &CHARS[*self.ptr(first) as usize] as *const TString<1>;
        };
        let ptr = alloc(len);
        (*ptr).copy_from(self.ptr(first));
        ptr
    }

    pub unsafe fn concat<A>(&self, other: &Self, alloc: A) -> *const TString<1>
    where
        A: Fn(usize) -> *mut TString<1>,
    {
        let ptr = alloc(self.size() + other.size());
        self.copy_to((*ptr).ptr_mut(0));
        other.copy_to((*ptr).ptr_mut(self.size()));
        ptr
    }

    fn itoa(mut num: isize, s: &mut [u8; 22]) -> &mut [u8] {
        if num == 0 {
            s[0] = '0' as u8;
            return &mut s[0..1];
        }
        let mut is_negative = false;
        if num < 0 {
            num = -num;
            is_negative = true;
        };
        let mut i = 0;

        while num != 0 {
            let rem = (num % 10) as u8;
            s[i] = '0' as u8 + rem;
            num = num / 10;
            i += 1;
        }
        if is_negative {
            s[i] = '-' as u8;
            i += 1
        };

        let slice = &mut s[0..i];
        slice.reverse();
        slice
    }

    pub unsafe fn string_of_int<A>(n: isize, alloc: A) -> *const TString<1>
    where
        A: Fn(usize) -> *mut TString<1>,
    {
        let mut a = [0; 22];
        let s = Self::itoa(n, &mut a);
        let ptr = alloc(s.len());
        (*ptr).copy_from(s.as_ptr());
        ptr
    }

    pub unsafe fn print(&self) {
        let ptr = self.ptr(0);
        for i in 0..self.size() as isize {
            libc::putchar(*ptr.offset(i) as i32);
        }
    }

    pub unsafe fn eprint(&self) {
        let ptr = self.ptr(0);
        for i in 0..self.size() as isize {
            libc::fputc(*ptr.offset(i) as i32, stderr);
        }
    }

    pub unsafe fn getchar_() -> *const TString<1> {
        let i = libc::getchar();

        if i == libc::EOF {
            &EMPTY_STRING
        } else {
            &CHARS[i as usize]
        }
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    #[test]
    fn string_test() {
        let s1 = TString::<2>::from_array(b"hello world\n");

        unsafe {
            assert!(s1.size() == 12);
            s1.print();
        }
    }

    #[test]
    fn string_test1() {
        let s7 = b"1234567";
        let s8 = b"12345678";
        let s9 = b"123456789";
        let s1 = b"heap string\n";
        unsafe {
            let s1a = &mut *TString::<1>::alloc(s1.len());
            s1a.copy_from(s1.as_ptr());

            assert!(s1.len() == s1a.size());
            s1a.print();

            let s7a = &mut *TString::<1>::alloc(s7.len());
            let s8a = &mut *TString::<1>::alloc(s8.len());
            let s9a = &mut *TString::<1>::alloc(s9.len());
            assert!(s7.len() == s7a.size());
            assert!(s8.len() == s8a.size());
            assert!(s9.len() == s9a.size());

            let p = s1a as *const TString<1> as *const usize;
            let p1 = p.offset(1);
            let p2 = p.offset(2);

            // dbg!(*p, *p1, *p2);
            println!("{:#x} {:#x} {:#x}", *p, *p1, *p2);
        }
    }

    #[test]
    fn substring_test() {
        let s1 = b"heap string\n";
        unsafe {
            let s1a = &mut *TString::<1>::alloc(s1.len());
            s1a.copy_from(s1.as_ptr());

            let ss = &*s1a.substring(5, 3, |len| TString::<1>::alloc(len));
            assert!(ss.to_slice() == b"str");
        }
    }

    #[test]
    fn concat_test() {
        let s1 = b"first string ";
        let s2 = b"second string\n";
        unsafe {
            let s1a = &mut *TString::<1>::alloc(s1.len());
            s1a.copy_from(s1.as_ptr());
            let s2a = &mut *TString::<1>::alloc(s2.len());
            s2a.copy_from(s2.as_ptr());

            let ss = &*s1a.concat(s2a, |len| TString::<1>::alloc(len));
            assert!(ss.to_slice() == b"first string second string\n");
        }
    }

    #[test]
    fn string_of_int_test() {
        let s1 = b"1239945";
        unsafe {
            let si = &*TString::<1>::string_of_int(1239945, |len| TString::<1>::alloc(len));

            assert!(si.to_slice() == s1);
        }
    }
}
