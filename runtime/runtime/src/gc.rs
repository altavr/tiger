use core::{marker::PhantomData, mem, mem::size_of, ptr, ptr::NonNull};

use crate::{
    layout::{self, graystack::GrayStack, *},
    size,
};

use crate::{TString, _error_handler};

static OUT_OF_MEMORY_MSG: TString<2> = TString::<2>::from_array(b"out of memory");

// unsafe fn mark(mut x: BL) {
//     if !(*x).mark() {
//         let mut t = ptr::null_mut();
//         // assert!(size_of::<Option<UDescriptor>>() == 8);
//         (*x).set_mark(true);
//         loop {
//             if let Some(f) = Descriptor::current_field(x) {
//                 let y = *f;
//                 if !(*y).mark() {
//                     *f = t;
//                     t = x;
//                     x = y;
//                     (*x).set_mark(true);
//                 } else {
//                     Descriptor::mark_done_field(x)
//                 };
//             } else {
//                 Descriptor::reset_iterator(x);
//                 let y = x;

//                 if !t.is_null() {
//                     x = t;
//                     t = *Descriptor::current_field(x).unwrap();
//                     *Descriptor::current_field(x).unwrap() = y;
//                     Descriptor::mark_done_field(x)
//                 } else {
//                     return;
//                 };
//             }
//         }
//     }
// }

unsafe trait Intrusive {
    type Container;
    const POSITION: isize;
}

#[repr(C)]
#[derive(Debug, Clone)]
struct Node<T> {
    next: Option<NonNull<Node<T>>>,
    _p: PhantomData<T>,
}

impl<T> Node<T>
where
    T: Intrusive,
{
    unsafe fn closed(ptr: *mut Self) {
        (*ptr).next = None;
    }

    unsafe fn init(ptr: *mut Self, next: Option<NonNull<Node<T>>>) {
        (*ptr).next = next
    }

    fn container(ptr: *mut Self) -> *mut T::Container {
        unsafe { (ptr as *mut usize).offset(-T::POSITION) as *mut T::Container }
    }
}

#[repr(C)]
#[derive(Debug, Clone)]
pub(crate) struct Space<const N: usize> {
    chunk: Chunk,
    a: [usize; N],
}

impl<const N: usize> Space<N> {
    pub(crate) const fn new() -> Space<N> {
        Space {
            chunk: Chunk::new(size_of::<Space<N>>() / size_of::<usize>()),
            a: [0; N],
        }
    }
}

#[repr(C)]
#[derive(Debug, Clone)]
pub(crate) struct Chunk {
    freelist: Node<Free>,
    size: usize,
    // _pin: marker::PhantomPinned,
}

#[derive(Debug, Clone)]
struct Free {}

unsafe impl Intrusive for Free {
    type Container = Chunk;
    const POSITION: isize = 0;
}

const HEADER_SIZE: usize = size_of::<Chunk>() / size_of::<usize>();
// const DESC_SIZE: usize = size_of::<Descriptor>() / size_of::<usize>();

impl Chunk {
    const fn new(size: usize) -> Self {
        Chunk {
            freelist: Node::<Free> {
                next: None,
                _p: PhantomData,
            },
            size,
        }
    }

    unsafe fn init_closed(ptr: *mut Self, size: usize) {
        Node::<Free>::closed(&mut (*ptr).freelist);
        (*ptr).size = size;
    }

    unsafe fn init(ptr: *mut Self, size: usize, next: Option<*mut Self>) {
        Node::<Free>::init(
            &mut (*ptr).freelist,
            next.map(|c| NonNull::new_unchecked(&mut (*c).freelist)),
        );

        (*ptr).size = size;
    }

    unsafe fn link(&mut self, c2: *mut Chunk) {
        self.freelist.next = Some(NonNull::new_unchecked(&mut (*c2).freelist));
    }

    unsafe fn close(&mut self) {
        self.freelist.next = None
    }

    unsafe fn next_freenode(&mut self) -> Option<NonNull<Node<Free>>> {
        self.freelist.next
    }

    unsafe fn set_next_freenode(&mut self, next: Option<NonNull<Node<Free>>>) {
        self.freelist.next = next
    }

    unsafe fn next_free(&mut self) -> Option<*mut Self> {
        self.freelist
            .next
            .map(|n| Node::<Free>::container(n.as_ptr()))
    }

    unsafe fn after_block(ptr: *mut Self) -> *mut usize {
        (ptr as *mut usize).offset(((*ptr).size) as isize)
    }
}

#[repr(C)]
struct PtrMap {
    spilled_arr_offset: u8,
    cs_offset: u8,
    cs_num: u8,
    _pad0: u8,
    _pad1: u32,
    locals_bm: usize,
    spilled: BitMap<MAX_SPILLED>,
}

const CALLEESAVED_NUM: usize = 5;
const MAX_SPILLED: usize = 8;
// const LOCAL_VAR_OFFSET: isize = -1;
#[repr(C)]
struct TempMap {
    ptrmap: &'static PtrMap,
    bitmap: BitMap<CALLEESAVED_NUM>, // data: [isize; CALLEESAVED_NUM],
}

#[repr(C)]
struct BitMap<const T: usize> {
    map: usize,
    data: [isize; T],
}

impl<const T: usize> BitMap<T> {
    // const MASK : usize = usize::MAX >> ( usize::BITS - ( 1 + T as u32) * 4 ) ;

    const fn prea_message() -> &'static str {
        if T == CALLEESAVED_NUM {
            "Calleesave"
        } else {
            "Spilled"
        }
    }

    unsafe fn find<const A: usize>(
        &self,
        arena: &mut Arena<A>,
        fp: *const usize,
        arr: &[usize; T], // tm: &TempMap,
    ) {
        dprint!(Self::prea_message(), ">> mark {}\n");
        // let mut tbm = tm.bitmap;
        let mut data_idx = 0;
        // let mut mask = 0xff_0000_0000;

        let mut i = T as isize - 1;
        while i >= 0 {
            match (self.map >> (i * 4)) & 0b1111 {
                1 => {
                    dprint!(Self::prea_message(), "{} {} raw - ", i);
                    arena.add_root(arr[i as usize] as BL);
                }
                2 => {
                    dprint!(Self::prea_message(), "{} {} const offset - ", i);
                    arena.add_root(
                        ((arr[i as usize] as *const usize)
                            .offset(-(self.data[data_idx] / size_of::<usize>() as isize)))
                            as BL,
                    );
                    data_idx += 1;
                }
                3 => {
                    dprint!(Self::prea_message(), "{} {} offset in array - ", i);
                    arena.add_root(
                        ((arr[i as usize] as *const usize).offset(
                            -(arr[self.data[data_idx] as usize] as isize
                                / size_of::<usize>() as isize),
                        )) as BL,
                    );
                    data_idx += 1;
                }
                4 => {
                    dprint!(Self::prea_message(), "{} {} offset in spilled - ", i);
                    let ofs = *fp.offset(-(self.data[data_idx])) as isize;
                    arena.add_root(
                        ((arr[i as usize] as *const usize)
                            .offset(-(ofs / size_of::<usize>() as isize)))
                            as BL,
                    );
                    data_idx += 1;
                }

                _ => (),
            };
            i -= 1;
        }
    }
}

unsafe fn get_previous_fp(fp: *const usize) -> *const usize {
    *fp as *const usize
}

unsafe fn get_return_addr_ptr(fp: *const usize) -> *const usize {
    fp.offset(1)
}

const JUMPSIZE: usize = 2;

unsafe fn get_temp_map(ret_addr_ptr: *const usize) -> &'static TempMap {
    debug_assert!(size_of::<usize>() == mem::align_of::<usize>());
    &*(((*ret_addr_ptr + JUMPSIZE + (size_of::<usize>() - 1))
        & (usize::MAX - (size_of::<usize>() - 1))) as *const TempMap)
}

unsafe fn get_static_link(fp: *const usize) -> usize {
    *fp.offset(-1)
}

#[derive(Debug)]
pub struct Arena<const N: usize> {
    // length: usize,
    origin: *mut Space<N>,
    freelist: Option<NonNull<Chunk>>,
    graylist: GrayStack,
    alloc_chunks: usize,
}

// const RESIDUAL: usize = CHUNK_HEAD_SIZE  + 1;

impl<const N: usize> Arena<N> {
    pub(crate) unsafe fn new(origin: *mut Space<N>) -> Self {
        Arena {
            // length,
            origin,
            freelist: Some(NonNull::new_unchecked(&mut (*origin).chunk)),
            graylist: GrayStack::new(),
            alloc_chunks: 0,
        }
    }

    pub const fn length() -> usize {
        size_of::<Space<N>>()
    }

    pub unsafe fn collect(
        &mut self,
        fp: *const usize,
        ret_addr_ptr: *const usize,
        temp_arr: *mut usize,
    ) {
        dprint!("collect\n");
        self.find_roots(
            fp,
            ret_addr_ptr,
            &mut *(temp_arr as *mut [usize; CALLEESAVED_NUM]),
        );
        self.graylist.mark();
        self.sweep();
    }

    pub unsafe fn add_root(&mut self, root: *mut Block) {
        let p = root as *const usize as usize;

        if p >= self.origin as usize && p < self.origin as usize + size_of::<Space<N>>() {
            assert!(
                (*root).descriptor.kind() == DataKind::Record
                    || (*root).descriptor.kind() == DataKind::Array
                    || ((*root).descriptor.kind() == DataKind::String
                        && (*root).descriptor.bitmap() == 1)
            );
            dprint!("mark heap {:?}\n", root);
            self.graylist.add(NonNull::new_unchecked(root))
        } else {
            assert!(
                p == 0
                    || ((*root).descriptor.kind() == DataKind::String
                        && (*root).descriptor.bitmap() == 0)
            );
            dprint!("null\n");
        };
    }

    unsafe fn find_locals(&mut self, fp: *const usize, pm: &PtrMap) {
        dprint!(">> find_locals\n");
        let mut bmloc = pm.locals_bm;
        loop {
            let pos = usize::leading_zeros(bmloc);
            if pos >= usize::BITS - 1 {
                break;
            };
            bmloc &= (usize::MAX >> 1) >> pos;
            let p = fp.offset(-(pos as isize));
            dprint!("local pos {} - ", pos);
            self.add_root(*p as BL);
        }
    }

    const MASK: usize = 0b1111 << (4 * (CALLEESAVED_NUM - 1));

    unsafe fn copy_callesaves(
        &self,
        fp: *const usize,
        tm: &TempMap,
        pm: &PtrMap,
        curr_cs: &mut [usize; CALLEESAVED_NUM],
    ) {
        dprint!(">> copy callesave\n");
        // let pm = tm.ptrmap;
        debug_assert!(Self::MASK == 0xf0000);
        let mut mask = Self::MASK;
        for i in 0..pm.cs_num {
            if mask & tm.bitmap.map > 0 {
                dprint!("copy {}\n", i);
                let p = fp.offset(-(i as isize + pm.cs_offset as isize));
                curr_cs[CALLEESAVED_NUM - 1 - i as usize] = *p;
            }
            mask >>= 4;
        }
    }

    unsafe fn find_roots(
        &mut self,
        mut fp: *const usize,
        ret_addr_ptr: *const usize,
        temp_arr: &[usize; CALLEESAVED_NUM as usize],
    ) {
        debug_assert!(mem::align_of::<TempMap>() == size_of::<usize>());

        let a = get_temp_map(ret_addr_ptr) as *const TempMap as *const usize as usize;

        let mut tm = &*(a as *const usize as *const TempMap);

        let mut curr_cs = (*temp_arr).clone();
        loop {
            tm.bitmap.find(self, fp, &curr_cs);

            self.find_locals(fp, (*tm).ptrmap);
            if get_static_link(fp) == 0 {
                break;
            };
            let pm = tm.ptrmap;
            self.copy_callesaves(fp, tm, pm, &mut curr_cs);
            let sp_arr: &'static [usize; MAX_SPILLED] =
                &*(fp.offset(-(pm.spilled_arr_offset as isize)) as *const [usize; MAX_SPILLED]);

            pm.spilled.find(self, fp, sp_arr);
            tm = get_temp_map(get_return_addr_ptr(fp));
            fp = get_previous_fp(fp);
        }
        dprint!("\n");
    }

    unsafe fn reserve(&mut self, size: u16) -> Option<NonNull<Block>> {
        let mut r = None;
        let mut prev_free: Option<*mut Chunk> = None;
        if let Some(c) = self.freelist {
            let mut c = c.as_ptr();
            loop {
                if (*c).size >= size as usize {
                    let d_offset = (*c).size - size as usize;
                    let d = if d_offset > Block::HEADER_SIZE {
                        (*c).size -= size as usize;
                        let d = (c as *mut usize).offset(d_offset as isize) as *mut Block;
                        *d = Block::new(size);
                        d
                    } else {
                        if let Some(prev) = prev_free {
                            if let Some(next) = (*c).next_free() {
                                (*prev).link(next);
                            } else {
                                (*prev).close()
                            }
                        } else {
                            if let Some(next) = (*c).next_free() {
                                self.freelist = Some(NonNull::new_unchecked(next))
                            } else {
                                self.freelist = None
                            }
                        };
                        let d = c as *mut Block;
                        *d = Block::new_extra(size, d_offset as u8);
                        d
                    };
                    self.alloc_chunks += 1;
                    r = Some(NonNull::new_unchecked(d));
                    break;
                } else {
                    if let Some(next) = (*c).next_free() {
                        prev_free = Some(c);
                        c = next;
                    } else {
                        break;
                    }
                }
            }
        };
        r
    }

    unsafe fn sweep_aux(
        &mut self,
        c_ptr: &mut *mut usize,
        prev_free: &mut Option<*mut Chunk>,
        next_free: Option<*mut Chunk>,
    ) {
        let d = *c_ptr as *mut Block;

        if (*(*c_ptr as *mut Block)).is_black() {
            (*d).set_white();
            *c_ptr = (*d).after_block();
        } else {
            dprint!(">>>> free {:?}\n", d);
            let size = (*d).full_size();
            let cf = *c_ptr as *mut Chunk;
            *c_ptr = (*d).after_block();
            if let Some(pf) = *prev_free {
                if (pf as *const usize).offset((*pf).size as isize) as usize == cf as usize {
                    (*pf).size += size as usize;
                } else {
                    Chunk::init(cf, size as usize, next_free);
                    (*pf).link(cf);
                    *prev_free = Some(cf);
                };
            } else {
                Chunk::init(cf, size as usize, next_free);
                *prev_free = Some(cf);
                self.freelist = Some(NonNull::new_unchecked(cf))
            }
        };
    }

    unsafe fn sweep(&mut self) {
        let mut c_ptr = self.origin as *mut usize;
        let mut prev_free: Option<*mut Chunk> = None;
        let mut next_free = self.freelist.map(|c| c.as_ptr());

        while (c_ptr as usize) < (self.origin as usize + Self::length()) {
            if let Some(nf) = next_free {
                if c_ptr as usize == nf as usize {
                    if let Some(pf) = prev_free {
                        if (pf as *const usize).offset((*pf).size as isize) as usize == nf as usize
                        {
                            (*pf).size += (*nf).size as usize;
                            (*pf).set_next_freenode((*nf).next_freenode());
                            next_free = (*nf).next_free();
                        } else {
                            next_free = (*nf).next_free();
                            prev_free = Some(nf);
                        }
                    } else {
                        next_free = (*nf).next_free();
                        prev_free = Some(nf);
                    }
                    c_ptr = Chunk::after_block(nf);
                } else {
                    self.sweep_aux(&mut c_ptr, &mut prev_free, next_free);
                };
            } else {
                self.sweep_aux(&mut c_ptr, &mut prev_free, next_free);
            };
        }
    }

    pub unsafe fn alloc(
        &mut self,
        size: u16,
        fp: *const usize,
        ret_addr_ptr: *const usize,
        temp_arr: *mut usize,
    ) -> *mut Block {
        // dbg!( fp, ret_addr_ptr, temp_arr);
        // libc::sleep(1);

        self.collect(fp, ret_addr_ptr, temp_arr);

        if let Some(d) = self.reserve(size).or_else(|| {
            self.collect(fp, ret_addr_ptr, temp_arr);
            self.reserve(size)
        }) {
            dprint!(">>>> alloc {:?}\n", d);
            d.as_ptr()
        } else {
            _error_handler(&OUT_OF_MEMORY_MSG);
        }
    }
}

#[cfg(test)]
mod tests {
    // #![allow(non_camel_case_types)]
    #![allow(non_snake_case)]

    use super::*;
    use crate::layout::aux_test::*;

    unsafe fn double_desc<const N: usize>(d0: DT<N>) -> (NonNull<Block>, NonNull<Block>) {
        let d1 = d0.clone();
        let d0_p = add_dt(d0);
        let d1_p = add_dt(d1);
        (d0_p, d1_p)
    }

    // #[test]
    // fn tests_mark1() {
    //     unsafe {
    //         let (r0, mut r0_m) = double_desc(DT::record([None, None]));
    //         let (r1, mut r1_m) = double_desc(DT::record([None]));

    //         let r3 = add_dt(DT::record([None, Some(r0), Some(r1)]));
    //         let mut r3_m = add_dt(DT::record([None, Some(r0_m), Some(r1_m)]));

    //         r0_m.as_mut().set_mark(true);
    //         r1_m.as_mut().set_mark(true);
    //         r3_m.as_mut().set_mark(true);

    //         mark(r3.as_ptr());

    //         assert!(r3.as_ref() == r3_m.as_ref());
    //     }
    // }

    trait Zeroed {
        fn zeroed(&mut self);
    }

    trait Chunked {
        fn chunk(&mut self) -> Vec<Option<*mut Chunk>>;
    }

    impl<const N: usize> Zeroed for CT<N> {
        fn zeroed(&mut self) {}
    }

    impl<const N: usize> Zeroed for DT<N> {
        fn zeroed(&mut self) {
            unsafe { self.d.zeros() }
        }
    }

    impl<const N: usize> Chunked for CT<N> {
        fn chunk(&mut self) -> Vec<Option<*mut Chunk>> {
            vec![Some(&mut (*self).c)]
        }
    }

    impl<const N: usize> Chunked for DT<N> {
        fn chunk(&mut self) -> Vec<Option<*mut Chunk>> {
            vec![None]
        }
    }

    macro_rules! tuple_impls {
        ( $( $name:ident )+ ) => {
            impl<$($name: Zeroed),+> Zeroed for ($($name,)+)
            {
                fn zeroed(&mut self)  {
                    let ($($name,)+) = self;
                    $($name.zeroed();)+
                }
            }

            impl<$($name: Chunked),+> Chunked for ($($name,)+)
            {
                fn chunk(&mut self) -> Vec<Option<*mut Chunk>> {
                    let ($($name,)+) = self;
                    vec![$($name.chunk()[0],)+]
                }
            }
        };
    }

    tuple_impls! { A }
    tuple_impls! { A B }
    tuple_impls! { A B C }
    tuple_impls! { A B C D }
    tuple_impls! { A B C D E }
    tuple_impls! { A B C D E F }
    tuple_impls! { A B C D E F G }
    tuple_impls! { A B C D E F G H }
    tuple_impls! { A B C D E F G H I }
    tuple_impls! { A B C D E F G H I J }
    tuple_impls! { A B C D E F G H I J K }
    tuple_impls! { A B C D E F G H I J K L }

    #[repr(C)]
    #[derive(Debug, Clone)]
    struct CT<const N: usize> {
        c: Chunk,
        _s: [usize; N],
    }

    impl<const N: usize> PartialEq for CT<N> {
        fn eq(&self, other: &Self) -> bool {
            self.c.size == other.c.size
        }
    }

    impl<const N: usize> CT<N> {
        fn new() -> Self {
            let n: Node<Free> = Node {
                next: None,
                _p: PhantomData,
            };
            CT {
                c: Chunk {
                    freelist: n,
                    size: size_of::<Self>() / size_of::<usize>(),
                },
                _s: [0; N],
            }
        }
    }

    impl<const N: usize> Arena<N> {
        unsafe fn new_test<T, const M: usize>(origin: *mut T, freelist: Option<*mut CT<M>>) -> Self
        where
            T: core::fmt::Debug + Chunked,
        {
            assert!(size_of::<Space<N>>() == size_of::<T>());
            make_links(origin);

            check_links(origin);

            Arena {
                // length: size_of::<T>(),
                origin: origin as *mut Space<N>,
                freelist: freelist.map(|c| NonNull::new_unchecked(&mut (*c).c)),
                graylist: GrayStack::new(),
                alloc_chunks: 0,
            }
        }
    }

    unsafe fn make_links<T: Chunked>(a: *mut T) {
        let mut prev: Option<*mut Chunk> = None;

        for c in (*a).chunk() {
            if let Some(c) = c {
                if let Some(p) = prev {
                    (*p).link(c);
                };

                prev = Some(c);
            }
        }
    }

    unsafe fn check_links<T: Chunked>(a: *mut T) {
        let mut prev: Option<*mut Chunk> = None;

        for c in (*a).chunk() {
            if let Some(c) = c {
                if let Some(p) = prev {
                    assert!((*p).next_free().unwrap() as usize == c as usize);
                };

                prev = Some(c);
            }
        }
    }

    unsafe fn reserve_compare<U, const S: usize>(
        arena: &mut Arena<S>,
        // freelist: Option<*mut CT<N>>,
        expect: &mut U,
        expect_freelist_offset: Option<isize>,
        size: u16,
    ) -> Option<Block>
    where
        U: core::fmt::Debug + PartialEq + Zeroed + Chunked,
        // T: core::fmt::Debug + Chunked,
    {
        assert!(size_of::<Space<S>>() == size_of::<U>());
        // assert!(size_of::<T>() == size_of::< Space<S> >());
        make_links(expect);
        // make_links(layout);

        check_links(expect);
        // check_links(layout);
        // dbg!(&*(layout));
        // println!("");
        // let p = expect as *mut U as *const usize;
        // for i in 0..(size_of::<T>() / size_of::<usize>()) {
        //     println!("{}: {} ", i, *p.offset(i as isize));
        // }
        // println!("");

        // let space = layout as *mut usize as *mut Space<S>;
        // let mut a = Arena::<S>::new_test(space, freelist);
        let d = arena.reserve(size);

        let p = arena.origin as *mut Space<S> as *const usize;
        let expect_freelist_ptr = expect_freelist_offset.map(|ofs| p.offset(ofs) as usize);
        let freelist_ptr = arena.freelist.map(|p| p.as_ptr() as usize);

        let o_u = arena.origin as *mut Space<S> as *mut usize as *mut U;
        (*o_u).zeroed();

        // for i in 0..(size_of::<T>() / size_of::<usize>()) {
        //     println!("{}: {} ", i, *p.offset(i as isize));
        // }

        // dbg!(&*(expect));
        // dbg!(&*(o_u));
        check_links(o_u);
        assert!(*o_u == *expect);
        // dbg!(freelist_ptr, expect_freelist_ptr);
        assert!(freelist_ptr == expect_freelist_ptr);

        d.map(|p| p.as_ref().clone())
    }
    #[test]
    fn test_reserve1() {
        unsafe {
            let mut origin = CT::<7>::new();

            let d_expect = DT::<1>::new(0);
            let mut expect = (CT::<4>::new(), d_expect.clone());

            let mut a = Arena::<7>::new_test(&mut origin, Some(&mut origin));
            let d = reserve_compare(&mut a, &mut expect, Some(0), 3);
            assert!(d == Some(d_expect.d));
        }
    }
    #[test]
    fn test_reserve2() {
        unsafe {
            let mut origin = (DT::<2>::new(0), CT::<5>::new());

            let d_expect = DT::<2>::new(0);
            let mut expect = (DT::<2>::new(0), CT::<1>::new(), d_expect.clone());

            let mut a = Arena::<9>::new_test(&mut origin, Some(&mut origin.1));
            let d = reserve_compare(&mut a, &mut expect, Some(4), 4);
            assert!(d == Some(d_expect.d));
        }
    }
    #[test]
    fn test_reserve3_rev2() {
        unsafe {
            let mut origin = (CT::<4>::new(), DT::<4>::new(0));

            let d_expect = DT::<1>::new(0);
            let mut expect = (CT::<1>::new(), d_expect.clone(), DT::<4>::new(0));

            let mut a = Arena::<10>::new_test(&mut origin, Some(&mut origin.0));
            let d = reserve_compare(&mut a, &mut expect, Some(0), 3);
            assert!(d == Some(d_expect.d));
        }
    }
    #[test]
    fn test_reserve3_rev4() {
        unsafe {
            let mut origin = (CT::<4>::new(), DT::<4>::new(0));

            let d_expect = DT::<4>::new(0);
            let mut expect = (d_expect.clone(), DT::<4>::new(0));

            let mut a = Arena::<10>::new_test(&mut origin, Some(&mut origin.0));
            let d = reserve_compare(&mut a, &mut expect, None, 6);
            assert!(d == Some(d_expect.d));
        }
    }
    #[test]
    fn test_reserve4_rev2() {
        unsafe {
            let mut origin = (DT::<2>::new(0), CT::<4>::new(), DT::<1>::new(0));

            let d_expect = DT::<4>::new(1);
            let mut expect = (DT::<2>::new(0), d_expect.clone(), DT::<1>::new(0));

            let mut a = Arena::<11>::new_test(&mut origin, Some(&mut origin.1));
            let d = reserve_compare(&mut a, &mut expect, None, 5);
            assert!(d == Some(d_expect.d));
        }
    }
    #[test]
    fn test_reserve4_rev3() {
        unsafe {
            let mut origin = (DT::<2>::new(0), CT::<4>::new(), DT::<1>::new(0));

            let d_expect = DT::<4>::new(0);
            let mut expect = (DT::<2>::new(0), d_expect.clone(), DT::<1>::new(0));

            let mut a = Arena::<11>::new_test(&mut origin, Some(&mut origin.1));
            let d = reserve_compare(&mut a, &mut expect, None, 6);
            assert!(d == Some(d_expect.d));
        }
    }
    #[test]
    fn test_reserve5_rev2() {
        unsafe {
            let mut origin = (CT::<1>::new(), DT::<2>::new(0), CT::<2>::new());

            let d_expect = DT::<1>::new(0);
            let mut expect = (d_expect.clone(), DT::<2>::new(0), CT::<2>::new());

            let mut a = Arena::<9>::new_test(&mut origin, Some(&mut origin.0));
            let d = reserve_compare(&mut a, &mut expect, Some(7), 3);
            assert!(d == Some(d_expect.d));
        }
    }
    #[test]
    fn test_reserve5_rev3() {
        unsafe {
            let mut origin = (CT::<1>::new(), DT::<2>::new(0), CT::<2>::new());

            let d_expect = DT::<2>::new(0);
            let mut expect = (CT::<1>::new(), DT::<2>::new(0), d_expect.clone());

            let mut a = Arena::<9>::new_test(&mut origin, Some(&mut origin.0));
            let d = reserve_compare(&mut a, &mut expect, Some(0), 4);
            assert!(d == Some(d_expect.d));
        }
    }
    #[test]
    fn test_reserve5_rev4() {
        unsafe {
            let mut origin = (CT::<1>::new(), DT::<2>::new(0), CT::<2>::new());

            let mut expect = (CT::<1>::new(), DT::<2>::new(0), CT::<2>::new());

            let mut a = Arena::<9>::new_test(&mut origin, Some(&mut origin.0));
            let d = reserve_compare(&mut a, &mut expect, Some(0), 5);
            assert!(d == None);
        }
    }
    #[test]
    fn test_reserve6_rev3() {
        unsafe {
            let mut origin = (
                CT::<1>::new(),
                DT::<2>::new(0),
                CT::<2>::new(),
                DT::<2>::new(0),
                CT::<3>::new(),
            );

            let d_expect = DT::<2>::new(0);
            let mut expect = (
                CT::<1>::new(),
                DT::<2>::new(0),
                d_expect.clone(),
                DT::<2>::new(0),
                CT::<3>::new(),
            );
            let mut a = Arena::<18>::new_test(&mut origin, Some(&mut origin.0));
            let d = reserve_compare(&mut a, &mut expect, Some(0), 4);
            assert!(d == Some(d_expect.d));
        }
    }

    #[test]
    fn test_reserve6_rev4() {
        unsafe {
            let mut origin = (
                CT::<1>::new(),
                DT::<2>::new(0),
                CT::<2>::new(),
                DT::<2>::new(0),
                CT::<3>::new(),
            );

            let d_expect = DT::<3>::new(0);
            let mut expect = (
                CT::<1>::new(),
                DT::<2>::new(0),
                CT::<2>::new(),
                DT::<2>::new(0),
                d_expect.clone(),
            );
            let mut a = Arena::<18>::new_test(&mut origin, Some(&mut origin.0));
            let d = reserve_compare(&mut a, &mut expect, Some(0), 5);
            assert!(d == Some(d_expect.d));
        }
    }

    unsafe fn sweep_compare<U, const S: usize>(
        arena: &mut Arena<S>,
        // freelist: Option<*mut CT<N>>,
        expect: &mut U,
        expect_freelist_offset: Option<isize>,
    ) where
        U: core::fmt::Debug + PartialEq + Zeroed + Chunked,
        // T: core::fmt::Debug + Chunked,
    {
        assert!(size_of::<Space<S>>() == size_of::<U>());
        make_links(expect);
        // make_links(layout);

        check_links(expect);
        // check_links(layout);

        // dbg!(&*(layout));
        // println!("");
        // let p = expect as *mut U as *const usize;
        // for i in 0..(size_of::<T>() / size_of::<usize>()) {
        //     println!("{}: {} ", i, *p.offset(i as isize));
        // }
        // println!("");

        // let space = layout as *mut usize as *mut Space<S>;
        // let mut a = Arena::<S>::new_test(space, freelist);
        arena.sweep();

        let p = arena.origin as *mut Space<S> as *const usize;
        let expect_freelist_ptr = expect_freelist_offset.map(|ofs| p.offset(ofs) as usize);
        let freelist_ptr = arena.freelist.map(|p| p.as_ptr() as usize);

        let o_u = arena.origin as *mut Space<S> as *mut usize as *mut U;
        (*o_u).zeroed();

        // for i in 0..(size_of::<T>() / size_of::<usize>()) {
        //     println!("{}: {} ", i, *p.offset(i as isize));
        // }

        // dbg!(&*(expect));
        // dbg!(&*(o_u));
        check_links(o_u);
        assert!(*o_u == *expect);
        // dbg!(freelist_ptr, expect_freelist_ptr);
        assert!(freelist_ptr == expect_freelist_ptr);
    }

    #[test]
    fn test_sweep_1() {
        unsafe {
            let mut origin = (DT::<1>::new(0), CT::<1>::new());

            let mut expect = (CT::<4>::new(),);

            let mut a = Arena::<4>::new_test(&mut origin, Some(&mut origin.1));
            sweep_compare(&mut a, &mut expect, Some(0));
        }
    }

    #[test]
    fn test_sweep_1_m() {
        unsafe {
            let mut origin = (DT::<1>::new(0).mark(), CT::<1>::new());

            let mut expect = (DT::<1>::new(0), CT::<1>::new());
            let mut a = Arena::<4>::new_test(&mut origin, Some(&mut origin.1));
            sweep_compare(&mut a, &mut expect, Some(3));
        }
    }

    #[test]
    fn test_sweep_2() {
        unsafe {
            let mut origin = (DT::<2>::new(1), CT::<0>::new());

            let mut expect = (CT::<4>::new(),);
            let mut a = Arena::<4>::new_test(&mut origin, Some(&mut origin.1));
            sweep_compare(&mut a, &mut expect, Some(0));
        }
    }

    #[test]
    fn test_sweep_3() {
        unsafe {
            let mut origin = (CT::<2>::new(), DT::<2>::new(0));

            let mut expect = (CT::<6>::new(),);
            let mut a = Arena::<6>::new_test(&mut origin, Some(&mut origin.0));
            sweep_compare(&mut a, &mut expect, Some(0));
        }
    }

    #[test]
    fn test_sweep_4() {
        unsafe {
            let mut origin = (CT::<1>::new(), DT::<1>::new(0), CT::<1>::new());

            let mut expect = (CT::<7>::new(),);
            let mut a = Arena::<7>::new_test(&mut origin, Some(&mut origin.0));
            sweep_compare(&mut a, &mut expect, Some(0));
        }
    }

    #[test]
    fn test_sweep_5() {
        unsafe {
            let mut origin = (DT::<1>::new(0), CT::<1>::new(), DT::<1>::new(0));

            let mut expect = (CT::<7>::new(),);
            let mut a = Arena::<7>::new_test(&mut origin, Some(&mut origin.1));
            sweep_compare(&mut a, &mut expect, Some(0));
        }
    }

    #[test]
    fn test_sweep_6_m2_m3() {
        unsafe {
            let mut origin = (
                DT::<2>::new(0),
                CT::<0>::new(),
                DT::<1>::new(0).mark(),
                CT::<1>::new(),
                DT::<1>::new(0).mark(),
            );

            let mut expect = (
                CT::<4>::new(),
                DT::<1>::new(0),
                CT::<1>::new(),
                DT::<1>::new(0),
            );
            let mut a = Arena::<13>::new_test(&mut origin, Some(&mut origin.1));
            sweep_compare(&mut a, &mut expect, Some(0));
        }
    }

    #[test]
    fn test_sweep_6_m1_m3() {
        unsafe {
            let mut origin = (
                DT::<2>::new(0).mark(),
                CT::<0>::new(),
                DT::<1>::new(0),
                CT::<1>::new(),
                DT::<1>::new(0).mark(),
            );

            let mut expect = (DT::<2>::new(0), CT::<6>::new(), DT::<1>::new(0));
            let mut a = Arena::<13>::new_test(&mut origin, Some(&mut origin.1));
            sweep_compare(&mut a, &mut expect, Some(4));
        }
    }

    #[test]
    fn test_sweep_6_m1_m2() {
        unsafe {
            let mut origin = (
                DT::<2>::new(0).mark(),
                CT::<0>::new(),
                DT::<1>::new(0).mark(),
                CT::<1>::new(),
                DT::<1>::new(0),
            );

            let mut expect = (
                DT::<2>::new(0),
                CT::<0>::new(),
                DT::<1>::new(0),
                CT::<4>::new(),
            );
            let mut a = Arena::<13>::new_test(&mut origin, Some(&mut origin.1));
            sweep_compare(&mut a, &mut expect, Some(4));
        }
    }

    #[test]
    fn test_sweep_6() {
        unsafe {
            let mut origin = (
                DT::<2>::new(0),
                CT::<0>::new(),
                DT::<1>::new(0),
                CT::<1>::new(),
                DT::<1>::new(0),
            );

            let mut expect = (CT::<13>::new(),);
            let mut a = Arena::<13>::new_test(&mut origin, Some(&mut origin.1));
            sweep_compare(&mut a, &mut expect, Some(0));
        }
    }

    #[test]
    fn test_sweep_7() {
        unsafe {
            let mut origin = (
                CT::<0>::new(),
                DT::<2>::new(0),
                CT::<1>::new(),
                DT::<1>::new(0),
                CT::<0>::new(),
            );

            let mut expect = (CT::<12>::new(),);
            let mut a = Arena::<12>::new_test(&mut origin, Some(&mut origin.0));
            sweep_compare(&mut a, &mut expect, Some(0));
        }
    }

    #[test]
    fn test_sweep_8_m2_m3() {
        unsafe {
            let mut origin = (
                CT::<0>::new(),
                DT::<1>::new(0),
                DT::<2>::new(0).mark(),
                DT::<2>::new(0).mark(),
                CT::<1>::new(),
            );

            let mut expect = (
                CT::<3>::new(),
                DT::<2>::new(0),
                DT::<2>::new(0),
                CT::<1>::new(),
            );
            let mut a = Arena::<14>::new_test(&mut origin, Some(&mut origin.0));
            sweep_compare(&mut a, &mut expect, Some(0));
        }
    }

    #[test]
    fn test_sweep_8_m1_m3() {
        unsafe {
            let mut origin = (
                CT::<0>::new(),
                DT::<1>::new(0).mark(),
                DT::<2>::new(0),
                DT::<2>::new(0).mark(),
                CT::<1>::new(),
            );

            let mut expect = (
                CT::<0>::new(),
                DT::<1>::new(0),
                CT::<2>::new(),
                DT::<2>::new(0),
                CT::<1>::new(),
            );
            let mut a = Arena::<14>::new_test(&mut origin, Some(&mut origin.0));
            sweep_compare(&mut a, &mut expect, Some(0));
        }
    }

    #[test]
    fn test_sweep_8_m1_m2() {
        unsafe {
            let mut origin = (
                CT::<0>::new(),
                DT::<1>::new(0).mark(),
                DT::<2>::new(0).mark(),
                DT::<2>::new(0),
                CT::<1>::new(),
            );

            let mut expect = (
                CT::<0>::new(),
                DT::<1>::new(0),
                DT::<2>::new(0),
                CT::<5>::new(),
            );
            let mut a = Arena::<14>::new_test(&mut origin, Some(&mut origin.0));
            sweep_compare(&mut a, &mut expect, Some(0));
        }
    }

    #[test]
    fn test_sweep_8() {
        unsafe {
            let mut origin = (
                CT::<0>::new(),
                DT::<1>::new(0),
                DT::<2>::new(0),
                DT::<2>::new(0),
                CT::<1>::new(),
            );

            let mut expect = (CT::<14>::new(),);
            let mut a = Arena::<14>::new_test(&mut origin, Some(&mut origin.0));
            sweep_compare(&mut a, &mut expect, Some(0));
        }
    }

    #[test]
    fn test_sweep_9() {
        unsafe {
            let mut origin = (DT::<2>::new(0), DT::<1>::new(0), DT::<3>::new(0));

            let mut expect = (CT::<10>::new(),);

            let fl: Option<*mut CT<0>> = None;
            let mut a = Arena::<10>::new_test(&mut origin, fl);
            sweep_compare(&mut a, &mut expect, Some(0));
        }
    }

    #[test]
    fn test_sweep_9_m1_m3() {
        unsafe {
            let mut origin = (
                DT::<2>::new(0).mark(),
                DT::<1>::new(0),
                DT::<3>::new(0).mark(),
            );

            let mut expect = (DT::<2>::new(0), CT::<1>::new(), DT::<3>::new(0));

            let fl: Option<*mut CT<0>> = None;
            let mut a = Arena::<10>::new_test(&mut origin, fl);
            sweep_compare(&mut a, &mut expect, Some(4));
        }
    }
}
