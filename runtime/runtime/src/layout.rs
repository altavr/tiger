use core::{marker::PhantomData, mem, mem::size_of, ops::BitAnd, pin::Pin, ptr, ptr::NonNull};
use modular_bitfield::prelude::*;

#[derive(BitfieldSpecifier, Debug, PartialEq)]
pub enum DataKind {
    N = 0,
    Record = 1,
    Array = 2,
    String = 3,
}

const WHITE: usize = 1;
const BLACK: usize = 2;

#[repr(C)]
#[derive(Debug, Clone, PartialEq)]
pub struct Block {
    link: usize,
    pub descriptor: Descriptor,
}

#[bitfield(bits = 64)]
#[derive(Debug, Clone, PartialEq)]
pub struct Descriptor {
    size: B16,
    pub bitmap: B12,
    extra_space: B2,
    #[bits = 2]
    pub kind: DataKind,
    status: B32,
}

pub const FIELD_OFFSET: u16 = 2;

pub type BL = *mut Block;

impl Block {
    pub const HEADER_SIZE: usize = size_of::<Block>() / size_of::<usize>();

    pub fn new(s: u16) -> Self {
        debug_assert!(s > 2);
        Block {
            link: WHITE,
            descriptor: Descriptor::new().with_size(s),
        }
    }

    pub fn new_extra(s: u16, es: u8) -> Self {
        // dbg!(s, es);
        debug_assert!(s > 2 && es <= 2);
        Block {
            link: WHITE,
            descriptor: Descriptor::new().with_extra_space(es).with_size(s),
        }
    }

    pub const fn new_with_descriptor(d: Descriptor) -> Self {
        Block {
            link: WHITE,
            descriptor: d,
        }
    }

    pub unsafe fn zeros(&mut self) {
        self.body()
            .write_bytes(0, self.full_size() as usize - Self::HEADER_SIZE)
    }

    pub fn full_size(&self) -> u16 {
        self.descriptor.size() + self.descriptor.extra_space() as u16
    }

    pub fn size(&self) -> u16 {
        self.descriptor.size() as u16
    }

    pub unsafe fn after_block(&mut self) -> *mut usize {
        self.ptr(self.full_size() as isize)
    }

    unsafe fn body(&mut self) -> *mut usize {
        self.ptr(Self::HEADER_SIZE as isize)
    }

    unsafe fn ptr(&mut self, offset: isize) -> *mut usize {
        (&mut *self as *mut Block as *mut usize).offset(offset)
    }

    pub fn new_string(size: u16, allocated: bool) -> Self {
        Block {
            descriptor: Descriptor::new()
                .with_kind(DataKind::String)
                .with_size(size)
                .with_bitmap(allocated as u16),
            link: WHITE,
        }
    }

    pub unsafe fn init_record(&mut self, size: u16, es: u8, bitmap: u16) {
        let d = Descriptor::new()
            .with_kind(DataKind::Record)
            .with_extra_space(es)
            .with_size(size)
            .with_bitmap(bitmap);
        self.descriptor = d;
        self.link = WHITE;
    }

    pub unsafe fn init_array(&mut self, size: u16, es: u8, ptr_content: bool) {
        let d = Descriptor::new()
            .with_kind(DataKind::Array)
            .with_extra_space(es)
            .with_bitmap(ptr_content as u16)
            .with_size(size);
        self.descriptor = d;
        self.link = WHITE;
    }

    unsafe fn set_gray(&mut self, next: Option<NonNull<Block>>) {
        self.link = mem::transmute(next);
    }

    fn set_black(&mut self) {
        self.link = BLACK
    }

    pub unsafe fn is_black(&self) -> bool {
        self.link == BLACK
    }

    pub unsafe fn is_white(&self) -> bool {
        self.link == WHITE
    }

    pub fn set_white(&mut self) {
        self.link = WHITE
    }

    fn is_grey(&self) -> bool {
        self.link & 0b11 == 0
    }

    unsafe fn next(&self) -> Option<NonNull<Block>> {
        mem::transmute(self.link)
    }
}

impl IntoIterator for &'static mut Block {
    type Item = NonNull<Block>;
    type IntoIter = BlockIter;

    fn into_iter(self) -> Self::IntoIter {
        BlockIter::new(self)
    }
}

pub struct BlockIter {
    block: &'static mut Block,
    i: u16,
}

impl BlockIter {
    fn new(block: &'static mut Block) -> Self {
        BlockIter { block, i: 0 }
    }
}

impl Iterator for BlockIter {
    type Item = NonNull<Block>;

    fn next(&mut self) -> Option<Self::Item> {
        use DataKind::*;
        match self.block.descriptor.kind() {
            N | String => None,
            Array => {
                let max = if self.block.descriptor.bitmap() == 1 {
                    self.block.descriptor.size() - Block::HEADER_SIZE as u16
                } else {
                    return None;
                };
                for i in self.i..max {
                    let ptr = unsafe {
                        NonNull::new(
                            *self.block.ptr((i + Block::HEADER_SIZE as u16) as isize) as *mut Block
                        )
                    };
                    if ptr.is_some() {
                        self.i = i + 1;
                        return ptr;
                    }
                }
                self.i = max;
                None
            }
            Record => {
                let bm = self.block.descriptor.bitmap();
                let max = self.block.descriptor.size() - Block::HEADER_SIZE as u16;
                for i in self.i..max {
                    if bm & (1 << i) != 0 {
                        let ptr = unsafe {
                            NonNull::new(
                                *self.block.ptr((i as usize + Block::HEADER_SIZE) as isize)
                                    as *mut Block,
                            )
                        };
                        if ptr.is_some() {
                            self.i = i + 1;
                            return ptr;
                        }
                    }
                }
                self.i = max;
                None
            }
        }
    }
}

pub mod graystack {

    use super::*;

    #[derive(Debug)]
    pub struct GrayStack(Option<NonNull<Block>>);

    impl GrayStack {
        pub const fn new() -> Self {
            GrayStack(None)
        }

        pub unsafe fn mark(&mut self) {
            let mut stack = self.0;
            while let Some(mut h) = stack {
                stack = h.as_ref().next();
                for mut child in h.as_mut() {
                    if child.as_ref().is_white() {
                        child.as_mut().set_gray(stack);
                        stack = Some(child);
                    }
                }
                h.as_mut().set_black();
            }
            self.0 = stack;
        }

        pub unsafe fn add(&mut self, mut l: NonNull<Block>) {
            if !l.as_ref().is_grey() {
                l.as_mut().set_gray(self.0);
                self.0 = Some(l);
            }
        }
    }
}

pub mod aux_test {
    use super::*;

    #[repr(C)]
    #[derive(Debug, Clone)]
    pub struct DT<const N: usize> {
        pub d: Block,
        pub f: [Option<NonNull<Block>>; N],
    }

    impl<const N: usize> PartialEq for DT<N> {
        fn eq(&self, other: &Self) -> bool {
            self.d == other.d
                && self
                    .f
                    .map(|f| f.map(|p| unsafe { p.as_ref() }))
                    .eq(&other.f.map(|f| f.map(|p| unsafe { p.as_ref() })))
        }
    }

    impl<const N: usize> DT<N> {
        fn _record(fields: [Option<NonNull<Block>>; N], extra: u8) -> Self {
            let mut bitmap = 0;
            let mut i = 0;

            for f in &fields[0..(N - extra as usize)] {
                if let Some(_) = f {
                    bitmap |= 1 << i
                };
                i += 1;
            }
            let mut dt = DT {
                d: Block::new(3),
                f: fields,
            };
            unsafe { dt.d.init_record(i + FIELD_OFFSET as u16, extra, bitmap) };
            dt
        }

        pub fn record(fields: [Option<NonNull<Block>>; N]) -> Self {
            Self::_record(fields, 0)
        }

        pub fn record_extra(fields: [Option<NonNull<Block>>; N], extra: u8) -> Self {
            Self::_record(fields, extra)
        }

        pub fn new(extra: u8) -> Self {
            DT {
                d: Block::new_extra((N - extra as usize + Block::HEADER_SIZE) as u16, extra),
                f: [None; N],
            }
        }

        pub fn mark(mut self) -> Self {
            self.d.set_black();
            self
        }

        pub fn array(mut fields: [Option<NonNull<Block>>; N], ptr: bool) -> Self {
            let d = Block::new(3);
            let mut dt = DT { d, f: fields };
            unsafe {
                dt.d.init_array(fields.len() as u16 + FIELD_OFFSET as u16, 0, ptr)
            };
            dt
        }
    }

    static mut SPACE: ([u8; 10240], isize) = ([0; 10240], 0);

    pub unsafe fn add_dt<const N: usize>(r: DT<N>) -> NonNull<Block> {
        assert!(size_of::<Option<NonNull<Block>>>() == size_of::<usize>());

        let pos = SPACE.1 + size_of::<DT<N>>() as isize;
        assert!(pos < 1024);

        let ptr = SPACE.0.as_mut_ptr().offset(SPACE.1) as *mut DT<N>;
        SPACE.1 = pos;
        *ptr = r;
        NonNull::new_unchecked(&mut (*ptr).d)
    }
}

#[cfg(test)]
mod tests {
    #![warn(non_camel_case_types)]
    #![warn(non_snake_case)]

    use super::*;

    use super::aux_test::*;

    #[test]
    fn test_record() {
        unsafe {
            let r0 = add_dt(DT::record([None, None]));
            let r1 = add_dt(DT::record([None]));

            let mut r3 = add_dt(DT::record([None, Some(r0), Some(r1)]));

            let mut iter = r3.as_mut().into_iter();

            assert!(iter.next() == Some(r0));
            assert!(iter.next() == Some(r1));
            assert!(iter.next() == None);

            assert!((*(r3.as_ptr() as *const usize as *const DT<3>)).f[0] == None);
            assert!((*(r3.as_ptr() as *const usize as *const DT<3>)).f[1].unwrap() == r0);
            assert!((*(r3.as_ptr() as *const usize as *const DT<3>)).f[2].unwrap() == r1);
        }
    }
    #[test]
    fn test_record1() {
        unsafe {
            let mut r0 = add_dt(DT::record([None, None]));

            // dbg!(&(*(r0.as_ptr() as *const usize as *const DT<2>)));

            let mut iter = r0.as_mut().into_iter();
            assert!(iter.next() == None);

            assert!((*(r0.as_ptr() as *const usize as *const DT<2>)).f[0] == None);
            assert!((*(r0.as_ptr() as *const usize as *const DT<2>)).f[1] == None);
        }
    }

    #[test]
    fn test_array() {
        unsafe {
            let r0 = add_dt(DT::record([None, None]));
            let r1 = add_dt(DT::record([None]));

            let mut a0 = add_dt(DT::array([Some(r0), Some(r1)], true));

            // dbg!(&(*(a0.as_ptr() as *const usize as *const DT<3>)));

            let mut iter = a0.as_mut().into_iter();

            assert!(iter.next() == Some(r0));
            assert!(iter.next() == Some(r1));
            assert!(iter.next() == None);

            assert!((*(a0.as_ptr() as *const usize as *const DT<2>)).f[0].unwrap() == r0);
            assert!((*(a0.as_ptr() as *const usize as *const DT<2>)).f[1].unwrap() == r1);
        }
    }

    unsafe fn double_desc<const N: usize>(d0: DT<N>) -> (NonNull<Block>, NonNull<Block>) {
        let d1 = d0.clone();
        let d0_p = add_dt(d0);
        let d1_p = add_dt(d1);
        (d0_p, d1_p)
    }

    #[test]
    fn tests_mark1() {
        unsafe {
            let (r0, mut r0_m) = double_desc(DT::record([None, None]));
            let (r1, mut r1_m) = double_desc(DT::record([None]));

            // dbg!(r0.as_ref());
            let r3 = add_dt(DT::record([None, Some(r0), Some(r1)]));
            let mut r3_m = add_dt(DT::record([None, Some(r0_m), Some(r1_m)]));

            r0_m.as_mut().set_black();
            r1_m.as_mut().set_black();
            r3_m.as_mut().set_black();

            use graystack::*;
            let mut s = GrayStack::new();
            s.add(r3);
            s.mark();

            dbg!(r1.as_ref(), r3_m.as_ref());

            assert!(r3.as_ref() == r3_m.as_ref());
            assert!(r1.as_ref() == r1_m.as_ref());
            assert!(r0.as_ref() == r0_m.as_ref());

            // assert!(r3.as_ref() == r3_m.as_ref());
        }
    }
}
