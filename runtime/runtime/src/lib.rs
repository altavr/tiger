#![cfg_attr(not(debug_assertions), no_std)]
#![feature(global_asm)]

use core::{mem, mem::MaybeUninit};

#[cfg(feature = "dprint")]
macro_rules!  dprint {
    ($($x : tt), +) => {
        eprint!( $($x),+);
    };
    ( $e:expr, $($x : tt), +) => {
        let dprint_expr = $e;
        eprint!(  $($x),+, dprint_expr);
    };

}

#[cfg(not(feature = "dprint"))]
macro_rules! dprint {
    ( $(  $x : tt), +) => {};
    ( $e:expr, $($x : tt), +) => {};
}

mod gc;
mod layout;
mod string;

use gc::*;
use layout::*;
use string::*;

#[cfg(not(debug_assertions))]
#[panic_handler]
fn panic_handler(_: &core::panic::PanicInfo) -> ! {
    static MSG: TString<1> = TString::<1>::from_array(b"panic\n");
    _error_handler(&MSG);
}

pub fn _error_handler<const N: usize>(msg: &TString<N>) -> ! {
    unsafe {
        TString::<N>::eprint(msg);
        libc::exit(-1)
    }
}

macro_rules! cs_wrapper {
    ( $f:ident, $_f:ident, $ret_reg:ident, $temp_arr_reg:ident ) => {
        global_asm!(
            concat!(".global ", stringify!($f)),
            concat!(stringify!($f), ":"),
            concat!("mov ", stringify!($ret_reg), ",  rsp"),
            "push r15",
            "push r14",
            "push r13",
            "push r12",
            "push rbx",
            concat!("mov ", stringify!($temp_arr_reg), ",  rsp"),
            concat!("call ", stringify!($_f)),
            "pop rbx",
            "pop r12",
            "pop r13",
            "pop r14",
            "pop r15",
            "ret",
        );
    };
}

pub mod alloc {

    cs_wrapper!(alloc_record, _alloc_record, rcx, r8);
    cs_wrapper!(init_array, _init_array, r8, r9);

    cs_wrapper!(substring, _substring, r8, r9);
    cs_wrapper!(concat, _concat, rcx, r8);
    cs_wrapper!(string_of_int, _string_of_int, rdx, rcx);
}

const ARENA_SIZE: usize = 1024;

const _ARENA_SIZE: usize = ARENA_SIZE - mem::size_of::<Chunk>() / mem::size_of::<usize>();

static mut SPACE: Space<_ARENA_SIZE> = Space::new();
static mut ARENA: MaybeUninit<Arena<_ARENA_SIZE>> = MaybeUninit::<Arena<_ARENA_SIZE>>::uninit();

#[no_mangle]
pub unsafe extern "C" fn gc_init() {
    ARENA.write(Arena::<_ARENA_SIZE>::new(&mut SPACE));
}

#[no_mangle]
unsafe extern "C" fn _alloc_record(
    fp: *const usize,
    size: u16,
    bitmap: u16,
    ret_addr_ptr: *const usize,
    temp_arr: *mut usize,
) -> *mut () {
    let d =
        ARENA
            .assume_init_mut()
            .alloc(size + Block::HEADER_SIZE as u16, fp, ret_addr_ptr, temp_arr);
    (*d).descriptor.set_kind(DataKind::Record);
    (*d).descriptor.set_bitmap(bitmap);
    (d as *mut usize)
        .offset(Block::HEADER_SIZE as isize)
        .write_bytes(0, size.into());
    d as *mut ()
}

#[no_mangle]
unsafe extern "C" fn _init_array(
    fp: *const usize,
    size: u16,
    init: usize,
    arr_of_ptr: u16,
    ret_addr_ptr: *const usize,
    temp_arr: *mut usize,
) -> *mut () {

    if arr_of_ptr == 1 {
        ARENA.assume_init_mut().add_root(init as BL);
    }
    let size = size + Block::HEADER_SIZE as u16;
    let d = ARENA
        .assume_init_mut()
        .alloc(size, fp, ret_addr_ptr, temp_arr);
    (*d).descriptor.set_kind(DataKind::Array);
    (*d).descriptor.set_bitmap(arr_of_ptr);
    for i in Block::HEADER_SIZE as isize..size as isize {
        *(d as *mut usize).offset(i) = init;
    }
    d as *mut ()
}

#[no_mangle]
pub unsafe extern "C" fn error_handler(msg: *const TString<1>) -> ! {
    _error_handler(&*msg)
}

#[no_mangle]
pub unsafe extern "C" fn stringEqual(s1: *const TString<1>, s2: *const TString<1>) -> isize {
    (*s1).equal(&*s2)
}

#[no_mangle]
pub unsafe extern "C" fn print(s: *const TString<1>) {
    (*s).print()
}

extern "C" {
    static stdout: *mut libc::FILE;
    static stderr: *mut libc::FILE;
}

#[no_mangle]
pub unsafe extern "C" fn flush() {
    libc::fflush(stdout);
}

#[no_mangle]
pub unsafe extern "C" fn ord(s: *const TString<1>) -> isize {
    let s = &*s;
    if s.size() == 0 {
        -1
    } else {
        s.to_slice()[0] as isize
    }
}

#[no_mangle]
pub unsafe extern "C" fn chr(i: isize) -> *const TString<1> {
    static MSG: TString<3> = TString::<3>::from_array(b"chr: out of range\n");

    if i >= 0 && i < 256 {
        &CHARS[i as usize]
    } else {
        _error_handler(&MSG)
    }
}

#[no_mangle]
pub unsafe extern "C" fn size(s: *const TString<1>) -> isize {
    (*s).size() as isize
}

#[cfg(not(test))]
#[no_mangle]
pub unsafe extern "C" fn _substring(
    fp: *const usize,
    s: *const TString<1>,
    first: usize,
    len: usize,
    ret_addr_ptr: *const usize,
    temp_arr: *mut usize,
) -> *const TString<1> {
    (*s).substring(first, len, |len| {
        TString::<1>::alloc(fp, len, ret_addr_ptr, temp_arr)
    })
}

#[cfg(not(test))]
#[no_mangle]
pub unsafe extern "C" fn _concat(
    fp: *const usize,
    s1: *const TString<1>,
    s2: *const TString<1>,
    ret_addr_ptr: *const usize,
    temp_arr: *mut usize,
) -> *const TString<1> {
    (*s1).concat(&*s2, |len| {
        TString::<1>::alloc(fp, len, ret_addr_ptr, temp_arr)
    })
}

#[no_mangle]
pub unsafe extern "C" fn getchar_() -> *const TString<1> {
    TString::<1>::getchar_()
}

#[cfg(not(test))]
#[no_mangle]
pub unsafe extern "C" fn _string_of_int(
    fp: *const usize,
    n: isize,
    ret_addr_ptr: *const usize,
    temp_arr: *mut usize,
) -> *const TString<1> {
    TString::<1>::string_of_int(n, |len| {
        TString::<1>::alloc(fp, len, ret_addr_ptr, temp_arr)
    })
}
