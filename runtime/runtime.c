// #undef __STDC__
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

int64_t tigermain(int64_t);

void gc_init();

int main()
{

  gc_init();

  return tigermain(0 /* static link!? */);
}

int64_t not(int64_t i)
{
  return !i;
}
