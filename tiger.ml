let runcmd x = if Sys.command x = 0 then () else exit 1

let () =
  let rec usage_msg = "tiger -b <src> [-o <output>]"
  and input_file = ref ""
  and output_dir = ref ""
  and anon_fun fn = input_file := fn
  and assembly_flag = ref false
  and speclist =
    [
      ("-o", Arg.Set_string output_dir, "Output directory");
      ("-b", Arg.Set assembly_flag, "Build executable file");
    ]
  in

  Arg.parse speclist anon_fun usage_msg;

  if not @@ Sys.file_exists !input_file then (
    prerr_endline "Error: Input file don't exist";
    exit 1);

  let in_file = Filename.concat (Sys.getcwd ()) !input_file in
  if String.length !output_dir > 0 then Sys.chdir !output_dir;

  let basename =
    Filename.(concat (Sys.getcwd ()) (chop_extension @@ basename !input_file))
  in

  let asm_file = basename ^ ".asm" in

  Compiler.main in_file asm_file;
  if !assembly_flag then (
    runcmd
    @@ Printf.sprintf "yasm -g dwarf2 -f elf64  -o %s.o -l %s.lst %s" basename
         basename asm_file;
    runcmd
    @@ Printf.sprintf
         "cc -g -Wall -Wextra -Wpedantic  -c -o runtime.o \
          /home/alex/documents/ocaml/ed_compiler/tiger/runtime/runtime.c ";
    runcmd
    @@ Printf.sprintf
         "gcc -g -no-pie -o %s %s.o runtime.o -L \
          /home/alex/documents/ocaml/ed_compiler/tiger/runtime/runtime/target/debug \
          -lrt  -lpthread -ldl"
         basename basename)
